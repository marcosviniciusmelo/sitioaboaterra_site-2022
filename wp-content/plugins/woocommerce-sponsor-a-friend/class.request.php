<?php 

/**
 * WooCommerce_Sponsor_a_Friend_Plugin_Request
 * Class for sponsor request Treatment.
 * @since 1.0
 */


if ( ! class_exists( 'WooCommerce_Sponsor_a_Friend_Plugin_Request' ) ) {

class WooCommerce_Sponsor_a_Friend_Plugin_Request{
		
		public $parrain;
		public $parrainEmail;
		private $parrainFirstName;
		public $parrainShortName;
		private $parrainLastName;
		private $parrainFullName;

		private $type;
		private $firstname;
		private $name;
		private $email;
		private $message;
		private $coupon_code;
		private $coupon_amount;
		private $emailHtml;
		private $filleul;
		private $validity;
		private $updatable_coupon;
		private $errors = array();
		private $options = array();

		function __construct()
		{	
			$this->options = get_option('wsafp_options');
		
		} // __construct


		public function populate($parrain, $filleul){

			$this->parrain = $parrain;
			$this->firstname = $filleul['firstname'];
			$this->name = $filleul['name'];
			$this->email = $filleul['email'];
			$this->message = $filleul['message'];
			$this->parrainFirstName = $filleul['gfirstname'];
			$this->parrainLastName = $filleul['gname'];

			if( is_email($parrain) ) :

				$this->parrainEmail = $this->parrain;
				$this->parrainFullName = $filleul['gfirstname'].' '.$filleul['gname'];
				$this->parrainShortName = $filleul['gfirstname'];

			else :

				$this->parrainEmail = $this->getParrainEmail();
				$this->parrainFullName = $this->getParrainFullName();
				$this->parrainShortName = $this->getParrainShortname();

			endif;

		} // populate
		

		public function setType($type){

			$this->type = $type;

		}

		public function setParrain($parrain){

			$this->parrain = $parrain;

		}

	
		public function setFilleul($filleul){

			$this->filleul = $filleul;

		}

		public function setFirstName($firstname){

			$this->firstname = $firstname;

		}

		public function getFilleul(){

			return $this->filleul;

		}

		public function isValid(){

			// empty data.

			if(empty($this->email))
			$this->errors[]=__('O Email não está preenchido','wsafp');

			if(empty($this->name))
			$this->errors[]=__('O Nome não está preenchido','wsafp');

			if(empty($this->firstname))
			$this->errors[]=__('O Primeiro Nome não está preenchido','wsafp');

			if(!is_email($this->email))
			$this->errors[]=__('O Email não é válido','wsafp');

			if(empty($this->parrainEmail))
			$this->errors[]=__('Seu Email não está preenchido','wsafp');
			
			if(empty($this->parrainLastName))
			$this->errors[]=__('Seu sobrenome não está preenchido','wsafp');

			if(empty($this->parrainFirstName))
			$this->errors[]=__('Seu primeiro nome não está preenchido','wsafp');

			if(!is_email($this->parrainEmail))
			$this->errors[]=__('Seu email não é válido','wsafp');

			// duplicate request.

			$args = array(

				'post_type' => 'shop_coupon',
				'meta_query' => array(

					array(
						'key' => 'customer_email',
						'value' => $this->email,
					),
					array(
						'key' => 'wsafp_sponsor_user_id',
						'value' => $this->parrain,
					)
				)
			 );

			$coupons_list = get_posts( $args );

			if(!empty($coupons_list))
			$this->errors[]=__('Solicitação já realizada','wsafp');	

			// Check if there are already orders for the friend.
			// Based on billing email.

			$args = array(
					    'meta_key' => '_billing_email',
					    'meta_value' => $this->email,
					    'post_type' => 'shop_order',
					    'post_status' => 'any',
					    'posts_per_page' => -1,
					    'orderby' => 'post_date',
						'order' => 'DESC',
			);

			$orders_made = get_posts($args);

			if(!empty($orders_made))
			$this->errors[]=__('O email já é um cliente','wsafp');


			// Check if the sponsor is an actual customer.
			// Based on billing email or customer id
		
			if( is_email( $this->parrain ) ) {

				$args = array(
					    'meta_key' => '_billing_email',
					    'meta_value' => $this->parrain,
					    'post_type' => 'shop_order',
					    'post_status' => 'any',
					    'posts_per_page' => -1,
					    'orderby' => 'post_date',
						'order' => 'DESC',
				);

			
			} else {

				$args = array(
					    'meta_key' => '_customer_user',
					    'meta_value' => $this->parrain,
					    'post_type' => 'shop_order',
					    'post_status' => 'any',
					    'posts_per_page' => -1,
					    'orderby' => 'post_date',
						'order' => 'DESC',
				);
			}
			
			$orders_made = get_posts($args);

			if( empty($orders_made) && $this->options['wsafp_allow_none_customer'] === 'no' )
			$this->errors[]=__('Somente os clientes que retornam podem indicar amigos.','wsafp');
			
			
			return empty($this->errors);

		
		} // isValid()

		public function getFullName(){

			return $this->firstname.' '.$this->name;

		} // getFullName

		public function getFirstName(){

			return $this->firstname;

		} // getFirstName

		public function getParrainFullName(){

			$fullname = get_user_meta($this->parrain, 'first_name', true).' '.get_user_meta($this->parrain, 'last_name', true);
			if(str_replace(' ','',$fullname)=='') $fullname = $this->parrainEmail;
			return $fullname;

		} // getFullName

		public function getParrainShortname(){

			if( is_email( $this->parrain ) ) :

				$args = array(
				    'meta_key' => 'wsafp_sponsor_user_id',
				    'meta_value' => $this->parrain,
				    'post_type' => 'shop_coupon',
				    'post_status' => 'any',
				    'posts_per_page' => -1,
				    'orderby' => 'post_date',
					'order' => 'DESC',
				);

				$posts = get_posts($args);
				$shortname = get_post_meta($posts[0]->ID, 'wsafp_sponsor_firstname', true );
				

			else :

				$shortname = get_user_meta($this->parrain, 'first_name', true);
				if( $shortname == '') $shortname = $this->parrainEmail;

			endif;

			return $shortname;

		} // getFullName

		public function getParrainEmail(){

			$user = get_user_by('id', $this->parrain );
			return $user->user_email;

		}

		public function generateCodePromo(){

			$this->coupon_code = apply_filters( 'wsafp_coupon_coupon_code_filter', uniqid() );

			switch ($this->type) {


				// Front means, after someone has successfully filled the form.

				case 'front':

					$coupon = array(

						'post_title' => $this->coupon_code,
						'post_content' => '',
						'post_status' => 'publish',
						'post_excerpt' => sprintf(__('from %s to %s','wsafp'),$this->parrainEmail,$this->email),
						'post_author' => 1,
						'post_type'	=> 'shop_coupon'
					);
					
					$new_coupon_id = wp_insert_post( $coupon );

					$this->coupon_amount = apply_filters( 'wsafp_coupon_amount_filter', $this->options['wsafp_coupon_amount'] );
					$this->validity = date('Y-m-d',strtotime("+{$this->options['wsafp_coupon_duration']} day"));

					update_post_meta( $new_coupon_id, 'discount_type', 'wsafp_coupon' );
					update_post_meta( $new_coupon_id, 'coupon_amount', $this->coupon_amount );
					update_post_meta( $new_coupon_id, 'individual_use', apply_filters( 'wsafp_coupon_individual_use', $this->options['wsafp_coupon_individual_use'] ) );
					

					// Product settings can be added through filters
					
					update_post_meta( $new_coupon_id, 'product_ids', apply_filters( 'wsafp_coupon_product_ids_filter', '' ));
					update_post_meta( $new_coupon_id, 'exclude_product_ids', apply_filters( 'wsafp_coupon_exclude_product_ids_filter', '' ) );
					
					// Categories settings can be added through filters
				
					update_post_meta( $new_coupon_id, 'product_categories', apply_filters( 'wsafp_coupon_product_categories_filter', array() ));
					update_post_meta( $new_coupon_id, 'exclude_product_categories', apply_filters( 'wsafp_coupon_exclude_product_categories_filter', array() ) );

					update_post_meta( $new_coupon_id, 'usage_limit', apply_filters( 'wsafp_usage_limit', '1' ) );
					update_post_meta( $new_coupon_id, 'expiry_date', $this->validity );
					update_post_meta( $new_coupon_id, 'free_shipping', 'no' );
					update_post_meta( $new_coupon_id, 'customer_email', apply_filters('wsafp_customer_email_filter', $this->email ) );
					update_post_meta( $new_coupon_id, 'minimum_amount', apply_filters('wsafp_minimum_amount_filter', $this->options['wsafp_minimum_amount'] ) );
					update_post_meta( $new_coupon_id, 'wsafp_coupon_type_alias', apply_filters('wsafp_coupon_type_alias_filter', $this->options['wsafp_coupon_type_alias'] ) );

					// Add specific meta to save sponsor's user

					update_post_meta( $new_coupon_id, 'wsafp_sponsor_user_id', $this->parrain );
					update_post_meta( $new_coupon_id, 'wsafp_sponsor_firstname', $this->parrainFirstName );
					update_post_meta( $new_coupon_id, 'wsafp_friend', $this->getFullName() );

					// Room for developpers.

					do_action('wsafp_coupon_generate_code_promo', $new_coupon_id );

					break;
				
				default;

				// This is the reward coupon generation. Triggered on "order_completed".


					$coupon = array(

						'post_title' => $this->coupon_code,
						'post_excerpt' => sprintf(__('Reward coupon to %s','wsafp'),$this->parrainEmail),
						'post_content' => '',
						'post_status' => 'publish',
						'post_author' => 1,
						'post_type'	=> 'shop_coupon'
					);
									
					$new_coupon_id = wp_insert_post( $coupon );
					
					$this->coupon_amount = apply_filters( 'wsafp_coupon_amount_filter', $this->options['wsafp_coupon_amount_sponsor'] );				
					$this->validity = date('Y-m-d',strtotime("+{$this->options['wsafp_coupon_duration_sponsor']} day"));

					// Add meta
					update_post_meta( $new_coupon_id, 'discount_type', 'wsafp_reward_coupon' );
					update_post_meta( $new_coupon_id, 'coupon_amount', $this->coupon_amount );
					update_post_meta( $new_coupon_id, 'individual_use', apply_filters( 'wsafp_coupon_individual_use', $this->options['wsafp_coupon_individual_use_sponsor'] ) );
					
					// Product settings can be added through filters
					update_post_meta( $new_coupon_id, 'product_ids', apply_filters( 'wsafp_coupon_product_ids_filter', '' ));
					update_post_meta( $new_coupon_id, 'exclude_product_ids', apply_filters( 'wsafp_coupon_exclude_product_ids_filter', '' ) );
					
					// Categories settings can be added through filters
					update_post_meta( $new_coupon_id, 'product_categories', apply_filters( 'wsafp_coupon_product_categories_filter', array() ));
					update_post_meta( $new_coupon_id, 'exclude_product_categories', apply_filters( 'wsafp_coupon_exclude_product_categories_filter', array() ) );

					update_post_meta( $new_coupon_id, 'usage_limit', apply_filters( 'wsafp_usage_limit', '1' ) );
					update_post_meta( $new_coupon_id, 'expiry_date', $this->validity );
					update_post_meta( $new_coupon_id, 'free_shipping', 'no' );
					update_post_meta( $new_coupon_id, 'customer_email', apply_filters('wsafp_customer_email_filter', $this->parrainEmail ) );
					update_post_meta( $new_coupon_id, 'minimum_amount', apply_filters('wsafp_minimum_amount_filter', $this->options['wsafp_minimum_amount_sponsor'] ) );
					update_post_meta( $new_coupon_id, 'wsafp_coupon_type_alias', apply_filters('wsafp_coupon_type_alias_filter', $this->options['wsafp_coupon_type_alias_sponsor'] ) );

					// Add specific meta to save filleul user
					// Used in email templates.

					update_post_meta( $new_coupon_id, 'wsafp_eligible_for_update', $this->options['wsafp_coupon_allow_aggregate'] );
					update_post_meta( $new_coupon_id, 'wsafp_filleul', $this->getFilleul() );

					// Room for developpers.

					do_action('wsafp_coupon_generate_code_promo_sponsor', $new_coupon_id );

					break;
			}
	

		} // generateCodePromo

		public function updateCodePromo(){

				$newamount = $this->options['wsafp_coupon_amount_sponsor']+get_post_meta($this->updatable_coupon, 'coupon_amount',true);
				$this->coupon_amount = $newamount;
				$this->validity = date('Y-m-d',strtotime("+{$this->options['wsafp_coupon_duration_sponsor']} day"));
				update_post_meta( $this->updatable_coupon, 'coupon_amount', $newamount ) ;
				update_post_meta( $this->updatable_coupon, 'expiry_date', $this->validity );
				
				// Room for developpers.

				do_action('wsafp_coupon_update_code_promo_sponsor', $this->updatable_coupon);

		}


		public function isUpdatable(){

		
			$args = array(

				'post_type' => 'shop_coupon',
				'meta_query' => array(

					array(
						'key' => 'customer_email',
						'value' => $this->parrainEmail,
					),
					array(
						'key' => 'wsafp_eligible_for_update',
						'value' => 'yes',
					),
					array(
						'key' => 'expiry_date',
						'value' => date('Y-m-d'),
						'compare' => '>',
						'type' => 'DATE'
					),
					array(
						'key' => 'usage_count',
						'value' => '1',
						'compare' => 'NOT EXISTS'
					)
				)
			 );

			
			$coupons_list = get_posts( $args );

			if(!empty($coupons_list)) {
				$this->updatable_coupon = $coupons_list[0]->ID;
				$this->coupon_code = get_the_title($coupons_list[0]->ID);
				return true;	
			}


			return false;



		}

		public function sendEmail(){


				$headers = 'From: '.get_option('woocommerce_email_from_name').' <'.get_option('woocommerce_email_from_address').'>' . "\r\n";

				switch ($this->type) {

					case 'front':

							$email_heading = str_replace(

										array('{{sponsor}}','{{sponsorfirstname}}','{{sitename}}'),
										array( $this->parrainFullName, $this->parrainShortName,  get_bloginfo() ),
										$this->options['wsafp_email_to_friend_heading'] 

							); // str_replace

							$htmlMessage = str_replace(

									array(
										'{{sponsor}}',
										'{{sponsorfirstname}}',
										'{{friend}}',
										'{{friendfirstname}}',
										'{{coupon}}',
										'{{amount}}', 
										'{{sitename}}',
										'{{message}}',
										'{{homeurl}}',
										'{{email}}',
										'{{validity}}'
									),

									array( 
										$this->parrainFullName,
										$this->parrainShortName,
										$this->getFullName(),
										$this->getFirstName(),
										$this->coupon_code,
										$this->coupon_amount,
										get_bloginfo(),
										$this->message,
										'<a href="'.get_home_url().'">'.get_bloginfo().'</a>',
										$this->email,
										date_i18n(get_option('date_format') ,strtotime($this->validity))
									),

									$this->options['wsafp_email_to_friend_content']
							);
							
							if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<' ) ) {


								ob_start();

								woocommerce_get_template( 'emails/email-header.php',

									array(

										'email_heading' => $email_heading

									) 
								);

								echo wpautop( wptexturize( $htmlMessage ) );

								woocommerce_get_template( 'emails/email-footer.php' );
								
								$this->emailHtml = ob_get_clean();

								add_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));

								wp_mail( $this->email , $this->get_email_subject() , $this->emailHtml , $headers );

								remove_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));

							}

							else{

								$mailer = WC()->mailer();
								$mailer->send( $this->email, $this->get_email_subject() , $mailer->wrap_message( $email_heading, $htmlMessage ), '', '' );

							}

						break;
					
					default:

							$email_heading = str_replace(

										array('{{friend}}','{{friendfirstname}}','{{sitename}}'),
										array( $this->getFilleul(), $this->getFirstName(), get_bloginfo() ),
										$this->options['wsafp_email_to_sponsor_heading'] 

							); // str_replace

							$htmlMessage = str_replace(
									array(

										'{{sponsor}}',
										'{{friend}}',
										'{{friendfirstname}}',
										'{{sponsorfirstname}}',
										'{{coupon}}',
										'{{amount}}',
										'{{sitename}}',
										'{{message}}',
										'{{homeurl}}',
										'{{email}}',
										'{{validity}}'
									),

									array( 

										$this->parrainFullName,
										$this->getFilleul(),
										$this->getFirstName(),
										$this->parrainShortName,
										$this->coupon_code,
										$this->coupon_amount,
										get_bloginfo(),
										$this->message,
										'<a href="'.get_home_url().'">'.get_bloginfo().'</a>',
										$this->parrainEmail,
										date_i18n(get_option('date_format') ,strtotime($this->validity))
									),

									$this->options['wsafp_email_to_sponsor_content']
							);

							if ( version_compare( WOOCOMMERCE_VERSION, '2.3', '<' ) ) {

							ob_start();

							woocommerce_get_template( 'emails/email-header.php',
								
								array(

									'email_heading' => $email_heading
								) 
							
							);


							echo wpautop( wptexturize( $htmlMessage ) );

							woocommerce_get_template( 'emails/email-footer.php' );
							
							$this->emailHtml = ob_get_clean();

							add_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));

							wp_mail( $this->parrainEmail , $this->get_email_subject() , $this->emailHtml, $headers );

							remove_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));

							}
							
							else{

								$mailer = WC()->mailer();
								$mailer->send( $this->parrainEmail , $this->get_email_subject() , $mailer->wrap_message( $email_heading, $htmlMessage ), '', '' );

							}
									
						break;
				}

			


			

		}

		public function set_html_content_type(){

				return 'text/html';

		} // set_html_content_type

		public function get_email_subject(){

				if($this->type == 'front'){

					$subject =  str_replace(array('{{sponsor}}','{{sponsorfirstname}}','{{sitename}}'),array($this->parrainFullName,$this->parrainShortName,get_bloginfo()),$this->options['wsafp_email_to_friend_subject']);
				
				}
				else{

					$subject =  str_replace(array('{{friend}}','{{friendfirstname}}','{{sitename}}'),array($this->getFilleul(),$this->getFirstName(),get_bloginfo()),$this->options['wsafp_email_to_sponsor_subject']);
				
				
				}
				return $subject;
		} // set_html_content_type

		public function success(){

				$redirect_to = add_query_arg( 'wsaf-status', 'success' );
				wp_redirect(  $redirect_to  );
				exit;

		} // success


		public function display_error(){

				global $woocommerce;
				
				foreach ($this->errors as $error) {
					
					if( function_exists('wc_add_notice') ){
						wc_add_notice($error,'error');
					}
					else{
						$woocommerce->add_error($error);
					}
					
				}

		
		} // display_error
	
	} // WooCommerce_Sponsor_a_Friend_Plugin_Request
}