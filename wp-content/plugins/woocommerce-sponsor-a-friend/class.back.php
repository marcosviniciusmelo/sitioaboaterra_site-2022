<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * WooCommerce_Sponsor_a_Friend_Plugin_Back
 * Class for backend.
 * @since 1.0
 */

if ( ! class_exists( 'WooCommerce_Sponsor_a_Friend_Plugin_Back' ) ) {

class WooCommerce_Sponsor_a_Friend_Plugin_Back{
		
		private $options = array();

		function __construct()
		{	


			$this->options = get_option('wsafp_options');
			$this->hooks();

		} //__construct
	
		public function hooks()
		{

			add_action( 'woocommerce_order_status_completed', array( &$this, 'check_for_sponsor' ) );
			add_action( 'admin_menu', array( &$this,'menu'));
			add_action( 'admin_init', array( &$this,'options_init') );
			add_filter( 'woocommerce_coupon_discount_types', array( &$this , 'add_coupon_type') );
					
		} // hooks

		

		public static function action_links( $links, $file ) {

    		array_unshift( $links, '<a href="http://docs.mbcreation.com/collection/19-woocommerce-sponsor-a-friend-plugin">' . __('Documentation') . '</a>' );
    		array_unshift( $links, '<a href="' . admin_url( 'admin.php?page=wsafp-sponsor-a-friend' ) . '">' . __('Settings') . '</a>' );
 			return $links;

		} // action_links


		public function menu()
		{

			add_submenu_page( 'woocommerce', __('Sponsor a friend','wsafp'), __('Sponsor a friend','wsafp'),'activate_plugins','wsafp-sponsor-a-friend', array( &$this, 'options_panel' ) );

		} // menu

		public function options_init()
		{

			register_setting( 'wsafp_options_group', 'wsafp_options', array( &$this, 'options_validate' ) );

		} // options_init


		public function options_panel()
		{

			echo '<div class="wrap"><div id="icon-options-general" class="icon32"><br></div><h2>'.__('Sponsor a Friend !','wsafp').'</h2>';
			
			echo '<form method="post" action="options.php">';

			settings_fields('wsafp_options_group');
			$options = $this->options;

			echo '<p>';
			echo __('Welcome to Sponsor a Friend settings screen. Note that the most important thing you have to do here is to choose the page where the plugin will display the form using the [wsafp-form-shortcode] shortcode. This is very similar to others WooCommerce pages. Everything else are tuning or translation jobs :). Refer documentation to use {{alias}} in templates','wsafp');
			echo '</p>';

			echo '<h3>'.__('General settings','wsafp').'</h3>';

			echo '<table class="form-table"><tr valign="top">';
			echo '<th scope="row"><label for="context">';
			echo __('Sponsor a friend page','wsafp');
			echo '</label></th>';
			echo '<td>';
			$args = array(
			    'depth' => 0,
			    'child_of' => 0,
			    'selected' => $options['wsafp_page'],
			    'echo' => 1,
			    'name' => 'wsafp_options[wsafp_page]',
			    'show_option_none' => ''
			);
			wp_dropdown_pages( $args ); 

			echo '<p class="description">'.__('You must add at least the [wsafp-form-shortcode] shortcode in one of your page and select it here.','wsafp').'</p></td></tr>';
			

			// Allow none customer

			echo '<tr valign="top">';
			echo '<th scope="row">'.__( 'Allow none customer to refer friends', 'wsafp' ).'</th>';
			echo '<td><label for="wsafp_allow_none_customer">';
			echo '<input name="wsafp_options[wsafp_allow_none_customer]" type="checkbox" id="wsafp_allow_none_customer" value="yes" '.checked( $options['wsafp_allow_none_customer'], 'yes', false ).'/>';
			echo  __( 'Allow none customer to refer friends', 'wsafp' );
			echo '</label>';
			echo '</fieldset></td>';
			echo '</tr>';

			// Display Form on Thank You Page

			echo '<tr valign="top">';
			echo '<th scope="row">'.__( 'Add form to thank you page', 'wsafp' ).'</th>';
			echo '<td><label for="wsafp_display_on_thankyou_page">';
			echo '<input name="wsafp_options[wsafp_display_on_thankyou_page]" type="checkbox" id="wsafp_display_on_thankyou_page" value="yes" '.checked( $options['wsafp_display_on_thankyou_page'], 'yes', false ).'/>';
			echo  __( 'Display refer a friend form on the Thank You page', 'wsafp' );
			echo '</label>';
			echo '</fieldset></td>';
			echo '</tr>';

			// Display before form.

			echo '<tr valign="top">';
			echo '<th scope="row">'.__('Before form text','wsafp').'</th>';
			echo '<td><fieldset><legend class="screen-reader-text"><span>'.__('Before form text','wsafp').'</span></legend>';
			echo '<p><textarea name="wsafp_options[wsafp_before_thankyou_form]" rows="10" cols="50" id="wsafp_before_thankyou_form" class="large-text code">';
			echo str_replace("\t",'',$options['wsafp_before_thankyou_form']);
			echo '</textarea></p>';
			echo '<p class="description">'.__('Here you can introduce and explain the "refer a friend" mecanism. This will be displayed before the form on the thank you page.','wsafp').'</p>';
			echo '</fieldset></td>';
			echo '</tr>';

			echo '</table>';

			echo '<h3 class="title">'.__('Invitation Coupon settings','wsafp').'</h3>';
			

			echo '<table class="form-table">';
			
			// coupon type

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_coupon_type_alias">'.__('Coupon type', 'woocommerce').'</label></th>';
			echo '<td><select name="wsafp_options[wsafp_coupon_type_alias]" id="wsafp_coupon_type_alias">';
			echo '<option'.selected( $options['wsafp_coupon_type_alias'],'fixed_cart',false ).' value="fixed_cart">'.__('Cart Discount','woocommerce').'</option>';
			echo '<option'.selected( $options['wsafp_coupon_type_alias'],'percent',false ).' value="percent">'.__('Cart % Discount','woocommerce').'</option>';
			echo '<option'.selected( $options['wsafp_coupon_type_alias'],'fixed_product',false ).' value="fixed_product">'.__('Product Discount','woocommerce').'</option>';
			
			// Back compat

			if( version_compare( WC_VERSION, '2.7', '<' ) )
				echo '<option'.selected( $options['wsafp_coupon_type_alias'],'percent_product',false ).' value="percent_product">'.__('Product % Discount','woocommerce').'</option>';
			echo '</select>';
			echo '</td>';
			echo '</tr>';

			// coupon amount

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_coupon_amount">'.__('Coupon amount','wsafp').'</label></th>';
			echo '<td><input type="number" class="small-text" name="wsafp_options[wsafp_coupon_amount]" id="wsafp_coupon_amount" value="'.$options['wsafp_coupon_amount'].'" placeholder="0" step="1" min="0">';
			echo '<p class="description">'.__('This is the amount for the coupon sent to the sponsored friend','wsafp').'</p></td>';
			echo '</tr>';

			// coupon minimum amount

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_minimum_amount">'.__('Coupon minimum amount','wsafp').'</label></th>';
			echo '<td><input type="number" class="small-text" name="wsafp_options[wsafp_minimum_amount]" id="wsafp_minimum_amount" value="'.$options['wsafp_minimum_amount'].'" placeholder="0" step="1" min="0">';
			echo '<p class="description">'.__('You can set minimum amount for the order. For better results you should leave this to 0','wsafp').'</p></td>';
			echo '</tr>';



			// coupon duration

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_coupon_duration">'.__('Coupon duration','wsafp').'</label></th>';
			echo '<td><input type="number" class="small-text" name="wsafp_options[wsafp_coupon_duration]" id="wsafp_coupon_duration" value="'.$options['wsafp_coupon_duration'].'" placeholder="0" step="1" min="0">';
			echo '<p class="description">'.__('Coupon duration is mandatory. Value is a number of days beginning on the coupon creation (on "form submit").','wsafp').'</p></td>';
			echo '</tr>';



			// Individual use

			echo '<tr valign="top">';
			echo '<th scope="row">'.__( 'Individual use', 'woocommerce' ).'</th>';
			echo '<td><label for="wsafp_coupon_individual_use">';
			echo '<input name="wsafp_options[wsafp_coupon_individual_use]" type="checkbox" id="wsafp_coupon_individual_use" value="yes" '.checked( $options['wsafp_coupon_individual_use'], 'yes', false ).'/>';
			echo __( 'Individual use', 'woocommerce' );
			echo '</label>';
			echo '</fieldset></td>';
			echo '</tr>';



			echo '</table>';
			echo '<h3 class="title">'.__('Invitation request email detail (sent after someone fill the form to sponsor someone)','wsafp').'</h3>';
			echo '<table class="form-table">';

			// wsafp_email_to_friend_heading text input


			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_email_to_friend_heading">'.__('email heading','wsafp').'</label></th>';
			echo '<td><input type="text" class="regular-text" name="wsafp_options[wsafp_email_to_friend_heading]" id="wsafp_email_to_friend_heading" value="'.$options['wsafp_email_to_friend_heading'].'" >';
			echo '<p class="description">'.__('Heading will appear on top of the email, just like other WooCommerce email. Refer documentation to use {{placeholder}} in templates','wsafp').'</p></td>';
			echo '</tr>';


			// wsafp_email_to_friend_subject text input


			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_email_to_friend_subject">'.__('email subject','wsafp').'</label></th>';
			echo '<td><input type="text" class="regular-text" name="wsafp_options[wsafp_email_to_friend_subject]" id="wsafp_email_to_friend_subject" value="'.$options['wsafp_email_to_friend_subject'].'" >';
			echo '<p class="description">'.__('This will be the email subject. Refer documentation to use {{placeholder}} in templates','wsafp').'</p></td>';
			echo '</tr>';


			// wsafp_email_to_friend_content textarea

			echo '<tr valign="top">';
			echo '<th scope="row">'.__('email content','wsafp').'</th>';
			echo '<td><fieldset><legend class="screen-reader-text"><span>'.__('email content','wsafp').'</span></legend>';
			echo '<p><textarea name="wsafp_options[wsafp_email_to_friend_content]" rows="10" cols="50" id="wsafp_email_to_friend_content" class="large-text code">';
			echo str_replace("\t",'',$options['wsafp_email_to_friend_content']);
			echo '</textarea></p>';
			echo '<p class="description">'.__('You can use HTML at your own risk :) Refer documentation to use {{placeholder}} in templates','wsafp').'</p>';
			echo '</fieldset></td>';
			echo '</tr>';


			echo '</table>';

			echo '<h3 class="title">'.__('Sponsor reward coupon settings','wsafp').'</h3>';
			echo '<table class="form-table">';

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_coupon_type_alias_sponsor">'.__('Coupon type', 'woocommerce').'</label></th>';
			echo '<td><select name="wsafp_options[wsafp_coupon_type_alias_sponsor]" id="wsafp_coupon_type_alias_sponsor">';
			echo '<option'.selected( $options['wsafp_coupon_type_alias_sponsor'],'fixed_cart',false ).' value="fixed_cart">'.__('Cart Discount','woocommerce').'</option>';
			echo '<option'.selected( $options['wsafp_coupon_type_alias_sponsor'],'percent',false ).' value="percent">'.__('Cart % Discount','woocommerce').'</option>';
			echo '<option'.selected( $options['wsafp_coupon_type_alias_sponsor'],'fixed_product',false ).' value="fixed_product">'.__('Product Discount','woocommerce').'</option>';
			
			// Back compat
			if( version_compare( WC_VERSION, '2.7', '<' ) )
				echo '<option'.selected( $options['wsafp_coupon_type_alias_sponsor'],'percent_product',false ).' value="percent_product">'.__('Product % Discount','woocommerce').'</option>';
			
			echo '</select>';
			echo '</td>';
			echo '</tr>';


			// Coupon aggregation.

			echo '<tr valign="top">';
			echo '<th scope="row">'.__( 'Allow accumulation', 'wsafp' ).'</th>';
			echo '<td><label for="wsafp_coupon_allow_aggregate">';
			echo '<input name="wsafp_options[wsafp_coupon_allow_aggregate]" type="checkbox" id="wsafp_coupon_allow_aggregate" value="yes" '.checked( $options['wsafp_coupon_allow_aggregate'], 'yes', false ).'/>';
			echo __( 'This option is only available for fixed_cart and fixed_product coupons. Sponsor will be able to gather amount on the same coupon every time one of his friends makes an order.', 'wsafp' );
			echo '</label>';
			echo '</fieldset></td>';
			echo '</tr>';

			// coupon amount.

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_coupon_amount_sponsor">'.__('Coupon amount','wsafp').'</label></th>';
			echo '<td><input type="number" class="small-text" name="wsafp_options[wsafp_coupon_amount_sponsor]" id="wsafp_coupon_amount_sponsor" value="'.$options['wsafp_coupon_amount_sponsor'].'" placeholder="0" step="1" min="0">';
			echo '<p class="description">'.__('This is the amount for the coupon sent as a reward. If allow accumulation is on, the amount will grow by this amount each time the user refered a friend.','wsafp').'</p></td>';
			echo '</tr>';

			// coupon minimum amount

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_minimum_amount_sponsor">'.__('Coupon minimum amount','wsafp').'</label></th>';
			echo '<td><input type="number" class="small-text" name="wsafp_options[wsafp_minimum_amount_sponsor]" id="wsafp_minimum_amount_sponsor" value="'.$options['wsafp_minimum_amount_sponsor'].'" placeholder="0" step="1" min="0">';
			echo '<p class="description">'.__('You can set minimum amount for the order. For better results you should leave this to 0','wsafp').'</p></td>';
			echo '</tr>';



			// coupon duration

			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_coupon_duration_sponsor">'.__('Coupon duration','wsafp').'</label></th>';
			echo '<td><input type="number" class="small-text" name="wsafp_options[wsafp_coupon_duration_sponsor]" id="wsafp_coupon_duration_sponsor" value="'.$options['wsafp_coupon_duration_sponsor'].'" placeholder="0" step="1" min="0">';
			echo '<p class="description">'.__('Coupon duration is mandatory. Value is a number of days beginning on the coupon creation (on "order completed"). Note that if allow accumulation is on, the duration will start over every time.','wsafp').'</p></td>';
			echo '</tr>';



			// Individual use

			echo '<tr valign="top">';
			echo '<th scope="row">'.__( 'Individual use', 'woocommerce' ).'</th>';
			echo '<td><label for="wsafp_coupon_individual_use_sponsor">';
			echo '<input name="wsafp_options[wsafp_coupon_individual_use_sponsor]" type="checkbox" id="wsafp_coupon_individual_use_sponsor" value="yes" '.checked( $options['wsafp_coupon_individual_use_sponsor'], 'yes', false ).'/>';
			echo __( 'Individual use', 'woocommerce' );
			echo '</label>';
			echo '</fieldset></td>';
			echo '</tr>';
			echo '</table>';



			echo '<h3 class="title">'.__('reward email details','wsafp').'</h3>';
			echo '<table class="form-table">';

			// wsafp_email_to_sponsor_heading text input


			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_email_to_sponsor_heading">'.__('email heading','wsafp').'</label></th>';
			echo '<td><input type="text" class="regular-text" name="wsafp_options[wsafp_email_to_sponsor_heading]" id="wsafp_email_to_sponsor_heading" value="'.$options['wsafp_email_to_sponsor_heading'].'" >';
			echo '<p class="description">'.__('Heading will appear on top of the email, just like other WooCommerce email. Refer documentation to use {{placeholder}} in templates','wsafp').'</p></td>';
			echo '</tr>';
			

			// wsafp_email_to_sponsor_subject text input


			echo '<tr valign="top">';
			echo '<th scope="row"><label for="wsafp_email_to_sponsor_subject">'.__('email subject','wsafp').'</label></th>';
			echo '<td><input type="text" class="regular-text" name="wsafp_options[wsafp_email_to_sponsor_subject]" id="wsafp_email_to_sponsor_subject" value="'.$options['wsafp_email_to_sponsor_subject'].'" >';
			echo '<p class="description">'.__('Refer documentation to use {{alias}} in templates','wsafp').'</p></td>';
			echo '</tr>';


			// wsafp_email_to_sponsor_content textarea

			echo '<tr valign="top">';
			echo '<th scope="row">'.__('email content','wsafp').'</th>';
			echo '<td><fieldset><legend class="screen-reader-text"><span>'.__('email content','wsafp').'</span></legend>';
			echo '<p><textarea name="wsafp_options[wsafp_email_to_sponsor_content]" rows="10" cols="50" id="wsafp_email_to_sponsor_content" class="large-text code">';
			echo  str_replace("\t",'',$options['wsafp_email_to_sponsor_content']);
			echo '</textarea></p>';
			echo '<p class="description">'.__('You can use HTML at your own risk :) Refer documentation to use {{placeholder}} in templates','wsafp').'</p>';
			echo '</fieldset></td>';
			echo '</tr>';

		
		

			echo '</table>';
			echo '<p class="submit">';
			echo '<input type="submit" class="button-primary" value="';
			echo _e('Save Changes');
			echo '" />';
			echo '</p></form></div>';

		} // Options panel

		public function add_coupon_type($types){

			$types['wsafp_coupon'] = __('Sponsor Coupon','wsafp');
			$types['wsafp_reward_coupon'] = __('Reward Coupon','wsafp');
			return $types;

		} // add_coupon_type

		public function options_validate($options){

			$options['wsafp_allow_none_customer'] = isset( $options['wsafp_allow_none_customer'] ) ? 'yes' : 'no';

			$options['wsafp_display_on_thankyou_page'] = isset( $options['wsafp_display_on_thankyou_page'] ) ? 'yes' : 'no';

			$options['wsafp_coupon_individual_use'] = isset( $options['wsafp_coupon_individual_use'] ) ? 'yes' : 'no';
			
			$options['wsafp_coupon_individual_use_sponsor'] = isset( $options['wsafp_coupon_individual_use_sponsor'] ) ? 'yes' : 'no';

			$options['wsafp_coupon_allow_aggregate'] = isset( $options['wsafp_coupon_allow_aggregate'] ) ? 'yes' : 'no';
			
			// Not allow coupon aggregation on % value

			if($options['wsafp_coupon_type_alias_sponsor']!='fixed_product' and $options['wsafp_coupon_type_alias_sponsor']!='fixed_cart' )
				$options['wsafp_coupon_allow_aggregate'] = 'no';
			

			// valid wsafp_coupon_duration_sponsor check
			// @return 45 as default
			if($options['wsafp_coupon_duration_sponsor']<1 or empty($options['wsafp_coupon_duration_sponsor']) or !is_numeric($options['wsafp_coupon_duration_sponsor']))
				$options['wsafp_coupon_duration_sponsor'] = '45';

			// valid wsafp_coupon_duration check
			// @return 45 as default
			if($options['wsafp_coupon_duration']<1 or empty($options['wsafp_coupon_duration']) or !is_numeric($options['wsafp_coupon_duration']))
				$options['wsafp_coupon_duration'] = '45';

			

			return $options;
		
		} // Options validate

		/**
		 * check_for_sponsor
		 * On order completed. Check if a sponsor coupon was used.
		 * @since 1.0
		 */

		public function check_for_sponsor($order_id)
		{
			

			$order = new WooCommerce_Sponsor_a_Friend_Plugin_Order($order_id);

			if( $order->has_coupon() && $order->has_sponsor_coupon()  && !$order->has_been_check_for_coupon() ) {			


				$request = new WooCommerce_Sponsor_a_Friend_Plugin_Request();

				$request->setType('back');

				if( is_email($order->parrain) ) :

					$request->parrain = $order->parrain;
					$request->parrainEmail = $order->parrain;
					$request->parrainShortName = $request->getParrainShortname();
	
				else :

					$request->parrain = $order->parrain;
					$request->parrainEmail = $request->getParrainEmail();
					$request->parrainShortName = $request->getParrainShortname();
					
				endif;

				if( version_compare( WC_VERSION, '2.7', '<' ) ){
					$request->setFirstName( $order->wc_order->billing_first_name );
					$request->setFilleul($order->wc_order->billing_first_name.' '.$order->wc_order->billing_last_name);
				}
				else{
					$request->setFirstName( $order->wc_order->get_billing_first_name() );
					$request->setFilleul( $order->wc_order->get_billing_first_name().' '.$order->wc_order->get_billing_last_name() );
				}

				if($this->options['wsafp_coupon_allow_aggregate']=='yes' && $request->isUpdatable()){

					$request->updateCodePromo();

				}

				else{

					$request->generateCodePromo();

				}

				$request->sendEmail();


				// Make sure coupon is issued just once
				$order->update_status();
			
			}

		} // check_for_sponsor

		public function set_html_content_type(){

				return 'text/html';

		} // set_html_content_type



	} // WooCommerce_Sponsor_a_Friend_Plugin_Back
}