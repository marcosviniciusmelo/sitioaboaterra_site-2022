<?php
/**
 * Formulário de checkout
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<fieldset id="vr-payment-form" class="<?php echo 'storefront' === basename( get_template_directory() ) ? 'woocommerce-pagseguro-form-storefront' : ''; ?>" data-cart_total="<?php echo esc_attr( number_format( $cart_total, 2, '.', '' ) ); ?>" style="width:100%;">
	<div style="width:100;">
		<label for="vr-card-name">
			<strong><?php _e( "VR Card owner's name", 'vr-woocommerce' );?></strong>
		</label>
		<input type="text" id="vr-card-name" name="vr-card-name" style="width:100%;" />
		<label for="vr-card-name">
			<strong><?php _e( "VR Card owner's Document", 'vr-woocommerce' );?></strong>
		</label>
		<input type="text" class="vr-wc-cpf" name="vr-card-cpf" onkeydown="javascript: fMasc( this, mCPF );" style="width:100%;" maxlength="14" />

		<label for="vr-card-num">
			<strong><?php _e( "Number", 'vr-woocommerce' );?></strong>
		</label>
		<input type="text" class="vr-wc-cc-num" id="vr-card-num" name="vr-card-num" style="width:100%;" maxlength="16" />
		<label for="vr-card-exp-date" style="width: 100%;clear:both;display:block;">
			<strong><?php _e( "Expiration date", 'vr-woocommerce' );?></strong>
			<br>
		</label>
		<input type="text" class="vr-wc-exp" id="vr-card-exp-date" onkeydown="javascript: fMasc( this, mData );" name="vr-card-exp-date" style="width:20%;" maxlength="5" />
		<label for="vr-card-security-code" style="width: 100%;clear:both;display:block;">
			<strong><?php _e( "Security Code", 'vr-woocommerce' );?></strong>
			<br>
			<small>
				<?php _e( 'The three numbers behind the card', 'vr-woocommerce' );?>
			</small>
		</label>
		<input type="text" class="vr-wc-num" id="vr-card-security-code" name="vr-card-security-code" style="width:20%;" maxlength="3" />


	</div>
</fieldset>


<script>
      
    function fMasc(objeto,mascara) {
		obj=objeto
		masc=mascara
		setTimeout("fMascEx()",1)
	}
	function fMascEx() {
		obj.value=masc(obj.value)
	}
    function mCPF(cpf){
		cpf=cpf.replace(/\D/g,"")
		cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
		cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
		cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
		return cpf
	}
	function mData(data){
		data=data.replace(/\D/g,"")
		data=data.replace(/^(\d{2})(\d)/,"$1/$2")
		return data
	}
      
</script>