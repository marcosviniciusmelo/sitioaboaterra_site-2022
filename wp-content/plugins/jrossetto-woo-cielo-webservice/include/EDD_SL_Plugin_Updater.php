<?php
class EDD_SL_Plugin_Updater {
  function pw_edd_sl_activate_license() {
    global $edd_options;

    if( !isset( $_POST['your_activation_button'] ) )
    return;

  

    if( get_option( 'pw_edd_sl_license_active' ) == 'active' )
    return;

    $license = sanitize_text_field( $_POST['edd_settings_misc']['edd_sl_license_key'] );

    // data to send in our API request
    $api_params = array(
      'edd_action'=> 'activate_license',
      'license' 	=> $license,
      'item_name' => urlencode( EDD_SL_PLUGIN_NAME ) // the name of our product in EDD
    );

    // Call the custom API.
    $response = wp_remote_get( add_query_arg( $api_params, EDD_SL_STORE_API_URL ) );

    // make sure the response came back okay
    if ( is_wp_error( $response ) )
    return false;

    // decode the license data
    $license_data = json_decode( wp_remote_retrieve_body( $response ) );

    update_option( 'pw_edd_sl_license_active', $license_data->license );

  }

}
?>
