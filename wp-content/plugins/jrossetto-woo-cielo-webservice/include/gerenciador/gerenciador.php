<?php
//Gerenciador de Pedidos
class WC_Cielo_Webservice_Jrossetto_Gerenciador
{
  public function __construct()
  {

    add_action("admin_menu", array(
      $this,
      "menu"
    ));
    add_action("wp_ajax_processar_pedidos_cielo_webservice", array(
      $this,
      "processar_pedidos_cielo_webservice"
    ));
    add_action("wp_ajax_logs_cielo_webservice_api_jrossetto", array(
      $this,
      "logs"
    ));
      add_action( 'admin_enqueue_scripts', array( $this, 'cielo_jrossetto_admin_style' ), 11 );
  }

  public function cielo_jrossetto_admin_style() {
    wp_enqueue_style('cielo-jrossetto-admin', plugins_url('../css/cielo-jrossetto-admin.css', __FILE__));
  }


  public function logs()
  {
    global $wpdb;
    if (is_admin()) {
      $config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
      if ($config->testmode == "yes") {
        $provider = "Simulado";
        $urlweb   = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/";
      } else {
        $provider = "Cielo";
        $urlweb   = "https://apiquery.cieloecommerce.cielo.com.br/1/";
      }
      include_once CIELO_WEBSERVICE_WOO_PATH . "/include/restclient.php";
      $headers = array(
        "Content-Type" => "application/json",
        "Accept" => "application/json",
        "MerchantId" =>trim($config->afiliacao),
        "MerchantKey" => trim($config->chave),
        "RequestId" => "",
      );

      if($config->testmode=='yes'){
        $headers = array(
          "Content-Type" => "application/json",
          "Accept" => "application/json",
          "MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
          "MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
          "RequestId" => "",
        );

      }
      $api          = new RestClient(array(
        "base_url" => $urlweb,
        "headers" => $headers
      ));
      $response     = $api->get("sales/" . $_GET["id"] . "");
      $dados_pedido = @json_decode($response->response, true);
      echo "<pre>";
      echo print_r($dados_pedido, true);
      echo "</pre>";
    }
    exit;
  }
  public function processar_pedidos_cielo_webservice()
  {
    global $wpdb;
    if (is_admin()) {
      $config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
      if ($config->testmode == "yes") {
        $provider = "Simulado";
        $urlweb   = "https://apisandbox.cieloecommerce.cielo.com.br/1/";
      } else {
        $provider = "Cielo";
        $urlweb   = "https://api.cieloecommerce.cielo.com.br/1/";
      }

      include_once CIELO_WEBSERVICE_WOO_PATH . "/include/restclient.php";

      $headers = array(
        "Content-Type" => "application/json",
        "Accept" => "application/json",
        "MerchantId" =>trim($config->afiliacao),
        "MerchantKey" => trim($config->chave),
        "RequestId" => "",
      );

      if($config->testmode=='yes'){
        $headers = array(
          "Content-Type" => "application/json",
          "Accept" => "application/json",
          "MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
          "MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
          "RequestId" => "",
        );

      }
      $api     = new RestClient(array(
        "base_url" => $urlweb,
        "headers" => $headers
      ));
      if (isset($_POST["acoes"]) && 0 < count($_POST["acoes"])) {
        $log = "";
        foreach ($_POST["acoes"] as $dados) {
          if (sha1(md5($dados["pedido"])) == $dados["hash"]) {
            $cielo = (array) $wpdb->get_row("SELECT * FROM `wp_cielo_api_jrossetto` WHERE `pedido` = '" . (int) $dados["pedido"] . "' ORDER BY id DESC;");
            if (isset($cielo["id_pagamento"])) {
              $log .= "Requisicao de " . strtoupper($dados["acao"]) . " feita via ADMIN em " . date("d/m/Y H:i:s") . " ao Pedido #" . (int) $dados["pedido"] . "";
              if ($dados["acao"] == "capturar") {
                $acao           = array();
                $acao["amount"] = number_format($cielo["total"], 2, ".", "");
                $response       = $api->put("sales/" . $cielo["id_pagamento"] . "/capture", json_encode($acao));
                if ("yes" === $config->debug) {
                  $logs = new WC_Logger();
                  $logs->add($this->id, "Log Captura Cielo: " . $response->response);
                }
                if (isset($response->status)) {
                  $dados_pedido = json_decode($response->response, true);
                  if (isset($dados_pedido["Status"]) && $dados_pedido["Status"] == 2) {
                    $detalhes = "Pedido #" . (int) $dados["pedido"] . " Capturado com sucesso via ADMIN em " . date("d/m/Y H:i:s") . "";
                    if (isset($dados_pedido["ProofOfSale"])) {
                      $detalhes .= " / NSU: " . $dados_pedido["ProofOfSale"];
                    }
                    if (isset($dados_pedido["AuthorizationCode"])) {
                      $detalhes .= " / Auth: " . $dados_pedido["AuthorizationCode"];
                    }
                    $order = new WC_Order((int) $dados["pedido"]);
                    $order->add_order_note($detalhes);
                    $order->update_status($config->pago);
                    $log .= " / TID: " . $dados_pedido["Tid"] . " realizada com sucesso!<br>";
                    $wpdb->query("UPDATE `wp_cielo_api_jrossetto` SET  `lr` =  '" . $dados_pedido["ReturnCode"] . "',
                      `lr_log` =  '" . $dados_pedido["ReturnMessage"] . "',
                      `status` =  '" . $dados_pedido["Status"] . "' WHERE `pedido` = '" . (int) $dados["pedido"] . "';");
                      echo $detalhes . "<br>";
                    } else {
                      echo $response->response;
                      $log .= " retornou um problema: " . $response->response . "<br>";
                    }
                  }
                }
                if ($dados["acao"] == "cancelar") {
                  $acao           = array();
                  $acao["amount"] = number_format($cielo["total"], 2, ".", "");
                  $response       = $api->put("sales/" . $cielo["id_pagamento"] . "/void", json_encode($acao));
                  if ("yes" === $config->debug) {
                    $logs = new WC_Logger();
                    $logs->add($this->id, "Log Cancelamento Cielo: " . $response->response);
                  }
                  if (isset($response->status)) {
                    $dados_pedido = json_decode($response->response, true);
                    if (isset($dados_pedido["Status"]) && $dados_pedido["Status"] == 10) {
                      $detalhes = "Pedido #" . (int) $dados["pedido"] . " Cancelado com sucesso via ADMIN em " . date("d/m/Y H:i:s") . "";
                      if (isset($dados_pedido["ProofOfSale"])) {
                        $detalhes .= " / NSU: " . $dados_pedido["ProofOfSale"];
                      }
                      if (isset($dados_pedido["AuthorizationCode"])) {
                        $detalhes .= " / Auth: " . $dados_pedido["AuthorizationCode"];
                      }
                      $order = new WC_Order((int) $dados["pedido"]);
                      $order->add_order_note($detalhes);
                      $order->update_status($config->cancelado);
                      $log .= " / TID: " . $dados_pedido["Tid"] . " realizada com sucesso!<br>";
                      $wpdb->query("UPDATE `wp_cielo_api_jrossetto` SET  `lr` =  '" . $dados_pedido["ReturnCode"] . "',
                        `lr_log` =  '" . $dados_pedido["ReturnMessage"] . "',
                        `status` =  '" . $dados_pedido["Status"] . "' WHERE `pedido` = '" . (int) $dados["pedido"] . "';");
                        if (CIELO_WEBSERVICE_WOO_REESTOCK == true) {
                          $config->restore_order_stock((int) $dados["pedido"]);
                        }
                        echo $detalhes . "<br>";
                      } else {
                        echo $response->response;
                        $log .= " retornou um problema: " . $response->response . "<br>";
                      }
                    }
                  }
                }
              }
            }
            if (!empty($log)) {
              $logs = new WC_Logger();
              $logs->add("jrossetto_woo_cielo_webservice", $log);
            }
            exit;
          } else {
            exit("Acesso negado!");
          }
        } else {
          exit("Acesso negado!");
        }
      }
      public function menu()
       {
         add_submenu_page("woocommerce", "Pedidos Cielo API", "Pagamentos Cielo API", "manage_woocommerce", "jrossetto-woo-cielo-webservice-pedidos", array(
           $this,
           "admin"
         ));
       }

      public function status_cielo($id)
      {
        switch ($id) {
          case "2":
          $return = '<mark class="order-status status-success tips"><span class="dashicons dashicons-yes-alt"></span><span>&nbsp;Pagamento Aprovado</span></mark>';
          break;
          case "1":
          $return = '<mark class="order-status status-autorized tips"><span class="dashicons dashicons-plus-alt"></span><span>&nbsp;Pagamento Autorizado</span></mark>';
          break;
          case "3":
          $return = '<mark class="order-status status-denied tips"><span class="dashicons dashicons-warning"></span><span>&nbsp;Pagamento Negado</span></mark>';
          break;
          case "10":
          case "13":
          $return = '<mark class="order-status status-canceled tips"><span class="dashicons dashicons-dismiss"></span><span>&nbsp;Pagamento Cancelado</span></mark>';
          break;
          default:
          $return = '<mark class="order-status status-pending tips"><span class="dashicons dashicons-backup"></span><span>&nbsp;Aguardando Pagamento</span></mark>';
        }
        return $return;


        // switch ($id) {
        //   case "2":
        //   $status = "<span style=\"color: #20bb20;\">Aprovada</span>";
        //   break;
        //   case "1":
        //   $status = "<span style=\"color: #2196f3;\">Autorizada</span>";
        //   break;
        //   case "3":
        //   $status = "<span style=\"color: red;\">Negada</span>";
        //   break;
        //   case "10":
        //   case "13":
        //   $status = "<span style=\"color: red;\">Cancelada</span>";
        //   break;
        //   default:
        //   $status = "Aguardando Pagamento";
        // }
        // return $status;
      }
      public function registro_cielo($order_id)
      {
        global $wpdb;
        return (array) $wpdb->get_row("SELECT * FROM `wp_cielo_api_jrossetto` WHERE `pedido` = '" . (int) $order_id . "' ORDER BY id DESC;");
      }
      public function admin()
      {
        $pagina = (int) isset($_GET["pg"]) ? $_GET["pg"] : 1;
        $tipo   = isset($_GET["tipo"]) ? $_GET["tipo"] : "";
        $args   = array(
          "limit" => get_option("posts_per_page"),
          "orderby" => "ID",
          "payment_method" => array(
            "jrossetto_woo_cielo_webservice",
            "jrossetto_woo_cielo_webservice_debito",
            "jrossetto_woo_cielo_webservice_tef",
            "jrossetto_woo_cielo_webservice_boleto"
          ),
          "order" => "DESC",
          "paged" => $pagina,
          "paginate" => true
        );
        if (!empty($tipo)) {
          $args = array_merge($args, array(
            "payment_method" => trim($tipo)
          ));
        }
        if (isset($_GET["pedido"])) {
          $orders                = new stdClass();
          $orders->max_num_pages = 1;
          $orders->total         = 1;
          $orders->orders[]      = new WC_Order((int) $_GET["pedido"]);
        } else {
          $orders = wc_get_orders($args);
        }
        $page_links = paginate_links(array(
          "base" => add_query_arg("pg", "%#%"),
          "format" => "",
          "prev_text" => __("&laquo;", "jrossetto-woo-rastreamentos"),
          "next_text" => __("&raquo;", "jrossetto-woo-rastreamentos"),
          "total" => $orders->max_num_pages,
          "current" => $pagina
        ));
        $total      = $orders->total;
        $pedidos    = $orders->orders;
        include_once dirname(__FILE__) . "/listar.php";
      }
    }

    ?>
