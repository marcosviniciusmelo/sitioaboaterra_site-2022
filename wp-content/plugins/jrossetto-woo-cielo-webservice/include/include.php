<?php
require_once 'pedido.php';

//ambientes
define('VERSAO', "1.4.0");

//pega dados do cartao
function CartaoJrossetto($bandeira,$tipo_parcela){
$bandeira = strtoupper($bandeira);
switch($bandeira){

//Indicador de autoriza��o:
//0 � N�o autorizar (somente autenticar).
//1 � Autorizar somente se autenticada.
//2 � Autorizar autenticada e n�o autenticada.
//3 � Autorizar sem passar por autentica��o (somente para cr�dito) � tamb�m conhecida como Autoriza��o Direta.
//Obs.: Para Diners, Discover, Elo, Amex, Aura e JCB o valor ser� sempre 3.
//4 � Transa��o Recorrente.

case 'VISA':
$op = 'visa';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'VISAELECTRON':
$op = 'visa';
$autorizar = 2;
$parcela = 'A';
break;

case 'MASTERCARD':
$op = 'mastercard';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'ELODEBITO':
$op = 'elo';
$autorizar = 2;
$parcela = 'A';
break;

case 'MAESTRO':
$op = 'mastercard';
$autorizar = 2;
$parcela = 'A';
break;

case 'ELO':
$op = 'elo';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'DINERS':
$op = 'diners';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'DISCOVER':
$op = 'discover';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'AMEX':
$op = 'amex';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'AURA':
$op = 'aura';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'JCB':
$op = 'jcb';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'HIPER':
case 'HIPERCARD':
$op = 'hipercard';
$autorizar = 3;
$parcela = $tipo_parcela;
break;

case 'BOLETO':
$op = 'boleto';
$autorizar = 1;
$parcela = 1;
break;

}
return array('cc'=>$op,'au'=>$autorizar,'tp'=>$parcela);
}

// Envia requisi��o
function httprequest($paEndereco, $paPost){

	$sessao_curl = curl_init();
	curl_setopt($sessao_curl, CURLOPT_URL, $paEndereco);

	curl_setopt($sessao_curl, CURLOPT_FAILONERROR, true);

	//  CURLOPT_SSL_VERIFYPEER
	//  verifica a validade do certificado
	curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYPEER, false);
	//  CURLOPPT_SSL_VERIFYHOST
	//  verifica se a identidade do servidor bate com aquela informada no certificado
	curl_setopt($sessao_curl, CURLOPT_SSL_VERIFYHOST, false);

	curl_setopt($sessao_curl, CURLOPT_SSLVERSION, 4);

	//  CURLOPT_CONNECTTIMEOUT
	//  o tempo em segundos de espera para obter uma conex�o
	curl_setopt($sessao_curl, CURLOPT_CONNECTTIMEOUT, 10);

	//  CURLOPT_TIMEOUT
	//  o tempo m�ximo em segundos de espera para a execu��o da requisi��o (curl_exec)
	curl_setopt($sessao_curl, CURLOPT_TIMEOUT, 40);

	//  CURLOPT_RETURNTRANSFER
	//  TRUE para curl_exec retornar uma string de resultado em caso de sucesso, ao
	//  inv�s de imprimir o resultado na tela. Retorna FALSE se h� problemas na requisi��o
	curl_setopt($sessao_curl, CURLOPT_RETURNTRANSFER, true);

	curl_setopt($sessao_curl, CURLOPT_POST, true);
	curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, $paPost );

	$resultado = curl_exec($sessao_curl);

	curl_close($sessao_curl);

	if ($resultado)
	{
		return $resultado;
	}
	else
	{
		return curl_error($sessao_curl);
	}
}

function get_ip_cielo_webservice() {
    $variables = array('REMOTE_ADDR',
                       'HTTP_X_FORWARDED_FOR',
                       'HTTP_X_FORWARDED',
                       'HTTP_FORWARDED_FOR',
                       'HTTP_FORWARDED',
                       'HTTP_X_COMING_FROM',
                       'HTTP_COMING_FROM',
                       'HTTP_CLIENT_IP');

    $return = 'Unknown';

    foreach ($variables as $variable)
    {
        if (isset($_SERVER[$variable]))
        {
            $return = $_SERVER[$variable];
            break;
        }
    }

    return $return;
}
?>
