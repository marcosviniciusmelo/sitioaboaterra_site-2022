jQuery( function( $ ) {



	function init_card(){


					var approved = new Object;
					approved['jrossetto_woo_cielo_webservice'] = {
						numberInput: '.mascaras_cartao_cielo_webservice',
						expiryInput: '#validade-cielo-webservice',
						cvcInput: '#cvv-cielo-webservice',
						nameInput: '.nome_titular_cielo_webservice'
					};

					var method = $('input[name="payment_method"]:checked').val();
					if(typeof(approved[method]) !== 'undefined') {
							var card = new Card({
								form: 'form.checkout',
							// a selector or jQuery object for the container
							// where you want the card to appear
							//container: 'li.payment_method_' + method, // *required*
							container: "#cielo-webservice-card",
							numberInput: approved[method].numberInput,
							expiryInput: approved[method].expiryInput, // optional — default input[name="expiry"]
							cvcInput: approved[method].cvcInput, // optional — default input[name="cvc"]
							nameInput: approved[method].nameInput,
							width: '100%', // optional — default 350px
							formatting: true, // optional - default true
							placeholders: {
								number: '•••• •••• •••• ••••',
								name: 'Nome completo',
								expiry: '••/••',
								cvc: '•••'
						}


						});
					}
					$('input[name="payment_method"]').click(function(){
						if($('form.checkout').data('card')){
							$('form.checkout').data('card').destroy();
							$('.card-container').remove();
						}
						console.log(card);
						init_card();

					});
				}


				$( 'body' ).on('updated_checkout', init_card );

				$( document ).on( 'blur', '#cc-number2', function() {
					if($('.jp-card.jp-card-unknown')[0]){
						alert('Cartão inválido');
						//$('#cc-number2').focus();
					}
				});
	//ativa mascaras cartao
	// $( document ).on( 'click focus', '.mascaras_cartao_cielo_webservice', function() {
	// 	$(this).payment('formatCardNumber');
	// });
	//
	// $( document ).on( 'click focus', '#validade-cielo-webservice', function() {
	// 	$(this).payment('formatCardExpiry');
	// });
	//
	// $( document ).on( 'click focus', '#cvv-cielo-webservice', function() {
	// 	$(this).payment('formatCardCVC');
	// });
	//
	// //debito
	//
	// $( document ).on( 'click focus', '#validade-cielo-webservice-debito', function() {
	// 	$(this).payment('formatCardExpiry');
	// });
	//
	// $( document ).on( 'click focus', '#cvv-cielo-webservice-debito', function() {
	// 	$(this).payment('formatCardCVC');
	// });





	//converte o nome do titular
	$( document ).on( 'keyup', '.nome_titular_cielo_webservice', function() {
		$(this).val(($(this).val()).toUpperCase());
	});

	// $( document ).on( 'focusout', '#cc-number', function() {
	// 	var cardType = $.payment.cardType($('#cc-number').val());
	// 	if(cardType){
	// 		aplicar_bandeira_cielo_webservice(cardType);
	// 	}
	// });

	$('.woocommerce').on('DOMNodeInserted', function(e) {
		if ($(e.target).is('.woocommerce-NoticeGroup-checkout')) {
			var error_count = $('.woocommerce-error li').size();
			if(error_count>0){
						$('#cc-number2').val('');
						$('#cvv-cielo-webservice').val('');
						$('#validade-cielo-webservice').val('');
						$('.nome_titular_cielo_webservice').val('');
			}

		}
	});



});
