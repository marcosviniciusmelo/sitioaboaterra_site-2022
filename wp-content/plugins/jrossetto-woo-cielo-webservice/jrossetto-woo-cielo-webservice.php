<?php
/*
 * Plugin Name: Cielo Webservice API 3.0  - Jrossetto
 * Plugin URI: https://www.jrossetto.com.br/plugin-cielo-3-0-woocommerce-wordpress/
 * Text Domain: jrossetto-woo-cielo-webservice
 * Description: Integra&ccedil;&atilde;o para API 3.0 Cielo Webservice para Cart&otilde;es Crédito / Débito / Boletos / TEF
 * Version: 2.2
 * Author: Juliano Rossetto
 * Author URI: http://www.jrossetto.com.br/
 * Copyright: © 2009-2019 Jrossetto.
 * License: Commercial
 * WC requires at least: 3.0.0
 * WC tested up to:      3.6.2
*/

//define a pasta do modulo
define('CIELO_WEBSERVICE_WOO_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ));
define('SL_VERSION', '2.2');
define('CIELO_WEBSERVICE_WOO_CURL_SSL', 6);
define('CIELO_WEBSERVICE_WOO_PRAZO_CAPTURA', 5);
define('CIELO_WEBSERVICE_WOO_REESTOCK', true);
define('CIELO_WEBSERVICE_WOO_BACKOFFICE', 'https://minhaconta2.cielo.com.br/login/');

if (!function_exists('woo_jrossetto_cielo_license_menu'))   {

	function woo_jrossetto_cielo_license_menu() {
		add_menu_page( 'Cielo API 3.0', 'Cielo API 3.0', 'manage_options', 'JROSSETTO_LICENSE', 'woo_jrossetto_cielo_license_page', plugins_url('images/icon-cielo.png', __FILE__) );


	}
}

add_action('admin_menu', 'woo_jrossetto_cielo_license_menu');





function woo_jrossetto_cielo_license_page() {



	if($_REQUEST['forceupdate'] == true && $_REQUEST['license'] != NULL){
		update_option( 'wcwsl_softlicense_key', $_REQUEST['license'] );
	}


	$status  = get_option( 'wcwsl_softlicense_key_status' );
	$license = get_option( 'wcwsl_softlicense_key' );

	?>
	<div class="wrap">
		<h2><?php _e('Ativação do plugin'); ?></h2>
		<form id="wcwsl_licenseform" method="post" action="">

			<?php settings_fields('edd_sample_license'); ?>

			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('Chave de licença'); ?>
						</th>
						<td>
							<input id="wcwsl_license" name="wcwsl_softlicense_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" <?php if($status == 'valid') echo 'disabled'; ?> />
							<label class="description <?php if($status == 'valid') echo 'hidden'; ?>" for="edd_sample_license_key"><?php _e('Coloque aqui sua chave'); ?></label>
						</td>
					</tr>


					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('Ativar Licença'); ?>
						</th>
						<td>
							<?php if( $status !== false && $status == 'valid' ) { ?>
								<span style="color:green;"><?php _e('ATIVA'); ?></span>
								<?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>

							<?php } else {
								wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
								<button id="wcwsl_license_submit" class="button-secondary" name="edd_license_activate"> Ativar licença </button>
							<?php } ?>
						</td>
					</tr>

				</tbody>
			</table>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
			<script>

			jQuery(document).bind('PgwModal::Close', function() {
				jQuery('#pgwModalBackdrop').remove();
				jQuery('#pgwModal').remove();
			});

			var frm = jQuery('#wcwsl_licenseform');

			frm.submit(function (e) {

				e.preventDefault();
				jQuery('#wcwsl_licenseform').block({
					message: '<br><center><b>Aguarde um momento...</b></center><br>',
					css: { border: '2px solid #CCC', 'border-radius': '5px' }
				});
				jQuery.ajax({
					url: ajaxurl,
					type: 'post',
					dataType: 'json',
					data: {
						action: 'wcwsl_licenseverify',
						data: jQuery('input#wcwsl_license').val()
					},
					success: function (response) {

						if (response.status && response.status != 'warning') {

							jQuery('form#wcwsl_licenseform').append('<div class="notice notice-success is-dismissible">' + response.message + '. A página será recarregada em breve.</div>');
							setTimeout(function () {
								jQuery('.notice.notice-success').slideUp('slow').fadeOut(function () {
									window.location.reload();
								});
							}, 2500);
						} else if (response.status == 'warning') {
							jQuery('form#wcwsl_licenseform').append('<div class="notice notice-warning is-dismissible">' + response.message + '</div>');
							jQuery('#wcwsl_licenseform').unblock();
						} else {
							jQuery('form#wcwsl_licenseform').append('<div class="notice notice-error is-dismissible">' + response.message + '</div>');
							jQuery('#wcwsl_licenseform').unblock();
						}

					}
				});
			});
			</script>

			<?php if( $status !== false && $status == 'valid' ) { ?>
				<div class="ativo-sucesso">

					<small>Dúvidas acesse a <a href="https://www.jrossetto.com.br/kb/plugin-cielo-api-3-0/" target="_blank">base de conhecimento</a> ou entre em contato: contato@jrossetto.com.br</small>
					<?php

					$links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice' ) ) . '">' . __( 'Configurar Crédito', 'jrossetto-woo-cielo-webservice' ) . '</a>';
					$links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice_debito' ) ) . '">' . __( 'Configurar Débito', 'jrossetto-woo-cielo-webservice' ) . '</a>';
					$links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice_boleto' ) ) . '">' . __( 'Configurar Boleto', 'jrossetto-woo-cielo-webservice' ) . '</a>';
					$links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice_tef' ) ) . '">' . __( 'Configurar TEF', 'jrossetto-woo-cielo-webservice' ) . '</a>';
					echo '<ul>';
					foreach ($links as $value) {
						echo '<li>' . $value .'</li>';
					}
					echo '</ul>';
					?>

				</div>
				<div class="descriptions">

					<?php
					$descriptions = descriptions();
					if($descriptions->status == "success" && $descriptions->status_code == "s205"){
						echo '<strong>Assinatura válida para atualizações</strong>: ' . date('d/m/Y', strtotime($descriptions->licence_start)) . ' até ' . date('d/m/Y', strtotime($descriptions->licence_expire));
					}

					if(strtotime($descriptions->licence_expire) < time()){
						echo '<p style="color:red;text-transform: uppercase;">ATENÇÃO, seu período de atualizações expirou.</p>';
					}
					?>


				</div>

			<?php } ?>

		</form>
		<?php
	}

	function descriptions(){

		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$args = array(
			'woo_sl_action' => 'status-check',
			'licence_key' => get_option( 'wcwsl_softlicense_key' ),
			'product_unique_id' => 'woo-jrossetto-cielo',
			'domain' => str_replace($protocol, "", get_bloginfo('wpurl'))
		);
		$request_uri = 'http://www.jrossetto.com.br/index.php' . '?' . http_build_query($args, '', '&');
		$data = wp_remote_get($request_uri);
		if (is_wp_error($data) || $data['response']['code'] != 200)
		return;
		$data_body = json_decode($data['body']);
		$data_body = $data_body[0];
		return $data_body;
	}

	//atalhos
	function plugin_action_links( $links ) {
		$plugin_links = array();
		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '2.1', '>=' ) ) {
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice' ) ) . '">' . __( 'Configura Crédito', 'jrossetto-woo-cielo-webservice' ) . '</a>';
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice_debito' ) ) . '">' . __( 'Configura Débito', 'jrossetto-woo-cielo-webservice' ) . '</a>';
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice_boleto' ) ) . '">' . __( 'Configura Boleto', 'jrossetto-woo-cielo-webservice' ) . '</a>';
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice_tef' ) ) . '">' . __( 'Configura TEF', 'jrossetto-woo-cielo-webservice' ) . '</a>';
		} else {
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_jrossetto_woo_cielo_webservice' ) ) . '">' . __( 'Configura Crédito', 'jrossetto-woo-cielo-webservice' ) . '</a>';
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_jrossetto_woo_cielo_webservice_debito' ) ) . '">' . __( 'Configura Débito', 'jrossetto-woo-cielo-webservice' ) . '</a>';
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_jrossetto_woo_cielo_webservice_boleto' ) ) . '">' . __( 'Configura Boleto', 'jrossetto-woo-cielo-webservice' ) . '</a>';
			$plugin_links[] = '<a href="' . esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_jrossetto_woo_cielo_webservice_tef' ) ) . '">' . __( 'Configura TEF', 'jrossetto-woo-cielo-webservice' ) . '</a>';
		}
		return array_merge( $plugin_links, $links );
	}
	if(is_admin()) {
		add_filter('plugin_action_links_'.plugin_basename( __FILE__ ),'plugin_action_links');
		include_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.cielo_widget.php' );
		$widget= new WC_Cielo_Webservice_Widget;
	}

	function check_migrate_version_16_to_20(){
		//update_option( 'jrossetto_cielo_16_to_20', '' );
		$migrou = get_option( 'jrossetto_cielo_16_to_20' );
		if(!$migrou){
			add_action( 'admin_notices', 'alert_migrate_version_16_to_20' );
		}

	}

	function alert_migrate_version_16_to_20() {
		$class = 'notice notice-error';
		$atualizar = '<a href="'.admin_url("admin-ajax.php").'?action=migrate_cielo_api_version_16_to_20" target="_blank">Clique aqui para atualizar.</a>';
		$message = __( '[Cielo API 3.0] ATENÇÃO: Uma atualização do banco é necessária para o funcionamento do plugin. ', 'jrossetto_woo_cielo_webservice' );

		printf( '<div class="%1$s"><p>%2$s %3$s</p></div>', esc_attr( $class ), esc_html( $message ), $atualizar );
	}

	add_action( 'wp_ajax_migrate_cielo_api_version_16_to_20', 'migrate_cielo_api_version_16_to_20');
	add_action( 'wp_ajax_nopriv_migrate_cielo_api_version_16_to_20','migrate_cielo_api_version_16_to_20');
	function migrate_cielo_api_version_16_to_20(){
		global $wpdb;

		if(get_option( 'jrossetto_cielo_16_to_20' )){
			echo get_option( 'jrossetto_cielo_16_to_20' );
			die();
		}
		$pedidosAntigosCartao = $wpdb->get_results("SELECT * FROM `wp_cielo_webservice`;");
		$log = 'Inicio da migração Cartão:' . date('d/m/Y H:m:s');

		if ( $pedidosAntigosCartao ):
			foreach ($pedidosAntigosCartao as $PA) {
				//$wpdb->query("INSERT INTO `wp_cielo_api_jrossetto` (`idUnica`, `tid`, `pedido`, `bandeira`, `parcela`, `lr`, `total`, `status`, `bin`) VALUES (NULL, '".$objResposta['tid']."', '".$order->get_id()."', '".$regras_cc["cc"]."', '".$parcela."', '', '".$total."', '".$objResposta['status']."', '".$bin."');");
				//$wpdb->query("INSERT INTO `wp_cielo_api_jrossetto` (`id`, `metodo`, `id_pagamento`, `tid`, `pedido`, `bandeira`, `parcela`, `lr`, `lr_log`, `total`, `status`, `bin`, `link`) VALUES (NULL, 'credito', '".$objResposta['id_pagamento']."', '".$objResposta['tid']."', '".$order->get_id()."', '".$regras_cc["cc"]."', '".$parcela."', '".$objResposta['lr']."', '".$objResposta['lr_log']."', '".$total."', '".$objResposta['status']."', '".$bin."', '');");
				$order = wc_get_order((int)($PA->pedido));
				if($order):
					$id_pagamento = get_post_meta( $order->id, '_transacao', true );
					$metodo = get_post_meta( $order->id, '_transacao_tipo', true );
					if($metodo == 'Crédito'){
						$metodo = 'credito';
					}else{
						$metodo = 'debito';
					}

					$wpdb->insert('wp_cielo_api_jrossetto' , array(
						'metodo'=>  $metodo,
						'id_pagamento'=>  $id_pagamento,
						'tid'=>  $PA->tid,
						'pedido' => $PA->pedido,
						'bandeira'=>  $PA->bandeira,
						'parcela'=>  $PA->parcela,
						'lr'=>  $PA->lr,
						'lr_log'=>  $PA->lr,
						'total'=>  $PA->total,
						'status'=>  $PA->status,
						'bin'=>  $PA->bin,
						'link'=> 'IMPORTADO VERSAO 1.6'
					));

					$log .= '<br /> #' . $PA->pedido;
				endif;
			}
		endif;
		$log .= '<br />Fim da migração Cartão:' . date('d/m/Y H:m:s');

		$log .= '<br /><br />Inicio da migração Boleto:' . date('d/m/Y h:m:s');
		$pedidosAntigosBoleto = $wpdb->get_results("SELECT * FROM `wp_cielo_webservice_boleto`;");

		if ( $pedidosAntigosBoleto ):
			foreach ($pedidosAntigosBoleto as $PA) {
				$order = wc_get_order((int)($PA->pedido));
				if($order):
					$linkBoleto = get_post_meta( $order->id, '_transacao_boletoURL', true );
					// $metodo = get_post_meta( $order->id, '_transacao_tipo', true );

					$wpdb->insert('wp_cielo_api_jrossetto' , array(
						'metodo'=>  'boleto',
						'id_pagamento'=>  $PA->payment_id,
						'tid'=>  '',
						'pedido' => $PA->pedido,
						'bandeira'=>  $PA->bandeira,
						'parcela'=>  $PA->parcela,
						'lr'=>  '',
						'lr_log'=>  '',
						'total'=>  $PA->total,
						'status'=>  $PA->status,
						'bin'=>  'IMPORTADO VERSAO 1.6',
						'link'=> $linkBoleto
					));
					$log .= '<br /> #' . $PA->pedido;
				endif;
			}
		endif;

		update_option( 'jrossetto_cielo_16_to_20', 'Migração realizada em:' . date('d/m/Y H:m:s'));
		$log .= '<br />Fim da migração Boleto:' . date('d/m/Y H:m:s');
		echo $log;
		die();
	}

	//funcao de inicializacao
	function jrossetto_woo_cielo_webservice_init() {

		check_migrate_version_16_to_20();


		require_once(CIELO_WEBSERVICE_WOO_PATH.'/wcwsl.php' );
		$verifica = new WCWSL;

		//versao
		if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '<' ) ) {
			add_action( 'admin_notices', 'woo_cielo_webservice_alerta_versao' );
		}

		//chama as classes do modulo
		if ( !class_exists( 'WC_Payment_Gateway' ) ) return;

		if ( !class_exists( 'WC_Gateway_Jrossetto_Woo_Cielo_Webservice' ) ){


			include_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.cielo_boleto.php' );
			include_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.cielo_credito.php' );
			include_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.cielo_debito.php' );
			include_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.cielo_tef.php' );

		}

		//class validar cpf/cnpj
		if ( !class_exists( 'ValidaCPFCNPJ' ) ){
			include_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.fiscal.php' );
		}

		//cria um metabox em detalhes do pedido
		if ( !class_exists( 'WC_Cielo_Webservice_Jrossetto_Metabox' ) ){
			require_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.metabox.cielo.php');
			new WC_Cielo_Webservice_Jrossetto_Metabox;
		}

		//permissao de escrita
		if(!is_writable(CIELO_WEBSERVICE_WOO_PATH)){
			add_action( 'admin_notices', 'woo_cielo_webservice_alerta_escrita' );
		}

		//admin
		if ( !class_exists( 'WC_Cielo_Webservice_Jrossetto_Gerenciador' ) ){
			//especifico por versao

			include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/gerenciador/gerenciador.php' );

			new WC_Cielo_Webservice_Jrossetto_Gerenciador;
		}

		//adiciona o plugin ao woocommerce
		function woocommerce_add_jrossetto_woo_cielo_webservice($methods) {
			$methods[] = 'WC_Gateway_Jrossetto_Woo_Cielo_Webservice';
			$methods[] = 'WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Debito';
			$methods[] = 'WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Boleto';
			$methods[] = 'WC_Gateway_Jrossetto_Woo_Cielo_Webservice_TEF';
			return $methods;
		}
		add_filter('woocommerce_payment_gateways', 'woocommerce_add_jrossetto_woo_cielo_webservice' );
	}

	//alerta versao
	function woo_cielo_webservice_alerta_versao(){
		echo '<div class="error">';
		echo '<p><strong>Cielo API:</strong> Requer vers&atilde;o Woo 3.x ou superior, atualize seu WooCommerce para vers&atilde;o compativel!</p>';
		echo '</div>';
	}

	//alerta permissao de escrita
	function woo_cielo_webservice_alerta_escrita(){
		echo '<div class="error">';
		echo '<p><strong>Cielo API:</strong> Aplique permiss&atilde;o de escrita ao diretorio <u>'.CIELO_WEBSERVICE_WOO_PATH.'</u> para que o m&oacute;dulo possa ser ativado corretamente!</p>';
		echo '</div>';
	}

	//inicializa o modulo no wordpress
	add_action('plugins_loaded', 'jrossetto_woo_cielo_webservice_init', 0);

	//carregamento de parcelas
	add_action( 'wp_ajax_parcelas_cielo_webservice', 'parcelas_cielo_webservice');
	add_action( 'wp_ajax_nopriv_parcelas_cielo_webservice','parcelas_cielo_webservice');
	function parcelas_cielo_webservice(){
		//carrega as config do modulo
		$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
		//envia o juros incluso
		$enviar_juros_embutido = true;
		//valida o valor esta correto
		if(isset($_POST['id']) && isset($_POST['total']) && sha1(md5($_POST['total']))==$_POST['hash']){
			//pega os dados e vars
			$minimo = (float)$config->minimo;
			$desconto = 0;
			$divmax = $config->div;
			$divsem = $config->sem;
			$juros  = $config->juros;
			$total  = $_POST['total'];

			//corrije bug erro etapa2
			$total = $total_real = number_format($total, 2, '.', '');

			//calcula os minimos
			$split = (int)$total/$minimo;
			if($split>=$divmax){
				$div = (int)$divmax;
			}elseif($split<$divmax){
				$div = (int)$split;
			}elseif($total<=$minimo){
				$div = 1;
			}

			//inicio
			$linhas[''] = "-- Selecione --";

			//seleta o tipo de parcelamento
			if($config->parcelamento=='operadora'){
				$pcom = 3;
			}else{
				$pcom = 2;
			}

			//avista
			if($desconto>0){
				$desconto_valor = ($total/100)*$desconto;
				$linhas[base64_encode('1|1|'.number_format(($total-$desconto_valor), 2, '.', '').'|'.base64_encode($_POST['id']).'|'.base64_encode($total).'|'.md5(($total-$desconto_valor)))] = "&Agrave; vista por ".wc_price(number_format(($total-$desconto_valor), 2, '.', ''))." (j&aacute; com ".$desconto."% off)";
			}else{
				$linhas[base64_encode('1|1|'.number_format(($total), 2, '.', '').'|'.base64_encode($_POST['id']).'|'.base64_encode($total).'|'.md5($total))] = "&Agrave; vista por ".wc_price(number_format(($total), 2, '.', ''))."";
			}

			//se tiver parcelado
			if($_POST['id']!='visaelectron' && $_POST['id']!='maestro' && $_POST['id']!='discover' && $_POST['id']!='jcb'){
				if($div>=2){
					for($i=1;$i<=$div;$i++){
						if($i>1){
							if($i<=$divsem){
								$total = number_format($total, 2, '.', '');
								$linhas[base64_encode(''.$i.'|2|'.number_format(($total), 2, '.', '').'|'.base64_encode($_POST['id']).'|'.base64_encode($total).'|'.md5($total))] = $i."x de ".wc_price(number_format(($total/$i), 2, '.', ''))." sem juros";
							}else{
								$parcela_com_juros = $config->calcular_juros_cielo_webservice($total_real, $juros, $i);
								//juros imbutido
								if($enviar_juros_embutido){
									$total = number_format(($parcela_com_juros*$i), 2, '.', '');
								}
								///
								$linhas[base64_encode(''.$i.'|'.$pcom.'|'.number_format(($total), 2, '.', '').'|'.base64_encode($_POST['id']).'|'.base64_encode($total).'|'.md5($total))] = $i."x de ".wc_price(number_format(($parcela_com_juros), 2, '.', ''))." com juros";
							}
						}
					}
				}
			}
			//converte json
			echo json_encode($linhas);
		}else{
			$linhas[''] = 'Ops, total invalido!';
			echo json_encode($linhas);
		}
		die();
	}

	//aplica os javascript ao template
	add_action( 'wp_enqueue_scripts', 'cielo_webservice_wp_enqueue_scripts_for_frontend', 99 );
	function cielo_webservice_wp_enqueue_scripts_for_frontend(){
		if( is_checkout() ){
			//wp_register_script('card-cielo-webservice',plugins_url()."/jrossetto-woo-cielo-webservice/js/jquery.payment.js",false,false,true);
			wp_register_script('card2-cielo-webservice',plugins_url()."/jrossetto-woo-cielo-webservice/js/card.js",false,false,true);
			wp_register_script('init-cielo-webservice',plugins_url()."/jrossetto-woo-cielo-webservice/js/init.js",false,false,true);
			wp_enqueue_script('init-cielo-webservice');
			//wp_enqueue_script('card-cielo-webservice');
			wp_enqueue_script('card2-cielo-webservice');
		}
		if( is_wc_endpoint_url( 'order-pay' ) ){
			//wp_register_script('card-cielo-webservice',plugins_url()."/jrossetto-woo-cielo-webservice/js/jquery.payment.js",false,false,true);
			wp_register_script('card2-cielo-webservice',plugins_url()."/jrossetto-woo-cielo-webservice/js/card.js",false,false,true);
			wp_register_script('init-op-cielo-webservice',plugins_url()."/jrossetto-woo-cielo-webservice/js/init-order-pay.js",false,false,true);
			wp_enqueue_script('init-op-cielo-webservice');
			//wp_enqueue_script('card-cielo-webservice');
			wp_enqueue_script('card2-cielo-webservice');
		}
	}

	/* ENVIAR LINK BOLETO POR EMAIL  */
	add_action( 'woocommerce_email_after_order_table', 'jrossetto_email_billet_link' );
	function jrossetto_email_billet_link( $order ) {

		$boleto = get_post_meta( $order->id, '_transacao_boletoURL', true );
		if ( ! empty( $boleto ) ) {
			echo "
			<div>

			<center>
			<a class='button' href='" . $boleto . "' target='_blank'>
			<img src='" .plugins_url() . "/jrossetto-woo-cielo-webservice/images/imprimir-boleto.png' alt='Clique aqui para imprimir o boleto' />
			</a>
			</center>
			<br />
			</div>";
		}
	}

	/* ENVIAR DADOS DE PAGAMENTO POR EMAIL  */
	add_filter('woocommerce_email_order_meta_fields', 'jrossetto_add_email_order_meta_fields', 10, 3 );
	function jrossetto_add_email_order_meta_fields( $fields, $sent_to_admin, $order_obj ) {

		$tipo_transacao = get_post_meta(  $order_obj->get_id(), '_transacao_tipo', true );
		$bandeira = get_post_meta(  $order_obj->get_id(), '_transacao_bandeira', true );
		$parcelas = get_post_meta(  $order_obj->get_id(), '_transacao_parcelas', true );

		/* ADD CUSTOM FILEDS */
		//
		// if((int)$parcelas){
		// 	$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
		// 	$total_order = number_format($order_obj->get_total(), 2, '.', '');
		// 	for($i=1; $i <= $parcelas; $i++){
		// 			$now = date('Y-m-d');
		// 			$effectiveDate = date('Y-m-d', strtotime("+".$i." months", strtotime($now)));
		// 			$month = date('m', strtotime($effectiveDate));
		// 		  $year = date('y', strtotime($effectiveDate));
		// 		  $custom_meta_1 =  "M".$month. '_' . $year. '_TX';
		// 			$custom_meta_2 =  "M".$month. '_' . $year. '_VL';
		// 			if(!add_post_meta($order_obj->get_id(),$custom_meta_1, $config->juros ,true)) {
		// 				update_post_meta($order_obj->get_id(),$custom_meta_1 , $config->juros);
		// 			}
		// 			if(!add_post_meta($order_obj->get_id(),$custom_meta_2, number_format($total_order/$i, 2, '.', '') ,true)) {
		// 				update_post_meta($order_obj->get_id(),$custom_meta_2 , number_format($total_order/$i, 2, '.', ''));
		// 			}
		//
		// 	}
		// }
		/* END ADD CUSTOM FIELDS */

		if($tipo_transacao == "Débito"){
			$pagamento = 'Pagamento à vista - ' . $bandeira;
		}
		if($tipo_transacao == "Crédito"){
			$pagamento = 'Pagamento parcelado em ' .$parcelas . 'x no cartão - '. $bandeira;
		}

		if(isset($pagamento)){
			$fields['tipo_transacao'] = array(
				'label' => 'Tipo',
				'value' => $tipo_transacao
			);

			$fields['tipo_pagamento'] = array(
				'label' => 'Pagamento',
				'value' => $pagamento
			);
			return $fields;
		}
		return true;
	}



	//cron de pagamento boleto e tef
	add_action( 'wp_ajax_cron_jrossetto_boleto_tef_cielo_webservice', 'cron_jrossetto_boleto_tef_cielo_webservice');
	add_action( 'wp_ajax_nopriv_cron_jrossetto_boleto_tef_cielo_webservice','cron_jrossetto_boleto_tef_cielo_webservice');

	// Schedule Cron Job Event
	//add_action( 'wp', 'cron_jrossetto_boleto_cielo_webservice' );
	// if (!wp_next_scheduled('cron_jrossetto_boleto_cielo_webservice')) {
	//     $time = date("Y-m-d 08:00",strtotime('today')); //returns today midnight
	//     $time = strtotime($time); //$time + 72000; //add an offset for the time of day you want, keeping in mind this is in GMT.
	//     wp_schedule_event($time, 'daily', 'cron_jrossetto_boleto_cielo_webservice');
	// }


	function cron_jrossetto_boleto_tef_cielo_webservice(){

		global $wpdb;

		//faz o include das config cielo
		include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );

		//consulta
		$pedidos = $wpdb->get_results("SELECT * FROM `wp_cielo_api_jrossetto` WHERE (metodo='boleto' OR metodo='tef') AND status='0' ORDER BY id DESC LIMIT 50;", 'ARRAY_A');

		foreach($pedidos as $registro){
			//pedido no woo
			$order = new WC_Order((int)($registro['pedido']));
			$status_atual = str_replace('wc-','',$order->get_status());

			//config de acordo o tipo de pagamento
			if($order->get_payment_method()=='jrossetto_woo_cielo_webservice'){
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
			}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_debito'){
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Debito();
			}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_tef'){
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_TEF();
			}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_boleto'){
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Boleto();
			}

			//somente pedidos aguardando pagamento
			if($status_atual==str_replace('wc-','',$config->aguardando)){

				//cielo api
				if($config->testmode=='yes'){
					$provider = 'Simulado';
					$urlweb = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/";
				}else{
					$provider = 'Cielo';
					$urlweb = "https://apiquery.cieloecommerce.cielo.com.br/1/";
				}
				$objResposta = array();
				$headers = array(
					"Content-Type" => "application/json",
					"Accept" => "application/json",
					"MerchantId" =>trim($config->afiliacao),
					"MerchantKey" => trim($config->chave),
					"RequestId" => "",
				);

				if($config->testmode=='yes'){
					$headers = array(
						"Content-Type" => "application/json",
						"Accept" => "application/json",
						"MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
						"MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
						"RequestId" => "",
					);

				}


				$api = new RestClient(array(
					'base_url' => $urlweb,
					'headers' => $headers,
				));

				$response = $api->get("sales/".$registro['id_pagamento']."");
				$dados_pedido = @json_decode($response->response,true);
				if(($response->status==200 || $response->status==201) && isset($dados_pedido['Payment']['Status'])){

					if($dados_pedido['Payment']['Status']==2){
						//atualiza
						$order->update_status($config->pago);
						$order->add_order_note("Pagamento processado em ". date('d/m/Y H:i'));
						echo 'Pedido #' .$registro['pedido'].' pago!<br>';
						//atualiza o pedido no banco de dados
						$wpdb->query("UPDATE `wp_cielo_api_jrossetto` SET `status` =  '2' WHERE `pedido` = '".(int)($registro['pedido'])."';");
					}elseif($dados_pedido['Payment']['Status']==3 || $dados_pedido['Payment']['Status']==10 || $dados_pedido['Payment']['Status']==13){
						//atualiza
						$order->update_status($config->cancelado);
						echo $registro['pedido'].' cancelado!<br>';
						//atualiza o pedido no banco de dados
						$wpdb->query("UPDATE `wp_cielo_api_jrossetto` SET `status` =  '10' WHERE `pedido` = '".(int)($registro['pedido'])."';");
					}else{
						//exibe
						echo $registro['pedido'].' aguardando!<br>';
					}
				}else{
					echo $registro['pedido'].' erro!<br>';
				}

			}
		}
		echo '<br>CRON FINALIZADO!';
		exit;
	}

	//retorno de dados postback cielo
	add_action( 'wp_ajax_retorno_ipn_cielo_webservice', 'retorno_ipn_cielo_webservice');
	add_action( 'wp_ajax_nopriv_retorno_ipn_cielo_webservice','retorno_ipn_cielo_webservice');
	function retorno_ipn_cielo_webservice(){
		global $wpdb;
		if(isset($_REQUEST['PaymentId']) && isset($_REQUEST['ChangeType'])){
			$id_pagamento = trim($_REQUEST['PaymentId']);
			$tipo_req = (int)$_REQUEST['ChangeType'];
			$id_meio_woo = 'jrossetto_woo_cielo_webservice';
			if($tipo_req==1){
				//faz o include das config cielo
				include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );

				//config
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();

				//cielo api
				if($config->testmode=='yes'){
					$provider = 'Simulado';
					$urlweb = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/";
				}else{
					$provider = 'Cielo';
					$urlweb = "https://apiquery.cieloecommerce.cielo.com.br/1/";
				}
				$objResposta = array();
				$headers = array(
					"Content-Type" => "application/json",
					"Accept" => "application/json",
					"MerchantId" =>trim($config->afiliacao),
					"MerchantKey" => trim($config->chave),
					"RequestId" => "",
				);

				if($config->testmode=='yes'){
					$headers = array(
						"Content-Type" => "application/json",
						"Accept" => "application/json",
						"MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
						"MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
						"RequestId" => "",
					);

				}
				$api = new RestClient(array(
					'base_url' => $urlweb,
					'headers' => $headers,
				));
				$response = $api->get("sales/".$id_pagamento."");
				$dados_pedido = @json_decode($response->response,true);

				//debug
				if ( 'yes' === $config->debug ) {
					$logs = new WC_Logger();
					$logs->add( $id_meio_woo, 'Log Postback Cielo '.strtoupper($tipo).': '.$response->response );
				}

				//se ocorreu erro salva os logs
				if($response->status < 200 || $response->status > 201){
					$logs = new WC_Logger();
					$logs->add( $id_meio_woo, 'Erro Cielo Postback em '.date('d/m/Y H:i:s').'');
					$logs->add( $id_meio_woo, 'Log: '.$response->response );
				}

				//resultado
				if(($response->status==200 || $response->status==201) && isset($dados_pedido['Payment']['PaymentId'])){
					//infors
					$pedido_id = $dados_pedido['MerchantOrderId'];
					$status_id = $dados_pedido['Payment']['Status'];
					$lr = isset($dados_pedido['Payment']['ReturnCode'])?$dados_pedido['Payment']['ReturnCode']:'';
					$lr_log = isset($dados_pedido['Payment']['ReturnMessage'])?$dados_pedido['Payment']['ReturnMessage']:'';

					//pega o pedido
					$order = new WC_Order((int)($pedido_id));
					if(!$order->get_id()){
						die('Pedido nao encontrado!');
					}
					$status_atual = str_replace('wc-','',$order->get_status());

					//status titulo
					switch($status_id){
						case '2':
						$status_mudar = $config->pago;
						$status = 'Aprovada';
						break;
						case '1':
						$status_mudar = $config->autorizado;
						$status = 'Autorizada';
						break;
						case '3':
						$status_mudar = $config->negado;
						$status = 'Negada';
						break;
						case '10':
						case '13':
						$status_mudar = $config->cancelado;
						$status = 'Cancelada';
						break;
					}

					//cria uma nota no pedido
					if(isset($status_mudar)){
						if($order->get_payment_method()=='jrossetto_woo_cielo_webservice_debito'){
							$order->add_order_note("Transa&ccedil;&atilde;o D&eacute;bito Cielo - TID ".$dados_pedido['Payment']['Tid']." - ".$status." (POST)");
						}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_tef'){
							$order->add_order_note("Transa&ccedil;&atilde;o TEF Cielo - ID ".$dados_pedido['Payment']['PaymentId']." - ".$status." (POST)");
						}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_boleto'){
							$order->add_order_note("Transa&ccedil;&atilde;o Boleto Cielo - ID ".$dados_pedido['Payment']['PaymentId']." - ".$status." (POST)");
						}else{
							$order->add_order_note("Transa&ccedil;&atilde;o Cr&eacute;dito Cielo - TID ".$dados_pedido['Payment']['Tid']." - ".$status." (POST)");
						}
					}

					//status
					if(isset($status_mudar) && str_replace('wc-','',$status_mudar)!=$status_atual){
						$order->update_status($status_mudar);
					}

					//atualiza o pedido no banco de dados
					$wpdb->query("UPDATE `wp_cielo_api_jrossetto` SET  `lr` =  '".$lr."',  `lr_log` =  '".$lr_log."', `status` =  '".$status_id."' WHERE `pedido` = '".(int)($pedido_id)."';");

				}

			}
		}
		echo 'IPN - OK';
		exit;
	}

	//retorno de dados debito/tef
	add_action( 'wp_ajax_retorno_debito_cielo_webservice', 'retorno_debito_cielo_webservice');
	add_action( 'wp_ajax_nopriv_retorno_debito_cielo_webservice','retorno_debito_cielo_webservice');
	function retorno_debito_cielo_webservice(){
		global $wpdb;
		if(isset($_REQUEST['PaymentId']) && !empty($_REQUEST['PaymentId'])){
			//faz o include das config cielo
			include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );

			$tipo = isset($_REQUEST['tipo'])?$_REQUEST['tipo']:'debito';

			//config
			if($tipo=='debito'){
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Debito();
				$id_meio_woo = 'jrossetto_woo_cielo_webservice_debito';
			}elseif($tipo=='tef'){
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_TEF();
				$id_meio_woo = 'jrossetto_woo_cielo_webservice_tef';
			}elseif($tipo=='boleto'){
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Boleto();
				$id_meio_woo = 'jrossetto_woo_cielo_webservice_boleto';
			}else{
				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
				$id_meio_woo = 'jrossetto_woo_cielo_webservice';
			}

			//cielo api
			if($config->testmode=='yes'){
				$provider = 'Simulado';
				$urlweb = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/";
			}else{
				$provider = 'Cielo';
				$urlweb = "https://apiquery.cieloecommerce.cielo.com.br/1/";
			}
			$objResposta = array();
			$headers = array(
				"Content-Type" => "application/json",
				"Accept" => "application/json",
				"MerchantId" =>trim($config->afiliacao),
				"MerchantKey" => trim($config->chave),
				"RequestId" => "",
			);

			if($config->testmode=='yes'){
				$headers = array(
					"Content-Type" => "application/json",
					"Accept" => "application/json",
					"MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
					"MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
					"RequestId" => "",
				);

			}
			$api = new RestClient(array(
				'base_url' => $urlweb,
				'headers' => $headers,
			));
			$response = $api->get("sales/".trim($_REQUEST['PaymentId'])."");
			$dados_pedido = @json_decode($response->response,true);

			//debug
			if ( 'yes' === $config->debug ) {
				$logs = new WC_Logger();
				$logs->add( $id_meio_woo, 'Log Retorno Cielo '.strtoupper($tipo).': '.$response->response );
			}

			//se ocorreu erro salva os logs
			if($response->status < 200 || $response->status > 201){
				$logs = new WC_Logger();
				$logs->add( $id_meio_woo, 'Erro Cielo Retorno '.strtoupper($tipo).' em '.date('d/m/Y H:i:s').'');
				$logs->add( $id_meio_woo, 'Log: '.$response->response );
			}

			//resultado
			if(($response->status==200 || $response->status==201) && isset($dados_pedido['Payment']['PaymentId'])){
				//infors
				$pedido_id = $dados_pedido['MerchantOrderId'];
				$status_id = $dados_pedido['Payment']['Status'];
				$lr = isset($dados_pedido['Payment']['ReturnCode'])?$dados_pedido['Payment']['ReturnCode']:'';
				$lr_log = isset($dados_pedido['Payment']['ReturnMessage'])?$dados_pedido['Payment']['ReturnMessage']:'';

				//pega o pedido
				$order = new WC_Order((int)($pedido_id));

				//status titulo
				switch($status_id){
					case '2':
					$status = 'Aprovada';
					break;
					case '1':
					$status = 'Autorizada';
					break;
					case '3':
					$status = 'Negada';
					break;
					case '10':
					case '13':
					$status = 'Cancelada';
					break;
					default:
					$status = 'Aguardando Pagamento';
				}

				//cria uma nota no pedido
				if($tipo=='debito'){
					$order->add_order_note("Transa&ccedil;&atilde;o D&eacute;bito Cielo - TID ".$dados_pedido['Payment']['Tid']." - ".$status."");
				}elseif($tipo=='tef'){
					$order->add_order_note("Transa&ccedil;&atilde;o TEF Cielo - ID ".$dados_pedido['Payment']['PaymentId']." - ".$status."");
				}elseif($tipo=='boleto'){
					$order->add_order_note("Transa&ccedil;&atilde;o Boleto Cielo - ID ".$dados_pedido['Payment']['PaymentId']." - ".$status."");
				}else{
					$order->add_order_note("Transa&ccedil;&atilde;o Cr&eacute;dito Cielo - TID ".$dados_pedido['Payment']['Tid']." - ".$status."");
				}

				//status
				switch($status_id){
					case '2':
					$order->update_status($config->pago);
					break;
					case '1':
					$order->update_status($config->autorizado);
					break;
					case '3':
					$order->update_status($config->negado);
					break;
					case '10':
					case '13':
					$order->update_status($config->cancelado);
					break;
				}

				//atualiza o pedido no banco de dados
				$wpdb->query("UPDATE `wp_cielo_api_jrossetto` SET  `lr` =  '".$lr."',  `lr_log` =  '".$lr_log."', `status` =  '".$status_id."' WHERE `pedido` = '".(int)($pedido_id)."';");

				//redireciona ao cupom
				$link = add_query_arg(array('pedido'=>$pedido_id,'hash'=>md5($pedido_id.$pedido_id)),$config->get_return_url( $order ));
				wp_redirect($link);
				exit;

			}else{
				//senao fim
				$link = get_option('woocommerce_myaccount_page_id');
				$link = get_permalink($link);
				wp_redirect($link);
				exit;
			}
		}else{
			//senao fim
			$link = get_option('woocommerce_myaccount_page_id');
			$link = get_permalink($link);
			wp_redirect($link);
			exit;
		}
	}

	function retornoCielo(WP_REST_Request $request){
		print_r($request->get_body());
		 // $parameters = $request->get_json_params();
		 // print_r($parameters);
		exit;
		//return json_decode($request->get_body());
	}

	add_action('rest_api_init', function () {
	  register_rest_route( 'retornocielo', 'credito',array(
	                'methods'  => 'POST',
	                'callback' => 'retorno_credito_cielo_webservice'
	      ));
	});

	//retorno de dados credito
	add_action( 'wp_ajax_retorno_credito_cielo_webservice', 'retorno_credito_cielo_webservice');
	add_action( 'wp_ajax_nopriv_retorno_credito_cielo_webservice','retorno_credito_cielo_webservice');
	function retorno_credito_cielo_webservice(WP_REST_Request $request){
		global $wpdb;
		$body = $request->get_body();
		$PaymentId = str_replace('PaymentId=', '',$body);

		if(isset($PaymentId) && $PaymentId != NULL){
			//faz o include das config cielo
			include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );

					//config

				$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
				$id_meio_woo = 'jrossetto_woo_cielo_webservice';


			//cielo api
			if($config->testmode=='yes'){
				$provider = 'Simulado';
				$urlweb = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/";
			}else{
				$provider = 'Cielo';
				$urlweb = "https://apiquery.cieloecommerce.cielo.com.br/1/";
			}
			$objResposta = array();
			$headers = array(
				"Content-Type" => "application/json",
				"Accept" => "application/json",
				"MerchantId" =>trim($config->afiliacao),
				"MerchantKey" => trim($config->chave),
				"RequestId" => "",
			);

			if($config->testmode=='yes'){
				$headers = array(
					"Content-Type" => "application/json",
					"Accept" => "application/json",
					"MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
					"MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
					"RequestId" => "",
				);

			}
			$api = new RestClient(array(
				'base_url' => $urlweb,
				'headers' => $headers,
			));
			$response = $api->get("sales/".trim($PaymentId)."");
			$dados_pedido = @json_decode($response->response,true);

			//debug
			if ( 'yes' === $config->debug ) {
				$logs = new WC_Logger();
				$logs->add( $id_meio_woo, 'Log Retorno Cielo '.strtoupper($tipo).': '.$response->response );
			}

			//se ocorreu erro salva os logs
			if($response->status < 200 || $response->status > 201){
				$logs = new WC_Logger();
				$logs->add( $id_meio_woo, 'Erro Cielo Retorno '.strtoupper($tipo).' em '.date('d/m/Y H:i:s').'');
				$logs->add( $id_meio_woo, 'Log: '.$response->response );
			}

			//resultado
			if(($response->status==200 || $response->status==201) && isset($dados_pedido['Payment']['PaymentId'])){
				//infors
				$pedido_id = $dados_pedido['MerchantOrderId'];
				$status_id = $dados_pedido['Payment']['Status'];
				$lr = isset($dados_pedido['Payment']['ReturnCode'])?$dados_pedido['Payment']['ReturnCode']:'';
				$lr_log = isset($dados_pedido['Payment']['ReturnMessage'])?$dados_pedido['Payment']['ReturnMessage']:'';

				//pega o pedido
				$order = new WC_Order((int)($pedido_id));

				//status titulo
				switch($status_id){
					case '2':
					$status = 'Aprovada';
					break;
					case '1':
					$status = 'Autorizada';
					break;
					case '3':
					$status = 'Negada';
					break;
					case '10':
					case '13':
					$status = 'Cancelada';
					break;
					default:
					$status = 'Aguardando Pagamento';
				}


					$order->add_order_note("Transa&ccedil;&atilde;o Cr&eacute;dito Cielo - TID ".$dados_pedido['Payment']['Tid']." - ".$status."");


				//status
				switch($status_id){
					case '2':
					$order->update_status($config->pago);
					break;
					case '1':
					$order->update_status($config->autorizado);
					break;
					case '3':
					$order->update_status($config->negado);
					break;
					case '10':
					case '13':
					$order->update_status($config->cancelado);
					break;
				}

				//atualiza o pedido no banco de dados
				$wpdb->query("UPDATE `wp_cielo_api_jrossetto` SET  `lr` =  '".$lr."',  `lr_log` =  '".$lr_log."', `status` =  '".$status_id."' WHERE `pedido` = '".(int)($pedido_id)."';");

				//redireciona ao cupom
				$link = add_query_arg(array('pedido'=>$pedido_id,'hash'=>md5($pedido_id.$pedido_id)),$config->get_return_url( $order ));
				wp_redirect($link);
				exit;

			}else{
				//senao fim
				$link = get_option('woocommerce_myaccount_page_id');
				$link = get_permalink($link);
				wp_redirect($link);
				exit;
			}
		}else{
			//senao fim
			$link = get_option('woocommerce_myaccount_page_id');
			$link = get_permalink($link);
			wp_redirect($link);
			exit;
		}
	}


	?>
