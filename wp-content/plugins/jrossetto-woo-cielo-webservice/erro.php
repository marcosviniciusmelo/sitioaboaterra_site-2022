<?php
include("../../../wp-load.php");
get_header(); 
?>

<div id="container">
<div id="content" role="main">
<div class="entry-content woocommerce">
<h1 class="entry-title">Erro ao consultar TID</h1>
<p>
Ocorreu um erro ao consultar o TID <?php echo $_GET['tid'];?> do seu pedido <?php echo (int)$_GET['pedido'];?>, <a href="ipn.php?pedido=<?php echo (int)$_GET['pedido'];?>">clique aqui</a> e tente novamente e caso ocorra o mesmo problema entre em contato com o atendimento da loja e informe o erro juntamente com o TID da mesma.
</p>
</div>
</div>
</div>

<?php
get_footer(); 
?>