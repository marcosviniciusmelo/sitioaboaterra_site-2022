<?php
if ( ! defined( 'ABSPATH' ) ) {
exit;
}
//se pf ou pj
$documento = '';
$customer_id = get_current_user_id();
if(get_user_meta( $customer_id, 'billing_cnpj', true )){
	$documento = get_user_meta( $customer_id, 'billing_cnpj', true );
}elseif(get_user_meta( $customer_id, 'billing_cpf', true )){
	$documento = get_user_meta( $customer_id, 'billing_cpf', true );
}
$documento = preg_replace('/\D/', '',$documento);
?>
<script>
function aplicar_bandeira_cielo_webservice_tef(bandeira){
    jQuery(".meio_cielo_webservice_img_tef").css({ opacity: 0.2 });
    jQuery("."+ bandeira ).css({ opacity: 1 });
    jQuery('#bandeira-cielo-webservice-tef').val(bandeira);
}
</script>

<div id="tela-cielo-webservice-tef" style="width:100%;">

<p style="margin-bottom: 5px;">Selecione abaixo a banco qual deseja realizar o pagamento, ao finalizar ser&aacute; redirecionado ao ambiente do mesmo para autorizar e concluir o pagamento.</p>

<fieldset class="wc-credit-card-form wc-payment-form">

<p id="tela-bandeiras-cielo-tef" class="form-row form-row-wide woocommerce-validated">
<span style="float:left;">
<?php
foreach($this->meios AS $k=>$b){
?>
<img style="cursor:pointer;float:left;min-height:40px;" class='meio_cielo_webservice_img_tef <?php echo $b;?>' onclick="aplicar_bandeira_cielo_webservice_tef('<?php echo $b;?>')" src='<?php echo plugins_url().'/jrossetto-woo-cielo-webservice/images/'.$b.'.png';?>' width="60">
<?php
}
?>
</span>
</p>

<input type="hidden" name="cielo_webservice_tef[bandeira]" id="bandeira-cielo-webservice-tef" value="">

</fieldset>

</div>
