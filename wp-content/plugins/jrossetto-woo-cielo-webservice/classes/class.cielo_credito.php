<?php
class WC_Gateway_Jrossetto_Woo_Cielo_Webservice extends WC_Payment_Gateway {

	protected $jr_product_id = 'WC Cielo API 3.0';
	protected $jr_software_version = '1.4';



	public function __construct() {


		global $woocommerce;
		$this->id           = 'jrossetto_woo_cielo_webservice';
		$this->icon         = apply_filters( 'woocommerce_jrossetto_woo_cielo_webservice', home_url( '/' ).'wp-content/plugins/jrossetto-woo-cielo-webservice/images/cielo.png' );
		$this->has_fields   = false;
		$this->description = true;
		$this->method_description = __( 'Ativa o pagamento por Crédito via Cielo.', 'jrossetto-woo-cielo-webservice-credito' );
		$this->method_title = 'Cielo API 3.0 - Cr&eacute;dito';
		$this->init_settings();
		$this->init_form_fields();
		$this->instalar_mysql_cielo_webservice();
		$this->instalar_mysql_cielo_webservice_check();
		$this->supports           = array(
			'products'
		);

		$this->debug = $this->settings['debug'];
		//$this->checkTLSv1_2();




		// Active logs.
		if ( 'yes' == $this->debug ) {
			if ( class_exists( 'WC_Logger' ) ) {
				$this->log = new WC_Logger();
			} else {
				$this->log = $this->woocommerce_method()->logger();
			}
		}




		foreach ( $this->settings as $key => $val ) $this->$key = $val;

		if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ){
			add_action('woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
		}else{
			add_action('woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
		}



		if($this->softdescriptor != '' && preg_match('/[^\w!@£]/', $this->softdescriptor) == 1){

			add_action( 'admin_notices', array( $this, 'check_softdescriptor_error' ) );

		}

		if($this->testmode=='yes'){
			add_action( 'admin_notices', array( $this, 'woocommerce_cielo_modo_teste' ) );
		}

		if($this->antifraude=='sim'){
			add_action( 'admin_notices', array( $this, 'check_alert_antifraude' ) );
		}

		if($this->captura =='posterior'){
			add_action( 'admin_notices', array( $this, 'check_alert_captura' ) );
		}



		add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );

		if ( !$this->is_valid_for_use() ) $this->enabled = false;



	}

	public function check_softdescriptor_error() {
		$class = 'notice notice-error';
		$message = __( '[Cielo API 3.0 - Crédito] ERRO: Verifique se o campo descrição da fatura contém no máximo 13 caracteres. Não é permitido utilizar espaços, traços, ascentos.', 'jrossetto_woo_cielo_webservice' );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	}

	public function check_alert_antifraude() {
		$class = 'notice notice-info is-dismissible';
		$message = __( '[Cielo API 3.0 - Crédito] ATENÇÃO: Ao habilitar o Anti-Fraude, tenha certeza que o mesmo encontra-se ativo junto a cielo.', 'jrossetto_woo_cielo_webservice' );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	}

	public function check_alert_captura() {
		$class = 'notice notice-info is-dismissible';
		$gerenciador = '<a href="admin.php?page=jrossetto-woo-cielo-webservice-pedidos">Gerenciador de Pagamento Cielo API</a>';
		$message = __( '[Cielo API 3.0 - Crédito] ATENÇÃO: Você selecionou a opção de captura manual, lembre-se, é necessário fazer a captura da transação na tela do ', 'jrossetto_woo_cielo_webservice' );

		printf( '<div class="%1$s"><p>%2$s %3$s</p></div>', esc_attr( $class ), esc_html( $message ), $gerenciador );
	}

	public function woocommerce_cielo_modo_teste() {
		$class = 'notice notice-info is-dismissible';
		$message = __( '[Cielo API 3.0 - Crédito] ATENÇÃO: O modo TESTE está habilitado para a função crédito, qualquer transação realizada não terá cobranças.', 'jrossetto_woo_cielo_webservice' );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
		//echo '<div class="notice notice-info is-dismissible"><p>' . sprintf( __( 'Cielo Webservice está configurado em modo de teste, acessar %s.', '' ), '<a href="admin.php?page=wc-settings&tab=checkout&section=jrossetto_woo_cielo_webservice">configurações</a>' ) . '</p></div>';
	}


	// public function check_softdescriptor_error() {
	// 	echo '<div class="notice notice "><p>' . sprintf( __( 'Verifique se o campo descrição da fatura contém no máximo 13 caracteres. Não é permitido utilizar espaços, traços, ascentos.', '' ), '' ) . '</p></div>';
	// 	return false;
	//
	// }


	function check_softdescriptor_update() {
		echo '<div class="updated notice is-dismissable"><p>' . sprintf( __( 'Campo descrição da fatura atualizado', '' ), '' ) . '</p></div>';
	}


	public function thankyou_page( $order_id ) {
		global $wpdb;
		//pega o pedido
		$order = new WC_Order((int)($order_id));

		//dados cielo mysql
		$cielo = (array)$wpdb->get_row("SELECT * FROM `wp_cielo_api_jrossetto` WHERE `pedido` = '".(int)($order_id)."' ORDER BY id DESC;");

		//define o status do pedido
		$status_cielo = isset($cielo['status'])?$cielo['status']:'0';
		switch($status_cielo){
			case '2':
			$status = '<span style="color: #20bb20;">Aprovada</span>';
			break;
			case '1':
			$status = '<span style="color: #2196f3;">Autorizada</span>';
			break;
			case '3':
			$status = '<span style="color: red;">Negada</span>';
			break;
			case '10':
			case '13':
			$status = '<span style="color: red;">Cancelada</span>';
			break;
			default:
			$status = 'Aguardando Pagamento';
		}

		//layout
		include_once(dirname(__FILE__) . '/cupom_cartao.php');
	}




	public function calcular_juros_cielo_webservice($valorTotal, $taxa, $nParcelas){
		$taxa = $taxa/100;
		$cadaParcela = ($valorTotal*$taxa)/(1-(1/pow(1+$taxa, $nParcelas)));
		return round($cadaParcela, 2);
	}

	public function is_valid_for_use() {
		if ( ! in_array( get_woocommerce_currency(), apply_filters( 'woocommerce_jrossetto_woo_cielo_webservice_supported_currencies', array( 'BRL' ) ) ) ) return false;
		return true;
	}

	public function instalar_mysql_cielo_webservice(){
		global $wpdb;
		$wpdb->query("CREATE TABLE IF NOT EXISTS `wp_cielo_api_jrossetto` (
			`id` int(10) NOT NULL AUTO_INCREMENT,
			`metodo` varchar(40) NOT NULL,
			`id_pagamento` varchar(40) NOT NULL,
			`tid` varchar(40) NOT NULL,
			`pedido` varchar(40) NOT NULL,
			`bandeira` varchar(40) NOT NULL,
			`parcela` varchar(40) NOT NULL,
			`lr` varchar(20) NOT NULL,
			`lr_log` varchar(180) NOT NULL,
			`total` float(10,2) NOT NULL,
			`status` varchar(40) NOT NULL,
			`bin` varchar(40) NOT NULL,
			`link` varchar(255) NOT NULL,
      `created`     DATETIME DEFAULT CURRENT_TIMESTAMP,
      `updated` DATETIME ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

	}

	public function instalar_mysql_cielo_webservice_check(){
		global $wpdb;
		global $jal_db_version;

		$table_name = $wpdb->prefix . 'cielo_webservice_check';

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			order_id mediumint(9) DEFAULT 0 NOT NULL,
			time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
			address varchar(55) DEFAULT '' NOT NULL,
			PRIMARY KEY  (id)
		) $charset_collate;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
		add_option( 'jal_db_version', $jal_db_version );

	}

	public function GetIP(){
		foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key)
		{
			if (array_key_exists($key, $_SERVER) === true)
			{
				foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip)
				{
					if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false)
					{
						return $ip;
					}
				}
			}
		}
		return $_SERVER['REMOTE_ADDR'];
	}

	public function cielo_webservice_check($order_id){
		global $wpdb;
		$max = $this->settings['verificaip'] > 0 ? $this->settings['verificaip'] : 3;
		$table_name = $wpdb->prefix . 'cielo_webservice_check';
		$wpdb->insert($table_name , array('order_id' => $order_id, 'time' =>  current_time( 'mysql' ),  'address' => $this->GetIP()));
		$resultCheckIP = $wpdb->get_row("SELECT COUNT(*) as count FROM $table_name WHERE `address` LIKE '" . $this->GetIP() . "' AND `time` > ('" . current_time( 'mysql' ) . "' - interval 3 minute)");
		if ($resultCheckIP->count > $max) {
			return false;
		}
		return true;
	}

	public function get_status_pagamento(){
		if(function_exists('wc_get_order_statuses')){
			return wc_get_order_statuses();
		}else{
			$taxonomies = array(
				'shop_order_status',
			);
			$args = array(
				'orderby'       => 'name',
				'order'         => 'ASC',
				'hide_empty'    => false,
				'exclude'       => array(),
				'exclude_tree'  => array(),
				'include'       => array(),
				'number'        => '',
				'fields'        => 'all',
				'slug'          => '',
				'parent'         => '',
				'hierarchical'  => true,
				'child_of'      => 0,
				'get'           => '',
				'name__like'    => '',
				'pad_counts'    => false,
				'offset'        => '',
				'search'        => '',
				'cache_domain'  => 'core'
			);
			foreach(get_terms( $taxonomies, $args ) AS $status){
				$s[$status->slug] = __( $status->slug, 'woocommerce' );
			}
			return $s;
		}
	}

	public function admin_options() {
		?>
		<?php if ( $this->is_valid_for_use() ) : ?>
			<table class="form-table">
				<?php
				$this->generate_settings_html();
				?>
			</table>
		<?php else : ?>
			<div class="inline error"><p><strong><?php _e( 'Gateway Desativado', 'woocommerce' ); ?></strong>: <?php _e( 'Cielo Webservice n&atilde;o aceita o tipo e moeda de sua loja, apenas BRL.', 'woocommerce' ); ?></p></div>
			<?php
		endif;
	}

	public function gerar_parcelas(){
		$parcelas = array();
		for($i=1;$i<=12;$i++){
			$parcelas[$i] = $i."x";
		}
		return $parcelas;
	}



	public function init_form_fields() {

		/*
		Copyright (c) 2017 Jrossetto - www.jrossetto.com.br
		LEI N. 9.609 , DE 19 DE FEVEREIRO DE 1998 que Dispoe sobre a protecao da propriedade intelectual!
		Todos os direitos reservados.
		Suporte e informacoes acesse https:/www.jrossetto.com.br
		*/
		require_once(CIELO_WEBSERVICE_WOO_PATH.'/wcwsl.php' );
		$verifica = new WCWSL;
		$statusCheck = $verifica->status_check2();
		$status  = get_option( 'wcwsl_softlicense_key_status' );

		if( $status == 'valid' ){

			$this->form_fields= array(

				"imagem"=> array("title"=>"",
				"type"=>"hidden",
				"description"=>"<img src='".plugins_url()."/jrossetto-woo-cielo-webservice/images/banner.png'>",
				"default"=>""
			),
			"enabled"=> array(
				"title"=>"<b>Ativar/Desativar</b>",
				"type"=>"checkbox",
				"label"=>"Ativar",
				"default"=>"no"
			),

			"title"=> array(
				"title"=>"<b>Titulo</b>",
				"type"=>"text",
				"description"=>"Nome que este meio de pagamento sera mostrado na finaliza&ccedil;&atilde;o.",
				"default"=>"Cart&atilde;o de Cr&eacute;dito Cielo"
			),
			"testmode"=> array("title"=>"<b>Modo Teste</b>",
			"type"=>"checkbox",
			"label"=>"Sim, ativar (modo teste)",
			"default"=>"yes",
			"description"=>	"Essa  opção envia os pedidos para o ambiente teste da Cielo (pagamentos não são processados). Quando for homologar lembre-se de desabilitar essa opção.",
		),

		'afiliacao_details' => array(
			'title'       => "Credenciais Cielo",
			'type'        => 'title',
			'description' => "Insira as credenciais enviadas pela cielo",
		),
		"afiliacao"=>array(
			"title"=>"<b>MerchantId</b>",
			"type"=>"text",
			"description"=>"MerchantId para api 3.0 ( caso ainda não tenha entre em contato a cielo para solicitar)",
			"default"=>""
		),
		"chave"=>array(
			"title"=>"<b>MerchantKey</b>",
			"type"=>"text",
			"description"=>"MerchantKey para api 3.0 ( caso ainda não tenha entre em contato a cielo para solicitar)",
			"default"=>""
		),
		"softdescriptor"=>array(
			"title"=>"<b>Descrição na fatura</b>",
			"type"=>"text",
			"description"=>"Texto impresso na fatura bancaria comprador - Exclusivo para VISA/MASTER - <strong>não é permitido caracteres especiais como espaços, traços, acentos, pontos ..</strong> (máximo 13 caractéres)",
			"default"=>""
		),
		"meios"=>array(
			"title"=>"<b>Bandeiras</b>",
			"type"=>"multiselect",
			"description"=>"Selecione as Bandeiras Desejadas (Use Ctrl)",
			"default"=>array("visa","mastercard"),
			"css" =>"height: 150px;",
			"options"=>array(
				"visa" => "Visa",
				"mastercard" => "Mastercard",
				"elo" => "Elo",
				"amex" => "Amex",
				"diners" => "Diners",
				"discover" => "Discover",
				"jcb" => "JCB",
				"aura" => "Aura",
				"hipercard" => "Hipercard"
			),

		),
		"autenticacao"=>array(
			"title"=>"<b>Autenticação</b>",
			"type"=>"select",
			"description"=>"Autenticar todas as transações com o banco, essa opção redireciona o usuário ao site do banco para autenticar e validar a transação. Auxilia na prevenção de fraudes.",
			"default" => "false",
			"options"=>array(
				"true"=>"Autenticar junto ao banco",
				"false"=>"Não autenticar"
			),
		),
		'antifraude_details' => array(
			'title'       => "Anti-fraude",
			'type'        => 'title',
			'description' => "Só habilite essa opção se ela estiver ativa junto a cielo.",
		),
		"antifraude"=>array(
			"title"=>"<b>Habilitar Anti-Fraude</b>",
			"type"=>"select",
			"description"=>"Habilite essa opção somente se o anti-fraude foi contratado",
			"default"=>"nao",
			"options"=>array(
				"nao"=>"Não",
				"sim"=>"Sim"
			),
		),

		"verificaip"=>array(
			"title"=>"<b>Máximo de tentativas de pagamento</b>",
			"type"=>"text","description"=>"Para maior segurança, por padrão o máximo de tentativas é 3 para evitar fraudes com testes de cartões. Deixe 0 para desabilitar a função. A quantidade de tentativas é sempre feita em cima de um intervalo de 3 minutos",
			"default"=>"3"
		),

		"div"=>array(
			"title"=>"<b>Dividir em at&eacute;</b>",
			"type"=>"select",
			"description"=>"O maximo &eacute; 12x.",
			"default"=>"6",
			"options"=>$this->gerar_parcelas(),
		),
		"sem"=>array(
			"title"=>"<b>Sem juros em at&eacute;</b>",
			"type"=>"select",
			"description"=>"Deve ser menor ou igual a Dividir.",
			"default"=>"6",
			"options"=>$this->gerar_parcelas(),
		),
		"juros"=>array(
			"title"=>"<b>Taxa de Juros</b>",
			"type"=>"text",
			"description"=>"De acordo seu contrato com a Cielo (para parcelamento com juros).",
			"default"=>"4.00"),
			"minimo"=>array(
				"title"=>"<b>Parcela Minima</b>",
				"type"=>"text",
				"description"=>"Sempre tem de ser Maior ou Igual a 5.00",
				"default"=>"5.00"
			),
			"parcelamento"=>array(
				"title"=>"<b>Parcelamento (Com Juros)</b>",
				"type"=>"select",
				"description"=>"Loja ou Operadora de acordo contrato Cielo",
				"default"=>"loja",
				"options"=>array(
					"loja"=>"Loja",
					"operadora"=>"Operadora"
				),
			),
			"captura"=>array(
				"title"=>"<b>Tipo de Captura</b>",
				"type"=>"select",
				"description"=>"Método de captura da transação. Na opção manual é preciso capturar a transação na tela do pedido.",
				"default"=>"automatica",
				"options"=>array(
					"automatica"=>"Automatica",
					"posterior"=>"Manual"
				),
			),
			"aguardando"=>array(
				"title"=>"<b>Aguardando Pagamento</b>",
				"type"=>"select",
				"description"=>"",
				"default"=>"wc-pending",
				"options"=>$this->get_status_pagamento(),
			),
			"pago"=>array(
				"title"=>"<b>Status Aprovado</b>",
				"type"=>"select",
				"description"=>"Transação autorizada e capturada",
				"default"=>"wc-completed",
				"options"=>$this->get_status_pagamento(),
			),
			"autorizado"=>array(
				"title"=>"<b>Status Autorizado</b>",
				"type"=>"select",
				"description"=>"Transação autorizada, é necessário capturar manualmente para concluir a transação.",
				"default"=>"wc-processing",
				"options"=>$this->get_status_pagamento(),
			),
			"cancelado"=>array(
				"title"=>"<b>Status Cancelado</b>",
				"type"=>"select",
				"description"=>"",
				"default"=>"wc-cancelled",
				"options"=>$this->get_status_pagamento(),
			),
			"negado"=>array("title"=>"<b>Status Negado</b>",
			"type"=>"select",
			"description"=>"",
			"default"=>"wc-failed",
			"options"=>$this->get_status_pagamento(),
		),
		'debug' => array(
			'title'            => 'Debug Log',
			'type'             => 'checkbox',
			'label'            => 'Habilitar logs',
			'default'          => 'no',
			'description'      => '(Logs em woocommerce > status > logs)'
		)
	);

}
else{

	$this->form_fields=array(
		"imagem"=>array("title"=>"",
		"type"=>"hidden",
		"description"=>"<img src='".plugins_url()."/jrossetto-woo-cielo-webservice/images/banner.png'>",
		"default"=>""),
		"your_activation_status"=>array(
			"type"=>"text",
			"description"=>"<a href='admin.php?page=JROSSETTO_LICENSE'>Clique aqui</a> para ativar",
			"default" => "Inativo",
			"title"=>"Status",
			"disabled"=> true),

		);
	}





}

public function payment_fields() {
	global $woocommerce;
	$anti_fraude = ($this->settings['antifraude']=='sim') ? true : false;

	if(!isset($_GET['pay_for_order'])){
		$total_cart = number_format($woocommerce->cart->total, 2, '.', '');
	}else{
		$order_id = woocommerce_get_order_id_by_order_key($_GET['key']);
		$order = new WC_Order( $order_id );
		$total_cart = number_format($order->get_total(), 2, '.', '');
	}

	$merchant_id = trim($this->settings['afiliacao']);
	$hash = md5($this->get_user_login()).date('YmdHis');
	if($this->settings['testmode']=='yes'){
		$oggid = "1snn5n9w";
	}else{
		$oggid = "k8vif92e";
	}

	include(dirname(__FILE__) . '/layout_credito.php');
}

public function validate_fields() {
	global $woocommerce;
	if($_POST['payment_method']=='jrossetto_woo_cielo_webservice'){
		$erros = 0;
		if($this->get_post('bandeira')==''){
			$this->tratar_erro("Digite um cartão válido!");
			$erros++;
		}
		if($this->get_post('titular')==''){
			$this->tratar_erro("Informe o nome do titular!");
			$erros++;
		}
		if($this->settings['antifraude']=='sim'){
			if($this->get_post('documento')==''){
				$this->tratar_erro("Informe um CPF/CNPJ v&aacute;lido!");
				$erros++;
			}
			$cpf_cnpj = new ValidaCPFCNPJ($this->get_post('documento'));
			if(!$cpf_cnpj->valida()){
				$this->tratar_erro("O CPF/CNPJ n&atilde;o &eacute; v&aacute;lido!");
				$erros++;
			}
		}
		if($this->get_post('numero')==''){
			$this->tratar_erro("Informe o n&uacute;mero do cart&atilde;o!");
			$erros++;
		}
		if($this->get_post('validade')==''){
			$this->tratar_erro("Informe a validade do cart&atilde;o!");
			$erros++;
		}
		if($this->get_post('cvv')==''){
			$this->tratar_erro("Informe o CVV do cart&atilde;o!");
			$erros++;
		}
		if($this->get_post('parcela')==''){
			$this->tratar_erro("Selecione a parcela desejada!");
			$erros++;
		}
		if($erros>0){
			return false;
		}
	}
	return true;
}

private function get_post( $name ) {
	if (isset($_POST['cielo_webservice'][$name])) {
		return $_POST['cielo_webservice'][$name];
	}
	return null;
}

public function tratar_erro($erro){
	global $woocommerce;
	if(function_exists('wc_add_notice')){
		wc_add_notice($erro,$notice_type = 'error' );
	}else{
		$woocommerce->add_error($erro);
	}
}



public function process_payment($order_id) {
	global $woocommerce,$wpdb;
	$order = new WC_Order( $order_id );
	if(!$this->cielo_webservice_check($order->get_id()) === true && $this->settings['verificaip'] > 0){
		$this->tratar_erro("Erro: Muitas tentativas de pagamento. Por favor aguarde alguns minutos para tentar novamente ou entre em contato conosco.");
		return false;
		die();
	}
	//cartao
	$validade =  preg_replace('/\D/', '',$this->get_post('validade'));
	$nome_completo = $this->get_post('titular');
	$hash = $this->get_post('hash');
	$documento = preg_replace('/\D/', '', $this->get_post('documento'));
	$numero_cartao = preg_replace('/\D/', '', $this->get_post('numero'));
	if(strlen($validade)==6){
		$mes_cartao = substr($validade,0,2);
		$ano_cartao = substr($validade,-4);
	}elseif(strlen($validade)==4){
		$mes_cartao = substr($validade,0,2);
		$ano_cartao = '20'.substr($validade,-2);
	}else{
		$this->tratar_erro("Ops, informe a validade de forma correta (MM/AA)!");
		return false;
	}
	$cod_cartao = preg_replace('/\D/', '',$this->get_post('cvv'));

	//trata a parcela
	$dados = explode('|',base64_decode($this->get_post('parcela')));
	if(!isset($dados[0])){
		$this->tratar_erro("Ops, problema ao enviar dados de parcelas!");
		return false;
	}
	$parcela = $dados[0];
	$tipo_parcela = $dados[1];
	$total = $dados[2];
	$bandeira = base64_decode($dados[3]);

	//funcoes cielo
	if ( !class_exists( 'Pedido' ) ){
		include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/include.php' );
	}
	$regras_cc = CartaoJrossetto($bandeira,$tipo_parcela);


	//api 3.0
include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );

	if($this->settings['testmode']=='yes'){
		$provider = 'Simulado';
		$urlweb = "https://apisandbox.cieloecommerce.cielo.com.br/1/";
		$this->afiliacao = '7d3576ab-8a34-4a9f-841b-ea32e79474aa';
		$this->chave = 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC';
	}else{
		$provider = 'Cielo';
		$urlweb = "https://api.cieloecommerce.cielo.com.br/1/";
	}
	$objResposta = array();
	$bandeira = $regras_cc["cc"];
	if($bandeira=='Mastercard' || $bandeira=='mastercard'){
		$bandeira = 'Master';
	}
	$headers = array(
		"Content-Type" => "application/json",
		"Accept" => "application/json",
		"MerchantId" =>trim($this->settings['afiliacao']),
		"MerchantKey" => trim($this->settings['chave']),
		"RequestId" => "",
	);

	if($this->settings['testmode']=='yes'){
		$headers = array(
			"Content-Type" => "application/json",
			"Accept" => "application/json",
			"MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
			"MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
			"RequestId" => "",
		);

	}
	$dados = array();
	$dados['MerchantOrderId'] = $order->get_id();


	//if anti-fraud
	if($this->settings['antifraude'] =='nao'){

		$dados['Customer'] = array(
			'Name' => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
			'Email' => $order->get_billing_email(),
		);

	}else{

		$dados['Customer'] = array(
			'Name' => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
			'Email' => $order->get_billing_email(),
			"Identity" => $documento,
			"IdentityType" => (strlen($documento)==11?'CPF':'CNPJ'),
			'Address' => array(
				'Street'=> $order->get_billing_address_1(),
				'Number'=> (isset($order->billing_number)?$order->billing_number:'*'),
				'District'=>(isset($order->billing_neighborhood)?$order->billing_neighborhood:$order->get_billing_address_2()),
				'Complement' => $order->get_billing_address_2() ,
				'ZipCode'=>preg_replace('/\D/', '', $order->get_billing_postcode()),
				'City'=>$order->get_billing_city(),
				'State'=>$order->get_billing_state(),
				'Country'=>'BRA',
			),
			'DeliveryAddress' => array(
				'Street'=>$order->get_shipping_address_1(),
				'Number'=>(isset($order->shipping_number)?$order->shipping_number:'*'),
				'District'=>(isset($order->shipping_neighborhood)?$order->shipping_neighborhood:$order->get_shipping_address_2()),
				'Complement' => $order->get_billing_address_2(),
				'ZipCode'=>preg_replace('/\D/', '', $order->get_shipping_postcode()),
				'City'=>$order->get_shipping_city(),
				'State'=>$order->get_shipping_state(),
				'Country'=>'BRA',
			),
		);


		$produtos = array();
		if ( 0 < count( $order->get_items() ) ) {
			foreach ( $order->get_items() as $order_item ) {
				if ( $order_item['qty'] ) {
					$item_total = $order->get_item_total( $order_item, false );

					if ( 0 > $item_total ) {
						continue;
					}

					$item_name = $order_item['name'];
					$produtos[] = array(
						'GiftCategory' => 'No',
						'HostHedge' => 'Normal',
						'NonSensicalHedge' => 'Normal',
						'ObscenitiesHedge' => 'Normal',
						'PhoneHedge' => 'Normal',
						'Type' => 'Default',
						'Name' => $item_name,
						'Quantity' => $order_item['qty'],
						'Sku' =>  $order_item['product_id'],
						'TimeHedge' => 'Normal',
						'UnitPrice' => number_format($item_total, 2, '', ''),
						'Risk' => 'Normal',
					);
				}
			}
		}



	}

	$dados['Payment'] = array(
		'Type' => 'CreditCard',
		'Amount' => number_format($total, 2, '', ''),
		'Currency' => 'BRL',
		'Country' => 'BRA',
		'Provider' => $provider,
		'ServiceTaxAmount' => 0,
		'Installments' => $parcela,
		'Interest' => (($regras_cc["tp"]==2)?'ByMerchant':'ByIssuer'),
		'Capture' =>  (($this->settings['captura']=='automatica')?'true':'false'),
		'Authenticate' => $this->settings['autenticacao'],
		'ReturnUrl' => get_rest_url() . 'retornocielo/credito', //admin_url('admin-ajax.php') .'?retorno_credito_cielo_webservice',
		'Recurrent' => 'false',
		'SoftDescriptor' => $this->settings['softdescriptor'],
		'CreditCard' => array(
			"CardNumber" => $numero_cartao,
			"Holder" => $nome_completo,
			"ExpirationDate" => $mes_cartao.'/'.$ano_cartao,
			"SecurityCode" => $cod_cartao,
			"SaveCard" => "false",
			"Brand" => ucfirst($bandeira)
		)
	);

	if($this->settings['antifraude'] =='sim'){
		$dados['Payment']['FraudAnalysis'] = array(
			'Sequence' => "AuthorizeFirst",
			'SequenceCriteria' => "Always",
			"FingerPrintId" => $hash,
			"Browser" => array(
				"CookiesAccepted" => true,
				"Email" => $order->get_billing_email(),
				"IpAddress" => (isset($_SERVER['REMOTE_ADDR'])?$_SERVER['REMOTE_ADDR']:'127.0.0.1'),
				"Type" => substr($this->get_user_agent(),0,39)
			),
			'Cart' => array(
				'IsGift' => false,
				'ReturnsAccepted' => true,
				'Items' => $produtos,
			)
		);
	}




	$api = new RestClient(array(
		'base_url' => $urlweb,
		'headers' => $headers,
	));

	if ( 'yes' == $this->debug) {
		$logDados = $dados;
		$bin = substr($logDados['Payment']['CreditCard']['CardNumber'],0,6);
		$bin .= '****';
		$bin .= substr($logDados['Payment']['CreditCard']['CardNumber'],-4);
		$logDados['Payment']['CreditCard']['CardNumber'] = $bin;
		$logDados['Payment']['CreditCard']['SecurityCode'] = '***';
		$this->log->add( $this->id, 'DADOS PEDIDO ENVIADO: ' . print_r($logDados, true));
	}

	$response = $api->post("sales", json_encode($dados));
	$dados_pedido = $this->json2array($response->response);

	if ( 'yes' == $this->debug) {
		$this->log->add( $this->id, 'DADOS RETORNO CIELO: ' . print_r($this->json2array($response->response), true));
	}


	//trata o resultado
	if(($response->status==200 || $response->status==201) && isset($dados_pedido['Payment']['Tid'])){
		$erro = false;
		$objResposta['tipo'] = 'credito';
		$objResposta['tid'] = isset($dados_pedido['Payment']['Tid'])?$dados_pedido['Payment']['Tid']:'';
		$objResposta['status'] = $dados_pedido['Payment']['Status'];
		$objResposta['lr'] = isset($dados_pedido['Payment']['ReturnCode'])?$dados_pedido['Payment']['ReturnCode']:'';
		$objResposta['lr_log'] = isset($dados_pedido['Payment']['ReturnMessage'])?$dados_pedido['Payment']['ReturnMessage']:'';
		$objResposta['parcelas'] = $dados_pedido['Payment']['Installments'];
		$objResposta['bin'] = $dados_pedido['Payment']['CreditCard']['CardNumber'];
		$objResposta['bandeira'] = $dados_pedido['Payment']['CreditCard']['Brand'];
		$objResposta['id_pagamento'] = $dados_pedido['Payment']['PaymentId'];
		if(isset($dados_pedido['Payment']['AuthenticationUrl'])){
			$objResposta['url_autenticacao'] = $dados_pedido['Payment']['AuthenticationUrl'];
		}
	}elseif(isset($dados_pedido[0]['Message'])){
		$erro = true;
		$objResposta['mensagem'] = $dados_pedido[0]['Message'];
		$objResposta['codigo'] = $dados_pedido[0]['Code'];
	}elseif(isset($dados_pedido['Message'])){
		$erro = true;
		$objResposta['mensagem'] = $dados_pedido['Message'];
		$objResposta['codigo'] = $dados_pedido['Code'];
	}else{
		$erro = true;
		$objResposta['mensagem'] = isset($dados_pedido['Payment']['ReasonMessage'])?$dados_pedido['Payment']['ReasonMessage']:'Erro cielo desconhecido ao processar pagamento Cielo, verificar se o mesmo encontra-se online (ver logs)!';
		$objResposta['codigo'] = isset($dados_pedido['Payment']['ReasonCode'])?$dados_pedido['Payment']['ReasonCode']:'999';
	}


	//se ocorreu erro salva os logs
	if($erro == true){
		$logs = new WC_Logger();
		$logs->add( $this->id, 'Erro Cielo Pedido: '.$order->get_id() .' em '.date('d/m/Y H:i:s').'');
		$logs->add( $this->id, 'Mensagem: '.$objResposta['codigo'].' - '.$objResposta['mensagem'] );
		$logs->add( $this->id, 'Log: '.print_r($objResposta,true) );
	}



	if(isset($objResposta['tid']) && !empty($objResposta['tid'])){

		$bin = substr($numero_cartao,0,6);
		$bin .= '****';
		$bin .= substr($numero_cartao,-4);

		//$wpdb->query("INSERT INTO `wp_cielo_api_jrossetto` (`idUnica`, `tid`, `pedido`, `bandeira`, `parcela`, `lr`, `total`, `status`, `bin`) VALUES (NULL, '".$objResposta['tid']."', '".$order->get_id()."', '".$regras_cc["cc"]."', '".$parcela."', '', '".$total."', '".$objResposta['status']."', '".$bin."');");
		$wpdb->query("INSERT INTO `wp_cielo_api_jrossetto` (`id`, `metodo`, `id_pagamento`, `tid`, `pedido`, `bandeira`, `parcela`, `lr`, `lr_log`, `total`, `status`, `bin`, `link`) VALUES (NULL, 'credito', '".$objResposta['id_pagamento']."', '".$objResposta['tid']."', '".$order->get_id()."', '".$regras_cc["cc"]."', '".$parcela."', '".$objResposta['lr']."', '".$objResposta['lr_log']."', '".$total."', '".$objResposta['status']."', '".$bin."', '');");
		//cria uma nota no pedido
		$order->add_order_note("Transa&ccedil;&atilde;o Cr&eacute;dito Cielo - TID ".$objResposta['tid']." em ".$parcela."x no ".strtoupper($regras_cc["cc"])." (".$bin.")");


		//salva AF - Anti Fraude
		if($this->settings['antifraude'] =='sim'){
			if(!add_post_meta($order->get_id(),'_transacao_fraudanalysis',$dados_pedido['Payment']['FraudAnalysis'],true)) {
				update_post_meta($order->get_id(),'_transacao_fraudanalysis',$dados_pedido['Payment']['FraudAnalysis']);
			}
		}


		//cria uma transacao pan ou transacao
		$pan = isset($objResposta['pan'])?$objResposta['pan']:'';
		if(!add_post_meta($order->get_id(),'_transacao',$pan,true)) {
			update_post_meta($order->get_id(),'_transacao',$pan);
		}

		if(!add_post_meta($order->get_id(),'_transacao_tipo','Crédito',true)) {
			update_post_meta($order->get_id(),'_transacao_tipo','Crédito');
		}
		if(!add_post_meta($order->get_id(),'_transacao_bandeira',$dados_pedido['Payment']['CreditCard']['Brand'],true)) {
			update_post_meta($order->get_id(),'_transacao_bandeira',$dados_pedido['Payment']['CreditCard']['Brand']);
		}
		if(!add_post_meta($order->get_id(),'_transacao_parcelas',$dados_pedido['Payment']['Installments'],true)) {
			update_post_meta($order->get_id(),'_transacao_parcelas',$dados_pedido['Payment']['Installments']);
		}

		if(!add_post_meta($order->get_id(),'_transacao_validade',  $mes_cartao.'/'.$ano_cartao,true)) {
			update_post_meta($order->get_id(),'_transacao_validade',  $mes_cartao.'/'.$ano_cartao);
		}




		//se foi aprovado logo ou cancelado
		if(isset($objResposta['status'])){
			switch($objResposta['status']){
				case '2':
				$order->update_status($this->pago);
				break;
				case '1':
				$order->update_status($this->autorizado);
				break;
				case '3':
				$order->update_status($this->negado);
				break;
				case '10':
				case '13':
				$order->update_status($this->cancelado);
				break;
				default:
				$order->update_status($this->aguardando);
			}
		}


		//limpa o carrinho
		$woocommerce->cart->empty_cart();

		//se precisar autenticar
				if(isset($objResposta['url_autenticacao']) && $objResposta['status']==0){
					$urlAutenticacaoLink = $objResposta['url_autenticacao'];
				}else{
					$urlAutenticacaoLink = add_query_arg(array('pedido'=>$order->get_id(),'hash'=>md5($order->get_id().$order->get_id())),$this->get_return_url( $order ));
				}


		//reduz um estoque

		if($objResposta['status']==1 || $objResposta['status']==2){
			//wc_reduce_stock_levels( $order->get_id() );
			$order->reduce_order_stock();
		}


		return array(
			'result' 	=> 'success',
			'redirect'	=>  $urlAutenticacaoLink
		);

	}elseif(isset($objResposta['codigo'])){

		$this->tratar_erro("Erro ".$objResposta['codigo'].": ".$objResposta['mensagem']."");
		return false;

	}else{

		$this->tratar_erro("Erro cielo desconhecido ao processar pagamento Cielo, verificar se o mesmo encontra-se online (ver logs)!");
		return false;

	}

}

public function get_user_agent() {
	return isset( $_SERVER['HTTP_USER_AGENT'] ) ? strtolower( $_SERVER['HTTP_USER_AGENT'] ) : '';
}

public function obj2array($obj){
	return json_decode(json_encode($obj),true);
}

public function json2array($obj){
	return json_decode($obj,true);
}

public function restore_order_stock($order_id) {
	$order = new WC_Order( $order_id );
	if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
		return;
	}
	foreach ( $order->get_items() as $item ) {
		if ( $item['product_id'] > 0 ) {
			$_product = $order->get_product_from_item( $item );
			if ( $_product && $_product->exists() && $_product->managing_stock() ) {
				$old_stock = $_product->stock;
				$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $this, $item );
				$new_quantity = $_product->increase_stock( $qty );
				do_action( 'woocommerce_auto_stock_restored', $_product, $item );
				$order->add_order_note( sprintf( __( 'Estoque do item #%s incrementado de %s para %s', 'woocommerce' ), $item['product_id'], $old_stock, $new_quantity) );
				$order->send_stock_notifications( $_product, $new_quantity, $item['qty'] );
			}
		}
	}
}

public function tipo_par($a){
	if($a=='A'){
		return 'D&eacute;bito';
	}elseif($a=='2'){
		return 'Cr&eacute;dito sem Juros';
	}elseif($a=='3'){
		return 'Cr&eacute;dito com Juros';
	}elseif($a=='1'){
		return 'Cr&eacute;dito &agrave; vista';
	}
}

private function get_user_login() {
	global $user_login;
	get_currentuserinfo();
	return $user_login;
}
}
?>
