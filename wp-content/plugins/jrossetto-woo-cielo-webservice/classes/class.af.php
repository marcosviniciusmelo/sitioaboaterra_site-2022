<?php

class WC_Cielo_Webservice_Jrossetto_AF{
  //check AF - Analise de fraude
  public function LookUpAddressInfoCode($code)
  {
    $_codes = array(
      "COR-BA" => "O endereço de cobrança pode ser normalizado.",
      "COR-SA" => " O endereço de entrega pode ser normalizado.",
      "INTL-BA" => " país de cobrança é fora dos U.S.",
      "INTL-SA" => "O país de entrega é fora dos U.S.",
      "MIL-USA" => "Este é um endereço militar nos U.S",
      "MM-A" => "Os endereços de cobrança e entrega usam nomes de ruas diferentes.",
      "MM-BIN" => "O BIN do cartão (os seis primeiros dígitos do número) não corresponde ao país.",
      "MM-C" => "Os endereços de cobrança e entrega usam cidades diferentes.",
      "MM-CO" => "Os endereços de cobrança e entrega usam países diferentes.",
      "MM-ST" => "Os endereços de cobrança e entrega usam estados diferentes.",
      "MM-Z" => "Os endereços de cobrança e entrega usam códidos postais diferentes.",
      "UNV-ADDR" => "O endereço é inverificável."
    );

    return isset($_codes[$code]) ? $_codes[$code] : false;
  }
  public function RiskFactorCodes($code){
    $_codes = array(
      "A" => "Mudança de endereço excessiva. O cliente mudou o endereço de cobrança duas ou mais vezes nos últimos seis meses.",
      "B" => "BIN do cartão ou autorização de risco. Os fatores de risco estão relacionados com BIN de cartão de crédito e/ou verificações de autorização do cartão.",
      "C" => "Elevado números de cartões de créditos. O cliente tem usado mais de seis números de cartões de créditos nos últimos seis meses.",
      "D" => "Impacto do endereço de e-mail. O cliente usa um provedor de e-mail gratuito ou o endereço de email é arriscado.",
      "E" => "Lista positiva. O cliente está na sua lista positiva.",
      "F" => "Lista negativa. O número da conta, endereço, endereço de e-mail ou endereço IP para este fim aparece sua lista negativa.",
      "G" => "Inconsistências de geolocalização. O domínio do cliente de e-mail, número de telefone, endereço de cobrança, endereço de envio ou endereço IP é suspeito.",
      "H" => "Excessivas mudanças de nome. O cliente mudou o nome duas ou mais vezes nos últimos seis meses.",
      "I" => "Inconsistências de internet. O endereço IP e de domínio de e-mail não são consistentes com o endereço de cobrança.",
      "N" => "Entrada sem sentido. O nome do cliente e os campos de endereço contém palavras sem sentido ou idioma.",
      "O" => "Obscenidades. Dados do cliente contém palavras obscenas.",
      "P" => "Identidade morphing. Vários valores de um elemento de identidade estão ligados a um valor de um elemento de identidade diferentes. Por exemplo, vários números de telefone estão ligados a um número de conta única.",
      "Q" => "Inconsistências do telefone. O número de telefone do cliente é suspeito.",
      "R" => "Ordem arriscada. A transação, o cliente e o lojista mostram informações correlacionadas de alto risco.",
      "T" => "Cobertura Time. O cliente está a tentar uma compra fora do horário esperado.",
      "U" => "Endereço não verificável. O endereço de cobrança ou de entrega não pode ser verificado.",
      "V" => "Velocity. O número da conta foi usado muitas vezes nos últimos 15 minutos.",
      "W" => "Marcado como suspeito. O endereço de cobrança ou de entrega é semelhante a um endereço previamente marcado como suspeito.",
      "Y" => "O endereço, cidade, estado ou país dos endereços de cobrança e entrega não se correlacionam.",
      "Z" => "Valor inválido. Como a solicitação contém um valor inesperado, um valor padrão foi substituído. Embora a transação ainda possa ser processada, examinar o pedido com cuidado para detectar anomalias."
    );
    return isset($_codes[$code]) ? $_codes[$code] : false;
  }

  public function VelocityCodes($code){
    $_codes = array(
      "VEL-ADDR" => "Diferente estados de faturamento e/ou o envio (EUA e Canadá apenas) têm sido usadas várias vezes com o número do cartão de crédito e/ou endereço de email.",
      "VEL-CC" => "Diferentes números de contas foram usados várias vezes com o mesmo nome ou endereço de email.",
      "VEL-NAME" => "Diferentes nomes foram usados várias vezes com o número do cartão de crédito e / ou endereço de email.",
      "VELS-CC" => "O número de conta tem sido utilizado várias vezes durante o intervalo de controle curto.",
      "VELI-CC" => "O número de conta tem sido utilizado várias vezes durante o intervalo de controle médio.",
      "VELL-CC" => "O número de conta tem sido utilizado várias vezes durante o intervalo de controle longo.",
      "VELV-CC" => "O número de conta tem sido utilizado várias vezes durante o intervalo de controle muito longo.",
      "VELS-EM" => "O endereço de e-mail tem sido utilizado várias vezes durante o intervalo de controle curto.",
      "VELI-EM" => "O endereço de e-mail tem sido utilizado várias vezes durante o intervalo de controle médio.",
      "VELL-EM" => "O endereço de e-mail tem sido utilizado várias vezes durante o intervalo de controle longo.",
      "VELV-EM" => "O endereço de e-mail tem sido utilizado várias vezes durante o intervalo de controle muito longo.",
      "VELS-FP" => "O device fingerprint tem sido utilizado várias vezes durante um intervalo curto",
      "VELI-FP" => "O device fingerprint tem sido utilizado várias vezes durante um intervalo médio",
      "VELL-FP" => "O device fingerprint tem sido utilizado várias vezes durante um intervalo longo",
      "VELV-FP" => "O device fingerprint tem sido utilizado várias vezes durante um intervalo muito longo.",
      "VELS-IP" => "O endereço IP tem sido utilizado várias vezes durante o intervalo de controle curto.",
      "VELI-IP" => "O endereço IP tem sido utilizado várias vezes durante o intervalo de controle médio.",
      "VELL-IP" => "O endereço IP tem sido utilizado várias vezes durante o intervalo de controle longo.",
      "VELV-IP" => "O endereço IP tem sido utilizado várias vezes durante o intervalo de controle muito longo.",
      "VELS-SA" => "O endereço de entrega tem sido utilizado várias vezes durante o intervalo de controle curto.",
      "VELI-SA" => "O endereço de entrega tem sido utilizado várias vezes durante o intervalo de controle médio.",
      "VELL-SA" => "O endereço de entrega tem sido utilizado várias vezes durante o intervalo de controle longo.",
      "VELV-SA" => "O endereço de entrega tem sido utilizado várias vezes durante o intervalo de controle muito longo.",
      "VELS-TIP" => "O endereço IP verdadeiro tem sido utilizado várias vezes durante o intervalo de controle curto.",
      "VELI-TIP" => "O endereço IP verdadeiro tem sido utilizado várias vezes durante o intervalo de controle médio.",
      "VELL-TIP" => "O endereço IP verdadeiro tem sido utilizado várias vezes durante o intervalo de controle longo."

    );
    return isset($_codes[$code]) ? $_codes[$code] : false;
  }

  public function InternetInfoCode($code){

    $_codes = array(
      "FREE-EM" => "O endereço de e-mail do cliente é de um provedor de e-mail gratuito.",
      "INTL-IPCO" => "O país do endereço de e-mail do cliente é fora do U.S.",
      "INV-EM" => "O endereço de e-mail do cliente é inválido.",
      "MM-EMBCO" => "O domínio do endereço de e-mail do cliente não é consistente com o país do endereço de cobrança.",
      "MM-IPBC" => "O endereço de e-mail do cliente não é consistente com a cidade do endereço de cobrança.",
      "MM-IPBCO" => "O endereço de e-mail do cliente não é consistente com a país do endereço de cobrança.",
      "MM-IPBST" => "O endereço IP do cliente não é consistente com o estado no endereço de cobrança. No entanto, este código de informação não pode ser devolvido quando a inconsistência é entre estados imediatamente adjacentes.",
      "MM-IPEM" => "O endereço de e-mail do cliente não é consistente com o endereço IP.",
      "RISK-EM" => "O domínio do e-mail do cliente (por exemplo, mail.example.com) está associada com alto risco.",
      "UNV-NID" => "O endereço IP do cliente é de um proxy anônimo. Estas entidades escondem completamente informações sobre o endereço de IP.",
      "UNV-RI400SK" => "O endereço IP é de origem de risco.",
      "UNV-EMBCO" => "O país do endereço do cliente de e-mail não corresponde ao país do endereço de cobrança."
    );
    return isset($_codes[$code]) ? $_codes[$code] : false;
  }

  public function resultadoAnaliseFraude($resposta){
		$texto='erro';
		switch ($resposta) {
			case '100':
			$texto = "Operação bem sucedida.";
			break;
			case '101':
			$texto = "O pedido está faltando um ou mais campos necessários. Possível ação: Veja os campos que estão faltando na lista AntiFraudResponse.MissingFieldCollection. Reenviar o pedido com a informação completa.";
			break;
			case '102':
			$texto = "Um ou mais campos do pedido contêm dados inválidos. Possível ação: Veja os campos inválidos na lista AntiFraudResponse.InvalidFieldCollection. Reenviar o pedido com as informações corretas.";
			break;
			case '150':
			$texto = "Falha no sistema geral. Possível ação: Aguarde alguns minutos e tente reenviar o pedido.";
			break;
			case '151':
			$texto = "O pedido foi recebido, mas ocorreu time-out no servidor. Este erro não inclui time-out entre o cliente e o servidor. Possível ação: Aguarde alguns minutos e tente reenviar o pedido.";
			break;
			case '152':
			$texto = "O pedido foi recebido, mas ocorreu time-out. Possível ação: Aguarde alguns minutos e reenviar o pedido.";
			break;
			case '202':
			$texto = "Prevenção à Fraude recusou o pedido porque o cartão expirou. Você também pode receber este código se a data de validade não coincidir com a data em arquivo do banco emissor. Se o processador de pagamento permite a emissão de créditos para cartões expirados, a CyberSource não limita essa funcionalidade. Possível ação: Solicite um cartão ou outra forma de pagamento.";
			break;
			case '231':
			$texto = "O número da conta é inválido. Possível ação: Solicite um cartão ou outra forma de pagamento.";
			break;
			case '234':
			$texto = "Há um problema com a configuração do comerciante. Possível ação: Não envie o pedido. Entre em contato com o Suporte ao Cliente para corrigir o problema de configuração.";
			break;
			case '400':
			$texto = "A pontuação de fraude ultrapassa o seu limite. Possível ação: Reveja o pedido do cliente.";
			break;
			case '480':
			$texto = "O pedido foi marcado para revisão pelo Gerenciador de Decisão.";
			break;
			case '481':
			$texto = "O pedido foi rejeitado pelo Gerenciador de Decisão";
			break;

			default:
			$texto = "";
			break;
		}

		return $texto;
	}


  public static function get_score_meta( $score_points ) {

    // No score? return defaults
    if ( '' == $score_points ) {
      return array( 'color' => '#adadad', 'label' => 'Nenhuma verificação de fraude foi feita neste pedido ainda' );
    }

    $meta = array(
      'label' => 'Nível de Risco: MUITO BAIXO',
      'color' => '#7AD03A'
    );
    if ( $score_points <= 95 && $score_points >= 76 ) {
    $meta = array(
      'label' => 'Nível de Risco: BAIXO',
      'color' => '#7AD03A'
    );
   }else if ( $score_points <= 75 && $score_points >= 25 ) {
      $meta['label'] = 'Nível de Risco: MÉDIO';
      $meta['color'] = '#FFAE00';
    } elseif ( $score_points < 25 ) {
      $meta['label'] = 'Nível de Risco: ALTO';
      $meta['color'] = '#D54E21';
    }

    return $meta;
  }

  public static function invert_score( $score ) {
    $score = ( $score < 0 ) ? 0 : $score;

    return 100 - $score;;
  }

  public static function generateLi($codes, $function){


    foreach(explode('^', $codes) as $InfoCode)
    {
      if($InfoCode != "")
      {
        $_InfoCodes[] = WC_Cielo_Webservice_Jrossetto_AF::$function($InfoCode);
      }
    }
    foreach ($_InfoCodes as $_InfoCodesItem) {
        $_InfoCodesItemLi =  "<li>$_InfoCodesItem</li>";
    }

    return isset($_InfoCodesItemLi) ? $_InfoCodesItemLi : '<li>--</li>';

  }

}
?>
