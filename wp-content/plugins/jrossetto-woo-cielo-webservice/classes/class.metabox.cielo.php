<?php
class WC_Cielo_Webservice_Jrossetto_Metabox {

	public function __construct() {
		// Order edit assets
		add_action( 'admin_print_scripts-post.php', array( $this, 'post_admin_scripts' ), 11 );

		// Order overview assets
		add_action( 'admin_print_scripts-edit.php', array( $this, 'edit_admin_scripts' ), 11 );

		add_action( 'add_meta_boxes', array( $this, 'register_metabox' ) );
		add_action( 'save_post', array( $this, 'save' ) );

		include_once(CIELO_WEBSERVICE_WOO_PATH.'/classes/class.af.php' );

		  add_action( 'admin_enqueue_scripts', array( $this, 'cielo_jrossetto_admin_style' ), 11 );

	}

	public function cielo_jrossetto_admin_style() {
    wp_enqueue_style('cielo-jrossetto-admin', plugins_url('../css/cielo-jrossetto-admin.css', __FILE__));
  }

	public function save(){
	}

	public function register_metabox() {
		add_meta_box(
			'jrossetto_woo_cielo_webservice',
			'Transa&ccedil;&atilde;o Cielo API 3.0',
			array( $this, 'metabox_content' ),
			'shop_order',
			'side',
			'high'
		);
	}

	public function post_admin_scripts() {
		global $post_type;

		// Check post type
		if ( 'shop_order' == $post_type ) {

			// Enqueue scripts
			wp_enqueue_script(
				'wc_af_knob_js',
				plugins_url( '/jrossetto-woo-cielo-webservice/js/jquery.knob.min.js'),
				array( 'jquery' )
			);

			wp_enqueue_script(
				'wc_af_edit_shop_order_js',
				plugins_url( '/jrossetto-woo-cielo-webservice/js/edit-shop-order' . ( ( ! SCRIPT_DEBUG ) ? '.min' : '' ) . '.js'),
				array( 'jquery', 'wc_af_knob_js' )
			);

			// CSS
			wp_enqueue_style(
				'wc_af_post_shop_order_css',
				plugins_url( '/jrossetto-woo-cielo-webservice/css/post-shop-order.css')
			);

		}

	}

	/**
	 * Enqueue edit admin scripts
	 *
	 * @since  1.0.0
	 * @access public
	 */
	public function edit_admin_scripts() {
		global $post_type;
		if ( 'shop_order' == $post_type ) {
			wp_enqueue_style(
				'wc_af_edit_shop_order_css',
				plugins_url( '/jrossetto-woo-cielo-webservice/css/edit-shop-order.css' )
			);
		}
	}


	public function metabox_content( $post ) {
		global $wpdb;

		//faz o include das config cielo

		include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );

		//dados do pedido
		$order = new WC_Order( $post->ID );

		//consulta os dados cielo relacionado ao pedido
		$dados = (array)$wpdb->get_row("SELECT * FROM `wp_cielo_api_jrossetto` WHERE pedido = '".$order->get_id()."';");

		//se o pedido for pago com cielo webservice
		if ( 'jrossetto_woo_cielo_webservice' == $order->get_payment_method() || 'jrossetto_woo_cielo_webservice_debito' == $order->get_payment_method() || 'jrossetto_woo_cielo_webservice_tef' == $order->get_payment_method() || 'jrossetto_woo_cielo_webservice_boleto' == $order->get_payment_method() ) {

			//se retornou tid
			$status = '';
			if ( isset( $dados['id_pagamento'] ) && !empty( $dados['id_pagamento'] ) ) {

				//detalhes
				$html = '<style>#jrossetto_woo_cielo.postbox {display: ;}</style>';
				if(isset($dados['id_pagamento'])){
					$html .= '<p><strong>ID Pagamento</strong> <a href="'.admin_url( 'admin-ajax.php' ).'?action=logs_cielo_webservice_api_jrossetto&id='.$dados['id_pagamento'].'" target="_blank">'.$dados['id_pagamento'].'</a><br>';
				}
				$html .= '<p><strong>Total:</strong> ' . $dados['total'] . '<br>';
				if($dados['metodo']=='credito' || $dados['metodo']=='debito'){
					$html .= '<p><strong>TID:</strong> ' . $dados['tid'] . '<br>';
					$html .= '<p><strong>Bandeira:</strong> '.strtoupper($dados['bandeira']).' / '.ucfirst($dados['metodo']).'<br>';
					$html .= '<p><strong>BIN:</strong> '.$dados['bin'].'<br>';
					$html .= '<p><strong>Parcela:</strong> '.$dados['parcela'].'x<br>';
				}else{
					$html .= '<p><strong>Nosso N&uacute;mero:</strong> '.strtoupper($dados['tid']).'<br>';
					$html .= '<p><strong>Banco:</strong> '.strtoupper($dados['bandeira']).' / '.ucfirst($dados['metodo']).'<br>';
				}

				//busca as configuracoes cielo webservice
				if($order->get_payment_method()=='jrossetto_woo_cielo_webservice'){
					$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice();
				}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_debito'){
					$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Debito();
				}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_tef'){
					$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_TEF();
				}elseif($order->get_payment_method()=='jrossetto_woo_cielo_webservice_boleto'){
					$config = new WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Boleto();
				}

				//pega a data do pedido
				$data_pedido = strtotime($order->get_date_created());

				//cielo api
				if($config->testmode=='yes'){
					$provider = 'Simulado';
					$urlweb = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/";
				}else{
					$provider = 'Cielo';
					$urlweb = "https://apiquery.cieloecommerce.cielo.com.br/1/";
				}
				$objResposta = array();
				$headers = array(
					"Content-Type" => "application/json",
					"Accept" => "application/json",
					"MerchantId" =>trim($config->afiliacao),
					"MerchantKey" => trim($config->chave),
					"RequestId" => "",
				);

				if($config->testmode=='yes'){
					$headers = array(
						"Content-Type" => "application/json",
						"Accept" => "application/json",
						"MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
						"MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
						"RequestId" => "",
					);

				}
				$api = new RestClient(array(
					'base_url' => $urlweb,
					'headers' => $headers,
				));
				$response = $api->get("sales/".$dados['id_pagamento']."");
				$dados_pedido = @json_decode($response->response,true);
				if(($response->status==200 || $response->status==201) && isset($dados_pedido['Payment']['Status'])){
					$status_id = $dados_pedido['Payment']['Status'];
					switch($dados_pedido['Payment']['Status']){
						case "2":
	          $status = '<mark class="order-status status-success tips"><span class="dashicons dashicons-yes-alt"></span><span>&nbsp;Pagamento Aprovado</span></mark>';
	          break;
	          case "1":
	          $status = '<mark class="order-status status-autorized tips"><span class="dashicons dashicons-plus-alt"></span><span>&nbsp;Pagamento Autorizado</span></mark>';
	          break;
	          case "3":
	          $status = '<mark class="order-status status-denied tips"><span class="dashicons dashicons-warning"></span><span>&nbsp;Pagamento Negado</span></mark>';
	          break;
	          case "10":
	          case "13":
	          $status = '<mark class="order-status status-canceled tips"><span class="dashicons dashicons-dismiss"></span><span>&nbsp;Pagamento Cancelado</span></mark>';
	          break;
	          default:
	          $status = '<mark class="order-status status-pending tips"><span class="dashicons dashicons-backup"></span><span>&nbsp;Aguardando Pagamento</span></mark>';
					}
					if($dados['metodo']=='boleto' && $status_id==1){
						$html .= '<p><strong>Status Cielo:</strong> <mark class="order-status status-pending tips"><span class="dashicons dashicons-backup"></span><span>&nbsp;Aguardando Pagamento</span></mark></p>';
					}else{
						$html .= '<p><strong>Status Cielo:</strong> ' . $status . '</p>';
					}
					if(!empty($dados['lr'])){
						$html .= '<p><strong>LR:</strong> ' . $dados['lr'] . ' ' . $dados['lr_log'] . '</p>';
					}
					if(isset($dados_pedido['Payment']['FraudAnalysis']['Id'])){
						$html .= '<p><strong>ID Consulta Anti-fraude:</strong> ' . $dados_pedido['Payment']['FraudAnalysis']['Id'] . '</p>';
						$html .= '<p><strong>Status Anti-fraude Atual:</strong> ' . $dados_pedido['Payment']['FraudAnalysis']['Status'] . '</p>';
						$html .= '<p><strong>Status Anti-fraude Anterior:</strong> ' . $dados_pedido['Payment']['FraudAnalysis']['PreviousStatus'] . '</p>';
						$html .= '<p><strong>Provedor:</strong> ' . $dados_pedido['Payment']['FraudAnalysis']['Provider'] . '</p>';
					}
					if($config->antifraude == 'sim' && $AF['StatusDescription'] =! 'Error'){
						$AF = $dados_pedido['Payment']['FraudAnalysis'];

						$_addressInfoCodesLi = NULL;
						$_riskInfoCodesItemLi = NULL;
						$_internetInfoCodesItemLi = NULL;
						$_velocityInfoCodesItemLi = NULL;


						//analise de endereço
						if(isset($AF['ReplyData']['AddressInfoCode']))
						{
							$_addressInfoCodesLi = WC_Cielo_Webservice_Jrossetto_AF::generateLi($AF['ReplyData']['AddressInfoCode'], 'LookUpAddressInfoCode' );
						}

						//analise de risco
						if(isset($AF['ReplyData']['FactorCode']))
						{
							$_riskInfoCodesItemLi = WC_Cielo_Webservice_Jrossetto_AF::generateLi($AF['ReplyData']['FactorCode'], 'RiskFactorCodes' );
						}

						//analise de internet
						if(isset($AF['ReplyData']['InternetInfoCode']))
						{
							$_internetInfoCodesItemLi = WC_Cielo_Webservice_Jrossetto_AF::generateLi($AF['ReplyData']['InternetInfoCode'], 'InternetInfoCode' );
						}

						//global Velocity
						if(isset($AF['ReplyData']['VelocityInfoCode']))
						{
							$_velocityInfoCodesItemLi = WC_Cielo_Webservice_Jrossetto_AF::generateLi($AF['ReplyData']['VelocityInfoCode'], 'VelocityCodes' );
						}


						$score_points = $AF['ReplyData']['Score'];

						$meta = WC_Cielo_Webservice_Jrossetto_AF::get_score_meta( $score_points );
						if ( '' != $score_points ) {
							// The label
							$html .= '<span class="mb-score-label" style="color:' . $meta['color'] . '"><b>' . $meta['label'] . '</b></span>' . PHP_EOL;
							// Circle points
							$circle_points = WC_Cielo_Webservice_Jrossetto_AF::invert_score( $score_points );
							// The circle
							$html .= '<input class="knob" data-fgColor="' . $meta['color'] . '" data-thickness=".4" data-readOnly=true value="0" rel="' . $circle_points . '">';
						}

						$html .= '<div class="woocommerce-af-risk-failure-list">' . PHP_EOL;

			        $html .='<ul>' . PHP_EOL;
							$html .= '<li><b>-- RESULTADO DA ANÁLISE DE FRAUDE --</b></li>';
							$html .='<li><strong>Status:</strong> '.$AF['Status'].' - '. $AF['StatusDescription'] .'</li>';
							$html .='<li><strong>Score:</strong>  <b>'.$AF['ReplyData']['Score'].'</b></li>';
							$html .='<li><strong>Reposta:</strong> '.WC_Cielo_Webservice_Jrossetto_AF::resultadoAnaliseFraude($AF['FraudAnalysisReasonCode']).'</li>';
							$html .='<li><strong>Análise de risco:</strong> <ul>'.$_riskInfoCodesItemLi.'</ul></li>';
							$html .='<li><strong>Análise de endereço:</strong> <ul>'.$_addressInfoCodesLi.'</ul></li>';
							$html .='<li><strong>Análise de internet:</strong> <ul>'.$_internetInfoCodesItemLi.'</ul></li>';
							$html .='<li><strong>Análise de Global Velocity:</strong> <ul>'.$_velocityInfoCodesItemLi.'</ul></li>';
							$html .='<li><strong>Análise feita por:</strong> '.$AF['Provider'].'</p>';
							$html .= '</ul>' . PHP_EOL;


						$html .= '</div>' . PHP_EOL;
						$html .= '<p style="text-align: center;"><a class="woocommerce-af-risk-failure-list-toggle" style="cursor:  pointer;"  data-toggle="Esconder detalhes">Exibir detalhes ANTI-FRAUDE</a></p>' . PHP_EOL;
					}

				}else{
					$status_id = $dados['status'];
					switch($dados['status']){
						case "2":
	          $status = '<mark class="order-status status-success tips"><span class="dashicons dashicons-yes-alt"></span><span>&nbsp;Pagamento Aprovado</span></mark>';
	          break;
	          case "1":
	          $status = '<mark class="order-status status-autorized tips"><span class="dashicons dashicons-plus-alt"></span><span>&nbsp;Pagamento Autorizado</span></mark>';
	          break;
	          case "3":
	          $status = '<mark class="order-status status-denied tips"><span class="dashicons dashicons-warning"></span><span>&nbsp;Pagamento Negado</span></mark>';
	          break;
	          case "10":
	          case "13":
	          $status = '<mark class="order-status status-canceled tips"><span class="dashicons dashicons-dismiss"></span><span>&nbsp;Pagamento Cancelado</span></mark>';
	          break;
	          default:
	          $status = '<mark class="order-status status-pending tips"><span class="dashicons dashicons-backup"></span><span>&nbsp;Aguardando Pagamento</span></mark>';


					}
					if($dados['metodo']=='boleto' && $status_id==1){
						$html .= '<p><strong>Status Cielo:</strong> <mark class="order-status status-pending tips"><span class="dashicons dashicons-backup"></span><span>&nbsp;Aguardando Pagamento</span></mark></p>';
					}else{
						$html .= '<p><strong>Status Cielo:</strong> ' . $status . '</p>';
					}
					if(!empty($dados['lr'])){
						$html .= '<p><strong>LR:</strong> ' . $dados['lr'] . ' ' . $dados['lr_log'] . '</p>';
					}
				}

				//autorizar ou capturar
				if($dados['metodo']=='credito' || $dados['metodo']=='debito'){
					if($status_id==1 || $status_id==2){
						$html .= '<a class="button button-primary" href="admin.php?page=jrossetto-woo-cielo-webservice-pedidos&pedido='.$order->get_id().'">Detalhes / Capturar / Cancelar</a>';
					}else{
						$html .= '<a class="button button-primary" href="admin.php?page=jrossetto-woo-cielo-webservice-pedidos&pedido='.$order->get_id().'">Detalhes</a>';
					}
				}else{
					$html .= '<a class="button button-primary" href="admin.php?page=jrossetto-woo-cielo-webservice-pedidos&pedido='.$order->get_id().'">Detalhes</a>';
				}
			}else{
				$html = '<style>#jrossetto_woo_cielo_webservice.postbox {display: none;}</style>';
			}
		}else{
			$html .= '<style>#jrossetto_woo_cielo_webservice.postbox {display: none;}</style>';
		}
		echo $html;
	}
}
