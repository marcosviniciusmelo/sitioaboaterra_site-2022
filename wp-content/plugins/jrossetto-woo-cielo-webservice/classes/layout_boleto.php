<?php
if ( ! defined( 'ABSPATH' ) ) {
exit;
}
//se pf ou pj
$documento = '';
$customer_id = get_current_user_id();
if(get_user_meta( $customer_id, 'billing_cnpj', true )){
	$documento = get_user_meta( $customer_id, 'billing_cnpj', true );
}elseif(get_user_meta( $customer_id, 'billing_cpf', true )){
	$documento = get_user_meta( $customer_id, 'billing_cpf', true );
}
$documento = preg_replace('/\D/', '',$documento);
?>
<script type="text/javascript" language="javascript">
//ajax acoes
var ajax_url = "<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php";

//dados definidos
var url_cielo_webservice_boleto = '<?php echo plugins_url();?>';
var total_pedido_cielo_boleto  = '<?php echo $total_cart;?>';
var hash_pedido_cielo_boleto  = '<?php echo sha1(md5($total_cart));?>';

//funcoes
function aplicar_bandeira_cielo_webservice_boleto(bandeira){
    jQuery('.campos_cielo_webservice_boleto').hide();
    jQuery('#tela-bandeiras-cielo-boleto').after('<p id="alert-load-cielo-boleto">Aguarde o carregamento...</p>');
    jQuery(".meio_cielo_webservice_img_boleto").css({ opacity: 0.2 });
    jQuery("."+ bandeira ).css({ opacity: 1 });
    jQuery('#parcela-cielo-webservice-boleto').html('<option value="">Aguarde...</option>');
    jQuery.post(ajax_url, {action : 'parcelas_cielo_webservice', id : bandeira,total: total_pedido_cielo_boleto,hash: hash_pedido_cielo_boleto }, retorno_parcelamento_boleto, 'JSON');
    jQuery('#bandeira-cielo-webservice-boleto').val(bandeira);
}

function retorno_parcelamento_boleto(data) {
    console.log(data);
    var items = '';
    jQuery.each(data, function(key, val) {
        items += '<option value="' + key + '">' + val + '</option>';
    });
    jQuery('#parcela-cielo-webservice-boleto').html(items);
    jQuery('.campos_cielo_webservice_boleto').show();
    jQuery('#alert-load-cielo-boleto').remove();
}
</script>

<div id="tela-cielo-webservice" style="width:100%;">



<fieldset class="wc-credit-card-form wc-payment-form">

<p id="tela-bandeiras-cielo-boleto" class="form-row form-row-wide woocommerce-validated">

  Pague com boleto bancário
  <br />
  Após clicar em "Finalizar compra" você receberá o seu boleto bancário, é possível imprimi-lo e pagar pelo site do seu banco ou por uma casa lotérica.
  Nota: O pedido será confirmado apenas após a confirmação do pagamento.

</p>


<input type="hidden" name="cielo_webservice_boleto[bandeira]" id="bandeira-cielo-webservice-boleto" value="boleto">





<?php if(strlen($documento)==11 || strlen($documento)==14){ ?>

<input type="hidden" id="fiscal-cielo-webservice" name="cielo_webservice_boleto[documento]" value="<?php echo $documento;?>">

<?php }else{ ?>

<p class="form-row form-row-wide woocommerce-validated campos_cielo_webservice">
<label style="padding: 5px 0 5px 5px;">CPF/CNPJ:</label>
<input style="box-shadow: inset 2px 0 0 #0f834d;height:40px;"  type="text" class="input-text mascaras_campos_cielo_webservice" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="CPF ou CNPJ" id="fiscal-cielo-webservice-boleto" name="cielo_webservice_boleto[documento]" value="<?php echo $documento;?>">
</p>

<?php } ?>







</fieldset>

</div>
