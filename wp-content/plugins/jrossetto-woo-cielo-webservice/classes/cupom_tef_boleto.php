<?php
if(isset($cielo['id_pagamento'])){
	$html = '<p>Sua transa&ccedil;&atilde;o refer&ecirc;nte ao pedido <b>#'.$order_id.'</b> foi processada junto a operadora.<br>
	A sua transa&ccedil;&atilde;o encontra-se <b>'.strtoupper($status).'</b>.<br><br>';
	if(isset($cielo['tid']) && !empty($cielo['tid'])){
		$html .= '<b>Nosso N&uacute;mero:</b>  '.$cielo['tid'].'<br>';
	}

	if(isset($_GET['linha']) && !empty($_GET['linha'])){
		$html .= '<b>Linha Digit&aacute;vel:</b>  '.urldecode($_GET['linha']).'<br>';
	}
	if($cielo['metodo']=='tef' && isset($cielo['link']) && !empty($cielo['link']) && $cielo['status']!=2){
		$html .= '<br><a class="button"  style="background: #32a2bb; color: #FFF;" href="'.$cielo['link'].'" target="_blank">Concluir Pagamento via '.ucfirst($cielo['bandeira']).'</a><br>';
	}elseif($cielo['metodo']=='boleto' && isset($cielo['link']) && !empty($cielo['link'])){
		if(isset($dados_pedido['Payment']['BarCodeNumber']) && !empty($dados_pedido['Payment']['BarCodeNumber'])){
			$html .= '<b>C&oacute;digo de Barras:</b><br><img src="'.plugins_url().'/jrossetto-woo-cielo-webservice/include/barra.php?codigo='.preg_replace('/\D/', '', $dados_pedido['Payment']['BarCodeNumber']).'">';
		}
		$html .= '<br><a class="button"  style="background: #32a2bb; color: #FFF;" href="'.$cielo['link'].'" target="_blank">Acessar e Pagar o Boleto</a><br>';
	}
	$html .= '<br>Caso tenha alguma d&uacute;vida referente a transa&ccedil;&atilde;o entre em contato com o atendimento da loja e se j&aacute; pagou aguarde a confirma&ccedil;&atilde;o do seu pedido.</p>';
	echo wpautop(wptexturize($html));
}
?>
