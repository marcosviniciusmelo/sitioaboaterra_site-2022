<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}
//se pf ou pj
$documento = '';
$customer_id = get_current_user_id();
if(get_user_meta( $customer_id, 'billing_cnpj', true )){
	$documento = get_user_meta( $customer_id, 'billing_cnpj', true );
}elseif(get_user_meta( $customer_id, 'billing_cpf', true )){
	$documento = get_user_meta( $customer_id, 'billing_cpf', true );
}
$documento = preg_replace('/\D/', '',$documento);
?>
<?php if($anti_fraude) { ?>
<script src="https://h.online-metrix.net/fp/check.js?org_id=<?php echo $oggid;?>&session_id=cielo_webservice<?php echo $hash;?>" type="text/javascript"></script>
<?php } ?>
<script type="text/javascript" language="javascript">
//ajax acoes
var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";

//dados definidos
var url_cielo_webservice = '<?php echo plugins_url();?>';
var total_pedido_cielo = '<?php echo $total_cart;?>';
var hash_pedido_cielo = '<?php echo sha1(md5($total_cart));?>';

//funcoes

function detectar_bandeira_cartao_credito_cielo(numero){
	var result = '';
	var bin = (numero).replace(/\D/g,'');
	if (/^3[47][0-9]{13}/.test(bin)) {
		result = "amex";
	}else if(/^(4011(78|79)|43(1274|8935)|45(1416|7393|763(1|2))|50(4175|6699|67[0-7][0-9]|9000)|627780|63(6297|6368)|650(03([^4])|04([0-9])|05(0|1)|4(0[5-9]|3[0-9]|8[5-9]|9[0-9])|5([0-2][0-9]|3[0-8])|9([2-6][0-9]|7[0-8])|541|700|720|901)|651652|655000|655021)/.test(bin)){
		result = "elo";
	} else if (/^4[0-9]{12}(?:[0-9]{3})?$/.test(bin)) {
		result = "visa";
	} else if (/^3(?:0[0-5]|[68][0-9])[0-9]{11}/.test(bin)) {
		result = "diners";
	} else if(/^6(?:011|5[0-9]{2})[0-9]{12}/.test(bin)) {
		result = "discover";
	} else if(/^(?:2131|1800|35\d{3})\d{11}/.test(bin)) {
		result = "jcb";
	} else if(/^(50)\d+$/.test(bin)) {
		result = "aura";
	} else if(/^5[1-5]|^2(2(2[1-9]|[3-9])|[3-6]|7([01]|20))/.test(bin)) {
		result = "mastercard";
	} else if(/^(38[0-9]{17}|60[0-9]{14})$/.test(bin)) {
		result = "hipercard";
	}
	console.log(result);
	if(result!=''){
		aplicar_bandeira_cielo_webservice(result);
	}
}


function aplicar_bandeira_cielo_webservice(bandeira){
  //jQuery('.campos_cielo_webservice').hide();
  jQuery('#tela-bandeiras-cielo').after('<p id="alert-load-cielo">Carregando parcelas...</p>');
  jQuery(".meio_cielo_webservice_img").css({ opacity: 0.2 });
  jQuery("."+ bandeira ).css({ opacity: 1 });
  jQuery('#parcela-cielo-webservice-credito').html('<option value="">Aguarde...</option>');
  jQuery.post(ajax_url, {action : 'parcelas_cielo_webservice', id : bandeira,total: total_pedido_cielo,hash: hash_pedido_cielo }, retorno_parcelamento, 'JSON');
  jQuery('#bandeira-cielo-webservice').val(bandeira);
  if(bandeira == 'amex'){
    jQuery("#cvv-cielo-webservice").attr('maxlength','4');
  }else{
    jQuery("#cvv-cielo-webservice").attr('maxlength','3');
  }
}


function retorno_parcelamento(data) {
  //console.log(data);
  var items = '';
  jQuery.each(data, function(key, val) {
    items += '<option value="' + key + '">' + val + '</option>';
  });
  jQuery('#parcela-cielo-webservice-credito').html(items);
  jQuery('.campos_cielo_webservice').show();
  jQuery('#alert-load-cielo').remove();
}

jQuery('#tela-cielo-webservice').bind('cut copy paste', function (e) {
  e.preventDefault();
});


</script>

<div id="tela-cielo-webservice" style="width:100%;">

  <div>
  <p style="margin-bottom: 5px;display: none;">Cartões de crédito aceitos.</p>
  <p id="tela-bandeiras-cielo" class="form-row form-row-wide woocommerce-validated" style="display: none;">
    <span style="float:left;">
      <?php
      foreach($this->meios AS $k=>$b){
        ?>
        <img style="cursor:pointer;float:left;min-height:30px;" class='meio_cielo_webservice_img <?php echo $b;?>' onclick="aplicar_bandeira_cielo_webservice('<?php echo $b;?>')" src='<?php echo plugins_url().'/jrossetto-woo-cielo-webservice/images/'.$b.'.png';?>' width="50">
        <?php
      }
      ?>
    </span>
  </p>

  </div>
  <div id="cielo-webservice-card"></div>

  <fieldset class="wc-credit-card-form wc-payment-form">



    <input type="hidden" name="cielo_webservice[bandeira]" id="bandeira-cielo-webservice" value="">
    <input type="hidden" name="cielo_webservice[hash]" value="<?php echo $hash;?>">

    <p class="form-row form-row-wide woocommerce-validated campos_cielo_webservice" style="">
      <label style="padding: 5px 0 5px 5px;">Nome do titular:</label>
      <input  type="text" class="input-text nome_titular_cielo_webservice" placeholder="Nome como impresso no cart&atilde;o" name="cielo_webservice[titular]" value="" autocomplete="off">
    </p>

    <?php if(strlen($documento)==11 || strlen($documento)==14 || $anti_fraude==false){ ?>

    <input type="hidden" id="documento-cielo-webservice" name="cielo_webservice[documento]" value="<?php echo $documento;?>">

    <?php }else{ ?>

    <p class="form-row form-row-wide woocommerce-validated campos_cielo_webservice">
    <label style="padding: 5px 0 5px 5px;">CPF/CNPJ:</label>
    <input style="box-shadow: inset 2px 0 0 #0f834d;height:40px;" type="text" class="input-text mascaras_campos_cielo_webservice" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="CPF ou CNPJ" id="documento-cielo-webservice" name="cielo_webservice[documento]" value="<?php echo $documento;?>">
    </p>

    <?php } ?>

<p class="form-row form-row-wide woocommerce-validated campos_cielo_webservice" >
  <label style="padding: 5px 0 5px 5px;">N&uacute;mero:</label>
  <input  type="text" class="input-text mascaras_cartao_cielo_webservice" onblur="detectar_bandeira_cartao_credito_cielo(this.value)"  name="cielo_webservice[numero]" id="cc-number2" value="" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="0000 0000 0000 0000">
</p>

<p class="form-row form-row-first woocommerce-validated campos_cielo_webservice" >
  <label style="padding: 5px 0 5px 5px;">Validade:</label>
  <input  type="text"  id="validade-cielo-webservice" class="input-text mascaras_campos_cielo_webservice" placeholder="MM/AA" name="cielo_webservice[validade]" value="" maxlength="7" autocomplete="off">
</p>

<p class="form-row form-row-last woocommerce-validated campos_cielo_webservice" >
  <label style="padding: 5px 0 5px 5px;">CVV:</label>
  <input  type="text" id="cvv-cielo-webservice"  class="input-text mascaras_campos_cielo_webservice" name="cielo_webservice[cvv]" placeholder="3 ou 4 digitos (amex)" value="" autocomplete="off">
</p>

<p class="form-row form-row-wide woocommerce-validated campos_cielo_webservice" >
  <label style="padding: 5px 0 5px 5px;">Parcelas:</label>
  <select  name="cielo_webservice[parcela]" id="parcela-cielo-webservice-credito">
    <option value="">Digite o número do cartão...</option>
  </select>
</p>

</fieldset>

</div>

<style>
input#cc-number2.jp-card-invalid.unknown {
    box-shadow: inset 2px 0 0 red;
}
</style>
