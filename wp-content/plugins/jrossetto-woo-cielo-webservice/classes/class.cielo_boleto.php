<?php
class WC_Gateway_Jrossetto_Woo_Cielo_Webservice_Boleto extends WC_Payment_Gateway {

  protected $jr_product_id = 'WC Cielo API 3.0';
  protected $jr_software_version = '1.4';

  public function __construct() {
    global $woocommerce;
    $this->id           = 'jrossetto_woo_cielo_webservice_boleto';
    $this->icon         = apply_filters( 'woocommerce_jrossetto_woo_cielo_webservice', home_url( '/' ).'wp-content/plugins/jrossetto-woo-cielo-webservice/images/boleto.png' );
    $this->has_fields   = false;
    $this->supports   = array('products');
    $this->description = true;
    $this->method_description = __( 'Ativa o pagamento por Boleto via Cielo.', 'jrossetto-woo-cielo-webservice-boleto' );
    $this->method_title = 'Cielo API 3.0 - Boleto';
    $this->init_settings();
    $this->init_form_fields();
    $this->instalar_mysql_cielo_webservice();

    $this->debug = $this->settings['debug'];

    foreach ( $this->settings as $key => $val ) $this->$key = $val;



    if ( version_compare( WOOCOMMERCE_VERSION, '2.0.0', '>=' ) ){
      add_action('woocommerce_update_options_payment_gateways_' . $this->id, array( &$this, 'process_admin_options' ) );
    }else{
      add_action('woocommerce_update_options_payment_gateways', array( &$this, 'process_admin_options' ) );
    }

    add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );



    if ( !$this->is_valid_for_use() ) $this->enabled = false;


    // Active logs.
    if ( 'yes' == $this->debug ) {
      if ( class_exists( 'WC_Logger' ) ) {
        $this->log = new WC_Logger();
      } else {
        $this->log = $this->woocommerce_method()->logger();
      }
    }

  }

  public function thankyou_page( $order_id ) {
    global $wpdb;
    //pega o pedido
    $order = new WC_Order((int)($order_id));

    //dados cielo mysql
    $cielo = (array)$wpdb->get_row("SELECT * FROM `wp_cielo_api_jrossetto` WHERE `pedido` = '".(int)($order_id)."' ORDER BY id DESC;");

    //rest

    include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );


    //cielo api
    if($this->settings['testmode']=='yes'){
      $provider = 'Simulado';
      $urlweb = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/";
    }else{
      $provider = 'Cielo';
      $urlweb = "https://apiquery.cieloecommerce.cielo.com.br/1/";
    }
    $objResposta = array();
    $headers = array(
      "Content-Type" => "application/json",
      "Accept" => "application/json",
      "MerchantId" =>trim($this->settings['afiliacao']),
      "MerchantKey" => trim($this->settings['chave']),
      "RequestId" => "",
    );
    if($this->settings['testmode']=='yes'){
      $headers = array(
        "Content-Type" => "application/json",
        "Accept" => "application/json",
        "MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
        "MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
        "RequestId" => "",
      );

    }

    $api = new RestClient(array(
      'base_url' => $urlweb,
      'headers' => $headers,
    ));
    $response = $api->get("sales/".$cielo['id_pagamento']."");
    $dados_pedido = @json_decode($response->response,true);

    //define o status do pedido
    $status_cielo = isset($cielo['status'])?$cielo['status']:'0';
    switch($status_cielo){
      case '2':
      $status = '<span style="color: #20bb20;">Aprovada</span>';
      break;
      case '1':
      $status = '<span style="color: #2196f3;">Autorizada</span>';
      break;
      case '3':
      $status = '<span style="color: red;">Negada</span>';
      break;
      case '10':
      case '13':
      $status = '<span style="color: red;">Cancelada</span>';
      break;
      default:
      $status = 'Aguardando Pagamento';
    }

    //layout
    include_once(dirname(__FILE__) . '/cupom_tef_boleto.php');
  }



  public function is_valid_for_use() {
    if ( ! in_array( get_woocommerce_currency(), apply_filters( 'woocommerce_jrossetto_woo_cielo_webservice_supported_currencies', array( 'BRL' ) ) ) ){
      return false;
    }
    return true;
  }



  public function instalar_mysql_cielo_webservice(){
    global $wpdb;
    $wpdb->query("CREATE TABLE IF NOT EXISTS `wp_cielo_api_jrossetto` (
      `id` int(10) NOT NULL AUTO_INCREMENT,
      `metodo` varchar(40) NOT NULL,
      `id_pagamento` varchar(40) NOT NULL,
      `tid` varchar(40) NOT NULL,
      `pedido` varchar(40) NOT NULL,
      `bandeira` varchar(40) NOT NULL,
      `parcela` varchar(40) NOT NULL,
      `lr` varchar(20) NOT NULL,
      `lr_log` varchar(180) NOT NULL,
      `total` float(10,2) NOT NULL,
      `status` varchar(40) NOT NULL,
      `bin` varchar(40) NOT NULL,
      `link` varchar(255) NOT NULL,
      `created`     DATETIME DEFAULT CURRENT_TIMESTAMP,
      `updated` DATETIME ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
  }

  public function get_status_pagamento(){
    if(function_exists('wc_get_order_statuses')){
      return wc_get_order_statuses();
    }else{
      $taxonomies = array(
        'shop_order_status',
      );
      $args = array(
        'orderby'       => 'name',
        'order'         => 'ASC',
        'hide_empty'    => false,
        'exclude'       => array(),
        'exclude_tree'  => array(),
        'include'       => array(),
        'number'        => '',
        'fields'        => 'all',
        'slug'          => '',
        'parent'         => '',
        'hierarchical'  => true,
        'child_of'      => 0,
        'get'           => '',
        'name__like'    => '',
        'pad_counts'    => false,
        'offset'        => '',
        'search'        => '',
        'cache_domain'  => 'core'
      );
      foreach(get_terms( $taxonomies, $args ) AS $status){
        $s[$status->slug] = __( $status->slug, 'woocommerce' );
      }
      return $s;
    }
  }

  public function admin_options() {

    if ( $this->is_valid_for_use() ) : ?>
    <table class="form-table">
      <?php
      $this->generate_settings_html();
      ?>
    </table>
  <?php else : ?>
    <div class="inline error"><p><strong><?php _e( 'Gateway Desativado', 'woocommerce' ); ?></strong>: <?php _e( 'Cielo Webservice n&atilde;o aceita o tipo e moeda de sua loja, apenas BRL.', 'woocommerce' ); ?></p></div>
    <?php
  endif;
}



public function init_form_fields() {

  require_once(CIELO_WEBSERVICE_WOO_PATH.'/wcwsl.php' );
  $verifica = new WCWSL;
  $statusCheck = $verifica->status_check2();
  $status  = get_option( 'wcwsl_softlicense_key_status' );

  if( $status == 'valid' ){

    $this->form_fields=array(
      'boleto_details' => array(
        'title'       => "Atenção",
        'type'        => 'title',
        'description' => "Essa opção é válida apenas para os bancos: Bradesco e Banco do Brasil. Só habilite se já estiver ativo junto a cielo e ao banco.",
      ),
      "imagem"=>array(
        "title"=>"",
        "type"=>"hidden",
        "description"=>"<img src='".plugins_url()."/jrossetto-woo-cielo-webservice/images/banner.png'>",
        "default"=>""
      ),
      "enabled"=>array(
        "title"=>"<b>Ativar/Desativar</b>",
        "type"=>"checkbox",
        "description"=>"Lembre-se de ativar o boleto junto a cielo e ao banco antes de habilitar essa opção. <a href='https://developercielo.github.io/tutorial/manual-boleto' target='_blank'>Veja como funciona</a>",
        "label"=>"Ativar",
        "default"=>"no"
      ),
      "title"=>array(
        "title"=>"<b>Titulo</b>",
        "type"=>"text",
        "description"=>"Nome que este meio de pagamento sera mostrado na finaliza&ccedil;&atilde;o.",
        "default"=>"Boleto"
      ),
      "api"=>array(
        "title"=>"<b>Vers&atilde;o API Cielo</b>",
        "type"=>"select",
        "description"=>"De acordo contratado",
        "default"=>"30",
        "options"=>array(
          "30"=>"3.0"
        ),
      ),
      "provider"=> array(
        "title"=>"<b>Selecionar o Banco</b>",
        "type"=>"select",
        "description"=>"De acordo contratado",
        "default"=>"",
        "options"=>array(
          "Bradesco2"=>"Banco Bradesco (Registrado)",
          "BancodoBrasil2"=>"Banco do Brasil (Registrado)"
        ),
      ),
      "testmode"=>array(
        "title"=>"<b>Modo Teste</b>",
        "type"=>"checkbox",
        "label"=>"Sim, ativar (modo teste)",
        "default"=>"yes",
        "description"=>	"Essa  opção envia os pedidos para o ambiente teste da Cielo. Quando for homologar lembre-se de desabilitar essa opção.",
      ),
      "afiliacao"=>array(
        "title"=>"<b>MerchantId</b>",
        "type"=>"text",
        "description"=>"MerchantId para api 3.0 ( caso ainda não tenha entre em contato a cielo para solicitar)",
        "default"=>""
      ),
      "chave"=>array(
        "title"=>"<b>MerchantKey</b>",
        "type"=>"text",
        "description"=>"MerchantKey para api 3.0 ( caso ainda não tenha entre em contato a cielo para solicitar)",
        "default"=>""
      ),
      "meios"=>array(
        "title"=>"<b>Tipo de pagamento</b>",
        "type"=>"select",
        "description"=>"Lembre-se de ativar o boleto junto a cielo e ao banco antes de habilitar essa opção",
        "default"=>array(
          "boleto","boleto"
        ),
        "options"=>array(
          "boleto"=>"Boleto",
        ),
      ),
      "vencimento"=>array(
        "title"=>"<b>Vencimento</b>",
        "type"=>"text",
        "description"=>"Número de dias para o vencimento do boleto",
        "default"=>"7"
      ),
      "cedente" => array(
        "title" => "<b>Nome do Beneficiario</b>",
        "type" => "text",
        "description" => "Nome que será exibido no boleto como o beneficiário.",
        "default" => ""
      ),
      "instrucoes" => array(
        "title" => "<b>Instru&ccedil;&otilde;es</b>",
        "type" => "text",
        "description" => "Instruções para pagamento de boleto.",
        "default" => ""
      ),
      "aguardando"=>array(
        "title"=>"<b>Aguardando Pagamento</b>",
        "type"=>"select",
        "description"=>"",
        "default"=>"pending",
        "options"=>$this->get_status_pagamento(),
      ),
      "pago"=>array(
        "title"=>"<b>Status Aprovado</b>",
        "type"=>"select",
        "description"=>"",
        "default"=>"wc-completed",
        "options"=>$this->get_status_pagamento(),
      ),
      "autorizado"=>array(
        "title"=>"<b>Status Autorizado</b>",
        "type"=>"select",
        "description"=>"",
        "default"=>"wc-processing",
        "options"=>$this->get_status_pagamento(),
      ),
      "cancelado"=>array(
        "title"=>"<b>Status Cancelado</b>",
        "type"=>"select",
        "description"=>"",
        "default"=>"wc-cancelled",
        "options"=>$this->get_status_pagamento(),
      ),
      "negado"=>array(
        "title"=>"<b>Status Negado</b>",
        "type"=>"select",
        "description"=>"",
        "default"=>"wc-failed",
        "options"=>$this->get_status_pagamento()
      ),
      "cron" => array(
        "title" => "URL de Cron",
        "type" => "hidden",
        "description" => admin_url("admin-ajax.php") . "?action=cron_jrossetto_boleto_tef_cielo_webservice" . "<br /><br />Configurar url do cron para rodar todos os dias as 08:00 AM para sincronizar os boletos pagos. (caso não seja configurado, a baixa dos boletos será feita de forma manual.)<br /><b>Exemplo: <small> 0 8 * * * wget -O /dev/null ".admin_url("admin-ajax.php") . "?action=cron_jrossetto_boleto_tef_cielo_webservice >/dev/null 2>&1</b></small>",
        "default" => ""
      ),
      'debug' => array(
        'title'            => 'Debug Log',
        'type'             => 'checkbox',
        'label'            => 'Habilitar logs',
        'default'          => 'no',
        'description'      => '(Logs em woocommerce > status > logs)'
      )
    );
  }
  else{

    $this->form_fields=array(
      "imagem"=>array("title"=>"",
      "type"=>"hidden",
      "description"=>"<img src='".plugins_url()."/jrossetto-woo-cielo-webservice/images/banner.png'>",
      "default"=>""),
      "your_activation_status"=>array(
        "type"=>"text",
        "description"=>"<a href='admin.php?page=JROSSETTO_LICENSE'>Clique aqui</a> para ativar",
        "default" => "Inativo",
        "title"=>"Status",
        "disabled"=> true),

      );
    }





  }
  public function payment_fields() {
    global $woocommerce;
    if(!isset($_GET['pay_for_order'])){
      $total_cart = number_format($woocommerce->cart->total, 2, '.', '');
    }else{
      $order_id = woocommerce_get_order_id_by_order_key($_GET['key']);
      $order = new WC_Order( $order_id );
      $total_cart = number_format($order->get_total(), 2, '.', '');
    }
    include(dirname(__FILE__) . '/layout_boleto.php');
  }

  public function validate_fields() {
    global $woocommerce;
    if($_POST['payment_method']=='jrossetto_woo_cielo_webservice_boleto'){
      $erros = 0;
      if($this->get_post('documento')==''){
        $this->tratar_erro("Informe um CPF/CNPJ v&aacute;lido!");
        $erros++;
      }
      $cpf_cnpj = new ValidaCPFCNPJ($this->get_post('documento'));
      if(!$cpf_cnpj->valida()){
        $this->tratar_erro("O CPF/CNPJ n&atilde;o &eacute; v&aacute;lido!");
        $erros++;
      }
      if($erros>0){
        return false;
      }
    }
    return true;
  }

  private function get_post( $name ) {
    if (isset($_POST['cielo_webservice_boleto'][$name])) {
      return $_POST['cielo_webservice_boleto'][$name];
    }
    return null;
  }

  public function tratar_erro($erro){
    global $woocommerce;
    if(function_exists('wc_add_notice')){
      wc_add_notice($erro,$notice_type = 'error' );
    }else{
      $woocommerce->add_error($erro);
    }
  }

  public function process_payment($order_id) {
    global $woocommerce,$wpdb;
    $order = new WC_Order( $order_id );

    //trata a parcela
    $dados = explode('|',base64_decode($this->get_post('parcela')));
    $parcela = $dados[0];
    $tipo_parcela = $dados[1];
    $total = $order->total;
    $bandeira = base64_decode($dados[3]);

    //funcoes cielo
    if ( !class_exists( 'Pedido' ) ){
      include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/include.php' );
    }
    $regras_cc = CartaoJrossetto($bandeira,$tipo_parcela);


    //api 3.0
    include_once(CIELO_WEBSERVICE_WOO_PATH.'/include/restclient.php' );

    if($this->settings['testmode']=='yes'){
      $provider = 'Simulado';
      $urlweb = "https://apisandbox.cieloecommerce.cielo.com.br/1/";
    }else{
      $provider = $this->settings['provider'];
      $urlweb = "https://api.cieloecommerce.cielo.com.br/1/";
    }
    $objResposta = array();
    $bandeira = $regras_cc["cc"];
    if($bandeira=='Mastercard' || $bandeira=='mastercard'){
      $bandeira = 'Master';
    }
    $headers = array(
      "Content-Type" => "application/json",
      "Accept" => "application/json",
      "MerchantId" =>trim($this->settings['afiliacao']),
      "MerchantKey" => trim($this->settings['chave']),
      "RequestId" => "",
    );

    if($this->settings['testmode']=='yes'){
      $headers = array(
        "Content-Type" => "application/json",
        "Accept" => "application/json",
        "MerchantId" =>'7d3576ab-8a34-4a9f-841b-ea32e79474aa',
        "MerchantKey" => 'AXKXJKREYAOZPDIFZNEFSCEFUOMSTSHBZKZUAWWC',
        "RequestId" => "",
      );

    }


    $dados = array();
    $dados['MerchantOrderId'] = $order->get_id();

    $documento = preg_replace('/\D/', '', $this->get_post('documento'));

    $numberoRua = '0';
    if($order->billing_number != '' && $order->billing_number != 'S/N' ){
      $numberoRua = (int) $order->billing_number;
    }

    $dados['Customer'] = array(
      'Name' => $order->billing_persontype == 2 ? $order->billing_company : $this->limpa_texto($order->get_billing_first_name()) . ' ' . $this->limpa_texto($order->get_billing_last_name()),
      'Email' => $order->get_billing_email(),
      'Identity' => $documento,
      'IdentityType' => (strlen($documento)==11?'CPF':'CNPJ'),
      'Address' => array(
        'Street'=> $this->limpa_texto($order->get_billing_address_1()),
        'Number'=> $numberoRua,
        'Complement'=> $this->limpa_texto($order->get_billing_address_2()),
        'District' => $order->billing_neighborhood != '' ? $this->limpa_texto($order->billing_neighborhood) : "não infomado",
        'ZipCode'=> $order->get_billing_postcode(),
        'City'=> $this->limpa_texto($order->get_billing_city()),
        'State'=> $this->limpa_texto($order->get_billing_state()),
        'Country'=>"BRA"
      )
    );

    //bb nosso numero
    $caracteres = 9;
    if($provider == 'Bradesco2'){
      $caracteres = 11;
    }

    $vencimentoDias = trim($this->settings['vencimento']);
    $validade = strtotime($order->get_date_created())+($this->settings['vencimento']*24*60*60);
    $dados['Payment'] = array(
      'Type' => 'Boleto',
      'Amount' => number_format($order->get_total(), 2, '', ''),
      'Provider' => $provider,
      'Address' => $order->get_billing_address_1(). ', '. $order->billing_number,
      'BoletoNumber' => str_pad($order->get_id(), $caracteres, '0', STR_PAD_LEFT),
      'Demonstrative' => 'Pagamento por Boleto',
      'ExpirationDate' => date('Y-m-d', $validade),
      'Identification' => $documento,
      'Instructions' => trim($this->settings['instrucoes']),
      'Assignor' => trim($this->settings['cedente']) != '' ? trim($this->settings['cedente']) : get_bloginfo('name')

    );

    $api = new RestClient(array(
      'base_url' => $urlweb,
      'headers' => $headers,
    ));


    if ( 'yes' == $this->debug) {
      $this->log->add( $this->id, 'BOLETO ---- DADOS ENVIADOS CIELO: ' . print_r($dados, true));
    }


    $response = $api->post("sales", json_encode($dados));

    $dados_pedido = $this->json2array($response->response);

    if ( 'yes' == $this->debug) {
      $this->log->add( $this->id, 'BOLETO ---- DADOS RECEBIDOS CIELO: ' . print_r($this->json2array($response->response), true));
    }




    //trata o resultado
    if(($response->status==200 || $response->status==201) && isset($dados_pedido['Payment']['PaymentId'])){
      $erro = false;
      $objResposta['tipo'] = 'boleto';
      $objResposta['tid'] = isset($dados_pedido['Payment']['BoletoNumber'])?$dados_pedido['Payment']['BoletoNumber']:'';
      $objResposta['status'] = $dados_pedido['Payment']['Status'];
      $objResposta['lr'] = isset($dados_pedido['Payment']['ReturnCode'])?$dados_pedido['Payment']['ReturnCode']:'';
      $objResposta['lr_log'] = isset($dados_pedido['Payment']['ReturnMessage'])?$dados_pedido['Payment']['ReturnMessage']:'';
      $objResposta['parcelas'] = 1;
      $objResposta['bin'] = isset($dados_pedido['Payment']['Url'])?$dados_pedido['Payment']['Url']:'';
      $objResposta['bandeira'] = $dados_pedido['Payment']['Provider'];
      $objResposta['id_pagamento'] = $dados_pedido['Payment']['PaymentId'];
      $objResposta['linha'] = isset($dados_pedido['Payment']['DigitableLine'])?$dados_pedido['Payment']['DigitableLine']:'';
    }elseif(isset($dados_pedido[0]['Message'])){
      $erro = true;
      $objResposta['mensagem'] = $dados_pedido[0]['Message'];
      $objResposta['codigo'] = $dados_pedido[0]['Code'];
    }elseif(isset($dados_pedido['Message'])){
      $erro = true;
      $objResposta['mensagem'] = $dados_pedido['Message'];
      $objResposta['codigo'] = $dados_pedido['Code'];
    }else{
      $erro = true;
      $objResposta['mensagem'] = isset($dados_pedido['Payment']['ReasonMessage'])?$dados_pedido['Payment']['ReasonMessage']:'Erro cielo desconhecido ao processar pagamento Cielo, verificar se o mesmo encontra-se online (ver logs)!';
      $objResposta['codigo'] = isset($dados_pedido['Payment']['ReasonCode'])?$dados_pedido['Payment']['ReasonCode']:'999';
    }

    //se ocorreu erro salva os logs
    if($erro == true){
      $logs = new WC_Logger();
      $logs->add( $this->id, 'Erro Cielo Pedido: '.$order->get_id() .' em '.date('d/m/Y H:i:s').'');
      $logs->add( $this->id, 'Mensagem: '.$objResposta['codigo'].' - '.$objResposta['mensagem'] );
      $logs->add( $this->id, 'Log: '.print_r($objResposta,true) );
    }



    //exit;

    //se nao retornou erro
    if(isset($objResposta['tid']) && $erro==false){

      //cria no banco de dados
      $wpdb->query("INSERT INTO `wp_cielo_api_jrossetto` (`id`, `metodo`, `id_pagamento`, `tid`, `pedido`, `bandeira`, `parcela`, `lr`, `lr_log`, `total`, `status`, `bin`, `link`) VALUES (NULL, 'boleto', '".$objResposta['id_pagamento']."', '".$objResposta['tid']."', '".$order->get_id()."', '".$objResposta['bandeira']."', '".$objResposta['parcelas']."', '".$objResposta['lr']."', '".$objResposta['lr_log']."', '".$order->get_total()."', '0', '', '".$objResposta['bin']."');");

      //cria uma nota no pedido
      $order->add_order_note("Aguardando Pagamento do <a href='".$objResposta['bin']."' target='_blank'>Boleto - ID ".$objResposta['id_pagamento']."</a>  - Vencimento: ". date('d/m/Y', strtotime($dados_pedido['Payment']['ExpirationDate'])));

      //cria uma transacao pan ou transacao
      $pan = isset($objResposta['id_pagamento'])?$objResposta['id_pagamento']:'';
      if(!add_post_meta($order->get_id(),'_transacao',$pan,true)) {
        update_post_meta($order->get_id(),'_transacao',$pan);
      }
      if(!add_post_meta($order->get_id(),'_transacao_boletoURL',$objResposta['bin'],true)) {
        update_post_meta($order->get_id(),'_transacao_boletoURL',$objResposta['bin']);
      }
      if(!add_post_meta($order->get_id(),'_transacao_boletoLinha',$objResposta['linha'],true)) {
        update_post_meta($order->get_id(),'_transacao_boletoLinha',$objResposta['linha']);
      }



      //se precisar autenticar
      $urlAutenticacaoLink = add_query_arg(array('linha'=>urlencode($objResposta['linha']), 'pedido'=>$order->get_id(),'hash'=>md5($order->get_id().$order->get_id())),$this->get_return_url( $order ));
      //se foi aprovado logo ou cancelado

      $order->update_status($this->aguardando);


      //limpa o carrinho
      $woocommerce->cart->empty_cart();


      wc_reduce_stock_levels( $order->get_id() );


      return array(
        'result' 	=> 'success',
        'redirect'	=>  $urlAutenticacaoLink
      );




    }elseif(isset($objResposta['codigo'])){

      $this->tratar_erro("Erro: ".$objResposta['codigo'].": ".$objResposta['mensagem']."");
      return false;

    }else{

      $this->tratar_erro("Erro cielo desconhecido ao processar pagamento Cielo, verificar se o mesmo encontra-se online (ver logs)!");
      return false;

    }

  }
  public function obj2array($obj){
    return json_decode(json_encode($obj),true);
  }

  public function json2array($obj){
    return json_decode($obj,true);
  }

  public function limpa_texto($texto){
    $pontos = array(".", "'", ",", "-");
    return str_replace($pontos, "", $this->removeAccents($texto));
    //return $this->removeAccents($texto);
  }

  public function is_utf8( $string ){
    return preg_match( '%^(?:
    [\x09\x0A\x0D\x20-\x7E]
    | [\xC2-\xDF][\x80-\xBF]
    | \xE0[\xA0-\xBF][\x80-\xBF]
    | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}
    | \xED[\x80-\x9F][\x80-\xBF]
    | \xF0[\x90-\xBF][\x80-\xBF]{2}
    | [\xF1-\xF3][\x80-\xBF]{3}
    | \xF4[\x80-\x8F][\x80-\xBF]{2}
    )*$%xs',
    $string
  );
}

/**
* Remove os acentos de uma string
* @param string $string
* @return string
*/
public function removeAccents( $string ){
  return preg_replace(
    array(
      //Maiúsculos
      '/\xc3[\x80-\x85]/',
      '/\xc3\x87/',
      '/\xc3[\x88-\x8b]/',
      '/\xc3[\x8c-\x8f]/',
      '/\xc3([\x92-\x96]|\x98)/',
      '/\xc3[\x99-\x9c]/',

      //Minúsculos
      '/\xc3[\xa0-\xa5]/',
      '/\xc3\xa7/',
      '/\xc3[\xa8-\xab]/',
      '/\xc3[\xac-\xaf]/',
      '/\xc3([\xb2-\xb6]|\xb8)/',
      '/\xc3[\xb9-\xbc]/',
    ),
    str_split( 'ACEIOUaceiou' , 1 ),
    $this->is_utf8( $string ) ? $string : utf8_encode( $string )
  );
}

public function restore_order_stock($order_id) {
  $order = new WC_Order( $order_id );
  if ( ! get_option('woocommerce_manage_stock') == 'yes' && ! sizeof( $order->get_items() ) > 0 ) {
    return;
  }
  foreach ( $order->get_items() as $item ) {
    if ( $item['product_id'] > 0 ) {
      $_product = $order->get_product_from_item( $item );
      if ( $_product && $_product->exists() && $_product->managing_stock() ) {
        $old_stock = $_product->stock;
        $qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $this, $item );
        $new_quantity = $_product->increase_stock( $qty );
        do_action( 'woocommerce_auto_stock_restored', $_product, $item );
        $order->add_order_note( sprintf( __( 'Estoque do item #%s incrementado de %s para %s', 'woocommerce' ), $item['product_id'], $old_stock, $new_quantity) );
        $order->send_stock_notifications( $_product, $new_quantity, $item['qty'] );
      }
    }
  }
}

public function tipo_par($a){
  if($a=='A'){
    return 'D&eacute;bito';
  }elseif($a=='2'){
    return 'Cr&eacute;dito sem Juros';
  }elseif($a=='3'){
    return 'Cr&eacute;dito com Juros';
  }elseif($a=='1'){
    return 'Cr&eacute;dito &agrave; vista';
  }
}

private function get_user_login() {
  global $user_login;
  get_currentuserinfo();
  return $user_login;
}
}
?>
