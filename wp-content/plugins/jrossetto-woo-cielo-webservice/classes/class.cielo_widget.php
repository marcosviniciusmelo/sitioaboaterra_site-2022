<?php

class WC_Cielo_Webservice_Widget{

  public function __construct(){
    add_action( 'wp_dashboard_setup', array( $this, 'jrossetto_cielo_add_dashboard_widgets' ), 11 );
    add_action( 'admin_enqueue_scripts', array( $this, 'cielo_jrossetto_admin_style' ), 11 );
  }



  public function jrossetto_cielo_add_dashboard_widgets() {
    wp_add_dashboard_widget( 'jrossetto_cielo_dashboard_widget', '<img style="width: 20px;margin: 0 10px;vertical-align: bottom;" src="'.plugins_url('/jrossetto-woo-cielo-webservice/images/logo-cielo.svg').'"/>Cielo API 3.0 - Status',  array( $this, 'jrossetto_cielo_dashboard_widget_function') );

    // Globalize the metaboxes array, this holds all the widgets for wp-admin

    global $wp_meta_boxes;

    // Get the regular dashboard widgets array
    // (which has our new widget already but at the end)

    $normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

    // Backup and delete our new dashboard widget from the end of the array

    $example_widget_backup = array( 'jrossetto_cielo_dashboard_widget' => $normal_dashboard['jrossetto_cielo_dashboard_widget'] );
    unset( $normal_dashboard['jrossetto_cielo_dashboard_widget'] );

    // Merge the two arrays together so our widget is at the beginning

    $sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );

    // Save the sorted array back into the original metaboxes

    $wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
  }



  public function jrossetto_cielo_dashboard_widget_function(){

    global $wpdb;
    $pedidos = $wpdb->get_results("SELECT * FROM `wp_cielo_api_jrossetto` ORDER BY pedido DESC LIMIT 10;", 'ARRAY_A');
    // Display whatever it is you want to show.
    echo  '<ul>';
    foreach($pedidos as $pedido){
      echo '<li class="cielo-pedido"><span class="dashicons dashicons-text-page"></span> <a href="'.esc_url( admin_url( 'post.php?post=' . $pedido['pedido'] .'&action=edit' ) ).'">Pedido #' . $pedido['pedido'] . '</a> - '. self::get_status($pedido['status'], $pedido['pedido']).'</li>';

    }
    echo  '</ul>';
    echo '
    <p class="community-events-footer">
		<a href="'.esc_url( admin_url( 'admin.php?page=jrossetto-woo-cielo-webservice-pedidos' ) ).'"><span class="dashicons dashicons-admin-generic"></span> Gerenciador de pagamentos </a> |
    <a href="https://minhaconta2.cielo.com.br/login/" target="_blank" ><span class="dashicons dashicons-businessperson"></span>BackOffice Cielo </a> |
    <a href="https://www.jrossetto.com.br/kb/plugin-cielo-api-3-0/" target="_blank" ><span class="dashicons dashicons-editor-help"></span> Dúvidas Plugin Cielo API 3.0</a>
		</p>';
  }
  public function get_status($status, $orderID)
  {
    switch ($status) {
      case "2":
      //$return = "<span style=\"color: #20bb20;\">Aprovada</span>";
      $return = '<mark class="order-status status-success tips"><span>Pagamento Aprovado</span></mark>';
      break;
      case "1":
      $url = '<a class="cielo-capture" href="'.esc_url( admin_url( 'admin.php?page=jrossetto-woo-cielo-webservice-pedidos&pedido=' . $orderID ) ).'" aria-label="Capturar"><span class="dashicons dashicons-yes-alt"></span>Capturar transação</a>';
      //$return = "<span style=\"color: #2196f3;\">Autorizada</span> " . $url;
      $return = '<mark class="order-status status-autorized tips"><span>Pagamento Autorizado</span></mark>' . $url;
      break;
      case "3":
      //$return = "<span style=\"color: red;\">Negada</span>";
      $return = '<mark class="order-status status-denied tips"><span>Pagamento Negado</span></mark>';
      break;
      case "10":
      case "13":
      //$return = "<span style=\"color: red;\">Cancelada</span>";
      $return = '<mark class="order-status status-canceled tips"><span>Pagamento Cancelado</span></mark>';
      break;
      default:
      $return = '<mark class="order-status status-pending tips"><span>Pagamento pendente</span></mark>';
    }
    return $return;
  }
  public function cielo_jrossetto_admin_style() {
    wp_enqueue_style('cielo-jrossetto-admin', plugins_url('../css/cielo-jrossetto-admin.css', __FILE__));
  }


}
?>
