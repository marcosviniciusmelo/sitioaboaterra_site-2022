import './fsl-admin.scss';

jQuery( document ).ready( function () {
	const $ = jQuery;

	const customAmountInput = $(
		'[name="devnet_fsl_general[custom_threshold]"]'
	);
	const customTreshold = customAmountInput.val();

	if ( ! customTreshold || 0 == customTreshold ) {
		customAmountInput.addClass( 'disabled' );
	}

	$( document ).on(
		'change',
		'[name="devnet_fsl_general[enable_custom_threshold]"]',
		function () {
			if ( $( this ).is( ':checked' ) ) {
				customAmountInput.removeClass( 'disabled' );
			} else {
				customAmountInput.addClass( 'disabled' );
				customAmountInput.attr( 'value', '' );
			}
		}
	);

	const multilingualBar = $( '[name="devnet_fsl_bar[multilingual]"]' );
	const showMessageAfter = $(
		'[name="devnet_fsl_bar[show_qualified_message]"]'
	);
	const titleInput = $( '[name="devnet_fsl_bar[title]"]' );
	const descriptionInput = $( '[name="devnet_fsl_bar[description]"]' );
	const qualifiedMessageInput = $(
		'[name="devnet_fsl_bar[qualified_message]"]'
	);
	const qualifiedMessageRow = $( 'tr.qualified_message' );

	if ( multilingualBar.is( ':checked' ) ) {
		titleInput.addClass( 'disabled' );
		descriptionInput.addClass( 'disabled' );
		qualifiedMessageInput.addClass( 'disabled' );
	}

	$( multilingualBar ).on( 'change', function () {
		if ( $( this ).is( ':checked' ) ) {
			titleInput.addClass( 'disabled' );
			descriptionInput.addClass( 'disabled' );
			qualifiedMessageInput.addClass( 'disabled' );
		} else {
			titleInput.removeClass( 'disabled' );
			descriptionInput.removeClass( 'disabled' );
			qualifiedMessageInput.removeClass( 'disabled' );
		}
	} );

	if ( showMessageAfter.is( ':checked' ) ) {
		qualifiedMessageRow.show();
	} else {
		qualifiedMessageRow.hide();
	}

	$( showMessageAfter ).on( 'change', function () {
		if ( $( this ).is( ':checked' ) ) {
			qualifiedMessageRow.show();
		} else {
			qualifiedMessageRow.hide();
		}
	} );

	const multilingualLabel = $( '[name="devnet_fsl_label[multilingual]"]' );
	const labeltextInput = $( '[name="devnet_fsl_label[text]"]' );

	if ( multilingualLabel.is( ':checked' ) ) {
		labeltextInput.addClass( 'disabled' );
	}

	$( multilingualLabel ).on( 'change', function () {
		if ( $( this ).is( ':checked' ) ) {
			labeltextInput.addClass( 'disabled' );
		} else {
			labeltextInput.removeClass( 'disabled' );
		}
	} );

	const infoRows = $( 'tr.info' );
	infoRows.map( ( _, row ) => {
		const $row = $( row );
		const infoLabel = $row.find( 'label' ).text();
		const infoDesc = $row.find( '.info-description' ).html();

		$row.html( `
		<td colspan="2">
			<span class="info-title">${ infoLabel }</span>
			<span class="info-description">${ infoDesc }</p>
		</td>
		` );
	} );

	$( '.info-description input' ).on( 'click', function () {
		$( this ).select();
	} );
} );
