<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @link       https://devnet.hr
 * @since      1.0.0
 *
 * @package    Devnet_FSL
 */

// If uninstall not called from WordPress, then exit.
if (!defined('WP_UNINSTALL_PLUGIN')) {
	exit;
}
