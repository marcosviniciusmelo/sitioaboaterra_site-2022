=== Free Shipping Label ===
Contributors: MarinMatosevic
Donate link: https://www.paypal.com/paypalme/marinmatosevic/
Tags: woocommerce, free shipping, amount left, progress bar, shipping, free, woo, flexible shipping, order revenue, order value, shipping costs, notification
Requires at least: 4.7
Tested up to: 5.9
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Increase order revenue by showing your customers just how close they are to your free shipping threshold.

== Description ==

Free shipping is important! Shipping costs are one of the major reasons shoppers abandon their cart. 
Around 70% of shoppers cancel their orders if shipping costs are too expensive.

Increase order revenue by showing your customers just how close they are to your free shipping threshold.
Many shoppers will more than likely add further items to their cart to qualify for free shipping. 

By having this notification, you’ll be able to help increase average order value as well as stopping shoppers from abandoning their cart and provide a smoother journey for the consumer.

= Features: =

* Hide shipping rates when free shipping is available
* Message after free shipping threshold is reached
* Show only for logged users
* Product Label for simple and variable products
* Customize Product Label
* Easy to use, no coding required
* Compatible with Flexible Shipping
* Display on the cart page
* Display in the mini cart widget
* Display on the checkout page
* Set your own title and description
* Free shipping progress bar
* Customize progress bar
* Animated progress bar
* Translation ready
* Responsive

== Installation ==

This plugin can be easily installed like any other WordPress integration by following the steps below:

1. Upload the plugin to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to the settings page: WooCommerce > Settings > Shipping > Free Shipping Label
4. Set up the plugin and when you're ready, enable it.

== Frequently Asked Questions ==

= How to setup and use =

This is WooCommerce add-on, and settings page is at: 
WooCommerce > Free Shipping Label
Set up the plugin and enable Progress Bar / Product Label.

= Mini cart not showing progress bar =

This is not working on some themes, or it is visible only when one item is removed from mini-cart.
Hopefully, it will be fixed soon.

= The Progress bar or the Label does not appear =

To offer the free shipping method it must be added to a Shipping Zone and you can add it to as many Shipping Zones as you like.

Go to: WooCommerce > Settings > Shipping.  
Select a Shipping Zone, click Add Shipping Method and a modal will display. Select Free Shipping from the dropdown and Add shipping method.

Then go to WooCommerce > Free Shipping Label > General Settings
Select Initial shipping zone. Enable and configure Progress bar / Product Label.

== Screenshots ==

1. Progress Bar
2. Product Label
2. Settings panel 1
3. Settings panel 2

== Changelog ==

= 2.3.0 =
* Enhancement - Initial shipping zone
* Fix - issues with showing labels before shipping method are chosen
* Fix - single product page: labels on variable products not showing up
* Fix - typos
* Performance – WP tested up to: 5.9
* Performance – WC tested up to: 6.1

= 2.2.3 =
* Dev - Added new `fsl_progress_bar_html` filter hook
* Dev - Added new `fsl_checkout_progress_bar_position` filter hook
* Performance – WC tested up to: 6.0
* Performance - Code optimization

= 2.2.2 =
* Fix - PHP Warning related to `fsl_shipping_method_min_amount` hook

= 2.2.1 =
* Fix - PHP Warning related to `fsl_flexible_shipping_min_amount` hook
* Dev - Added new `fsl_product_price` filter hook
* Performance – WC tested up to: 5.7

= 2.2.0 =
* Tweak - Added additional classes to progress bar
* Fix - Gutenberg error
* Enhancement - Hide shipping rates when free shipping is available
* Performance – WC tested up to: 5.6
* Dev - Added new `fsl_min_amount` filter hook
* Dev - Added new `fsl_free_shipping_instance_key` filter hook
* Dev - Added new `fsl_shipping_method_min_amount` filter hook
* Dev - Added new `fsl_flexible_shipping_min_amount` filter hook

= 2.1.1 =
* Fix - Animated progress bar

= 2.1.0 =
* Fix - Minor bug fixes
* Fix – minor style issues
* Enhancement - Disable for logged out users
* Enhancement - Message after free shipping threshold is reached
* Performance – WC tested up to: 5.2
* Update - pot file

= 2.0.3 =
* Fix – minor style issues
* Performance – WC tested up to: 4.9

= 2.0.2 =
* Fix – Syntax error
* Performance – WC tested up to: 4.8

= 2.0.1 =
* Enhancement - Multilingual support
* Update - pot file
* Fix - Minor bug fixes
* Performance - Code optimization
* Performance - Tested with latest WooCommerce version (4.6.1)

= 2.0.0 =
* Enhancement - Added product label
* Enhancement - New plugin menu page and tab pages
* Performance - Refactored code, queries, and options

= 1.0.1 =
* Fix - loading translations
* Update - textdomain

= 1.0. =
* First Launch

== Upgrade Notice ==

= 2.3.0 =
* Enhancement - Initial shipping zone
* Fix - issues with showing labels before shipping method are chosen
* Fix - single product page: labels on variable products not showing up
* Fix - typos
* Performance – WP tested up to: 5.9
* Performance – WC tested up to: 6.1

= 2.2.3 =
* Dev - Added new `fsl_progress_bar_html` filter hook
* Dev - Added new `fsl_checkout_progress_bar_position` filter hook
* Performance – WC tested up to: 6.0
* Performance - Code optimization

= 2.2.2 =
* Fix - PHP Warning related to `fsl_shipping_method_min_amount` hook

= 2.2.1 =
* Fix - PHP Warning related to `fsl_flexible_shipping_min_amount` hook
* Dev - Added new `fsl_product_price` filter hook
* Performance – WC tested up to: 5.7

= 2.2.0 =
* Tweak - Added additional classes to progress bar
* Fix - Gutenberg error
* Enhancement - Hide shipping rates when free shipping is available
* Performance – WC tested up to: 5.6
* Dev - Added new `fsl_min_amount` filter hook
* Dev - Added new `fsl_free_shipping_instance_key` filter hook
* Dev - Added new `fsl_shipping_method_min_amount` filter hook
* Dev - Added new `fsl_flexible_shipping_min_amount` filter hook

= 2.1.1 =
* Fix - Animated progress bar

= 2.1.0 =
* Fix - Minor bug fixes
* Fix – minor style issues
* Enhancement - Disable for logged out users
* Enhancement - Message after free shipping threshold is reached
* Performance – WC tested up to: 5.2
* Update - pot file

= 2.0.3 =
* Fix – minor style issues
* Performance – WC tested up to: 4.9

= 2.0.2 =
* Fix – Syntax error
* Performance – WC tested up to: 4.8

= 2.0.1 =
* Enhancement - Multilingual support
* Update - pot file
* Fix - Minor bug fixes
* Performance - Code optimization
* Performance - Tested with latest WooCommerce version (4.6.1)

= 2.0.0 =
* Enhancement - Added product label
* Enhancement - New plugin menu page and tab pages
* Performance - Refactored code, queries, and options

= 1.0.1 =
* Fix - loading translations
* Update - textdomain

= 1.0. =
* First Launch