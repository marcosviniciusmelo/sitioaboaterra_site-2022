<?php

/**
 *
 * @link              https://devnet.hr
 * @since             1.0.0
 * @package           Devnet_FSL
 *
 * @wordpress-plugin
 * Plugin Name:       Free Shipping Label
 * Plugin URI:
 * Description:       Increase order revenue in WooCommerce store by showing your customers just how close they are to your free shipping threshold.
 * Version:           2.3.0
 * Author:            Devnet
 * Author URI:        https://devnet.hr
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       free-shipping-label
 * Domain Path:       /languages
 * WC requires at least: 3.5.1
 * WC tested up to: 6.1
 */


// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 */
define('DEVNET_FSL_VERSION', '2.3.0');

define('DEVNET_FSL_NAME', 'Free Shipping Label');
define('DEVNET_FSL_SLUG', plugin_basename(__FILE__));


/**
 * Default settings class
 */
require_once plugin_dir_path(__FILE__) . 'includes/fsl-defaults.php';

/**
 * The code that runs during plugin initiation.
 */
add_action('init', function () {
	require_once plugin_dir_path(__FILE__) . 'includes/class-fsl-activator.php';
	Devnet_FSL_Activator::check_and_format_options();
});


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-devnet_fsl-activator.php
 */
function activate_devnet_fsl()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-fsl-activator.php';
	Devnet_FSL_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-devnet_fsl-deactivator.php
 */
function deactivate_devnet_fsl()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-fsl-deactivator.php';
	Devnet_FSL_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_devnet_fsl');
register_deactivation_hook(__FILE__, 'deactivate_devnet_fsl');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-fsl.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_devnet_fsl()
{
	$plugin = new Devnet_FSL();
	$plugin->run();
}

add_action('plugins_loaded', function () {

	// Go ahead only if WooCommerce is activated
	if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

		run_devnet_fsl();
	} else {
		// no woocommerce :(
		add_action('admin_notices', function () {
			$class = 'notice notice-error';
			$message = __('The “Free Shipping Label” plugin cannot run without WooCommerce. Please install and activate WooCommerce plugin.', 'free-shipping-label');

			printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
		});

		return;
	}
});