<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://devnet.hr
 * @since      1.0.0
 *
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/public
 * @author     Devnet <info@devnet.hr>
 */
class Devnet_FSL_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		wp_enqueue_style('free-shipping-label-public', plugin_dir_url(__DIR__) . 'assets/build/fsl-public.css', [], $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		// $script_asset_path = plugin_dir_url(__DIR__) . 'assets/build/fsl-public.asset.php';
		// $script_info       = file_exists($script_asset_path)
		// 	? include $script_asset_path
		// 	: ['dependencies' => ['jquery'], 'version' => $this->version];

		// wp_enqueue_script(
		// 	'fsl-public',
		// 	plugin_dir_url(__DIR__) . 'assets/build/fsl-public.js',
		// 	$script_info['dependencies'],
		// 	$script_info['version'],
		// 	true
		// );
	}


	/**
	 * Build Free shipping bar and set styles.
	 * 
	 * @since    1.0.0
	 */
	public function free_shipping_bar()
	{
		$amount_for_free_shipping = Devnet_FSL_Helper::get_free_shipping_min_amount();

		if (!$amount_for_free_shipping) return;

		$show_shipping_before_address = WC()->cart->show_shipping();

		if (!$show_shipping_before_address) return;

		$cart_total 		 = WC()->cart->get_displayed_subtotal();
		$discount 		     = WC()->cart->get_discount_total();
		$discount_tax 		 = WC()->cart->get_discount_tax();
		$price_including_tax = WC()->cart->display_prices_including_tax();
		$price_decimal 		 = wc_get_price_decimals();

		$defaults = Devnet_FSL_Defaults::bar();
		$fsl_bar = get_option('devnet_fsl_bar');
		$ignore_cupon      = isset($fsl_bar['ignore_cupon']) ? $fsl_bar['ignore_cupon'] : $defaults['ignore_cupon'];
		$local_pickup      = isset($fsl_bar['local_pickup']) ? $fsl_bar['local_pickup'] : $defaults['local_pickup'];
		$multilingual 	   = isset($fsl_bar['multilingual']) ? $fsl_bar['multilingual'] : $defaults['multilingual'];
		$border_shadow 	   = isset($fsl_bar['hide_border_shadow']) ? $fsl_bar['hide_border_shadow'] : $defaults['hide_border_shadow'];
		$disable_animation = isset($fsl_bar['disable_animation']) ? $fsl_bar['disable_animation'] : $defaults['disable_animation'];
		$bar_border_color  = isset($fsl_bar['bar_border_color']) ? $fsl_bar['bar_border_color'] : $defaults['bar_border_color'];
		$bar_bg_color 	   = isset($fsl_bar['bar_bg_color']) ? $fsl_bar['bar_bg_color'] : $defaults['bar_bg_color'];
		$bar_inner_color   = isset($fsl_bar['bar_inner_color']) ? $fsl_bar['bar_inner_color'] : $defaults['bar_inner_color'];
		$bar_height 	   = isset($fsl_bar['bar_height']) ? $fsl_bar['bar_height'] : $defaults['bar_height'];
		$title 			   = isset($fsl_bar['title']) ? $fsl_bar['title'] : $defaults['title'];
		$description 	   = isset($fsl_bar['description']) ? $fsl_bar['description'] : $defaults['description'];
		$show_qualified_msg = isset($fsl_bar['show_qualified_message']) ? $fsl_bar['show_qualified_message'] : $defaults['show_qualified_message'];
		$qualified_message  = isset($fsl_bar['qualified_message']) && $fsl_bar['qualified_message'] ? $fsl_bar['qualified_message'] : $defaults['qualified_message'];

		if ($multilingual) {
			$title            = $defaults['title'];
			$description      = $defaults['description'];
			$qualified_message = $defaults['qualified_message'];
		}

		if (!$local_pickup && Devnet_FSL_Helper::starts_with(Devnet_FSL_Helper::chosen_shipping_method(), 'local_pickup')) {
			return;
		}

		if ($ignore_cupon) {
			$discount = 0;
			$discount_tax = 0;
		}

		if ($price_including_tax) {
			$cart_total = round($cart_total - ($discount + $discount_tax), $price_decimal);
		} else {
			$cart_total = round($cart_total - $discount, $price_decimal);
		}

		$remaining = $amount_for_free_shipping - $cart_total;
		$percent   = 100 - ($remaining / $amount_for_free_shipping) * 100;

		$title = $this->convert_placeholders($title, $remaining, $amount_for_free_shipping);
		$description = $this->convert_placeholders($description, $remaining, $amount_for_free_shipping);
		$qualified_message = $this->convert_placeholders($qualified_message, $remaining, $amount_for_free_shipping);

		$output = '';

		if ($cart_total < $amount_for_free_shipping) {

			$classes  = 'devnet_fsl-free-shipping ';
			$classes .= $border_shadow  ? 'devnet_fsl-no-shadow ' : '';
			$classes .= $disable_animation ? 'devnet_fsl-no-animation ' : '';

			$style_pb = 'style="';
			if ($bar_bg_color) {
				$style_pb .= 'background-color:' . $bar_bg_color . ';';
			}
			if ($bar_border_color) {
				$style_pb .= 'border-color:' . $bar_border_color . ';';
			}
			$style_pb .= '"';

			$progress_bar  = '';
			$progress_bar .= '<div class="' . $classes . '">';
			$progress_bar .= '<h4 class="fsl-title title">' . $title . '</h4>';
			$progress_bar .= '<div class="fsl-progress-bar progress-bar shine stripes" ' . $style_pb . '>';
			$progress_bar .= '<span class="fsl-progress-amount progress-amount" style="width:' . $percent . '%; height:' . $bar_height . 'px; background-color:' . $bar_inner_color . ';""></span>';
			$progress_bar .= '</div>';
			$progress_bar .= '<span class="fsl-notice notice">';
			$progress_bar .= $description;
			$progress_bar .= '</span>';
			$progress_bar .= '</div>';

			$output = $progress_bar;
		} else {

			if ($show_qualified_msg) {

				$classes  = 'devnet_fsl-free-shipping qualified-message ';
				$classes .= $border_shadow  ? 'devnet_fsl-no-shadow ' : '';

				$message  = '';
				$message .= '<div class="' . $classes . '">';
				$message .= '<h4 class="title">' . $qualified_message . '</h4>';
				$message .= '</div>';

				$output = $message;
			}
		}

		echo apply_filters('fsl_progress_bar_html', $output);
	}


	/**
	 * Convert placeholders to strings.
	 * 
	 * @since    2.1.0
	 */
	public function convert_placeholders($input_string = '', $remaining = null, $amount_for_free_shipping = null)
	{

		if ($remaining) {
			$input_string = str_replace('{remaining}', wc_price($remaining), $input_string);
		}

		if ($amount_for_free_shipping) {
			$input_string = str_replace('{free_shipping_amount}', wc_price($amount_for_free_shipping), $input_string);
		}

		return $input_string;
	}



	/**
	 * Show a message at the cart/checkout displaying
	 * how much to go for free shipping.
	 */
	public function free_shipping_notice()
	{

		if (!is_cart() && !is_checkout()) {
			return;
		}

		$amount_for_free_shipping = Devnet_FSL_Helper::get_free_shipping_min_amount();

		$cart_total = WC()->cart->get_displayed_subtotal();

		if (WC()->cart->display_prices_including_tax()) {

			$cart_total = round($cart_total - (WC()->cart->get_discount_total() + WC()->cart->get_discount_tax()), wc_get_price_decimals());
		} else {

			$cart_total = round($cart_total - WC()->cart->get_discount_total(), wc_get_price_decimals());
		}

		if ($amount_for_free_shipping && $cart_total < $amount_for_free_shipping) {
			$remaining = $amount_for_free_shipping - $cart_total;
			wc_add_notice(sprintf('Add %s more to get free shipping!', wc_price($remaining)));
		}
	}

	/**
	 * Add sufix (free shipping label) to product after the price.
	 * 
	 * @since    2.0.0
	 */
	public function get_price_html($price_html, $product)
	{
		if (is_admin() && !wp_doing_ajax()) return $price_html;
		if ($product->is_virtual() || $product->is_downloadable()) return $price_html;

		$fsl_label = get_option('devnet_fsl_label');
		$show_on_single_simple_product   = isset($fsl_label['show_on_single_simple_product']) ? $fsl_label['show_on_single_simple_product'] : false;
		$show_on_single_variable_product = isset($fsl_label['show_on_single_variable_product']) ? $fsl_label['show_on_single_variable_product'] : false;
		$show_on_single_variation        = isset($fsl_label['show_on_single_variation']) ? $fsl_label['show_on_single_variation'] : false;
		$show_on_list_variable_products  = isset($fsl_label['show_on_list_variable_products']) ? $fsl_label['show_on_list_variable_products'] : false;
		$show_on_list_simple_products    = isset($fsl_label['show_on_list_simple_products']) ? $fsl_label['show_on_list_simple_products'] : false;

		$type = $product->get_type();

		if ('variable' === $type) {
			$min_var_reg_price  = $product->get_variation_regular_price('min', true);
			$min_var_sale_price = $product->get_variation_sale_price('min', true);

			$price = $min_var_sale_price ? $min_var_sale_price : $min_var_reg_price;
		} else {
			$regular_price = $product->get_regular_price();
			$sale_price    = $product->get_sale_price();

			$price = $sale_price ? $sale_price : $regular_price;
		}

		$price = apply_filters('fsl_product_price', $price);

		$fsl = '';

		if (is_product()) {
			if (
				'simple' === $type && $show_on_single_simple_product ||
				'variable' === $type && $show_on_single_variable_product ||
				'variation' === $type && $show_on_single_variation
			) {
				$fsl = $this->product_label_html($price);
			}
		} else {
			if ('simple' === $type && $show_on_list_simple_products || 'variable' === $type && $show_on_list_variable_products) {
				$fsl = $this->product_label_html($price);
			}
		}

		return $price_html . $fsl;
	}

	/**
	 * Product label html and styles.
	 * 
	 * @since    2.0.0
	 */
	public function product_label_html($price = 0)
	{
		$amount_for_free_shipping = Devnet_FSL_Helper::get_free_shipping_min_amount();

		$fsl  = '';

		if ($price && $amount_for_free_shipping && $price >= $amount_for_free_shipping) {

			$defaults = Devnet_FSL_Defaults::label();
			$fsl_label = get_option('devnet_fsl_label');
			$text_color    = isset($fsl_label['text_color']) ? $fsl_label['text_color'] : $defaults['text_color'];
			$bg_color 	   = isset($fsl_label['bg_color']) ? $fsl_label['bg_color'] : $defaults['bg_color'];
			$hide_border_shadow = isset($fsl_label['hide_border_shadow']) ? $fsl_label['hide_border_shadow'] : $defaults['hide_border_shadow'];
			$multilingual  = isset($fsl_label['multilingual']) ? $fsl_label['multilingual'] : $defaults['multilingual'];
			$text 		   = isset($fsl_label['text']) ? $fsl_label['text'] : $defaults['text'];

			if ($multilingual) $text = $defaults['text'];

			$styles = 'style="';
			if ($text_color) {
				$styles .= 'color:' . $text_color . ';';
			}
			if ($bg_color) {
				$styles .= 'background-color:' . $bg_color . ';';
			}
			if ($hide_border_shadow) {
				$styles .= 'box-shadow:none;';
			}
			$styles .= '"';


			$fsl .= '<span class="devnet_fsl-label" ' . $styles . '>';
			$fsl .= $text;
			$fsl .= '</span>';
		}

		return $fsl;
	}


	/**
	 * Hide shipping rates when free shipping is available.
	 *
	 * @param array $rates Array of rates found for the package.
	 * @return array
	 */
	public function hide_shipping_when_free_is_available($rates)
	{
		$fsl_general = get_option('devnet_fsl_general');
		$hide_shipping_rates = isset($fsl_general['hide_shipping_rates']) ? $fsl_general['hide_shipping_rates'] : false;

		if (!$hide_shipping_rates) return $rates;

		$free = array();

		if ('hide_all' === $hide_shipping_rates) {
			foreach ($rates as $rate_id => $rate) {
				if ('free_shipping' === $rate->method_id) {
					$free[$rate_id] = $rate;
					break;
				}
			}
			return !empty($free) ? $free : $rates;
		}

		if ('hide_except_local' === $hide_shipping_rates) {
			foreach ($rates as $rate_id => $rate) {
				// Only modify rates if free_shipping is present.
				if ('free_shipping' === $rate->method_id) {
					$new_rates[$rate_id] = $rate;
					break;
				}
			}

			if (!empty($new_rates)) {
				//Save local pickup if it's present.
				foreach ($rates as $rate_id => $rate) {
					if ('local_pickup' === $rate->method_id) {
						$new_rates[$rate_id] = $rate;
						break;
					}
				}
				return $new_rates;
			}
		}

		return $rates;
	}
}