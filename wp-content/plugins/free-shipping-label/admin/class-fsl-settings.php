<?php

/**
 * The admin settings of the plugin.
 *
 * @link       https://devnet.hr
 * @since      2.0.0
 *
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/admin
 */

/**
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/admin
 * @author     Devnet <info@devnet.hr>
 */

class Devnet_FSL_Settings
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;


    private $settings_api;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        $this->settings_api = new Devnet_FSL_Settings_API;
    }

    public function admin_init()
    {

        //set the settings
        $this->settings_api->set_sections($this->get_settings_sections());
        $this->settings_api->set_fields($this->get_settings_fields());

        //initialize settings
        $this->settings_api->admin_init();
    }

    public function admin_menu()
    {
        add_submenu_page(
            'woocommerce',
            'Free Shipping Label',
            'Free Shipping Label',
            'manage_options',
            'free-shipping-label-settings',
            array($this, 'plugin_page')
        );
    }

    public function get_settings_sections()
    {
        $sections = array(
            array(
                'id'    => 'devnet_fsl_general',
                'title' => __('General Settings', 'free-shipping-label')
            ),
            array(
                'id'    => 'devnet_fsl_bar',
                'title' => __('Progress Bar', 'free-shipping-label')
            ),
            array(
                'id'    => 'devnet_fsl_label',
                'title' => __('Product Label', 'free-shipping-label')
            ),
        );
        return $sections;
    }

    /**
     * Returns all the settings fields
     *
     * @return array settings fields
     */
    public function get_settings_fields()
    {
        $settings_fields = array(
            'devnet_fsl_general' => array(
                array(
                    'type'     => 'select',
                    'name'     => 'initial_zone',
                    'label'    => __('Initial shipping zone', 'free-shipping-label'),
                    'desc'     => __("This zone's free shipping threshold will be used only if customer didn't already enter address.", 'free-shipping-label'),
                    'options'  => Devnet_FSL_Helper::shipping_zones_option_list(),
                    'default'  => '1'
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'enable_custom_threshold',
                    'label' => __('Enable Custom threshold', 'free-shipping-label'),
                    'desc'  => __('This will ignore free shipping threshold in WooCommerce settings.', 'free-shipping-label'),
                ),
                array(
                    'type'              => 'number',
                    'name'              => 'custom_threshold',
                    'label'             => __('Custom threshold', 'free-shipping-label'),
                    'desc'              => __('This will be used only to determine if we should show the bar / label.', 'free-shipping-label'),
                    'placeholder'       => __('Custom threshold', 'free-shipping-label'),
                    'min'               => 0,
                    //'max'               => 100,
                    'step'              => '1',
                    'sanitize_callback' => 'absint'
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'only_logged_users',
                    'label' => __('Visibility', 'free-shipping-label'),
                    'desc'  => __('Show only for logged in users.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'select',
                    'name'  => 'hide_shipping_rates',
                    'label' => __('Hide shipping rates when free shipping is available?', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                    'options' => [
                        ''                  => __('No', 'free-shipping-label'),
                        'hide_all'          => __('Hide all other shipping methods and only show "Free Shipping"', 'free-shipping-label'),
                        'hide_except_local' => __('Hide all other shipping methods and only show "Free Shipping" and "Local Pickup"', 'free-shipping-label'),
                    ]
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'delete_options',
                    'label' => __('Delete plugin data on deactivation', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
            ),

            'devnet_fsl_bar' => array(
                array(
                    'type'  => 'checkbox',
                    'name'  => 'enable_bar',
                    'label' => __('Enable Progress Bar', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'ignore_cupon',
                    'label' => __('Ignore cupon', 'free-shipping-label'),
                    'desc'  => __('Ignore applied cupons.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'local_pickup',
                    'label' => __('Enable on Local Pickup', 'free-shipping-label'),
                    'desc'  => __('Show bar if Local Pickup is selected.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-bar-positions',
                    'label' => __('Positions', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                    'class' => 'info',
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_checkout',
                    'label' => __('Display on the checkout page', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_cart',
                    'label' => __('Display on the cart page', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_minicart',
                    'label' => __('Display in the mini cart widget', 'free-shipping-label'),
                    'desc'  => __('On some themes, this option does not work properly.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-bar-text',
                    'label' => __('Text', 'free-shipping-label'),
                    'class' => 'info',
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'multilingual',
                    'label' => __('Multilingual', 'free-shipping-label'),
                    'desc'  => __('Use your own translated strings.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-bar-placeholders',
                    'label' => __('', 'free-shipping-label'),
                    'desc'  => sprintf(
                        '%s<br><br><input type="text" readonly="readonly" value="{free_shipping_amount}" />%s<br><br><input type="text" readonly="readonly" value="{remaining}" />%s',
                        __('Placeholders: ', 'free-shipping-label'),
                        __(' Amount for free shipping.', 'free-shipping-label'),
                        __(' Remaining amount.', 'free-shipping-label')
                    ),
                    'class' => 'info subinfo',
                ),
                array(
                    'type'        => 'text',
                    'name'        => 'title',
                    'label'       => __('Title', 'free-shipping-label'),
                    'desc'        => __('', 'free-shipping-label'),
                    'placeholder' => __('', 'free-shipping-label'),
                    //'sanitize_callback' => 'sanitize_text_field',
                ),
                array(
                    'type'        => 'text',
                    'name'        => 'description',
                    'label'       => __('Description', 'free-shipping-label'),
                    'desc'        => __('', 'free-shipping-label'),
                    'placeholder' => __('', 'free-shipping-label'),
                    //'sanitize_callback' => 'sanitize_text_field',
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_qualified_message',
                    'label' => __('Show message after free shipping threshold is reached', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'        => 'text',
                    'name'        => 'qualified_message',
                    'label'       => __('Qualified for free shipping message', 'free-shipping-label'),
                    'desc'        => __('', 'free-shipping-label'),
                    'placeholder' => __('', 'free-shipping-label'),
                    'sanitize_callback' => 'sanitize_text_field',
                    'max'         => 100
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-bar-design',
                    'label' => __('Design', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                    'class' => 'info',
                ),
                array(
                    'type'    => 'color',
                    'name'    => 'bar_inner_color',
                    'label'   => __('Progress bar inner color', 'free-shipping-label'),
                    'desc'    => __('', 'free-shipping-label'),
                ),
                array(
                    'type'    => 'color',
                    'name'    => 'bar_bg_color',
                    'label'   => __('Progress bar background color', 'free-shipping-label'),
                    'desc'    => __('', 'free-shipping-label'),
                ),
                array(
                    'type'    => 'color',
                    'name'    => 'bar_border_color',
                    'label'   => __('Progress bar border color', 'free-shipping-label'),
                    'desc'    => __('', 'free-shipping-label'),
                ),
                array(
                    'type'              => 'number',
                    'name'              => 'bar_height',
                    'label'             => __('Progress bar height', 'free-shipping-label'),
                    'desc'              => __('Height in pixels (px)', 'free-shipping-label'),
                    'placeholder'       => __('height in px', 'free-shipping-label'),
                    'min'               => 0,
                    //'max'               => 300,
                    'step'              => '1',
                    'sanitize_callback' => 'absint',
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'disable_animation',
                    'label' => __('Disable animation', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'hide_border_shadow',
                    'label' => __('Hide border shadow', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
            ),

            'devnet_fsl_label' => array(
                array(
                    'type'  => 'checkbox',
                    'name'  => 'enable_label',
                    'label' => __('Enable Product Label', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-label-single-product',
                    'label' => __('Single product page', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                    'class' => 'info',
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_single_simple_product',
                    'label' => __('Enable for simple products', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_single_variable_product',
                    'label' => __('Enable for variable products', 'free-shipping-label'),
                    'desc'  => __('The label will be displayed only if the lowest variation price is qualified for free shipping.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_single_variation',
                    'label' => __('Enable for single variation', 'free-shipping-label'),
                    'desc'  => __('Customer needs to select a variation first.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-label-listed-products',
                    'label' => __('Listed products', 'free-shipping-label'),
                    'desc'  => __('Main shop page, category pages, archive pages, etc.', 'free-shipping-label'),
                    'class' => 'info',
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_list_simple_products',
                    'label' => __('Enable for simple products', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'show_on_list_variable_products',
                    'label' => __('Enable for variable products', 'free-shipping-label'),
                    'desc'  => __('The label will be displayed only if the lowest variation price is qualified for free shipping.', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-label-text',
                    'label' => __('Text', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                    'class' => 'info',
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'multilingual',
                    'label' => __('Multilingual', 'free-shipping-label'),
                    'desc'  => __('Use your own translated strings.', 'free-shipping-label'),
                ),
                array(
                    'type'              => 'text',
                    'name'              => 'text',
                    'label'             => __('Label Text', 'free-shipping-label'),
                    'desc'              => __('', 'free-shipping-label'),
                    'placeholder'       => __('', 'free-shipping-label'),
                    'max'               => 25,
                    'sanitize_callback' => 'sanitize_text_field',
                ),
                array(
                    'type'  => 'info',
                    'name'  => 'info-label-design',
                    'label' => __('Design', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                    'class' => 'info',
                ),
                array(
                    'type'  => 'color',
                    'name'  => 'text_color',
                    'label' => __('Text color', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'color',
                    'name'  => 'bg_color',
                    'label' => __('Background color', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
                array(
                    'type'  => 'checkbox',
                    'name'  => 'hide_border_shadow',
                    'label' => __('Hide border shadow', 'free-shipping-label'),
                    'desc'  => __('', 'free-shipping-label'),
                ),
            ),

        );

        return $settings_fields;
    }

    public function plugin_page()
    {
        echo '<div class="fsl-wrap">';

        $this->settings_api->show_navigation();
        $this->settings_api->show_forms();

        echo '</div>';
    }

    /**
     * Get all the pages
     *
     * @return array page names with key value pairs
     */
    public function get_pages()
    {
        $pages = get_pages();
        $pages_options = array();
        if ($pages) {
            foreach ($pages as $page) {
                $pages_options[$page->ID] = $page->post_title;
            }
        }

        return $pages_options;
    }
}