<?php

/**
 * The plugin helper functions
 *
 * @link       https://devnet.hr
 * @since      1.0.0
 *
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/includes
 */

/**
 * The plugin helper functions
 *
 * Defines the plugin name, version.
 *
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/includes
 * @author     Devnet <info@devnet.hr>
 */
class Devnet_FSL_Helper
{

    /**
     * Check if string starts with string.
     *
     * @since    1.0.0
     * @return   boolean
     */
    static function starts_with($string, $start_string)
    {
        $len = strlen($start_string);
        return (substr($string, 0, $len) === $start_string);
    }


    /**
     * Get information about shipping method.
     *
     * @since    2.1.0
     * @param    string    $shipping_id       The id of the method.
     * @return   number                                 
     */
    static function get_shipping_method_min_amount($shipping_id)
    {

        $packages = WC()->shipping->get_packages();

        $amount = null;

        foreach ($packages as $key => $package) {

            if (isset($package['rates']) && isset($package['rates'][$shipping_id])) {
                $rate = $package['rates'][$shipping_id];

                $meta = $rate->get_meta_data();

                // Flexible Shipping
                if (isset($meta['_fs_method']['method_free_shipping'])) {
                    $amount = $meta['_fs_method']['method_free_shipping'] ?: null;
                }
            }
        }

        return apply_filters('fsl_shipping_method_min_amount', $amount, $shipping_id);
    }

    /**
     * Get chosen shipping method.
     *
     * @since    2.1.0
     */
    static function chosen_shipping_method()
    {

        $wc_session = isset(WC()->session) ? WC()->session : null;

        if (!$wc_session) return;

        $chosen_methods = $wc_session->get('chosen_shipping_methods');

        if (!$chosen_methods) return null;

        $chosen_shipping_id = $chosen_methods ? $chosen_methods[0] : '';

        return $chosen_shipping_id;
    }

    /**
     * Get minimal amount for free shipping.
     *
     * @since    1.0.0
     * @return   number  
     */
    static function get_free_shipping_min_amount()
    {

        $amount = null;


        $general_options = get_option('devnet_fsl_general');
        $initial_zone  = isset($general_options['initial_zone']) ? $general_options['initial_zone'] : '1';
        $enable_custom_threshold  = isset($general_options['enable_custom_threshold']) ? $general_options['enable_custom_threshold'] : false;
        $custom_threshold  = isset($general_options['custom_threshold']) ? $general_options['custom_threshold'] : false;

        if ($enable_custom_threshold) {
            $amount =  $custom_threshold ?: $amount;

            return $amount;
        }

        $chosen_shipping_id = self::chosen_shipping_method();

        //if (!$chosen_shipping_id) return;

        $is_flexible_shipping = self::starts_with($chosen_shipping_id, 'flexible_shipping');
        // $is_free_shipping = self::starts_with($chosen_shipping_id, 'free_shipping');
        // $is_local_pickup = self::starts_with($chosen_shipping_id, 'local_pickup');
        // $is_flate_rate = self::starts_with($chosen_shipping_id, 'flat_rate');


        if ($is_flexible_shipping) {

            $option_name = 'woocommerce_' . str_replace(':', '_', $chosen_shipping_id) . '_settings';
            $option = get_option($option_name);

            $amount = isset($option['method_free_shipping']) ? $option['method_free_shipping'] : null;

            $amount = apply_filters('fsl_flexible_shipping_min_amount', $amount, $chosen_shipping_id);


            // Return $amount from options if exists,otherwise go through packages and find FlexibleShipping.
            return $amount ? $amount : self::get_shipping_method_min_amount($chosen_shipping_id);
        }

        $packages = WC()->cart->get_shipping_packages();
        $package = reset($packages);
        $zone = wc_get_shipping_zone($package);

        $known_customer = self::destination_info_exists($package);

        if (!$known_customer && $initial_zone || $initial_zone == 0) {
            $zone = WC_Shipping_Zones::get_zone_by('zone_id', $initial_zone);
        }

        $amount = null;

        foreach ($zone->get_shipping_methods(true) as $key => $method) {

            if ($method->id === 'free_shipping') {
                $instance = isset($method->instance_settings) ? $method->instance_settings : null;

                $min_amount_key = apply_filters('fsl_free_shipping_instance_key', 'min_amount');

                $amount = isset($instance[$min_amount_key]) ? $instance[$min_amount_key] : null;

                // If filter fails, go back to default 'min_amount' key.
                if (!$amount && isset($instance['min_amount'])) {
                    $amount = $instance['min_amount'];
                }
            }
        }


        return apply_filters('fsl_min_amount', $amount);
    }

    /**
     * Check package to determine if is a returning customer.
     * 
     * TODO: better check on more parameters.
     */
    static function destination_info_exists($package = [])
    {
        $country  = isset($package['destination']['country']) ? $package['destination']['country'] : null;
        $state    = isset($package['destination']['state']) ? $package['destination']['state'] : null;
        $postcode = isset($package['destination']['postcode']) ? $package['destination']['postcode'] : null;
        $city     = isset($package['destination']['city']) ? $package['destination']['city'] : null;


        // If country is set to AF - this is probably default selection for the first country on the list.
        // Just to be sure, we'll check if city is empty or not.
        if ($country === 'AF' && !$city) {
            $country = null;
        }

        $exists = true;

        // If there's no country, state and postcode, we are probably dealing with "first-timer" or
        // a customer that hasn't filled out checkout form recently.
        if (!$country && !$state && !$postcode) {
            $exists = false;
        }

        return $exists;
    }


    static function shipping_zones_option_list()
    {

        $zones = WC_Shipping_Zones::get_zones();

        $options = ['' => __('-- None --', 'free-shipping-label')];

        foreach ($zones as $key => $zone) {
            $id = isset($zone['zone_id']) ? $zone['zone_id'] : null;
            $name = isset($zone['zone_name']) ? $zone['zone_name'] : null;

            if ($id && $name) {
                $options[$id] = $name;
            }
        }

        return $options;
    }
}