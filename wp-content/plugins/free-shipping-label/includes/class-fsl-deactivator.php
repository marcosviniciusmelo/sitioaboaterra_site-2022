<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://devnet.hr
 * @since      1.0.0
 *
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Devnet_FSL
 * @subpackage Devnet_FSL/includes
 * @author     Devnet <info@devnet.hr>
 */
class Devnet_FSL_Deactivator
{

	/**
	 * @since    1.0.0
	 */
	public static function deactivate()
	{
		$general_options = get_option('devnet_fsl_general');
		$delete_options  = isset($general_options['delete_options']) ? $general_options['delete_options'] : false;

		if ($delete_options) {
			delete_option('devnet_fsl');
			delete_option('devnet_fsl_general');
			delete_option('devnet_fsl_bar');
			delete_option('devnet_fsl_label');
		}
	}
}