<div class="wrap" id="hp_woo_rewards_dashboard">
	<h1 class="wp-heading-inline"><?php esc_html_e( 'Hostplugin - Woocommerce Rewards & Points Dashboard', 'hp-woo-rewards' ); ?></h1>

<?php
function page_tabs( $current = 'first' ) {
    $tabs = array(
        'tutorial'   => 'Tutorial'
    );

    if (HOSTPLUGIN_WOO_POINTS_LICENSE <> 'Premium') {
    	$tabs['premium'] = 'Premium Version';
    }
    else {
    	$tabs['support'] = 'Premium Support';
    }

    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=hp_woo_rewards_points_dashboard&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'tutorial';
page_tabs( $tab );

if ( $tab == 'tutorial' ) {
?>
<div class="postbox-container fullsize">
	<div class="metabox-holder">
		<div class="meta-box-sortables ui-sortable">
			<div class="postbox">
				<h3 class="hndle"><span>Using Hostplugin - Woocommerce Rewards & Points</span></h3>
				<div class="inside vid-container">
					<iframe width="960" height="540" src="https://www.youtube.com/embed/-JQ056f7uAw?VQ=HD720" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
else if ($tab == 'premium') {
?>

<section>
	<ul>
		<li><img src="<?php echo HOSTPLUGIN_PLUGIN_URL; ?>/assets/images/favourite.svg" class="premium-logo"></li>
		<li>
			<h2>Premium Version</h2>
			Our premium version provides more features and functionalities that gives you more control over your WooCommerce store! To get a copy of our premium version simply donate at least US$25 and we will email you the plugin zip file within 1 business day. <a href="https://www.hostplugin.com/woo-comparison.php" target="_blank">Please take a look on the feature comparison to see how the Premium Version can reward your loyal customers even more!</a>
			<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="DY6X6XWJFH8MW">
			<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		</li>
	</ul>
</section>

<section>
	<ul>
		<li><img src="<?php echo HOSTPLUGIN_PLUGIN_URL; ?>/assets/images/female-customer.svg" class="premium-logo"></li>
		<li>
			<h2>Premium Support</h2>
		Premium license entitles you to <strong>lifetime</strong> update and 6 months premium support. Each installation of the plugin will require a licence key in order for you to receive updates and support.</li>
	</ul>
</section>

<section>
	<ul>
		<li><img src="<?php echo HOSTPLUGIN_PLUGIN_URL; ?>/assets/images/refund.png"></li>
		<li>
			<h2>Refund Feature</h2>
				- Option to remove points for refunded / cancelled orders<br>
				- Option to return redeemed points for refunded / cancelled orders
		</li>
	</ul>
</section>

<section>
	<ul>
		<li><img src="<?php echo HOSTPLUGIN_PLUGIN_URL; ?>/assets/images/message.png"></li>
		<li>
			<h2>Easy to Customize</h2>
		Customize the frontend messages that are shown on the product, cart, checkout & order received page. You can also change the points label so that customers could earn "Coins" or "Bucks" instead of "Points".</li>
	</ul>
</section>

<section>
	<ul>
		<li><img src="<?php echo HOSTPLUGIN_PLUGIN_URL; ?>/assets/images/discount-tag.svg" class="premium-logo"></li>
		<li>
			<h2>Maximum Points Discount</h2>
		Completely control the maximum discount available when redeeming points.
	</li>
	</ul>
</section>

<section>
	<ul>
		<li><img src="<?php echo HOSTPLUGIN_PLUGIN_URL; ?>/assets/images/min-purchase-amount.png"></li>
		<li>
			<h2>Minimum Purchase Amount</h2>
		Ability to set the minimum purchase amount in order to redeem points.</li>
	</ul>
</section>

<section>
	<ul>
		<li><img src="<?php echo HOSTPLUGIN_PLUGIN_URL; ?>/assets/images/coupon.png"></li>
		<li>
			<h2>Coupon</h2>
		- Ability to disable points redemption when using coupons<br>
		- Ability to disable customers from earning points if coupons are use
		</li>
	</ul>
</section>

<?php    
}
else if ($tab == 'support') {
?>
<div class="postbox-container fullsize">
	<div class="metabox-holder">
		<div class="meta-box-sortables ui-sortable">
			<div class="postbox">
				<h3 class="hndle"><span>Premium Support</span></h3>
				<div class="inside vid-container">
					Please <a href="https://www.hostplugin.com/members/" target="_blank">Click Here</a>, login and create a ticket
				</div>
			</div>
		</div>
	</div>
</div>
<?php	
}

?>
	
</div>
<!-- wrap
