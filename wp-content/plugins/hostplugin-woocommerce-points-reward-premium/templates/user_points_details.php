<h2><?php esc_html_e( 'Woo Rewards', 'hp-woo-rewards' ); ?></h2>
<table class="form-table">
  <tbody><tr class="user-point-wrap">
  	<th><label for="point"><?php esc_html_e( 'Total de Pontos', 'hp-woo-rewards' ); ?></label></th>
  	<td>
      <?php
        $points = $this->points->get_customer_total_points($profileuser->ID);
      ?>
          <input type="number" step="1" name="hp_woo_reward_points" id="hp_woo_reward_points" value="<?php  esc_attr_e( $points, 'hp-woo-rewards' ); ?>" class="short">
          Reason: <input type="text" name="hp_woo_reward_points_update_reason" id="hp_woo_reward_points_update_reason" placeholder="Optional" class="regular-text">
  	</td>
  </tr>
  </tbody>
</table>

<?php
  $purchase_details = $this->points->get_customer_points_details($profileuser->ID);
  if (!empty($purchase_details)) {
?>

<table class="wp-list-table widefat fixed striped">
	<thead>
  	<tr>
  		<th scope="col" id="hp_woo_rewards_log_date" class="manage-column column-date column-primary">Date</th>
      <th scope="col" id="hp_woo_rewards_log_order_id" class="manage-column column-format">Order ID</th>
      <th scope="col" id="hp_woo_rewards_log_event" class="manage-column column-event">Event</th>
      <th scope="col" id="hp_woo_rewards_log_point" class="manage-column column-format">Points</th>
    </tr>
	</thead>
	<tbody>
    <?php

      foreach ($purchase_details as $detail) {
        $detail['event'] = $this->points->parse_point_name($detail['event']);
    ?>
        <tr>
          <td class="log_date column-date"><?php
          echo date_i18n( get_option( 'date_format' ), $detail['date'] )?></td>
          <td class="log_order_id column-order_id">
            <?php
            if (strlen($detail['order_id']) > 0) {
            ?>
              <a href="<?php echo admin_url( 'post.php?post='.$detail['order_id'].'&action=edit');?>">#<?php echo $detail['order_id']?></a>
           <?php
            }
            ?>
          </td>
          <td class="log_event column-event"><?php echo $detail['event']?></td>
          <td class="log_point column-point"><?php echo $detail['points']?></td>
        </tr>
    <?php
      }//foreach
    ?>
  </tbody>
	<tfoot>
    <tr>
  		<th scope="col" id="hp_woo_rewards_log_date" class="manage-column column-date column-primary">Date</th>
      <th scope="col" id="hp_woo_rewards_log_order_id" class="manage-column column-format">Order ID</th>
      <th scope="col" id="hp_woo_rewards_log_event" class="manage-column column-event">Event</th>
      <th scope="col" id="hp_woo_rewards_log_point" class="manage-column column-format">Points</th>
    </tr>
	</tfoot>
</table>
<?php
}//if log not empty
?>
