<?php
/**
 * @package  hostpluginWoocommercePointsRewards
 */

namespace HPWooRewardsIncludes\Admin;

class SettingsPremium extends Settings {

	/**
	 * Construct
	 *
	 * A place to add hooks and filters
	 *
	 * @since 1.0
	 *
	 */
	public function __construct() {
		parent::__construct();
		add_filter('hp_woo_rewards_validate_fields', array($this, 'validate_premium_fields'), 10, 2);
	}

	/**
	 *
	 * validate and set premium fields
	 *
	 * @since 1.0
	 * @param array $validated
	 * @param array $input
	 * @return array $validated
	 *
	 */
	public function validate_premium_fields($validated, $input) {
		$validated['points_name'] = (isset($input['points_name']) && !empty($input['points_name']))? $input['points_name'] : 'Points';

		$validated['signup_message'] = sanitize_textarea_field( $input['signup_message'] );
		$validated['single_product_message'] = sanitize_textarea_field( $input['single_product_message'] );
		$validated['cart_checkout_message'] = sanitize_textarea_field( $input['cart_checkout_message'] );
		$validated['guest_reminder_message'] = sanitize_textarea_field( $input['guest_reminder_message'] );
		$validated['thankyou_message'] = sanitize_textarea_field( $input['thankyou_message'] );
		$validated['total_points_message'] = sanitize_textarea_field( $input['total_points_message'] );
		$validated['min_purchase_amount_message'] = sanitize_textarea_field( $input['min_purchase_amount_message'] );

		return $validated;
	}

}