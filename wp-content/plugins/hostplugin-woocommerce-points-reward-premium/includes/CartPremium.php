<?php
/**
 * @package  hostpluginWoocommercePointsRewards
 */

namespace HPWooRewardsIncludes;

Use \HPWooRewardsIncludes\Cart;

/**
 * CartPremium class, @see Cart
 */
class CartPremium extends Cart {

	/**
	 * Construct
	 *
	 * A place to add hooks and filters
	 *
	 * @since 1.0
	 *
	 */
	public function __construct() {

		parent::__construct();
	}

	/**
	 * premium function. allow customer to use points only if the minimum purchase amount reached
	 *
	 *
	 * @since 1.0
	 * @return void
	 *
	 */
	public function display_points_in_account() {
		$display_apply_point_alert = true;
		$net_total = WC()->cart->get_cart_contents_total(); //total in cart minus all discounts

		$need_to_add_amount = $this->points->is_min_purchase_amount_reached($net_total);
		if ($need_to_add_amount !== true) {

			error_log('HP-WOO-REWARDS: min purchase amount not reached: '.$net_total. ' need to add: '.$need_to_add_amount);

			$this->display_min_purchase_requirement($need_to_add_amount);			
			$display_apply_point_alert = false;
		}

		$coupon_applied = WC()->cart->applied_coupons;
		
		if (!$this->points->can_combine_with_coupon() && !empty($coupon_applied)) {
			error_log('HP-WOO-REWARDS: cannot be combined with coupon');
			$display_apply_point_alert = false;
		}

		if ($display_apply_point_alert) {
			parent::display_points_in_account();
		}
		
	}

	/**
	 * display min purchase amount requirement
	 *
	 *
	 * @since 1.0
	 * @return void
	 *
	 */
	public function display_min_purchase_requirement($amount) {
		$message = Helper::get_settings_option('min_purchase_amount_message');
		$total_points = $this->points->get_customer_total_points($this->customer_id);

		$values = array(
	      'points_label'  => $this->points->point_name,
	      'money'		  => get_woocommerce_currency_symbol().$amount  
	    );

		$message =Helper::parse_customized_message($message, $values);
		echo '<div class="woocommerce-info" role="alert">';
		echo $message;
	  	echo '</div>';
	}

	/**
	 * display 'complete your order and earn...' message only if settings is allowed
	 *
	 *
	 * @since 1.0
	 * @return void
	 *
	 */
	public function display_total_points() {
		$coupon_applied = WC()->cart->applied_coupons;
		if (!$this->points->can_earn_points_with_coupon() && !empty($coupon_applied)) return;

		parent::display_total_points();
	}

	/**
	 * Get the maximum discount based on items in cart
	 *
	 * @since 1.0
	 * @return float
	 *
	 */
	protected function get_max_cart_discount() {
		$max_discount = parent::get_max_cart_discount();
		$max_points_discount = (int)Helper::get_settings_option('max_points_discount');
		$max_points_discount_type = Helper::get_settings_option('max_points_discount_type');
		if ($max_points_discount > 0) {
			if ($max_points_discount_type == 'currency') {
				if ($max_points_discount < $max_discount) return $max_points_discount;
			}
			else {
				//percentage
				$max_discount = $max_discount * $max_points_discount / 100;
			}
		}

		return $max_discount;
	}
}