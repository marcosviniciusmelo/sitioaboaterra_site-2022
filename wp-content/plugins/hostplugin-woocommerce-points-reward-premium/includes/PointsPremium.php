<?php
/**
 * @package  hostpluginWoocommercePointsRewards
 */

namespace HPWooRewardsIncludes;

class PointsPremium extends Points {

	const LOG_TYPE_REFUND_PURCHASE_REWARDS = 11;	//(-)   remove purchase rewards
	const LOG_TYPE_REFUND_REDEEMED_REWARDS = 12;	//(+)	refund already redeeemed points

	/**
	 * Check if the min purchase amount has reached, if so return true, if not, return the
	 * difference
	 *
	 * @since 1.0
	 * @param  parsefloat $amount
	 * @return mixed
	 *
	 */
	public function is_min_purchase_amount_reached($amount = 0) {
		error_log('HP-WOO-REWARDS: Calling '.__FUNCTION__);
		$min_purchase_amount = (int)$this->option['min_purchase_amount'];
		error_log('HP-WOO-REWARDS: Min purchase amount: '.$min_purchase_amount.' Amount: '.$amount);
		if ($amount >= $min_purchase_amount) return true;
		return $min_purchase_amount - $amount;
	}

	/**
	 *
	 * return point label
	 *
	 * @since 1.0
	 * @return string
	 *
	 */
	public function get_point_name() {
		$point_name = parent::get_point_name();
		if ( isset($this->option['points_name']) && !empty($this->option['points_name']))
			$point_name = $this->option['points_name'];
		
		return $point_name;
	}

	/**
	 *
	 * check if system allows customer to combine points with woocommerce coupons
	 *
	 * @since 1.0
	 * @return boolean
	 *
	 */
	public function can_combine_with_coupon() {
		if (isset($this->option['disable_point_when_using_coupon']) && !empty($this->option['disable_point_when_using_coupon']))
			return false;
		return true;
	}

	/**
	 *
	 * check if system allows customer to earn points when woocommerce coupons are in use
	 *
	 * @since 1.0
	 * @return boolean
	 *
	 */
	public function can_earn_points_with_coupon() {
		if (isset($this->option['no_earn_point_when_using_coupon']) && !empty($this->option['no_earn_point_when_using_coupon']))
			return false;
		return true;
	}

	/**
	 * refund purchase rewards for a particular order
	 *
	 * @since 1.0
	 * @param int $order_id
	 * @param int $customer_id
	 * @return int
	 *
	 */
	private function refund_reward_points_for_order($order_id, $customer_id) {
		error_log('HP-WOO-REWARDS: calling refund_reward_points_for_order() for customer ID: '.$customer_id);
		if ($customer_id) {
			error_log('HP-WOO-REWARDS: customer ID found: '.$customer_id);
			if ($this->is_already_processed($order_id, $customer_id, self::LOG_TYPE_REFUND_PURCHASE_REWARDS) === false) {
				error_log('HP-WOO-REWARDS: preparing to refund rewards points to customer: '.$customer_id);

				//find out how many points to refund
				$refund_points = $this->is_already_processed($order_id, $customer_id, self::LOG_TYPE_PURCHASE_REWARDS);
				if ($refund_points !== false) {
					error_log('HP-WOO-REWARDS: refund points: '.$refund_points.' to Customer ID: '.$customer_id);
					$this->update_customer_points($customer_id, -$refund_points);
					$note = 'Earned {points_label} deducted';
					$points_string = "-$refund_points";
					$this->update_customer_points_details($customer_id, $points_string, $note, self::LOG_TYPE_REFUND_PURCHASE_REWARDS, $order_id);
				}
			}//check if points already added to the system
		}//customer id found
	}

	/**
	 * refunded redeemed rewards for a particular order
	 *
	 * @since 1.0
	 * @param int $order_id
	 * @param int $customer_id
	 * @return int
	 *
	 */
	private function refund_redeemed_points_for_order($order_id, $customer_id) {
		error_log('HP-WOO-REWARDS: calling refund_redeemed_points_for_order() for customer ID: '.$customer_id);
		if ($customer_id) {
			error_log('HP-WOO-REWARDS: customer ID found: '.$customer_id);
			//check if it's already refunded
			if ($this->is_already_processed($order_id, $customer_id, self::LOG_TYPE_REFUND_REDEEMED_REWARDS) === false) {
				error_log('HP-WOO-REWARDS: preparing to refund redeemed points to customer: '.$customer_id);

				//find out how many points to refund
				$refund_points = $this->is_already_processed($order_id, $customer_id, self::LOG_TYPE_REDEEM_REWARDS);
				if ($refund_points !== false) {
					$refund_points = abs($refund_points);
					error_log('HP-WOO-REWARDS: refund points: '.$refund_points.' to Customer ID: '.$customer_id);
					$this->update_customer_points($customer_id, $refund_points);
					$note = '{points_label} credited for cancelled / refunded order';
					$points_string = "+$refund_points";
					$this->update_customer_points_details($customer_id, $points_string, $note, self::LOG_TYPE_REFUND_REDEEMED_REWARDS, $order_id);
				}
			}//check if points already added to the system
		}//customer id found
	}

	/**
	 *
	 * return all points associated with a particular order (earned points, already redeemed points)
	 *
	 * @since 1.0
	 * @param int $order_id
	 *
	 */
	public function refund_points_for_customer($order_id) {
		error_log('HP-WOO-REWARDS: calling '.__FUNCtION__.' for order id: '.$order_id);
		$order = wc_get_order( $order_id );
		$customer_id = $order->get_customer_id();

		$remove_earned_points =  Helper::get_settings_option('is_remove_earned_points_if_refunded');
		$return_redeemed_points = Helper::get_settings_option('is_return_redeemed_points_if_refunded');

		if ($remove_earned_points == 'on') {
			error_log('HP-WOO-REWARDS: Processing Earned Point removal');
			$this->refund_reward_points_for_order($order_id, $customer_id);
		}
		
		if ($return_redeemed_points == 'on') {
			error_log('HP-WOO-REWARDS: Returning Redeemed points');
			$this->refund_redeemed_points_for_order($order_id, $customer_id);
		}
	}//
}