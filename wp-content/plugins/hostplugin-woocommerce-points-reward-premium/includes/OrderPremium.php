<?php
/**
 * @package  hostpluginWoocommercePointsRewards
 */

namespace HPWooRewardsIncludes;

Use \HPWooRewardsIncludes\Helper;

/**
 * OrderPremium class extends order, handle order status
 */
class OrderPremium extends Order {

	/**
   * Construct
   *
   * A place to add hooks and filters
   *
   * @since 1.0
   *
   */
	public function __construct() {
		add_action( 'woocommerce_order_status_refunded', array($this, 'refund_points'));
		add_action( 'woocommerce_order_status_cancelled', array($this, 'refund_points'));
		//add_action( 'woocommerce_order_refunded', array($this, 'refund_points'));
		parent::__construct();
	}

	/**
	 * check if customer is allowed to earn points when combine with coupon, if so process add points
	 *
	 * @since 1.0
	 * @param int $order_id
	 * @return void
	 *
	 */
	public function add_points($order_id) {
		$order = wc_get_order( $order_id );
		$applied_coupons = $order->get_used_coupons();
		if (!empty($applied_coupons) && !$this->points->can_earn_points_with_coupon()) {
			error_log('HP-WOO-REWARDS: Cannot earn points with coupon being used: '.print_r($applied_coupons, true));
			return;		
		}
		parent::add_points($order_id);
	}

	/**
	 * Fire when order status is refunded / cancelled
	 * if refunded, return redeemed points to customer and deduct rewarded points from account
	 *
	 * @since 1.0
	 * @param int $order_id
	 * @return void
	 *
	 */
	public function refund_points($order_id) {
		$this->points->refund_points_for_customer($order_id);
	}
}