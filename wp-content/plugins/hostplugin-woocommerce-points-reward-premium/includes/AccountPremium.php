<?php
/**
 * @package  hostpluginWoocommercePointsRewards
 */

namespace HPWooRewardsIncludes;

Use \HPWooRewardsIncludes\Account;

class AccountPremium extends Account {

	/**
	 * Construct
	 *
	 * A place to add hooks and filters
	 *
	 * @since 1.0
	 *
	 */
	public function __construct() {

		parent::__construct();
	}

	public function add_signup_points($user_id) {
		if ($this->can_earn_signup_points($user_id)) {
      		parent::add_signup_points($user_id);
    	}
	}

	/**
    * check whether user can earn signup points, which the list is defined in admin settings
    * @since  1.0.3
    * @param  int $user_id
    * @return boolean
    */
  	private function can_earn_signup_points($user_id) {
	    error_log('HP-WOO-REWARDS: calling '.__FUNCTION__.' for User ID: '.$user_id);
	    $user_meta = get_userdata($user_id);
	    $user_roles = $user_meta->roles; //array of roles the user is part of.
	    $signup_roles = Helper::get_settings_option('signup_points_role');

	    if (in_array('all', $reward_roles)) return true;

	    error_log('HP-WOO-REWARDS: Check if user roles: '.print_r($user_roles, true).' is in settings: '.print_r($signup_roles, true));

	    foreach ($user_roles as $user_role) {
	      if ( in_array($user_role, $signup_roles) ) {
	        error_log('HP-WOO-REWARDS: User role: '.$user_role.' found');
	        return true;
	      }
	    }

    	return false;
  	}
}