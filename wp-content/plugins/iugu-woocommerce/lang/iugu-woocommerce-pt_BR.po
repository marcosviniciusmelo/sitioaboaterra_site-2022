msgid ""
msgstr ""
"Project-Id-Version: WooCommerce iugu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-31 21:26-0300\n"
"PO-Revision-Date: 2021-08-31 21:27-0300\n"
"Last-Translator: admin <eduardo.marcovecchio@iugu.com>\n"
"Language-Team: Portuguese (Brazil)\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.0\n"

#: inc/class-wc-iugu-api.php:890
#, php-format
msgid "Order %s"
msgstr "Pedido %s"

#: inc/class-wc-iugu-api.php:988
#, php-format
msgid "Shipping via %s"
msgstr "Entrega via %s"

#: inc/class-wc-iugu-api.php:1307
msgid ""
"An error has occurred while processing your payment. Please, try again or "
"contact us for assistance."
msgstr ""
"Houve um erro enquanto processávamos seu pagamento. Por favor, tente "
"novamente ou entre em contato para obter ajuda."

#: inc/class-wc-iugu-api.php:1418
msgid "Sign up fee"
msgstr "Taxa de inscrição (%s)"

#: inc/class-wc-iugu-api.php:1671
#, php-format
msgid "Payment method created for order %s"
msgstr "Método de pagamento para o pedido %s"

#: inc/class-wc-iugu-api.php:1675
msgid "Payment method created"
msgstr "Forma de pagamento criada"

#: inc/class-wc-iugu-api.php:1965
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:165
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:226
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:363
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:435
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:327
msgid "iugu bank slip URL"
msgstr "URL do boleto bancário da iugu"

#: inc/class-wc-iugu-api.php:2001
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:170
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:231
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:368
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:440
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:244
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:339
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:586
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:751
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:332
msgid "iugu transaction details"
msgstr "Detalhes da transação iugu"

#: inc/class-wc-iugu-api.php:2009 inc/class-wc-iugu-api.php:2081
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:173
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:234
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:371
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:443
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:335
msgid ""
"iugu: The customer generated a bank slip. Awaiting payment confirmation."
msgstr ""
"iugu: O cliente gerou um boleto bancário. Aguardando confirmação de "
"pagamento."

#: inc/class-wc-iugu-api.php:2013 inc/class-wc-iugu-api.php:2085
msgid ""
"iugu: The customer generated a pix payment. Awaiting payment confirmation."
msgstr ""
"iugu: O cliente gerou um pagamento no pix. Aguardando a confirmação do "
"pagamento."

#: inc/class-wc-iugu-api.php:2025
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:346
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:761
msgid "iugu: Invoice paid successfully by credit card."
msgstr "iugu: Fatura paga com sucesso com cartão de crédito."

#: inc/class-wc-iugu-api.php:2031
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:343
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:757
msgid "iugu: Credit card declined."
msgstr "iugu: Cartão de crédito recusado."

#: inc/class-wc-iugu-api.php:2089
msgid ""
"iugu: Invoice paid by credit card. Waiting for the acquirer confirmation."
msgstr ""
"iugu: Fatura paga com cartão de crédito. Aguardando a confirmação da "
"adquirente."

#: inc/class-wc-iugu-api.php:2101
msgid "iugu: Invoice paid successfully."
msgstr "iugu: Fatura paga com sucesso."

#: inc/class-wc-iugu-api.php:2143
msgid "iugu: Invoice canceled."
msgstr "iugu: Fatura cancelada."

#: inc/class-wc-iugu-api.php:2149
msgid "iugu: Invoice partially paid."
msgstr "iugu: Fatura parcialmente paga."

#: inc/class-wc-iugu-api.php:2155
msgid "iugu: Invoice refunded."
msgstr "iugu: Fatura reembolsada."

#: inc/class-wc-iugu-api.php:2158
#, php-format
msgid "Invoice for order %s was refunded"
msgstr "A fatura do pedido %s foi reembolsada"

#: inc/class-wc-iugu-api.php:2159
msgid "Invoice refunded"
msgstr "Fatura reembolsada"

#: inc/class-wc-iugu-api.php:2160
#, php-format
msgid "Order %s has been marked as refunded by iugu."
msgstr "A iugu marcou o pedido %s como reembolsado."

#: inc/class-wc-iugu-api.php:2167
msgid "iugu: Invoice expired."
msgstr "iugu: Fatura expirada."

#: inc/class-wc-iugu-api.php:2242
msgid "The request failed!"
msgstr "A requisição falhou!"

#: inc/class-wc-iugu-api.php:2267
msgid "Can't do partial refunds"
msgstr "Não é possível reembolsos parciais"

#: inc/class-wc-iugu-api.php:2312
msgid "Email address"
msgstr "Email"

#: inc/class-wc-iugu-api.php:2314
msgid "Street address"
msgstr "Logradouro"

#: inc/class-wc-iugu-api.php:2315
msgid "Neighborhood"
msgstr "Bairro"

#: inc/class-wc-iugu-api.php:2316
msgid "Town / City"
msgstr "Cidade"

#: inc/class-wc-iugu-api.php:2317
msgid "State / County"
msgstr "Estado"

#: inc/class-wc-iugu-api.php:2318
msgid "Postcode / ZIP"
msgstr "CEP"

#: inc/class-wc-iugu-hooks.php:106 inc/class-wc-iugu-hooks.php:140
#: views/bank-slip/emails/html-instructions.php:17
#: views/bank-slip/payment-instructions.php:16
#: views/pix/emails/html-instructions.php:17
msgid "Pay the bank slip"
msgstr "Pagar boleto bancário"

#: inc/class-wc-iugu-hooks.php:162
msgid "Iugu Subscriptions"
msgstr "Assinaturas Iugu"

#: inc/class-wc-iugu-hooks.php:167
msgid "Enable Iugu Subscriptions"
msgstr "Habilitar assinaturas iugu"

#: inc/class-wc-iugu-hooks.php:168
msgid ""
"By activating this option, the management of subscriptions will be done by "
"Iugu automatically. Your products will be synced as plans and subscriptions "
"will be created in Iugu."
msgstr ""
"Ao ativar esta opção, a gestão das assinaturas será feita pela Iugu "
"automaticamente. Seus produtos serão sincronizados como planos e as "
"assinaturas serão criadas na Iugu."

#: inc/class-wc-iugu-hooks.php:175
#, fuzzy
#| msgid "Bank Slip discount"
msgid "Bank Slip discount type"
msgstr "Desconto no boleto"

#: inc/class-wc-iugu-hooks.php:176
msgid ""
"The subscription using the bank slip has what kind of discount. Persistent, "
"ie every month has the same discount, or only on the first payment?"
msgstr ""
"A assinatura utilizando o boleto bancário tem que tipo de desconto: "
"Persistente, ou seja, todos os meses tem o mesmo desconto, ou apenas no "
"primeiro pagamento?"

#: inc/class-wc-iugu-hooks.php:181
msgid "Persistent"
msgstr "Todo Mês"

#: inc/class-wc-iugu-hooks.php:182
msgid "One Time"
msgstr "Uma Vez"

#: inc/class-wc-iugu-hooks.php:330
msgid "Avaiable Iugu Payments"
msgstr "Pagamentos disponíveis da Iugu"

#: inc/class-wc-iugu-hooks.php:332
msgid "All"
msgstr "Todos"

#: inc/class-wc-iugu-hooks.php:333
msgid "Bank Slip"
msgstr "Boleto Bancário"

#: inc/class-wc-iugu-hooks.php:334
msgid "Credit Card"
msgstr "Cartão de Crédito"

#: inc/class-wc-iugu-hooks.php:335
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:143
msgid "PIX"
msgstr "PIX"

#: inc/class-wc-iugu-hooks.php:381 inc/class-wc-iugu-hooks.php:383
msgid "Iugu Gateway"
msgstr "Gateway iugu"

#: inc/class-wc-iugu-hooks.php:406
msgid "Plan ID"
msgstr "ID do Plano"

#: inc/class-wc-iugu-hooks.php:416
msgid "No plan for this product"
msgstr "Nenhum plano para este produto"

#: inc/class-wc-iugu-hooks.php:443
msgid "Subscription ID"
msgstr "ID da Assinatura"

#: inc/class-wc-iugu-hooks.php:453
msgid "No subscription for this product"
msgstr "Nenhuma assinatura para este produto"

#: inc/class-wc-iugu-hooks.php:559 inc/class-wc-iugu-hooks.php:581
msgid "Bank Slip discount"
msgstr "Desconto no boleto"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:236
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:445
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:349
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:767
#, php-format
msgid "iugu: Pre-order payment failed (%s)."
msgstr "iugu: O pagamento da pré-encomenda falhou (%s)."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:262
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:471
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:394
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:410
msgid "iugu: Subscription paid successfully."
msgstr "iugu: Assinatura paga com sucesso."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway-deprecated.php:269
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:478
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:423
msgid "iugu: Subscription payment failed."
msgstr "iugu: O pagamento da assinatura falhou."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:314
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:227
msgid "Subscription paid successfully by Iugu - PIX."
msgstr "Assinatura paga com sucesso pela Iugu - PIX."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-addons-gateway.php:320
#: inc/gateways/pix/class-wc-iugu-pix-addons-gateway.php:233
msgid "Iugu Subscription waiting payment."
msgstr "Pagamento por Assinatura Iugu em espera."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:29
msgid "iugu - Bank slip"
msgstr "iugu - Boleto bancário"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:30
msgid "Accept bank slip payments using iugu."
msgstr "Aceite pagamentos com boleto bancário usando a iugu."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:120
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:132
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:133
msgid "Enable/Disable"
msgstr "Habilitar/Desabilitar"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:122
msgid "Enable bank slip payments with iugu"
msgstr "Habilitar boleto bancário com a iugu"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:126
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:138
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:140
msgid "Title"
msgstr "Título"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:128
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:140
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:142
msgid "Payment method title seen on the checkout page."
msgstr "Nome do método de pagamento exibido no checkout."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:129
msgid "Bank slip"
msgstr "Boleto bancário"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:132
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:144
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:147
msgid "Description"
msgstr "Descrição"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:134
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:146
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:149
msgid "Payment method description seen on the checkout page."
msgstr "Descrição do método de pagamento exibida no checkout."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:135
msgid "Pay with bank slip"
msgstr "Pague com boleto bancário"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:138
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:150
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:154
msgid "Integration settings"
msgstr "Configurações da integração"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:143
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:155
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:160
msgid "Account ID"
msgstr "ID da conta"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:145
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:157
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:162
#, php-format
msgid "Your iugu account's unique ID, found in %s."
msgstr "O ID da sua conta iugu, encontrado em %s."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:145
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:154
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:225
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:157
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:166
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:228
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:162
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:172
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:210
msgid "iugu account settings"
msgstr "Configurações de conta iugu"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:152
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:164
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:170
msgid "API Token"
msgstr "Token de API"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:154
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:166
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:172
#, php-format
msgid ""
"For real payments, use a LIVE API token. When iugu sandbox is enabled, use a "
"TEST API token. API tokens can be found/created in %s."
msgstr ""
"Para pagamentos reais, use um token de API live. Quando o iugu sandbox "
"estiver habilitado, use um token de API de teste. Tokens de API podem ser "
"encontrados/criados em %s."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:161
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:173
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:180
msgid "Ignore due email"
msgstr "Ignora envio de emails de cobrança"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:163
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:175
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:182
msgid "When checked, Iugu will not send billing emails to the payer"
msgstr ""
"Quando marcado, a Iugu não vai enviar emails de cobrança para o cliente"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:167
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:179
msgid "Payment options"
msgstr "Opções de pagamento"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:172
msgid "Enable Bank Slip Discount"
msgstr "Habilitar o desconto de boleto bancário"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:174
#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:176
msgid "A discount for customers who choose this payment method."
msgstr "Um desconto para clientes que escolhem esta forma de pagamento."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:179
msgid "Discount Type"
msgstr "Tipo de desconto"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:181
msgid ""
"Discount can be a percentage amount or a fixed amount of the total order."
msgstr ""
"O desconto pode ser um valor percentual ou um valor fixo do pedido total."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:184
msgid "(%) Percentage"
msgstr "(%) Porcentagem"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:185
msgid "Fixed Amount"
msgstr "Quantia fixa"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:189
msgid "Discount value"
msgstr "Valor de desconto"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:191
msgid "The amount of discount."
msgstr "O valor do desconto."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:195
msgid "Default payment deadline"
msgstr "Prazo de pagamento padrão"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:197
msgid "Number of days the customer will have to pay the bank slip."
msgstr "Número de dias que o cliente terá para pagar o boleto bancário."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:205
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:208
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:187
msgid "Integration behavior"
msgstr "Comportamento da integração"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:210
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:213
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:193
msgid "Send only the order total"
msgstr "Enviar apenas o total do pedido"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:212
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:215
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:195
msgid ""
"When enabled, the customer only gets the order total, not the list of "
"purchased items."
msgstr ""
"Quando habilitado, o cliente só recebe o total do pedido, não a lista de "
"itens comprados."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:216
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:219
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:200
msgid "Gateway testing"
msgstr "Modo de testes"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:221
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:224
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:206
msgid "iugu sandbox"
msgstr "iugu sandbox"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:223
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:226
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:208
msgid "Enable iugu sandbox"
msgstr "Habilitar o iugu sandbox"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:225
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:228
#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:210
#, php-format
msgid ""
"Used to test payments. Don't forget to use a TEST API token, which can be "
"found/created in %s."
msgstr ""
"Usado para testar pagamentos. Não se esqueça de usar um token de API de "
"teste, que pode ser encontrado/criado em %s."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:228
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:231
msgid "Debugging"
msgstr "Depuração"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:230
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:233
msgid "Enable logging"
msgstr "Habilitar depuração"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:232
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:235
#, php-format
msgid ""
"Log iugu events, such as API requests, for debugging purposes. The log can "
"be found in %s."
msgstr ""
"Registre eventos da iugu, como requisições da API, para depurá-los. Esse "
"registro pode ser encontrado em %s."

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:395
#, php-format
msgid "Discount for %s (%s off)"
msgstr "Desconto para %s (%s off)"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:399
#, php-format
msgid "Discount for %s"
msgstr "Desconto para %s"

#: inc/gateways/bank-slip/class-wc-iugu-bank-slip-gateway.php:432
#, php-format
msgid "%s off"
msgstr "%s de Desconto"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:62
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:127
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:156
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:457
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:387
msgid ""
"Please, make sure your credit card details have been entered correctly and "
"that your browser supports JavaScript."
msgstr ""
"Certifique-se de que suas informações de cartão de crédito estão corretas e "
"seu navegador suporta JavaScript."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:74
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:139
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:469
msgid ""
"An error occurred while trying to save your data. Please, contact us to get "
"help."
msgstr ""
"Houve um erro enquanto tentávamos salvar seus dados. Por favor, entre em "
"contato conosco para obter ajuda."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:229
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:324
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:565
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:730
msgid "Customer payment method not found!"
msgstr "O método de pagamento do cliente não foi encontrado!"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:248
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:367
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:592
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:854
msgid "iugu: Subscription paid successfully by credit card."
msgstr "iugu: Assinatura paga com sucesso com cartão de crédito."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway-deprecated.php:253
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:375
#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:600
msgid "iugu: Subscription payment failed. Credit card declined."
msgstr "iugu: O pagamento da assinatura falhou. Cartão de crédito recusado."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:232
msgid "Customer not found."
msgstr "Cliente não encontrado."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-addons-gateway.php:240
msgid "Plan not found."
msgstr "Plano não encontrado."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:27
msgid "iugu - Credit card"
msgstr "iugu - Cartão de crédito"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:28
msgid "Accept credit card payments using iugu."
msgstr "Aceite pagamentos com cartão de crédito usando a iugu."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:134
msgid "Enable credit card payments with iugu"
msgstr "Habilitar pagamentos com cartão de crédito com a iugu"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:141
msgid "Credit card"
msgstr "Cartão de crédito"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:147
msgid "Pay with credit card"
msgstr "Pague com cartão de crédito"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:184
msgid "Smallest installment value"
msgstr "Menor valor de parcela"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:186
msgid "Smallest value of each installment. Value can't be lower than 5."
msgstr "Menor valor de cada parcela. Não pode ser menor que 5."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:190
msgid "Pass on interest"
msgstr "Repasse de juros"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:192
msgid "Pass on the installments' interest to the customer."
msgstr "Transmitir o juros das parcelas ao cliente."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:193
#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:200
msgid ""
"This option is only for display and should mimic your iugu account's "
"settings."
msgstr ""
"Esta opção é só para exibição e deve refletir as configurações da sua conta."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:198
msgid "Transaction rate"
msgstr "Taxa de transação"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:200
msgid "Enter the transaction rate set up in your iugu plan."
msgstr "Insira a taxa de transação configurada no seu plano iugu."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:258
#: views/credit-card/payment-form.php:50
msgid "Card number"
msgstr "Número do cartão"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:259
#: views/credit-card/payment-form.php:89
msgid "Security code"
msgstr "Código de segurança"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:260
#: views/credit-card/payment-form.php:78
msgid "Expiry date"
msgstr "Data de vencimento"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:261
msgid "First name"
msgstr "Nome"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:262
msgid "Last name"
msgstr "Sobrenome"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:263
#: views/credit-card/payment-form.php:110
msgid "Installments"
msgstr "Parcelamento"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:264
msgid "is invalid"
msgstr "é inválido"

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:544
msgid "Unable to add this credit card. Please, try again."
msgstr ""
"Incapaz de adicionar este cartão de crédito. Por favor, tente novamente."

#: inc/gateways/credit-card/class-wc-iugu-credit-card-gateway.php:553
msgid "Card information not valid."
msgstr "Informações do cartão não válidas."

#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:30
msgid "iugu - PIX"
msgstr "iugu - PIX"

#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:31
msgid "Accept pix payments using iugu."
msgstr "Aceite pagamentos usando o BoaCompra."

#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:135
msgid "Enable pix payments with iugu"
msgstr "Habilite pagamentos com Pix da iugu"

#: inc/gateways/pix/class-wc-iugu-pix-gateway.php:150
msgid "Pay with PIX."
msgstr "Pague com PIX."

#: iugu-woocommerce.php:257
msgid "System status &gt; logs"
msgstr "Status do sistema &gt; logs"

#: iugu-woocommerce.php:313
msgid "Credit card settings"
msgstr "Configurações de cartão de crédito"

#: iugu-woocommerce.php:315
msgid "Bank slip settings"
msgstr "Configurações de boleto bancário"

#: iugu-woocommerce.php:317
msgid "PIX settings"
msgstr "Configurações PIX"

#: views/admin-notices/html-notice-account-id-missing.php:13
#: views/admin-notices/html-notice-api-token-missing.php:13
#: views/admin-notices/html-notice-currency-not-supported.php:13
#: views/admin-notices/html-notice-ecfb-missing.php:20
#: views/admin-notices/html-notice-woocommerce-missing.php:20
msgid "iugu disabled"
msgstr "iugu desativada"

#: views/admin-notices/html-notice-account-id-missing.php:13
msgid "Please, inform a valid API token."
msgstr "Por favor, informe um token de API válido."

#: views/admin-notices/html-notice-api-token-missing.php:13
msgid "You should inform your account ID."
msgstr "Informe o ID da sua conta iugu."

#: views/admin-notices/html-notice-currency-not-supported.php:13
#, php-format
msgid ""
"Currency <code>%s</code> is not supported. WooCommerce iugu only works with "
"Brazilian real (BRL)."
msgstr ""
"A moeda <code>%s</code> não é suportada. O WooCommerce iugu só funciona com "
"o real brasileiro (BRL)."

#: views/admin-notices/html-notice-ecfb-missing.php:20
#: views/admin-notices/html-notice-woocommerce-missing.php:20
#, php-format
msgid "WooCommerce iugu requires the latest version of %s to work!"
msgstr "O WooCommerce iugu requer a última versão do %s para funcionar!"

#: views/admin-notices/html-notice-ecfb-missing.php:20
msgid "WooCommerce Extra Checkout Fields for Brazil"
msgstr "WooCommerce Extra Checkout Fields for Brazil"

#: views/admin-notices/html-notice-woocommerce-missing.php:20
msgid "WooCommerce"
msgstr "WooCommerce"

#: views/bank-slip/checkout-instructions.php:16
msgid ""
"After clicking on \"Place order\", you will have access to the bank slip, "
"which you can print and pay on your internet banking or in a lottery "
"retailer."
msgstr ""
"Ao clicar em \"Finalizar pedido\", você terá acesso ao boleto bancário, que "
"pode ser impresso e pago via internet banking ou numa lotérica."

#: views/bank-slip/checkout-instructions.php:16
#: views/pix/checkout-instructions.php:25
msgid "Note: The order will be confirmed only after the payment approval."
msgstr "Nota: O pedido será confirmado só depois da confirmação do pagamento."

#: views/bank-slip/emails/html-instructions.php:15
#: views/bank-slip/emails/plain-instructions.php:14
#: views/credit-card/emails/html-instructions.php:15
#: views/credit-card/emails/plain-instructions.php:14
#: views/pix/emails/html-instructions.php:15
#: views/pix/emails/plain-instructions.php:14
msgid "Payment"
msgstr "Pagamento"

#: views/bank-slip/emails/html-instructions.php:17
#: views/bank-slip/emails/plain-instructions.php:18
#: views/pix/emails/html-instructions.php:17
#: views/pix/emails/plain-instructions.php:18
msgid ""
"Use the link below to view your bank slip. You can print and pay it on your "
"internet banking or in a lottery retailer."
msgstr ""
"Use o link abaixo para ver o seu boleto bancário. Você pode imprimi-lo e "
"pagá-lo via internet banking ou numa lotérica."

#: views/bank-slip/emails/html-instructions.php:17
#: views/bank-slip/emails/plain-instructions.php:26
#: views/bank-slip/payment-instructions.php:16
#: views/pix/emails/html-instructions.php:17
#: views/pix/emails/plain-instructions.php:26
msgid ""
"After we receive the bank slip payment confirmation, your order will be "
"processed."
msgstr ""
"O seu pedido será processado assim que recebermos a confirmação do pagamento "
"do boleto."

#: views/bank-slip/payment-instructions.php:16
msgid "Please click in the following button to view your bank slip."
msgstr ""
"Por favor, clique no botão a seguir para visualizar o seu boleto bancário."

#: views/bank-slip/payment-instructions.php:16
msgid ""
"You can print and pay it on your internet banking or in a lottery retailer."
msgstr "Você pode imprimi-lo e pagá-lo via internet banking ou numa lotérica."

#: views/credit-card/emails/html-instructions.php:17
#: views/credit-card/emails/plain-instructions.php:18
#: views/credit-card/payment-instructions.php:17
#, php-format
msgid "Payment successfully made using credit card in %s."
msgstr "Pagamento realizado com sucesso por cartão de crédito em %s."

#: views/credit-card/payment-form.php:36
msgid "New credit card"
msgstr "Novo cartão de crédito"

#: views/credit-card/payment-form.php:63
msgid "Name printed on card"
msgstr "Nome impresso no cartão"

#: views/credit-card/payment-form.php:83
msgid "MM / YYYY"
msgstr "MM / AAAA"

#: views/credit-card/payment-form.php:90
msgid "CVC"
msgstr "CVV"

#: views/credit-card/payment-form.php:98
msgid "Save this credit card"
msgstr "Salvar este cartão de crédito"

#: views/credit-card/payment-form.php:114
msgid "Select"
msgstr "Selecione"

#: views/credit-card/payment-form.php:122
msgid "free interest"
msgstr "sem juros"

#: views/credit-card/payment-form.php:135
msgid "with interest"
msgstr "com juros"

#: views/credit-card/payment-form.php:150
#, php-format
msgid "%dx of %s %s (Total: %s)"
msgstr "%dx de %s %s (Total: %s)"

#: views/pix/checkout-instructions.php:21
msgid ""
"After clicking on \"Place order\", you will have access to the QR Code, "
"which you can read with your payment app."
msgstr ""
"Após clicar em \"Fazer pedido\", você terá acesso ao Código QR, que você "
"pode ler com seu aplicativo de pagamento."

#: views/pix/payment-instructions.php:24
msgid "PIX QRCode"
msgstr "PIX QRCode"

#: views/pix/payment-instructions.php:38
msgid "Copy Code"
msgstr "Copiar código"

#. Plugin Name of the plugin/theme
msgid "WooCommerce iugu"
msgstr "WooCommerce iugu"

#. Plugin URI of the plugin/theme
msgid "https://github.com/iugu/iugu-woocommerce"
msgstr "https://github.com/iugu/iugu-woocommerce"

#. Description of the plugin/theme
msgid "iugu payment gateway for WooCommerce."
msgstr "Gateway de pagamento da iugu para WooCommerce."

#. Author of the plugin/theme
msgid "iugu"
msgstr "iugu"

#. Author URI of the plugin/theme
msgid "https://iugu.com/"
msgstr "https://iugu.com/"

#~ msgid "Payment method was not added. Please, try again!"
#~ msgstr ""
#~ "A forma de pagamento não foi adicionada. Por favor, tente novamente!"

#~ msgid "Payment method add successfully"
#~ msgstr "Forma de pagamento adicionado com sucesso"

#~ msgid ""
#~ "Pass on the installments' interest to the customer. (Please, be aware "
#~ "that this option currently only applies to iugu accounts created before "
#~ "2017.)"
#~ msgstr ""
#~ "Repasse os juros da sua antecipação de recebíveis para o cliente. (Esta "
#~ "opção atualmente só se aplica para contas iugu criadas antes de 2017.)"

#~ msgid "Expiry date (MM/YYYY)"
#~ msgstr "Data de expiração (MM/AAAA)"

#~ msgid "Installments limit"
#~ msgstr "Limite de parcelas"

#~ msgid ""
#~ "The maximum number of installments allowed for credit card payments. This "
#~ "can't be greater than the setting allowed in your iugu account."
#~ msgstr ""
#~ "Número máximo de parcelas permitidas para pagamentos com cartão de "
#~ "crédito. Não pode ser maior que o permitido na sua conta iugu."

#~ msgid "Free interest"
#~ msgstr "Sem juros"

#~ msgid ""
#~ "Indicate how many installments shall not bear any interest. Enter 0 to "
#~ "disable this option."
#~ msgstr ""
#~ "Quantas parcelas não devem ter juros. Use o valor 0 para desativar a "
#~ "opção."
