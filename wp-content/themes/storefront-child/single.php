<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
?>

<main class="single-post-page">

	<div class="ctnPostContent">
		<section class="post-content">
			<div class="main">

				<div class="ctnScroll">

					<div class="lineTitlePost">
						<?php
							single_cat_title();
							$category = get_the_category(); 
							$category_parent_id = $category[0]->category_parent;
							if ( $category_parent_id != 0 ) {
							    $category_parent = get_term( $category_parent_id, 'category' );
							    $categoryName = $category_parent->name;
							    $categoryId = $category_parent->term_id;
							} else {
							    $categoryName = $category[0]->name;
							    $categoryId = $category[0]->term_id;
							}
							$color = get_field('cor', 'category_'.$categoryId );
						?>
						<div class="category" style="color: <?php echo $color ?>"><?php echo $categoryName; ?></div>
						<h2><?php the_title() ?></h2>

						<?php if( get_field('frase_destaque') != '' ): ?><h4 class="olhoPost"><?php echo get_field('frase_destaque') ?></h4><?php endif; ?>

						<span class="date"><?php echo get_the_date('j \d\e F \d\e Y') ?></span>

					</div>

					<figure class="imageMain"><?php the_post_thumbnail('large'); ?></figure>

					<div class="barFloatLeft">
						<?php include(get_stylesheet_directory() . '/inc/barShared.php') ?>
					</div>

					<div class="contentText">
						<?php the_content() ?>
					</div>

					<?php include(get_stylesheet_directory() . '/inc/related_posts.php') ?>

				</div>
			</div>

		</section>

	</div>

</main>

<?php
get_footer();