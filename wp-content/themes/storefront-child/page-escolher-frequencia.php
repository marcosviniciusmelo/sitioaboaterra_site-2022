<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="assinatura-page">
    
    <header class="entry-header">
		  <h1 class="entry-title">Adicionar mais produtos</h1>
    </header>

    <div class="content-frequencia">
        <div class="main">
            <h4>Escolha de quanto em quanto tempo gostaria de receber sua cesta</h4>

            <div class="grid">
                <a href="#checkFrequencia" class="_item">
                    <figure class="yellow"><img src="<?php echo $url_tema?>assets/images/icons/semanal.svg"></figure>
                    <div class="right">
                        <strong>SEMANAL</strong>
                        <span>Você recebe toda semana</span>
                    </div>
                    <input type="checkbox" name="frequencia" value="semanal" checked>
                </a>
                <a href="#checkFrequencia" class="_item">
                    <figure class="blue"><img src="<?php echo $url_tema?>assets/images/icons/mensal.svg"></figure>
                    <div class="right">
                        <strong>QUINZENAL</strong>
                        <span>Você recebe a cada duas semanas.</span>
                    </div>
                    <input type="checkbox" name="frequencia" value="quinzenal">
                </a>
            </div>

            <a href="<?php echo $link_blog ?>/revise-sua-assinatura/" class="btn">CONTINUAR →</a>
        </div>
    </div>

</main>	

<?php
get_footer();