<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="assinatura-page">
    
    <header class="entry-header">
		  <h1 class="entry-title"><?php the_title() ?></h1>
    </header>

    <div class="main">

        <div class="colMain">
            <h4 class="top">SUGESTÕES DE ASSINATURAS</h4>
            <div class="grid" id="gridPackage">
                <?php
                    $cesta01 = wc_get_product_id_by_sku('0037');
                    $product01 = wc_get_product( $cesta01 );

                    $cesta02 = wc_get_product_id_by_sku('0036');
                    $product02 = wc_get_product( $cesta02 );

                    $cesta03 = wc_get_product_id_by_sku('0035');
                    $product03 = wc_get_product( $cesta03 );

                    $cesta04 = wc_get_product_id_by_sku('0038');
                    $product04 = wc_get_product( $cesta04 );

                    $cesta05 = wc_get_product_id_by_sku('0039');
                    $product05 = wc_get_product( $cesta05 );
                ?>
                <div class="_item active" data-price="<?php echo $product01->get_price() ?>" data-unid="7" data-product="<?php echo $cesta01 ?>">
                    <div class="box">CESTA PEQUENA</div>
                    <strong>CESTA PEQUENA</strong>
                    <div class="price">R$ <?php echo number_format($product01->get_price(),2,",",".") ?> / 7 un.</div>
                </div>
                <div class="_item" data-price="<?php echo $product02->get_price() ?>" data-unid="10" data-product="<?php echo $cesta02 ?>">
                    <div class="box red">CESTA MÉDIA</div>
                    <strong>CESTA MÉDIA</strong>
                    <div class="price">R$ <?php echo number_format($product02->get_price(),2,",",".") ?> / 10 un.</div>
                </div>
                <div class="_item" data-price="<?php echo $product03->get_price() ?>" data-unid="12" data-product="<?php echo $cesta03 ?>">
                    <div class="box red">CESTA GRANDE</div>
                    <strong>CESTA GRANDE</strong>
                    <div class="price">R$ <?php echo number_format($product03->get_price(),2,",",".") ?> / 12 un.</div>
                </div>
                <div class="_item" data-price="<?php echo $product04->get_price() ?>" data-unid="5" data-product="<?php echo $cesta04 ?>">
                    <div class="box ">CESTA DE FRUTAS</div>
                    <strong>CESTA DE FRUTAS</strong>
                    <div class="price">R$ <?php echo number_format($product04->get_price(),2,",",".") ?> / 5 un.</div>
                </div>
                <div class="_item" style="display: none;" data-price="<?php echo $product05->get_price() ?>" data-unid="custom" data-product="<?php echo $cesta05 ?>"></div>
            </div>

            <div class="lineRestricoes">
                <div class="left">
                    <h4>Restrições</h4>
                    <p>Produtos que <strong>não vamos te enviar</strong></p>
                </div>
                <a href="#openModalAss" data-modal="restricoes" class="btn">ESCOLHER RESTRIÇÕES</a>
            </div>

            <div id="restricoesSelecionadas">
                <h4>Restrições selecionadas</h4>
                <div class="ctn" id="listRestricoes"></div>
            </div>

            <p class="info">No pagamento você poderá escolher a data de entrega da sua primeira compra, que definirá o dia da semana da sua assinatura.</p>
        </div>

        <div class="sidebar">
            <h4 class="top">Ajuste a quantidade</h4>
            <span>Defina quantas porções de cada categoria quer receber</span>
            <div class="listCategories">
                <div class="_item" data-price="4.50">
                    <div class="icon"><img src="<?php echo $url_tema?>assets/images/icons/verduras.svg"></div>
                    <div class="right">
                        <div class="ctn">
                            <strong>Verduras</strong>
                            <p>R$ 4,50 / un</p>
                            <a href="#openModalAss" data-modal="opcoes" data-type="Verduras">Ver exemplos</a>
                        </div>
                        <div class="buttons-qty"><span>+</span><input type="text" value="0" min="0" /><span>-</span></div>
                    </div>
                </div>
                <div class="_item" data-price="8.50">
                    <div class="icon yellow"><img src="<?php echo $url_tema?>assets/images/icons/legumes.svg"></div>
                    <div class="right">
                        <div class="ctn">
                            <strong>Legumes</strong>
                            <p>R$ 8,50 / un</p>
                            <a href="#openModalAss" data-modal="opcoes" data-type="Legumes">Ver exemplos</a>
                        </div>
                        <div class="buttons-qty"><span>+</span><input type="text" value="0" min="0" /><span>-</span></div>
                    </div>
                </div>
                <div class="_item" data-price="10.00">
                    <div class="icon"><img src="<?php echo $url_tema?>assets/images/icons/frutas.svg"></div>
                    <div class="right">
                        <div class="ctn">
                            <strong>Frutas</strong>
                            <p>R$ 10,00 / un</p>
                            <a href="#openModalAss" data-modal="opcoes" data-type="Frutas">Ver exemplos</a>
                        </div>
                        <div class="buttons-qty"><span>+</span><input type="text" value="0" min="0" /><span>-</span></div>
                    </div>
                </div>
                <div class="_item" data-price="4.00">
                    <div class="icon blue"><img src="<?php echo $url_tema?>assets/images/icons/temperos.svg"></div>
                    <div class="right">
                        <div class="ctn">
                            <strong>Temperos</strong>
                            <p>R$ 4,00 / un</p>
                            <a href="#openModalAss" data-modal="opcoes" data-type="Temperos">Ver exemplos</a>
                        </div>
                        <div class="buttons-qty"><span>+</span><input type="text" value="0" min="0" /><span>-</span></div>
                    </div>
                </div>
            </div>

            <div class="box">
                <p id="namePackage">Cesta Pequena</p>
                <h3 class="subtotal"><span id="pricePackage">45,00</span> / <span id="unidPackage">5</span> unidades</h3>
            </div>

            <a href="<?php echo $link_blog ?>/escolher-produtos/" class="btn green" style="display: block">CONTINUAR →</a>
        </div>
    </div>

    <div class="modal box-opcoes" id="modal-opcoes">
        <div class="top">
            <span>OPÇÕES DE <b id="typeModalProduct"></b> PARA SUA CESTA.</span>
            <div class="close">X</div>
        </div>
        <div class="list" id="listModalProduct"></div>
    </div>


    <div class="modal box-restricoes" id="modal-restricoes">
        <div class="top">
            <span>Escolha suas restrições</span>
            <div class="close">X</div>
        </div>
        <div class="text">Busque e <strong>selecione até 10 produtos</strong> que você não gostaria de receber dentre as categorias abaixo.</div>
        <div class="buscar">
            <form action="">
                <input type="text" name="busca" placeholder="Buscar por produto">
                <button type="submit">BUSCAR</button>
            </form>
        </div>
        <div class="scroll">
            <div class="categories">
                <a href="" class="item active"><img src="<?php echo $url_tema?>assets/images/icons/verduras.svg" class="icon"><span>Verduras</span></a>
                <a href="" class="item"><img src="<?php echo $url_tema?>assets/images/icons/legumes.svg" class="icon"><span>Legumes</span></a>
                <a href="" class="item"><img src="<?php echo $url_tema?>assets/images/icons/frutas.svg" class="icon"><span>Frutas</span></a>
                <a href="" class="item"><img src="<?php echo $url_tema?>assets/images/icons/temperos.svg" class="icon"><span>Temperos</span></a>
            </div>
        </div>
        <div class="list checkbox" id="listProdRestricoes"></div>
        <div class="lineBottom">
            <div class="left">Selecionados: <span class="checkeds">0</span> / <span class="totalCheck">10</span></div>
            <a href="#closeModalAss" class="btn">CONFIRMAR RESTRIÇÕES</a>
        </div>
    </div>

</main>	

<?php
get_footer();