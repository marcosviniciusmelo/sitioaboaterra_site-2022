<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
?>

<main class="home-page">
	
    <section class="sliderHome">
        <div class="slider">
            <?php
                $args = array(
                    'post_type'         => 'slider',
                    'post_status'       => 'publish',
                    'posts_per_page'    => 10
                );
                $query = new WP_Query($args);
                while ($query->have_posts()) : $query->the_post();
                     echo '<div class="_item">
                                <a href="'.get_field('link').'">
                                    <img data-src-desktop="'.get_field('imagem_desktop')['url'].'" data-src-mobile="'.get_field('imagem_mobile')['sizes']['large'].'" alt="'.get_the_title().'">
                                </a>
                            </div>';
                endwhile;
            ?>
        </div>
        <div class="arrows">
            <button id="prevSlider"><img src="<?php echo $url_tema?>assets/images/icons/arrow-prev.svg"></button>
            <button id="nextSlider"><img src="<?php echo $url_tema?>assets/images/icons/arrow-next.svg"></button>
        </div>
    </section>

    <section class="categories">
        <div class="main">
            <div class="box">
                <div class="scroll">
                    <a href="<?php echo $link_blog ?>/beneficios-assinatura" class="item"><img src="<?php echo $url_tema?>assets/images/icons/assinatura.svg" class="icon"><span>Assinatura</span></a>
                    <a href="<?php echo $link_blog ?>/categoria-produto/da-roca/frutas/" class="item"><img src="<?php echo $url_tema?>assets/images/icons/frutas.svg" class="icon"><span>Frutas</span></a>
                    <a href="<?php echo $link_blog ?>/categoria-produto/da-roca/legumes/" class="item"><img src="<?php echo $url_tema?>assets/images/icons/legumes.svg" class="icon"><span>Legumes</span></a>
                    <a href="<?php echo $link_blog ?>/categoria-produto/da-roca/verduras/" class="item"><img src="<?php echo $url_tema?>assets/images/icons/verduras.svg" class="icon"><span>Verduras</span></a>
                    <a href="<?php echo $link_blog ?>/categoria-produto/temperos/" class="item"><img src="<?php echo $url_tema?>assets/images/icons/temperos.svg" class="icon"><span>Temperos</span></a>
                    <a href="<?php echo $link_blog ?>/categoria-produto/mercearia-organica/" class="item"><img src="<?php echo $url_tema?>assets/images/icons/mercearia.svg" class="icon"><span>Mercearia</span></a>
                    <a href="<?php echo $link_blog ?>/categoria-produto/laticinios-e-padaria/padaria/" class="item"><img src="<?php echo $url_tema?>assets/images/icons/padaria.svg" class="icon"><span>Padaria</span></a>
                    <a href="<?php echo $link_blog ?>/categoria-produto/variedades/" class="item"><img src="<?php echo $url_tema?>assets/images/icons/variedades.svg" class="icon"><span>Variedades</span></a>
                </div>
            </div>
        </div>
    </section>

    <section class="colluns">
        <div class="main">
            <div class="collun green">
                <img src="<?php echo $url_tema?>assets/images/icons/truck.svg" class="icon">
                <h3><?php echo get_field('box_frete_gratis', 'options')['titulo'] ?></h3>
                <div class="text"><?php echo get_field('box_frete_gratis', 'options')['texto'] ?></div>
            </div>
            <div class="collun yellow">
                <img src="<?php echo $url_tema?>assets/images/icons/place.svg" class="icon">
                <h3><?php echo get_field('box_quer_nos_visitar', 'options')['titulo'] ?></h3>
                <div class="text"><?php echo get_field('box_quer_nos_visitar', 'options')['texto'] ?></div>
            </div>
            <div class="collun orange selos">
                <img src="<?php echo $url_tema?>assets/images/icons/organico-br.svg" class="icon">
                <img src="<?php echo $url_tema?>assets/images/icons/selo-02.svg" class="icon">
            </div>
        </div>
    </section>

    <!-- <section class="colluns-02">
        <div class="main">
            <div class="collun">
                <div class="inner">
                    <div class="ctn">
                        <h4>ASSINATURA SEMANAL</h4>
                        <div class="text">Toda semana ou quinzena uma cesta com frutas, legumes e verduras orgânicos, frescos e da estação entregues na sua casa.</div>
                        <a href="<?php echo $link_blog ?>/beneficios-assinatura" class="link"><img src="<?php echo $url_tema?>assets/images/icons/arrow-02.svg"> CRIAR ASSINATURA</a>
                    </div>
                    <img src="<?php echo $url_tema?>assets/images/temp/assinatura-semanal.png?a" class="bg">
                </div>
            </div>
            <div class="collun">
                <div class="inner">
                    <div class="ctn">
                        <h4>CARTEIRA DIGITAL</h4>
                        <div class="text"><strong>Compre produtos e ganhe pontos.</strong> A cada R$1 em compras, você ganha 1 ponto e pode utilizar seus pontos e adquirir nossos produtos.</div>
                        <a href="" class="link"><img src="<?php echo $url_tema?>assets/images/icons/arrow-02.svg"> SAIBA MAIS</a>
                    </div>
                    <img src="<?php echo $url_tema?>assets/images/temp/carteira-digital.png?a" class="bg">
                </div>
            </div>
        </div>
    </section> -->


    <section class="bloco-posts">
		<div class="main">
            <div class="title">
                <h3>ASSINATURA</h3>
            </div>
            <div class="contents">
                <div class="item active"><?php echo do_shortcode('[recent_products limit=4 tag="home-assinatura"]') ?></div>
            </div>
		</div>	
	</section>


    <section class="bloco-posts slider">
		<div class="main">
            <div class="scroll">
                <div class="abas">
                    <div class="item click-slider active">PERSONALIZE SUA CESTA</div>
                    <div class="item">OFERTAS DA SEMANA</div>
                    <div class="item">NOVIDADES</div>
                </div>
            </div>
            <div class="contents">
                <?php $arrTotal = []; ?>
                <div class="item item-first active">
                    <?php for ($iM=1; $iM <= 2; $iM++): ?>
                    <div class="slider">
                        <div class="arrows"><i class="fas fa-arrow-left"></i><i class="fas fa-arrow-right"></i></div>
                        <div class="slide">
                            <?php
                                for ($i=0; $i < 4; $i++):

                                    $args = array(
                                        'post_type'         => 'product',
                                        'post_status'       => 'publish',
                                        'posts_per_page'    => 4,
                                        'orderby'           => 'rand',
                                        'post__not_in'      => $arrTotal,
                                        'meta_query' => array(
                                            array(
                                                'key' => '_stock_status',
                                                'value' => 'instock',
                                                'compare' => '=',
                                            )
                                        ),
                                        'tax_query'  => array(
                                            array(
                                                'taxonomy'  => 'product_tag',
                                                'field'     => 'slug',
                                                'terms'     =>  array('home-'.$iM),
                                            )
                                        )
                                    );

                                        
                                    $query = new WP_Query($args);
                                    $arr = [];

                                    while ($query->have_posts()) : $query->the_post();
                                        array_push($arr, $post->ID);
                                        array_push($arrTotal, $post->ID);
                                    endwhile;
                                    wp_reset_postdata();

                                    echo do_shortcode('[recent_products limit=4 ids="'.implode(',', $arr).'"]');

                                endfor;
                            ?>
                        </div>
                    </div>
                    <?php endfor; ?>
                    <a href="<?php echo $link_blog?>/categoria-produto/da-roca/" class="btn center">MAIS PRODUTOS</a>
                </div>
                <div class="item">
                    <?php echo do_shortcode('[recent_products limit=4 tag="home-oferta"]') ?>
                    <a href="<?php echo $link_blog?>/categoria-produto/ofertas/" class="btn center">MAIS PRODUTOS</a>
                </div>
                <div class="item">
                    <?php echo do_shortcode('[recent_products limit=4 tag="home-novidades"]') ?>
                    <a href="<?php echo $link_blog?>/loja" class="btn center">MAIS PRODUTOS</a>
                </div>
            </div>
		</div>	
	</section>


    <section class="bloco-depoimentos">
        <div class="main">
            <div class="title">
                <h3>DEPOIMENTOS</h3>
                <div class="arrows"><i class="fas fa-arrow-left"></i><i class="fas fa-arrow-right"></i></div>
            </div>
            
            <div class="scroll">
                <div class="slider">
                    <div class="item">
                    <?php
                        $args = array(
                            'post_type'         => 'depoimento',
                            'post_status'       => 'publish',
                            'posts_per_page'    => 10
                        );
                        $query = new WP_Query($args);
                        $i=1;
                        while ($query->have_posts()) : $query->the_post();

                            echo ' <div class="depoimento">
                                    <div class="text">'.wp_trim_words( get_the_content(), 30, '...' ).'</div>
                                    <strong class="name">'.get_the_title().'</strong>
                                </div>';

                                if($i % 3 == 0 && $i < $query->found_posts )
                                    echo '</div><div class="item">';
                        $i++;
                        endwhile;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="bloco-blog">
        <div class="main">
            <div class="title">
                <h3>BLOG</h3>
            </div>
            
            <div class="scroll">
                <div class="grid">
                    <?php
                        $args = array(
                            'post_type'         => 'post',
                            'post_status'       => 'publish',
                            'posts_per_page'    => 3
                        );
                        $query = new WP_Query($args);
                        while ($query->have_posts()) : $query->the_post();
                            $urlThumb = get_the_post_thumbnail_url($post->ID, 'large');
                            // $urlThumb = 'https://www.aboaterra.com.br/wp-content/' . explode('wp-content/', $urlThumb)[1];

                            echo '<div class="_item">
                                    <figure><a href="'.get_the_permalink().'"><img src="'.$urlThumb.'"></a></figure>
                                    <div class="ctnText">
                                        <h4>'.get_the_title().'</h4>
                                        <span class="date">'.get_the_date('j \d\e F \d\e Y').'</span>
                                        <div class="text">'.wp_trim_words( get_the_content(), 6, '...' ).'</div>
                                        <a href="'.get_the_permalink().'" class="link">SAIBA MAIS</a>
                                    </div>
                            </div>';
                        endwhile;
                    ?>
                </div>
            </div>
            <a href="<?php echo $link_blog?>/blog" class="btn center">MAIS PUBLICAÇÕES</a>
        </div>
    </section>

    <section class="bloco-links">
        <div class="main">
            <div class="collun">
                <img src="<?php echo $url_tema?>assets/images/icons/indique.svg" class="icon">
                <div class="right">
                    <a href="<?php echo $link_blog?>/indique-um-amigo/">
                        <h3>INDIQUE UM AMIGO</h3>
                        <div class="text"><strong>Indique um amigo</strong> e ganhe desconto em suas próximas compras.</div>
                    </a>
                </div>
            </div>
            <div class="collun">
                <img src="<?php echo $url_tema?>assets/images/icons/land.svg" class="icon">
                <div class="right">
                    <a href="<?php echo $link_blog?>/quer-nos-visitar/">
                        <h3>QUER NOS VISITAR?</h3>
                        <div class="text">Todo sábado, temos Feira Orgânica, de <strong>8h30 às 12h30</strong>.</div>
                    </a>
                </div>
            </div>
            <div class="collun">
                <img src="<?php echo $url_tema?>assets/images/icons/chat.svg" class="icon">
                <div class="right">
                    <a href="<?php echo $link_blog?>/faq">
                        <h3>COMO FUNCIONA?</h3>
                        <div class="text">Acesse nossa página e tire todas as suas dúvidas.</div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <?php if( get_field('ativar_popup', 'options') ): ?>
    <div class="popup-abt">
        <i class="close"><i class="fas fa-times"></i></i>
        <div class="content"><img src="<?php echo get_field('imagem_popup', 'options')['sizes']['large'] ?>" alt=""></div>
    </div>
    <div class="popup-abt-overflow"></div>
    <?php endif; ?>

</main>	

<?php
get_footer();