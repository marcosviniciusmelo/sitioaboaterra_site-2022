<?php
	

// add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
//   function jk_related_products_args( $args ) {
// 	$args['posts_per_page'] = 5; // 4 related products
// 	$args['columns'] = 5; // arranged in 2 columns
// 	return $args;
// }


add_filter( 'add_to_cart_text', 'woo_custom_cart_button_text' ); // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' ); // 2.1 +

function woo_custom_cart_button_text() {
    return __( 'Adicionar', 'woocommerce' );
}


