<?php

/**
 * Export to TXT class.
 *
 */
class Brasa_Order_process {
    /**
     * @var string
     */
    private $nonce_action = 'process_products';


    /**
     * init class
     * @return boolean
     */
    public function __construct() {
        // register metabox
        add_action( 'admin_init', array( $this, 'register_metabox_process' ) );

        add_action( 'wp_ajax_brasa_order_process', array( $this, 'process_products' ) );
    }
    /**
     * Register metabox
     * @return boolean
     */
    public function register_metabox_process() {
        if ( ! isset( $_GET[ 'post'] ) ) {
            return;
        }
        add_meta_box( 'order-process', __( 'Processar assinatura', 'odin' ), array( $this, 'metabox_display' ), 'shop_subscription', 'side', 'default' );
    }
    /**
     * Display download button
     * @return boolean
     */
    public function metabox_display() {
        if ( ! isset( $_GET[ 'post'] ) ) {
            return;
        }

        // $url = get_bloginfo('url') . '/processa-pedidos-auto/?order_id='.$_GET[ 'post'];

        $url = admin_url( 'admin-ajax.php' );
        $url .= '?action=brasa_order_process';
        $url .= '&id=' . $_GET[ 'post' ];
        $url = wp_nonce_url( $url, $this->nonce_action, '_wpnonce' );

        // printf( '<a class="button button-primary" href="%s">%s</a>', $url, __( 'Processar', 'odin' ) );
        printf( '<a class="button button-primary" href="%s">%s</a>', $url, __( 'Processar', 'odin' ) );
    }

    /**
     * create & download TXT file
     * @return boolean
     */

    public function process_products() {
        if ( ! isset ( $_GET[ '_wpnonce' ] ) || ! wp_verify_nonce( $_GET[ '_wpnonce' ], $this->nonce_action ) ) {
            return;
        }

        $order_id = $_GET['id'];
        process($order_id);
        wp_die();
    }
}


function process($order_id) {
    $subscription = new WC_Subscription( $order_id );
    $order = wc_get_order($subscription->parent_id);

    $arrProducts = null;
    // echo '<pre>';

    $quantity = 1;

    // if( $order_id == '48196' ):
    //     $productId = 26352;
    //     $price = 50.0;
    //     addItemOrder($order_id, get_the_title($productId), $productId, intval($quantity), $price);
    //     addItemOrder($subscription->parent_id, get_the_title($productId), $productId, intval($quantity), $price);
    //     return;
    // endif;

    // if( $order_id == '70930' ){
    //     $nextDate = getNextDateOrder( $order_id, $subscription->parent_id );
    //     print_r( $nextDate );
    //     return;
    // }


    $hasItens = false;

    if( get_post_meta( $order_id, "assinatura_itens", true ) )
        if( get_post_meta( $order_id, "assinatura_itens", true ) != 'null' )
            $hasItens = true;

    if( $hasItens ):

        $arrProducts = json_decode(get_post_meta( $order_id, "assinatura_itens", true ));
        
        if( get_post_meta( $order_id, "assinatura_product_quantity", true ) ):
            $quantity = get_post_meta( $order_id, "assinatura_product_quantity", true );
        else:
            foreach ( $order->get_items() as $item_id => $item ) {
                $quantity = $item->get_quantity();
            }
            saveInfoOrder($order_id, null, null, $quantity);
            saveInfoOrder($subscription->parent_id, null, null, $quantity);
        endif;

    else:
        foreach ( $order->get_items() as $item_id => $item ) {

            $quantity = $item->get_quantity();

            $sku = $item->get_product()->get_sku();
            switch( $sku ){
                case '0006':
                    $arrProducts = [2,5,2,1];
                    break;

                case '0007':
                    $arrProducts = [0,0,5,0];
                    break;

                case '0008':
                    $arrProducts = [2,5,4,1];
                    break;

                case '0009':
                    $arrProducts = [1,3,2,1];
                    break;

                case '0012':
                    $arrProducts = [2,3,5,0];
                    break;

                case '0013':
                    $arrProducts = [0,6,5,0];
                    break;

                case '0014':
                    $arrProducts = [1,3,7,1];
                    break;

                case '0015':
                    $arrProducts = [2,5,7,1];
                    break;

                case '0016':
                    $arrProducts = [2,5,9,1];
                    break;
            }

            saveInfoOrder($order_id, $item->get_product()->get_id(), json_encode($arrProducts), $quantity);
            saveInfoOrder($subscription->parent_id, $item->get_product()->get_id(), json_encode($arrProducts), $quantity);

        }
    endif;

    
    $parent_id = $subscription->parent_id;
    $listFinal = [];
    $renew_id = null;

    if( $subscription->get_related_orders() ):
        $orderRelated = $subscription->get_related_orders();
        $renew_id = array_key_first($orderRelated);
    endif;

    if( $renew_id )
        if( $renew_id == $parent_id )
            $renew_id = null;
    

    // Update next date
    getNextDateOrder( $order_id, $subscription->parent_id, $renew_id );

    // return;

    /*
    * Remove all items
    */
    removeItemsOrder($order_id);
    removeItemsOrder($parent_id);
    if( $renew_id ) removeItemsOrder($renew_id);

    /*
    * Get product avulsos
    */
    if( get_post_meta( $order_id, "assinatura_product_avulsos", true ) ):
        foreach ( json_decode(get_post_meta( $order_id, "assinatura_product_avulsos", true )) as $avulso) {

            $price = $avulso->price / $avulso->quantity;

            addItemOrder($order_id, get_the_title($avulso->productId), $avulso->productId, $avulso->quantity, $price);
            addItemOrder($parent_id, get_the_title($avulso->productId), $avulso->productId, $avulso->quantity, $price);
            if( $renew_id ) addItemOrder($renew_id, get_the_title($avulso->productId), $avulso->productId, $avulso->quantity, $price);
            array_push($listFinal, $avulso->productId);
        }
    endif;

    // return;

    /*
    * Get products by categories
    */
    foreach ( $arrProducts as $key => $value) {
        if( $value > 0 ):
            switch ($key) {
                case '0':
                    $category = 'Verduras';
                    break;

                case '1':
                    $category = 'Legumes';
                    break;

                case '2':
                    $category = 'Frutas';
                    break;

                case '3':
                    $category = 'Temperos';
                    break;
            
            }

            for ($i=0; $i < $value; $i++) { 
                $productId = getProductByCategory($category, get_post_meta( $order_id, "assinatura_restricoes", true ), $listFinal);
                $product = wc_get_product( $productId );

                if( $product instanceof WC_Product ):
                    $price = 0.00;
                    if( $product->get_price() != '' )
                        $price = $product->get_price();

                    addItemOrder($order_id, get_the_title($productId), $productId, intval($quantity), $price);
                    addItemOrder($parent_id, get_the_title($productId), $productId, intval($quantity), $price);
                    if( $renew_id ) addItemOrder($renew_id, get_the_title($productId), $productId, intval($quantity), $price);
                    array_push($listFinal, $productId);
                endif;
            }
        endif;
    }

    echo '<p><h3>Assinatura atualizada com sucesso</h3></p>';
    echo '<p>Redirecionando...</p>';
    $redirect = get_bloginfo('url').'/wp-admin/post.php?post='.$order_id.'&action=edit';
    echo '<script>window.location.replace("'.$redirect.'");</script>';
    wp_die();
}


// Save info in old orders
function saveInfoOrder($order_id, $product_id = null, $items = null, $quantity = null){
    if( $product_id ) update_post_meta($order_id, 'assinatura_product', $product_id );
    if( $items ) update_post_meta($order_id, 'assinatura_itens', $items);
    if( $quantity ) update_post_meta($order_id, 'assinatura_product_quantity', $quantity);
}


// Remove all products
function removeItemsOrder($order_id){
    $order = wc_get_order($order_id);
    $order_items = $order->get_items();
    foreach( $order_items as $item_id => $order_item ) {
        wc_delete_order_item($item_id);
    }
}

// Add new product
function addItemOrder($order_id, $productName, $productId, $qty, $price){
    $order_item_id = wc_add_order_item( $order_id, array(
        'order_item_name' => $productName,
        'order_item_type' => 'line_item', // product
    ));

    
    wc_add_order_item_meta( $order_item_id, '_qty', $qty, true ); // quantity
    wc_add_order_item_meta( $order_item_id, '_product_id', $productId, true ); // ID of the product
    wc_add_order_item_meta( $order_item_id, '_line_subtotal', $price * $qty, true ); // price per item
    wc_add_order_item_meta( $order_item_id, '_line_total', $price * $qty, true ); // total price

    $order = wc_get_order($order_id);
    //$order->calculate_totals(); // Recalculate
}

// Get product by price in the list
function getProductByCategory($category, $restricoes, $listFinal){
    $separated = [];
    $arrSeparated = ['frutas', 'verduras', 'legumes', 'temperos'];
    foreach ($arrSeparated as $key) {
        foreach (get_field('produtos-selecao-semana-'.$key, 'options') as $k) {
            array_push($separated, $k );
        }
    }

    $return = null;
    foreach ($separated as $item) {
        $cat = get_the_terms($item, 'product_cat');

        // if( $listFinal )
        //     if( count($listFinal) > 0 )
        if( $cat[ count($cat) - 1 ]->name == $category && !in_array($item, json_decode($restricoes)) && !in_array($item, $listFinal) )
            $return = $item;
    }

    if( !$return ):
        $rand = $separated[ rand(0, count($separated)) ];
        if( !in_array($rand, json_decode($restricoes) ) )
            $return = $rand;
    endif;

    return $return;
}


// Save info in old orders
function getNextDateOrder($order_id, $parent_id, $renew_id = null){
    
    $day = get_post_meta( $order_id, "data_entrega", true );

    if( !$day ) return;
    if( $day == '' ) return;

    $day = mb_strtolower(trim($day));
    
    $frequencia = 'semanal';
    if( get_post_meta( $parent_id, "assinatura_frequencia", true ) ) $frequencia = get_post_meta( $parent_id, "assinatura_frequencia", true );

    $day = explode(' (', $day);
    switch (trim($day[0])) {
        case 'segunda-feira':
            $weekDay = 'monday';
            break;
        case 'terça-feira':
            $weekDay = 'tuesday';
            break;
        case 'quarta-feira':
            $weekDay = 'wednesday';
            break;
        case 'quinta-feira':
            $weekDay = 'thursday';
            break;
        case 'sexta-feira':
            $weekDay = 'friday';
            break;
        case 'sábado':
            $weekDay = 'saturday';
            break;
        case 'domingo':
            $weekDay = 'sunday';
            break;
    }

    

    $nextData = strtotime('next '. $weekDay);
    $arrayDay = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];

    $nextDay = $arrayDay[date('w', $nextData)] . ' (' .date('d/m/Y', $nextData).')';

    if( $arrayDay[date('w', $nextData)] == $arrayDay[date('w')] ){
        $nextDay = $arrayDay[date('w')] . ' (' .date('d/m/Y').')';
    }

    // echo $nextDay;
    // return;
    
    update_post_meta($order_id, 'data_entrega', $nextDay);
    update_post_meta($parent_id, 'data_entrega', $nextDay);

    if( $renew_id )
        update_post_meta($renew_id, 'data_entrega', $nextDay);

    return $nextDay;
    
}


add_action( 'manage_posts_extra_tablenav', 'admin_order_list_top_bar_button', 20, 1 );
function admin_order_list_top_bar_button( $which ) {
    global $typenow;

    if ( 'shop_subscription' === $typenow && 'top' === $which ) {
        ?>
        <div class="alignleft actions custom">
            <a href="<?php echo get_bloginfo('url') . '/processa-pedidos-auto' ?>" style="height:32px; background-color: #2271b1; color: #fff;" class="button"><?php
                echo __( 'Processar assinaturas', 'woocommerce' ); ?></a>
        </div>
        <?php
    }
}

new Brasa_Order_process();
