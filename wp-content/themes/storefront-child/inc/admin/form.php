<?php
global $url_tema, $url_site, $nome_blog; the_post();
global $wpdb;

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="<?php echo $url_tema; ?>assets/css/admin.css" rel="stylesheet">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<style>
		.submit{
			width: 150px;
			height: 40px;
		}
	</style>
</head>
<body>
	
	<div class="jumbotron adminContent">
		<ul class="nav nav-tabs" id="myTab" role="tablist">

			<li class="nav-item">
		    	<a class="nav-link active" id="geral-tab" data-toggle="tab" href="#geral" role="tab" aria-controls="geral" aria-selected="false">Geral</a>
		  	</li>
			<li class="nav-item">
		    	<a class="nav-link" id="regras-tab" data-toggle="tab" href="#regras" role="tab" aria-controls="regras" aria-selected="false">Regras por bairros</a>
		  	</li>
		 	<li class="nav-item">
		    	<a class="nav-link" id="csv-tab" data-toggle="tab" href="#csv" role="tab" aria-controls="csv" aria-selected="true">Importar CSV</a>
		  	</li>
		</ul>

		<div class="tab-content mt-4" id="myTabContent">
			
			<div class="tab-pane fade show active" id="geral" role="tabpanel" aria-labelledby="geral-tab">
				<form method="POST">
					<?php
						global $wpdb;
						$rolesG = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_roles_shipping_general") );
					?>
					<div class="row">
						<div class="col">
							<label>Dias de entrega</label>
							<input type="text" class="form-control" placeholder="Dias de entrega" name="dias_entrega" value="<?php if( isset($rolesG[0]->dias) ) echo $rolesG[0]->dias ?>">
						</div>
						<div class="col">
							<label>Horários de entrega</label>
							<input type="text" class="form-control" placeholder="Horários de entrega" name="horario_entrega" value="<?php if( isset($rolesG[0]->horario) ) echo $rolesG[0]->horario ?>">
						</div>
						<div class="col">
							<label>Hora Limite</label>
							<input type="text" class="form-control" placeholder="Hora Limite" name="hora_limite" value="<?php if( isset($rolesG[0]->hora_limite) ) echo $rolesG[0]->hora_limite ?>">
						</div>
						<div class="col">
							<label style="width: 100%">Bloquear Segunda-feira (Quando comprado fora do horário)</label>
							<input type="checkbox" class="form-control" name="block_saturday" value="1" <?php if( isset($rolesG[0]->block_saturday) ) echo ($rolesG[0]->block_saturday == 1) ? 'checked' : '' ?>>
						</div>
						<button type="submit" class="btn btn-primary submit" style="margin-top: 30px;">ATUALIZAR</button>
					</div>
				</form>
			</div>

			<div class="tab-pane fade" id="regras" role="tabpanel" aria-labelledby="regras-tab">
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Cidade</th>
							<th scope="col">Bairro</th>
							<th scope="col">Dias</th>
							<th scope="col">Horários</th>
						</tr>
					</thead>
					<tbody>
						<?php
							global $wpdb;
							$roles = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_roles_shipping") );

							// print_r($roles);
							foreach ($roles as $key) {

								$days = '';
								$roles_days = $wpdb->get_results(
									$wpdb->prepare("SELECT * FROM wp_roles_shipping_day WHERE role_id=%d", $key->id)
								);

								$i=1;
								foreach ($roles_days as $keyD) {

									if( count($roles_days) == $i ):
										$days .= $keyD->day;
									else:
										$days .= $keyD->day.' | ';
									endif;
									$i++;
								}

								echo '<tr>
										<th scope="row">'.$key->id.'</th>
										<td>'.$key->cidade.'</td>
										<td>'.$key->bairro.'</td>
										<td>'.$days.'</td>
										<td>'.$key->horario.'</td>
									</tr>';

							}
						?>
						
						
					</tbody>
				</table>
			</div>

			<div class="tab-pane fade show" id="csv" role="tabpanel" aria-labelledby="csv-tab">
				<form id="formUpload" class="" method="POST" enctype="multipart/form-data" action="">
					<div class="form-group groupFile groupContent">
						<label for="imput2">Arquivo (.csv)</label>
						<input type="file" name="files" class="form-control" id="imput2" required>
						<button type="submit" class="btn btn-primary submit" style="margin-top: 10px;">ATUALIZAR <img src="<?php echo $url_tema; ?>assets/images/icones/ajax-loader.gif" style="display: none;"></button>
					</div>
				</form>
			</div>
		</div>

	</div>

	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

	
</body>
</html>
