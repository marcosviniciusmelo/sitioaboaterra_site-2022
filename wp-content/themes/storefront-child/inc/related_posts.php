<div class="relateds">
	
	<div class="title">Textos relacionados</div>

	<div class="posts col-3">
		<?php

			$post_terms = wp_get_object_terms(get_the_ID(), 'category', array('fields'=>'ids'));

			$args = array(
			    'post_type' 	 => 'post',
			    'posts_per_page' => 3,
			    'post_status'    => 'publish',
			    'post__not_in'   => array(get_the_ID()),
			    'tax_query' => array(
			        array(
			            'taxonomy' => 'category',
			            'field' => 'id',
			            'terms' => $post_terms
			        )
			    )
			);

			$query = new WP_Query($args);
			while ($query->have_posts()) : $query->the_post();
			?>	
			
			<li class="_itemGrid">
					<div class="ctn">
						<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
						<div class="bottom">
							<ul class="tags">
								<?php
									$ic=0;
									foreach ( get_the_terms($post->ID, 'category') as $key):
										if( $ic > 0 ) continue;

										$idCat = $key->term_id;
										if( $key->name != 'Crítica' && $key->parent != '' )
											$idCat = $key->parent;

										echo '<li><a style="background-color: '.get_field('cor', 'category_'.$idCat).';" href="'.get_the_permalink().'">'.get_cat_name( $idCat ).'</a></li>';
										$ic++;
									endforeach;
								?>
							</ul>
							<span class="date"><?php echo get_the_date('j \d\e F \d\e Y') ?></span>
							<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
						</div>
						<figure class="bg">
							<?php 
								if( get_field('imagem_listagem') != '' ):
									echo '<img src="'.get_field('imagem_listagem')['url'].'" alt="">';
								else:
									the_post_thumbnail('medium');
								endif; 
							?>
						</figure>
					</div>
				</li>

			<?php	
			endwhile;
			wp_reset_postdata();
		?>
	</div>
</div>