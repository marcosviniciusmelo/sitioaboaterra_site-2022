<?php
    global $wpdb;

    $paged = 1;
    if( isset($_GET['pagina']) )
        $paged = $_GET['pagina'];

    $showposts = 10;
    $offset = ($paged - 1) * $showposts;

    $query = "
        SELECT ID FROM {$wpdb->prefix}posts 
        WHERE post_type LIKE 'shop_subscription' AND post_status LIKE 'wc-active'
    ";
    $subscriptions_ids = $wpdb->get_col($query);
    $listOrders = [];
    $iF = 1;
    
    foreach ($subscriptions_ids as $order_id):
        if( !get_post_meta( $order_id, "assinatura_itens", true ) ) continue;

        // if( $order_id != '67293' ) continue;
       
        if( $iF > $offset):

            if( count($listOrders) >= $showposts ) continue;

            $subscription = new WC_Subscription( $order_id );
            $order = wc_get_order($subscription->parent_id);

            $data = $order->get_data();
            $nextPayment = date('d/m/Y', strtotime($subscription->get_date('next_payment', 'site')));
            $today = date('d/m/Y');

            array_push($listOrders, $order_id);

            $parent_id = $subscription->parent_id;
            $renew_id = null;

            if( $subscription->get_related_orders() ):
                $orderRelated = $subscription->get_related_orders();
                $renew_id = array_key_first($orderRelated);
            endif;
    
            if( $renew_id )
                if( $renew_id == $parent_id )
                    $renew_id = null;

            if( $order->get_status() != 'processing' ){

                if( $renew_id ):
                    $renew_order = wc_get_order($renew_id);
                        if( $renew_order->get_status() != 'processing' ):
                            continue;
                        endif;
                else:
                    continue;
                endif;

            }

            // Update next date
            getNextDateOrder( $order_id, $parent_id, $renew_id );

            $listFinal = [];

            //Get quantity
            if( get_post_meta( $order_id, "assinatura_product_quantity", true ) ):
                $quantity = get_post_meta( $order_id, "assinatura_product_quantity", true );
            else:
                foreach ( $order->get_items() as $item_id => $item ) {
                    $quantity = $item->get_quantity();
                }
                saveInfoOrder($order_id, null, null, $quantity);
                saveInfoOrder($parent_id, null, null, $quantity);
            endif;

            /*
            * Remove all items
            */
            removeItemsOrder($order_id);
            removeItemsOrder($parent_id);
            if( $renew_id ) removeItemsOrder($renew_id);

            /*
            * Get product avulsos
            */
            if( get_post_meta( $order_id, "assinatura_product_avulsos", true ) ):
                foreach ( json_decode(get_post_meta( $order_id, "assinatura_product_avulsos", true )) as $avulso) {
                    addItemOrder($order_id, get_the_title($avulso->productId), $avulso->productId, $avulso->quantity, $avulso->price);
                    addItemOrder($parent_id, get_the_title($avulso->productId), $avulso->productId, $avulso->quantity, $avulso->price);
                    if( $renew_id ) addItemOrder($renew_id, get_the_title($avulso->productId), $avulso->productId, $avulso->quantity, $avulso->price);
                    array_push($listFinal, $avulso->productId);
                }
            endif;

            /*
            * Get products by categories
            */
            foreach ( json_decode(get_post_meta( $order_id, "assinatura_itens", true )) as $key => $value) {
                if( $value > 0 ):
                    switch ($key) {
                        case '0':
                            $category = 'Verduras';
                            break;

                        case '1':
                            $category = 'Legumes';
                            break;

                        case '2':
                            $category = 'Frutas';
                            break;

                        case '3':
                            $category = 'Temperos';
                            break;
                    
                    }

                    for ($i=0; $i < $value; $i++) { 
                        $productId = getProductByCategory($category, get_post_meta( $order_id, "assinatura_restricoes", true ), $listFinal);
                        $product = wc_get_product( $productId );

                        if( $product instanceof WC_Product ):
                            $price = 0.00;
                            if( $product->get_price() != '' )
                                $price = $product->get_price();
                            addItemOrder($order_id, get_the_title($productId), $productId, intval($quantity), $price);
                            addItemOrder($parent_id, get_the_title($productId), $productId, intval($quantity), $price);
                            if( $renew_id ) addItemOrder($renew_id, get_the_title($productId), $productId, intval($quantity), $price);
                            array_push($listFinal, $productId);
                        endif;
                    }
                endif;
            }
        endif;
    $iF++;
    endforeach;

    if( count($listOrders) < ($showposts - 1) ){
        echo '<p><h3>Assinaturas atualizadas com sucesso</h3></p>';
        echo '<br><br>';
        echo '<a href="'.get_bloginfo('url').'/wp-admin/edit.php?post_type=shop_subscription">Voltar para as assinaturas</a>';
        echo '<br><br>';
        echo '<p><h4>Assinaturas:</h4></p>';
        echo '<pre>';
        print_r($listOrders);
    }else{
        $pR = intval($paged)+1;
        header('Location: '.get_bloginfo('url').'/processa-pedidos-auto/?pagina='.$pR);
    }

    //Save custom data
    // $cart_item_data = array(
	// 	'assinatura_itens' => [2,2,1,1],
	// 	'assinatura_price' => 113.6,
	// 	'assinatura_frequencia' => 'semanal',
	// 	'assinatura_restricoes' => '[\"56906\",\"56901\",\"56898\",\"56899\",\"56142\",\"55787\"]',
	// 	'assinatura_product_avulsos' => '[{\"productId\":59590,\"quantity\":\"4\",\"price\":39.2},{\"productId\":59240,\"quantity\":\"1\",\"price\":12.5},{\"productId\":59238,\"quantity\":\"1\",\"price\":16.9}]',
	// 	'assinatura_items_total' => 12
	// );

    // foreach( $cart_item_data as $key => $value ){
    //     if( $key == 'assinatura_itens' ) $value = json_encode($value);
    //     update_post_meta( 58287, $key, $value);
    // }

?>