<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
?>
<?php global $url_tema, $nome_blog, $link_blog; ?>
<?php if( !is_front_page() && !is_page('contato') ): ?>
		</div><!-- .col-full -->
	</div><!-- #content -->
<?php endif; ?>

	<?php do_action( 'storefront_before_footer' ); ?>


	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->


<div class="modalBusca">
	<div class="bottom">
		<div class="main">
			<form action="<?php echo $link_blog ?>" method="GET">
				<div class="close"><i class="fas fa-times"></i></div>
				<input type="text" name="s" placeholder="Faça sua pesquisa">
				<button class="btn btn-bordered" type="submit">Buscar</button>
			</form>
		</div>
	</div>
</div>
<div class="overModalBusca"></div>


<section class="newsletter">
	<div class="main">
		<div class="text">
			<h4>MAIS SOBRE A BOA TERRA</h4>
			<span>Se cadastre e receba ofertas fresquinhas, novidades e notícias orgânicas direto no seu e-mail!</span>
		</div>

		<?php //echo do_shortcode('[contact-form-7 id="65610" title="Newsletter"]') ?>
		<div role="main" id="newsletter-site-9dd0d23a7b4a3aeb3e34"></div><script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script><script type="text/javascript"> new RDStationForms('newsletter-site-9dd0d23a7b4a3aeb3e34', 'UA-86552056-1').createForm();</script>

	</div>
</section>

<?php
	if( is_page('nossa-historia') || is_page('blog') || is_page('quer-nos-visitar') || is_singular('post') || is_page('contato') ){
		echo do_shortcode('[instagram-feed feed=1]');
	}
?>

<footer>
	<div class="main">
		<div class="lineTop">
			<div class="selos">
				<img src="<?php echo $url_tema?>assets/images/icons/organico-br.svg" class="selo">
                <img src="<?php echo $url_tema?>assets/images/icons/selo-02.svg" class="selo">
				<img src="<?php echo $url_tema ?>assets/images/icons/selo-qrcode.png" class="selo">
			</div>
			<div class="menu">
				<div class="collun">
					<strong>Sítio A Boa Terra</strong>
					<?php echo sno_show_menu('nav-institucional') ?>
				</div>
				<div class="collun">
					<strong>Fale Conosco</strong>
					<li>
						<i class="fab fa-whatsapp"></i>
						<span><a href="https://api.whatsapp.com/send?phone=5519991697729" target="_blank">(19) 99169-7729</a></span>
					</li>
					<li>
					<i class="far fa-envelope"></i>
						<span><a href="mailto:contato@aboaterra.com.br" target="_blank">contato@aboaterra.com.br</a></span>
					</li>
					<li>
						<i class="far fa-comment"></i>
						<span><a href="<?php echo $link_blog ?>/faq" target="_blank">Dúvidas frequentes</a></span>
					</li>
				</div>
				<div class="collun">
					<strong>Links Importantes</strong>
					<?php echo sno_show_menu('nav-politica') ?>
				</div>
			</div>
		</div>

		<div class="redesSociais">
			<span>Siga A Boa Terra:</span>
			<div class="redes">
				<a href="https://www.instagram.com/aboaterra/" target="_blank"><i class="fab fa-instagram"></i> Instagram</a>
				<a href="https://www.facebook.com/ABoaTerra" target="_blank"><i class="fab fa-facebook-f"></i> Facebook</a>
				<!-- <a href="" target="_blank"><i class="fab fa-linkedin-in"></i> Linkedin</a> -->
			</div>
		</div>

		<div class="address">Sítio A Boa Terra : Rodovia SP 350 . Km 245 . Estrada Casa Branca . Itobi . SP  <br> Centro de Distribuição: Rua Alvarenga Peixoto, 380 - Vila Anastácio. São Paulo. SP</div>

		<div class="lineBottom">
			<div class="left">
				<p>Copyright <?php echo date('Y') ?> @ A Boa Terra Essência Orgânica</p>
				<p>CNPJ: 03.253.797/0001-27</p>
			</div>
			<div class="brand"><a href="<?php echo $link_blog ?>"><img src="<?php echo $url_tema ?>assets/images/brand-vertical.svg" alt="<?php echo $nome_blog ?>"></a></div>
			<div class="right">
				<div class="icon"><img src="<?php echo $url_tema ?>assets/images/icons/visa.svg"></div>
				<div class="icon"><img src="<?php echo $url_tema ?>assets/images/icons/master.svg"></div>
				<div class="icon"><img src="<?php echo $url_tema ?>assets/images/icons/picpay.svg"></div>
				<div class="icon"><img src="<?php echo $url_tema ?>assets/images/icons/pix.svg"></div>
				<div class="btnUp"><i class="fas fa-arrow-up"></i></div>
			</div>
		</div>
	</div>
</footer>

<div class="badgeAdded"><i class="fas fa-check-circle"></i> Carrinho atualizado <i class="fas fa-times close"></i></div>

<?php wp_footer(); ?>

<?php if( !is_page('finalizar-compra') ): ?>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/func-slick.js"></script>
<?php endif; ?>


<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/functions.js?<?php echo rand(000,9999) ?>"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/assinatura.js?<?php echo rand(000,9999) ?>"></script>

<?php if( is_checkout() || is_page('revise-sua-assinatura')): ?>

	<?php
		$cidadeAtendidas = [];
		$bairrosAtendidos = [];
		foreach (get_field('cidades_atendidas', 'options') as $key) {
			array_push($cidadeAtendidas, $key['cidade']);
		}

		foreach (get_field('cidades_atendidas', 'options') as $key) {
			if( $key['bairros'] != '' ):
				$bairros = explode(',', $key['bairros']);
				foreach ($bairros as $k) {
					array_push($bairrosAtendidos, $key['cidade'].'-'.trim($k));
				}
			endif;
		}
	?>
	<input type="hidden" id="cidadesAtendidas" name="cidadesAtendidas" value="<?php echo implode(',', $cidadeAtendidas) ?>">
	<textarea id="bairrosAtendidos" name="bairrosAtendidos" style="display: none"><?php echo implode(',', $bairrosAtendidos) ?></textarea>

	<script src="https://amsul.ca/pickadate.js/vendor/pickadate/lib/picker.js"></script>
	<script src="https://amsul.ca/pickadate.js/vendor/pickadate/lib/picker.date.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/calendario-entrega.js?<?php echo rand(000,9999) ?>"></script>
<?php endif; ?>

<div class="overflowMenu"></div>

<?php if ( is_checkout() && !( is_wc_endpoint_url('order-received') ) ): ?>
	<input type="hidden" name="datasRegrasArr" id="datasRegrasArr">
	<script>
		var $ = jQuery;
		var html = '<div class="entrega24h" style="padding-left: 20px"></div><div class="calendario-datas"><h3>Dia da entrega</h3><p style="font-size: 16px; font-weight: 500;">*Para assinaturas, será a data da primeira entrega.</p><div class="calendario" id="dateShipping">Digite seu CEP, Cidade e Bairro na etapa anterior. </div><div id="clickCalendario" class="btn btn-success" style="display: none;">Escolher outra data de entrega</div></div>';
		$(html).insertAfter('#order_review_heading');

		setTimeout(function(){
			var html24h = $('#order_delivery_time_field').html();
			$('.entrega24h').html(html24h);
			$('#order_delivery_time_field').remove();
			
		}, 200);
	</script>
<?php endif; ?>

<?php echo get_field('codigo_footer', 'options') ?>

</body>
</html>
