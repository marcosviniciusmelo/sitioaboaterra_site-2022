<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */	
?>
<?php global $url_tema, $nome_blog, $link_blog, $api_url; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo $url_tema ?>assets/images/favicon.png" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/normalize.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css?<?php echo rand(000,999) ?>" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


<?php if(is_single()): ?>
	<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5f9fdd353b1cfe00120b72ed&product=inline-share-buttons" async="async"></script>
<?php endif; ?>

<?php if( is_page('finalizar-compra') || is_page('revise-sua-assinatura')): ?>
	<link rel="stylesheet" href="https://amsul.ca/pickadate.js/vendor/pickadate/lib/themes/default.css" id="theme_base">
	<link rel="stylesheet" href="https://amsul.ca/pickadate.js/vendor/pickadate/lib/themes/default.date.css" id="theme_date">
<?php endif; ?>

<?php if( !is_page('finalizar-compra') ): ?>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.css"/>
<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

<?php if( is_page('nossa-historia') ): ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<?php endif; ?>

<script>
    var url = '<?php echo $link_blog ?>';
    var api_url = '<?php echo $api_url ?>';
</script>

<?php wp_head(); ?>

<?php echo get_field('codigo_header', 'options') ?>

</head>

<body <?php body_class(); ?>>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">

	<header id="masthead" class="site-header">
		<div class="lineTop">
			<div class="main">
				<div class="left">
					<div class="item">
						<img src="<?php echo $url_tema ?>assets/images/icons/shipping.svg" alt="" class="icon">
						<span><strong>Entrega Grátis</strong> EM SUA primeira compra</span>
					</div>
				</div>
				<div class="right">
					<!-- <div class="item">
						<a href="">
							<img src="<?php echo $url_tema ?>assets/images/icons/heart.svg" alt="" class="icon">
							<span>Minha Lista</span>
						</a>
					</div> -->
					<div class="item redesSociais">
						<span>Siga A Boa Terra:</span>
						<div class="redes">
							<a href="https://www.instagram.com/aboaterra/" target="_blank"><img src="<?php echo $url_tema ?>assets/images/icons/instagram.svg" class="icon"></a>
							<a href="https://www.facebook.com/ABoaTerra" target="_blank"><img src="<?php echo $url_tema ?>assets/images/icons/facebook.svg" class="icon"></a>
							<!-- <a href="" target="_blank"><img src="<?php echo $url_tema ?>assets/images/icons/linkedin.svg" class="icon"></a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lineBottom">
			<div class="main">
				<div class="left">
					<img src="<?php echo $url_tema ?>assets/images/icons/icon-menu.svg" class="iconMenu">
					<div class="brand"><a href="<?php echo $link_blog ?>"><img src="<?php echo $url_tema ?>assets/images/brand.svg" alt="<?php echo $nome_blog ?>"></a></div>
				</div>
				
				<div class="menu">
					<i class="fas fa-times closeMenuMobile"></i>
					<?php echo sno_show_menu('nav-main') ?>
				</div>
				<div class="right">
					<a href="#openSearch" class="item clickBusca"><img src="<?php echo $url_tema ?>assets/images/icons/search.svg" class="icon"><span>BUSCAR</span></a>
					<div class="item ItemConta">
						<img src="<?php echo $url_tema ?>assets/images/icons/account.svg" class="icon"><span>MINHA CONTA</span>
						<div class="dropdownConta <?php echo( is_user_logged_in() ? 'mini' : '' ) ?>">
							<?php if( is_user_logged_in() ): ?>
								<?php echo sno_show_menu('nav-conta') ?>
							<?php else: ?>
								<?php echo do_shortcode('[xoo_el_inline_form]') ?>
								<div class="form" id="loginHeader">
									<!-- <div class="field"><input type="text" name="login" placeholder="Login" required></div>
									<div class="field"><input type="password" name="password" placeholder="Senha" required></div>
									<div class="row">
										<button type="submit">ENTRAR</button>
										<a href="<?php echo $link_blog ?>/minha-conta" class="esqueci">Esqueci minha senha ou email</a>
									</div> -->
									<div class="row"><?php echo do_shortcode('[nextend_social_login redirect="'.$link_blog.'/'.trim( $_SERVER["REQUEST_URI"] , '/' ).'"]') ?></div>

									<div class="row">
										<div class="left">
											<strong>Novo usuário?</strong>
											<a href="<?php echo $link_blog ?>/minha-conta">Cadastre-se</a>
										</div>
										<div class="right"></div>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<a href="<?php echo $link_blog ?>/carrinho" class="item linkCart">
						<div class="number"></div>
						<img src="<?php echo $url_tema ?>assets/images/icons/cart.svg" class="icon"><span>MEU CARRINHO</span>
					</a>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	
	<?php if( !is_front_page() ): ?>
		<div class="submenu_main">
			<div class="main">
				<?php echo sno_show_menu('nav-submenu') ?>
			</div>
		</div>
	<?php endif; ?>

	<?php if( !is_front_page() && !is_page('contato') && !is_search() && !is_shop() && !is_product_category() && !is_page('blog') && !is_page('nossa-historia') && !is_page('escolher-produtos') && !is_page('revise-sua-assinatura') ): ?>
	<div id="content" class="site-content <?php echo (!is_shop()) ? 'inner-page' : 'produtos-page' ?>" tabindex="-1">
		<div class="col-full">
	<?php endif; ?>
		<?php do_action( 'storefront_content_top' );
