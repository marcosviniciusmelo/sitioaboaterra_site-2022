<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

// Variáveis para todo o tema
$nome_blog = get_bloginfo('name');
$link_blog = get_bloginfo('url');
$url_tema  = get_stylesheet_directory_uri() . '/';
 


/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}

date_default_timezone_set('America/Sao_Paulo');


function removeAssTitle($string){
	$string = str_replace('(Assinatura)', '', $string);
	$string = str_replace('(assinaturas)', '', $string);
	return str_replace('(assinatura)', '', $string);
}

register_sidebars( 2, array( 'name' => 'Sidebar Product 01' ) );
register_sidebars( 1, array( 'name' => 'Sidebar Product 02' ) );

function remove_menu_items() {

    // remove_menu_page( 'index.php' ); //Painel
    // remove_menu_page('edit.php'); // Posts
    remove_menu_page('edit-comments.php'); // Comentarios

}
add_action( 'admin_menu', 'remove_menu_items' );

function sno_register_menus() 
{
    register_nav_menus(
        array(
            'nav-main' => 'Principal',
            'nav-footer' => 'Footer',
            'nav-institucional' => 'Institucional',
            'nav-contato' => 'Contato',
            'nav-politica' => 'Política',
            'nav-conta' => 'Conta',
			'nav-submenu' => 'Submenu',
        )
    );
}

add_action('init', 'sno_register_menus');


// Função para exibir menu
function sno_show_menu($name_menu) 
{
	$output = wp_nav_menu(array(
		'theme_location' => $name_menu,
		'items_wrap' => '<ul class="listaMenu">%3$s</ul>',
		'echo' => 0,
		'link_before' => '',
		'link_after' => ''
	));
    return $output;
}

// Função para exibir menu
function my_acf_init() {

    // acf_update_setting('google_api_key', 'AIzaSyD98GTZK-eHP4wk0XGwM6E0S1Vf1nUwt3Q');
    
    if( function_exists('acf_add_options_page') ) {
        
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Gerenciar produtos semanal', 'my_text_domain'),
            'menu_title'    => __('Gerenciar produtos semanal', 'my_text_domain'),
            'menu_slug'     => 'opcoes',
        ));
    }
}
add_action('acf/init', 'my_acf_init');


include('inc/custom-woo.php');

/*
* Slider
*/
function custom_slider() {
    $labels = array(
      'name'                => ( 'Sliders'),
      'singular_name'       => ( 'Slider'),
      );
    
    register_post_type( 'slider',
        array(
            'menu_icon'   => 'dashicons-admin-post',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array( 'title' ),
        )
    );

}
add_action('init', 'custom_slider');


/*
* Slider
*/
function custom_depoimento() {
    $labels = array(
      'name'                => ( 'Depoimentos'),
      'singular_name'       => ( 'Depoimento'),
      );
    
    register_post_type( 'depoimento',
        array(
            'menu_icon'   => 'dashicons-admin-post',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array( 'title', 'editor' ),
        )
    );

}
add_action('init', 'custom_depoimento');

/*
* Update number cart by ajax
*/
function updateNumberCart(){
    global $woocommerce;
    $count = $woocommerce->cart->cart_contents_count;

    echo $count;
    die();
}
add_action('wp_ajax_updateNumberCart', 'updateNumberCart');
add_action('wp_ajax_nopriv_updateNumberCart', 'updateNumberCart');



/*
* Build modal
*/
function buildModalProducts(){
    $type = $_POST['type'];

    $args = array(
        'post_type'         => 'product',
        'post_status'       => 'publish',
        'posts_per_page'    => 20,
        'orderby'   		=> 'menu_order',
        'order'     		=> 'ASC',
        'tax_query' => array(
            array (
                'taxonomy' => 'product_cat',
                'field' => 'name',
                'terms' => $type,
			),
			array(
				'taxonomy' => 'product_tag',
				'field'    => 'slug',
				'terms'    => 'para-assinaturas'
			)
        )
    );

    $query = new WP_Query($args);
    $html = '';
    while ($query->have_posts()) : $query->the_post();
        $thumb = get_the_post_thumbnail_url($post->ID, 'medium');
        $html .= '<div class="_item">
                    <figure><img src="'.$thumb.'"></figure>
                    <div class="right">'.removeAssTitle(get_the_title()).'</div>
                </div>';
    endwhile;

    echo $html;
    die();
}
add_action('wp_ajax_buildModalProducts', 'buildModalProducts');
add_action('wp_ajax_nopriv_buildModalProducts', 'buildModalProducts');


/*
* Build modal restrições
*/
function buildModalRestricoes(){
    $type = 'Verduras';

    if( isset($_POST['type']) && $_POST['type'] != null )
        $type = $_POST['type'];

    $args = array(
        'post_type'         => 'product',
        'post_status'       => 'publish',
        'posts_per_page'    => 200,
        'orderby'   		=> 'title',
        'order'     		=> 'ASC',
        'tax_query' => array(
            array (
                'taxonomy' => 'product_cat',
                'field' => 'name',
                'terms' => $type,
			),
			array(
				'taxonomy' => 'product_tag',
				'field'    => 'slug',
				'terms'    => 'para-assinaturas'
			)
        )
    );

    if( $_POST['search'] )
        $args['s'] = $_POST['search'];

    $query = new WP_Query($args);
    $html = '';
    while ($query->have_posts()) : $query->the_post();
        $thumb = get_the_post_thumbnail_url($post->ID, 'medium');
        $html .= '<a href="#checkRestricoes" class="_item">
                    <figure><img src="'.$thumb.'"></figure>
                    <div class="right">'.removeAssTitle(get_the_title()).'</div>
                    <input type="checkbox" name="productRestrict" value="'.get_the_ID().'">
                </a>';
    endwhile;

    echo $html;
    die();
}
add_action('wp_ajax_buildModalRestricoes', 'buildModalRestricoes');
add_action('wp_ajax_nopriv_buildModalRestricoes', 'buildModalRestricoes');


/*
* Get restrições
*/
function getListRestricoes(){
    
    $list = explode(',', $_POST['list']);
    $html = '';
    foreach ($list as $key) {
        $html .= '<li>• '.removeAssTitle(get_the_title($key)).'</li>';
    }
    echo $html;
    die();
}
add_action('wp_ajax_getListRestricoes', 'getListRestricoes');
add_action('wp_ajax_nopriv_getListRestricoes', 'getListRestricoes');




/*
* ADMIN
*/
add_action( 'admin_menu', 'gerenciarEntregas' );
function gerenciarEntregas()
{
    add_menu_page(
        'Regras de Entregas',     // page title
        'Regras de Entregas',     // menu title
        'manage_options',   // capability
        'regra_entrega',     // menu slug
        'render_regra_entrega' // callback function
    );
}

function render_regra_entrega(){
    global $title;
    global $wpdb;

    print '<div class="wrap">';
    print "<h1>$title</h1>";

    if( isset($_FILES['files']) ){

    	header("Content-type: text/html; charset=utf-8");

        $arquivos = $_FILES['files'];
        $path = plugin_dir_path( __FILE__ ) . "files";
        $extension = explode('.', $arquivos['name'][0]);
        $fileName = rand(11111,99999).'.csv';
        $uploaded = move_uploaded_file($arquivos['tmp_name'], $path .'/'. $fileName );


        if($uploaded){

            $csv = $path.'/'.$fileName;
            $handle = fopen($csv, "r");
            $header = true;
            $erro = '';
             // ==== GET HEADERS ==== 
            $counter = 0;
            while ( ! feof ( $handle ) )
            {     
                if ( $counter === 1 )
                    break;
             
                $buffer = fgetcsv ( $handle, 5000, ',' );

                ++$counter;
            }

            $wpdb->query('DELETE FROM wp_roles_shipping'); //Delete all role
            $wpdb->query('DELETE FROM wp_roles_shipping_day');  //Delete all role days


            /* ==== </> GET HEADERS ==== */
            while ($csvLine = fgetcsv($handle, 5000, ',') ):
                
                if( trim(utf8_encode($csvLine[0])) != '' ):

                    $data['cidade']  = trim($csvLine[0]);
                    $data['bairro']  = trim($csvLine[1]);
                    $data['horario'] = trim($csvLine[3]);

                    $wpdb->insert('wp_roles_shipping', $data); //Save roles
                    $role_id = $wpdb->insert_id;

                    if( trim($csvLine[2]) != '' ):
                    	$dias = explode('|', trim($csvLine[2]));

                    	foreach ($dias as $key) {
                    		$dataD['role_id'] = $role_id;
                    		$dataD['day'] = trim($key);
                    		$wpdb->insert('wp_roles_shipping_day', $dataD); //Save roles days
                    	}

                    endif;

                endif;

            endwhile;

            print '<div class="alert alert-success" role="alert">
                <h3>Regras atualizadas com sucesso.</h3>
            </div>';

            print '<a href="">Atualizar novamente</a>';

        }

    }elseif( isset($_POST['dias_entrega']) ){

    	$data['dias'] = trim($_POST['dias_entrega']);
    	$data['horario'] = trim($_POST['horario_entrega']);
    	$data['hora_limite'] = trim($_POST['hora_limite']);
    	$data['block_saturday'] = trim($_POST['block_saturday']);

    	$wpdb->query('DELETE FROM wp_roles_shipping_general'); //Delete all role
        $wpdb->insert('wp_roles_shipping_general', $data); //Save general roles

        print '<div class="alert alert-success" role="alert">
            <h3>Regras atualizadas com sucesso.</h3>
        </div>';

        print '<a href="">Voltar</a>';

    }else{
        $file = plugin_dir_path( __FILE__ ) . "inc/admin/form.php";

        if ( file_exists( $file ) )
            require $file;
    }

    print '</div>';
}


function checkCEP() {
	$cep = $_REQUEST['cep'];
	$type = 'billing';

	/*Remove sassion*/
	delete_user_meta( get_current_user_id(), $type.'_postcode' );
	delete_user_meta( get_current_user_id(), 'shipping_postcode' );

	$code = wc_format_postcode($cep, WC()->customer->get_shipping_country() );
	update_user_meta( get_current_user_id(), $type.'_postcode', $code );
	update_user_meta( get_current_user_id(), 'shipping_postcode', $code );

	if ( Brasa_Check_Delivery::get_instance()->check_postcode() ) {
		echo 'CEP OK';
	}else {
		echo 'CEP não atendido';
	}
	wp_die();
}
add_action( 'wp_ajax_checkCEP', 'checkCEP' );
add_action( 'wp_ajax_nopriv_checkCEP', 'checkCEP');


function getDay($day){

	switch ( mb_strtolower($day) ) {
		case 'segunda-feira':
			$dayW = 'monday';
			break;
		
		case 'terça-feira':
			$dayW = 'tuesday';
			break;

		case 'quarta-feira':
			$dayW = 'wednesday';
			break;

		case 'quinta-feira':
			$dayW = 'thursday';
			break;

		case 'sexta-feira':
			$dayW = 'friday';
			break;

		case 'sábado':
			$dayW = 'saturday';
			break;

		case 'domingo':
			$dayW = 'sunday';
			break;
	}

	return $dayW;

}

function filterString($string){
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    $string = str_replace('-', ' ', $string);
    return strtolower(trim($string, '-'));
}

function checkRolesCEP() {
	$cidade = $_REQUEST['cidade'];
	$bairro = filterString($_REQUEST['bairro']);

	global $wpdb;

	setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');

	$today = mb_strtolower(date("l", strtotime('today')));

	$roles = $wpdb->get_results( $wpdb->prepare('SELECT * FROM wp_roles_shipping WHERE cidade = "'.$cidade.'" AND bairro LIKE "'.$bairro.'" ') );
	
	$daysWeek = [];
	$data['blockTomorrow'] = false;
	$data['blockMonday'] = false;

	if( count($roles) > 0 ):
	
		foreach ($roles as $key) {

			$dias = $wpdb->get_results(	$wpdb->prepare('SELECT * FROM wp_roles_shipping_day WHERE role_id = "'.$key->id.'" ') );
			$rolesG = $wpdb->get_results( $wpdb->prepare('SELECT * FROM wp_roles_shipping_general') );
			$limite = str_replace(':', '', $rolesG[0]->hora_limite);
			$horarioArr = explode('|', $key->horario);
			$blockSaturday = $rolesG[0]->block_saturday;

			$iD=0;
			foreach ($dias as $keyD) {
				$show = true;


				if( intval( date('Hi') ) >= intval($limite) ){
					$getDay = strtotime('next '. getDay($keyD->day) );

					if( $getDay == strtotime('tomorrow') ){
						$data['blockTomorrow'] = true;
						$getDay = strtotime('second '. getDay($keyD->day) );
					}

					if( $today == 'saturday' && date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
						$getDay = strtotime('+1 week '. getDay($keyD->day) );
						$data['blockMonday'] = true;
					}elseif( $today == 'sunday' && date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
						$getDay = strtotime('+1 week '. getDay($keyD->day) );
						$data['blockTomorrow'] = true;
					}
				}else{

					$getDay = strtotime('next '. getDay($keyD->day) );

					if( $today == 'sunday' ){
						if( $getDay == strtotime('tomorrow') ){
							$data['blockTomorrow'] = true;
							$getDay = strtotime('second '. getDay($keyD->day) );
						}

						if( date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
							$getDay = strtotime('+1 week '. getDay($keyD->day) );
							$data['blockTomorrow'] = true;
						}
					}
				}

				if( date('m', $getDay) == '12' ){
					if( date('d', $getDay) > 23 && date('d', $getDay) < 27 ){
						$getDay = strtotime('+2 week', $getDay );
					}elseif( date('d', $getDay) > 27  ){
						$getDay = strtotime('+1 week', $getDay );
					}
				}elseif( date('m', $getDay) == '01' ){
					if( date('d', $getDay) == 1 ){
						$getDay = strtotime('+1 week', $getDay );
					}
				}

				array_push($daysWeek, ['dia' => $keyD->day, 'time' => $getDay, 'show' => $show, 'horario' => $horarioArr[$iD], 'data' => date('d/m/Y', $getDay), 'rule1' => $today ]);
				$iD++;
			}
		}

	else:

		$roles = $wpdb->get_results( $wpdb->prepare('SELECT * FROM wp_roles_shipping WHERE cidade = "'.$cidade.'" AND bairro = ""') );

		if( count($roles) > 0 ):

			// if( get_current_user_id() == 1 ){
			// 	echo $cidade;
			// 	echo $bairro;
			// }

			foreach ($roles as $key) {

				$dias = $wpdb->get_results(	$wpdb->prepare('SELECT * FROM wp_roles_shipping_day WHERE role_id = "'.$key->id.'" ') );
				$rolesG = $wpdb->get_results( $wpdb->prepare('SELECT * FROM wp_roles_shipping_general') );
				$limite = str_replace(':', '', $rolesG[0]->hora_limite);
				$horarioArr = explode('|', $key->horario);
				$blockSaturday = $rolesG[0]->block_saturday;

				$iD=0;
				foreach ($dias as $keyD) {
					$show = true;

					if( intval(date('Hi')) >= intval($limite) ){
						$getDay = strtotime('next '. getDay($keyD->day) );

						if( $getDay == strtotime('tomorrow') ){
							$data['blockTomorrow'] = true;
							$getDay = strtotime('second '. getDay($keyD->day) );
						}

						if( $today == 'saturday' && date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
							$getDay = strtotime('+1 week '. getDay($keyD->day) );
							$data['blockMonday'] = true;
						}elseif( $today == 'sunday' && date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
							$getDay = strtotime('+1 week '. getDay($keyD->day) );
							$data['blockTomorrow'] = true;
						}

					}else{
						$getDay = strtotime('next '. getDay($keyD->day) );

						if( $today == 'sunday' ){
							if( $getDay == strtotime('tomorrow') ){
								$data['blockTomorrow'] = true;
								$getDay = strtotime('second '. getDay($keyD->day) );
							}

							if( date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
								$getDay = strtotime('+1 week '. getDay($keyD->day) );
								$data['blockMonday'] = true;
							}
						}
					}

					if( date('m', $getDay) == '12' ){
						if( date('d', $getDay) > 23 && date('d', $getDay) < 27 ){
							$getDay = strtotime('+2 week', $getDay );
						}elseif( date('d', $getDay) > 27  ){
							$getDay = strtotime('+1 week', $getDay );
						}
					}elseif( date('m', $getDay) == '01' ){
						if( date('d', $getDay) == 1 ){
							$getDay = strtotime('second '. getDay($keyD->day) );
						}
					}
					
					array_push($daysWeek, ['dia' => $keyD->day, 'time' => $getDay, 'show' => $show, 'horario' => $horarioArr[$iD], 'rule2' ]);

					$iD++;
				}
			}

		else:

			$rolesG = $wpdb->get_results( $wpdb->prepare('SELECT * FROM wp_roles_shipping_general') );
			$dias = explode('|', $rolesG[0]->dias);
			$limite = str_replace(':', '', $rolesG[0]->hora_limite);
			$horarioArr = explode('|', $rolesG[0]->horario);
			$blockSaturday = $rolesG[0]->block_saturday;

			$iD=0;
			foreach ($dias as $keyD) {
				$show = true;

				if( intval(date('Hi')) >= intval($limite) ){
					$getDay = strtotime('next '. getDay(trim($keyD)) );

					if( $getDay == strtotime('tomorrow') ){
						$data['blockTomorrow'] = true;
						$getDay = strtotime('second '. getDay(trim($keyD)) );
					}

					if( $today == 'saturday' && date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
						$getDay = strtotime('+1 week '. getDay(trim($keyD)) );
						$data['blockMonday'] = true;
					}elseif( $today == 'sunday' && date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
						$getDay = strtotime('+1 week '. getDay(trim($keyD)) );
						$data['blockTomorrow'] = true;
					}

				}else{

					$getDay = strtotime('next '. getDay(trim($keyD)) );

					if( $today == 'sunday' ){
						if( $getDay == strtotime('tomorrow') ){
							$data['blockTomorrow'] = true;
							$getDay = strtotime('second '. getDay(trim($keyD)) );
						}

						if( date('l', $getDay) == 'Monday' && $blockSaturday == 1 ){
							$getDay = strtotime('+1 week '. getDay(trim($keyD)) );
							$data['blockTomorrow'] = true;
						}
					}
				}

				if( date('m', $getDay) == '12' ){
					if( date('d', $getDay) > 23 && date('d', $getDay) < 27 ){
						$getDay = strtotime('+2 week', $getDay );
					}elseif( date('d', $getDay) > 27  ){
						$getDay = strtotime('+1 week', $getDay );
					}
				}elseif( date('m', $getDay) == '01' ){
					if( date('d', $getDay) == 1 ){
						$getDay = strtotime('+1 week', $getDay );
					}
				}

				array_push($daysWeek, ['dia' => trim($keyD), 'time' => $getDay, 'show' => $show, 'horario' => $horarioArr[$iD], 'rule3', $cidade, $bairro ]);

				$iD++;
			}

		endif;

	endif;


	array_multisort( array_column($daysWeek, "time"), SORT_ASC, $daysWeek );

	$i=0;
	foreach ($daysWeek as $keyD) {

		if( $i > 0 ) continue;

		if( $keyD['show'] == true ):
            $data['time'] = $keyD['horario'];
            $data['date'] = $keyD['dia'].' ('.date('d/m/Y', $keyD['time']).')';

			$i++;

		endif;
	}
    
	$data['daysWeek'] = $daysWeek;

	echo json_encode($data);
	
	wp_die();
}
add_action( 'wp_ajax_checkRolesCEP', 'checkRolesCEP' );
add_action( 'wp_ajax_nopriv_checkRolesCEP', 'checkRolesCEP');



function selectDay() {
	$regras = $_REQUEST['regras'];
	$arrayDay = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];
	$dia = $_REQUEST['dia'];
	$dia = intval(substr($dia, 0, 10));
	$dataFinal = $arrayDay[date('w', $dia)] . ' (' .date('d/m/Y', $dia).')';
	$data['dataFinal'] = $dataFinal;

	$horario = '';
	foreach ($regras as $key) {

		if( trim($key['dia']) == $arrayDay[date('w', $dia)] ){
			$data['horarioFinal'] = trim($key['horario']);
			continue;
		}
	}

	echo json_encode($data);

	wp_die();
}
add_action( 'wp_ajax_selectDay', 'selectDay' );
add_action( 'wp_ajax_nopriv_selectDay', 'selectDay');


/**
 * Add the field to the checkout
 */
add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );
function my_custom_checkout_field( $checkout ) {

	$fields = [
		'data_entrega',
		'assinatura_product',
		'assinatura_price',
		'assinatura_itens',
		'assinatura_frequencia',
		'assinatura_restricoes',
		'assinatura_product_avulsos'
	];

	foreach ($fields as $field) {

		woocommerce_form_field( $field, array(
			'type'          => 'hidden',
			'class'         => array($field.'_field'),
			), $checkout->get_value( $field ));
	}
	
}



/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {

	$fields = [
		'data_entrega',
		'assinatura_product',
		'assinatura_price',
		'assinatura_itens',
		'assinatura_frequencia',
		'assinatura_restricoes',
		'assinatura_product_avulsos'
	];

	foreach ($fields as $field) {
		if ( ! empty( $_POST[$field] ) ) {
			update_post_meta( $order_id, $field, sanitize_text_field( $_POST[$field] ) );
		}
	}
    
}


/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){

	if( get_post_meta( $order->get_id(), 'data_entrega', true ) )
    	echo '<p><strong>'.__('Data de entrega').':</strong> ' . get_post_meta( $order->get_id(), 'data_entrega', true ) . '</p>';

	if( get_post_meta( $order->get_id(), 'assinatura_itens', true ) ):

		$arr = json_decode(get_post_meta( $order->get_id(), 'assinatura_itens', true ));
        $arrCats = ['Verduras', 'Legumes', 'Frutas', 'Temperos'];
		$html = '';
        foreach ($arr as $key => $value) {
            $html .= '<strong>'.$value.'</strong> x ' . $arrCats[$key];
            $html .= '<br>';
        }
		echo '<p><strong>'.__('Itens da assinatura').':</strong> <br>' . $html . '</p>';
	endif;

	if( get_post_meta( $order->get_id(), 'assinatura_product_avulsos', true ) ):

		$arrA = json_decode(get_post_meta( $order->get_id(), 'assinatura_product_avulsos', true ));
		$htmlA = '';
        foreach ($arrA as $key) {
			$htmlA .= '<strong>'.$key->quantity.'</strong> x ' . get_the_title($key->productId);
            $htmlA .= '<br>';
        }
		echo '<p><strong>'.__('Itens avulsos').':</strong> <br>' . $htmlA . '</p>';
	endif;


	if( get_post_meta( $order->get_id(), 'assinatura_restricoes', true ) ):
		$arrA = json_decode(get_post_meta( $order->get_id(), 'assinatura_restricoes', true ));
		$htmlA = '';
        foreach ($arrA as $key) {
			$htmlA .= get_the_title($key);
            $htmlA .= '<br>';
        }
		echo '<p><strong>'.__('Restrições').':</strong> <br>' . $htmlA . '</p>';
	endif;

	if( get_post_meta( $order->get_id(), 'assinatura_product', true ) ):
		$product = json_decode(get_post_meta( $order->get_id(), 'assinatura_product', true ));
		echo '<p><strong>'.__('Produto de origem').':</strong> <br>' . get_the_title( $product ) . '</p>';
	endif;

	if( get_post_meta( $order->get_id(), 'assinatura_product_quantity', true ) ):
		$data = json_decode(get_post_meta( $order->get_id(), 'assinatura_product_quantity', true ));
		echo '<p><strong></strong>' . $data . '</p>';
	endif;


}


add_action( "woocommerce_email_after_order_table", "custom_woocommerce_email_after_order_table", 10, 1);

function custom_woocommerce_email_after_order_table( $order ) {

    echo '<p><strong>Data de entrega:</strong> '. get_post_meta( $order->id, "data_entrega", true ) .'</p>';

}


add_action( 'woocommerce_thankyou', 'custom_content_thankyou', 4 );
function custom_content_thankyou( $order_id ) {

    echo '<p style="margin: 20px 0; text-align: left; color: #000;"><strong>Data de entrega:</strong> '. get_post_meta( $order_id, "data_entrega", true ) .'</p>';
}



function remove_date_shipping( $subscription , $last_order ) {
    update_post_meta( $last_order->get_id(), 'Data de entrega', '');
}
add_action( 'woocommerce_subscription_renewal_payment_complete', 'remove_date_shipping', 10, 2 );


function saveOrderAssinatura() {
	global $woocommerce;

	$data = $_REQUEST['data'];

	$cart_item_data = array(
		'custom_product' => $data[0]['product'],
		'custom_items' => $data[0]['items'],
		'custom_price' => $data[0]['totalPrice'],
		'custom_frequencia' => $data[0]['frequencia'],
		'custom_restricoes' => $data[0]['restricoes'],
		'custom_avulsos' => $data[0]['avulsos'],
		'custom_items_total' => $data[0]['totalItems'],
		'Data de entrega' => $data[0]['entrega']
	);

	$product = wc_get_product($data[0]['product']);
	$variations = $product->get_available_variations();
	$variation_id = null;
	foreach ($variations as $key) {
		if( $key['attributes']['attribute_pa_frequencia'] == $data[0]['frequencia'] )
			$variation_id = $key['variation_id'];
	}

	if( !WC()->cart->is_empty()  )
		WC()->cart->empty_cart();

	$woocommerce->cart->add_to_cart( $data[0]['product'], 1, $variation_id, wc_get_product_variation_attributes( $variation_id ), $cart_item_data );
	
	print_r($cart_item_data);
	wp_die();
}
add_action( 'wp_ajax_saveOrderAssinatura', 'saveOrderAssinatura' );
add_action( 'wp_ajax_nopriv_saveOrderAssinatura', 'saveOrderAssinatura');



function getListAvulsos() {
	global $woocommerce;

	$list = $_REQUEST['list'];

	$html = '<h3 class="topAvulsos">Mais produtos selecionados</h3>';
	foreach ($list as $key) {
		$price = ($key['price'] / $key['quantity']);
		$html .= '<div class="_item">
					<div class="icon"><img src="'.get_the_post_thumbnail_url($key['productId'], 'thumbnail').'"></div>
					<div class="right">
						<div class="ctn">
							<strong>'. removeAssTitle(get_the_title($key['productId']) ).'</strong>
							<span class="price">R$ '.number_format($price,2,",",".").' / un</span>
						</div>
						<div class="buttons-qty"><input type="text" value="'.$key['quantity'].'" min="0" disabled></div>
					</div>
				</div>';
	}

	print_r($html);
	wp_die();
}
add_action( 'wp_ajax_getListAvulsos', 'getListAvulsos' );
add_action( 'wp_ajax_nopriv_getListAvulsos', 'getListAvulsos');



add_filter('woocommerce_cart_item_name','add_usr_custom_session',1,3);
function add_usr_custom_session($product_name, $values, $cart_item_key ) {

	if( isset($values['custom_items_total']) ):
    	$return_string = $product_name . "<br />" . $values['custom_items_total'] . ' itens';
	else:
		$return_string = $product_name;
	endif;
		
    return $return_string;

}


add_action( 'woocommerce_before_calculate_totals', 'add_custom_item_price', 10 );
function add_custom_item_price( $cart_object ) {

    foreach ( $cart_object->get_cart() as $item_values ) {

		if( isset($item_values['custom_price']) ):

			#  Get cart item data
			// $item_id = $item_values['data']->id; // Product ID
			// $original_price = $item_values['data']->price; // Product original price

			
			## Get your custom fields values
			$price1 = $item_values['custom_price'];

			// CALCULATION FOR EACH ITEM:
			## Make HERE your own calculation 
			$new_price = $price1 ;

			## Set the new item price in cart
			$item_values['data']->set_price($new_price);

		endif;
    }
}


/**
 * Order to TXT
 */

include('inc/process-products-assinatura.php');
include('inc/class-order-to-txt.php');


/*
* Redirect user after login
*/
function iconic_login_redirect( ) {

    if( isset($_GET['redirect']) ){
        wp_redirect( $_GET['redirect'] );
    }else{
        wp_redirect( '/' );
    }
}
add_filter( 'woocommerce_login_redirect', 'iconic_login_redirect' );


/*
* Redirect user after register
*/
function iconic_register_redirect( ) {
    if( isset($_GET['redirect']) ){
        wp_redirect( $_GET['redirect'] );
    }else{
        wp_redirect( '/' );
    }
}
add_filter( 'woocommerce_registration_redirect', 'iconic_register_redirect' );


add_filter( 'manage_edit-product_columns', 'visibility_product_column', 10);
function visibility_product_column($columns){
    $new_columns = [];
    foreach( $columns as $key => $column ){
        $new_columns[$key] = $columns[$key];
        if( $key == 'price' ) { // Or use: if( $key == 'featured' ) {
            $new_columns['visibility'] = '<span style="display: none">Listado</span>';
        }
    }
    return $new_columns;
}


// Add content to new column rows in Admin products list
add_action( 'manage_product_posts_custom_column',  'visibility_product_column_content', 10, 2 );
function visibility_product_column_content( $column, $product_id ){
	global $post;

	if( $column =='visibility' ){
		if( has_term( array('exclude-from-catalog', 'outofstock'),'product_visibility', $product_id ) )
			echo '<p style="border: 2px solid red;
			width: 12px;
			position: absolute;
			right: 28px;
			height: 12px;
			font-size: 0;
			margin: auto;
			border-radius: 100%;">' . __("No") . '</p>';
			   else
		  echo '<p style="border: 2px solid green;
		  width: 12px;
		  position: absolute;
		  right: 28px;
		  height: 12px;
		  font-size: 0;
		  margin: auto;
		  border-radius: 100%;">' . __("Yes") . '</p>';
	}
}


/*
* Build modal restrições
*/
function clearToCart(){
    
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		WC()->cart->remove_cart_item( $cart_item_key );
   	}
    die();
}
add_action('wp_ajax_clearToCart', 'clearToCart');
add_action('wp_ajax_nopriv_clearToCart', 'clearToCart');
