var $ = jQuery;

// SLICK //
$(".home-page .sliderHome .slider").each(function(){
    $(this).slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 1000,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('#prevSlider'),
        nextArrow: $('#nextSlider'),
        // autoplaySpeed: 12000,
        responsive: [
        {
            breakpoint: 800,
            settings: {
            dots: true,
            prevArrow: false,
            nextArrow: false,
            }
        }
        ]
    });
})

// SLICK //
function runSliderHome(){
    $(".bloco-posts.slider .slider").each(function () {
        $(this).find('.slide').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 1000,
        autoplay: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $(this).find('.fa-arrow-left'),
        nextArrow: $(this).find('.fa-arrow-right'),
        autoplaySpeed: 12000,
        responsive: [
            {
            breakpoint: 800,
            settings: {
                dots: true
            }
            }
        ]
        });
    })
}


function runSliderDepo(){
    $(".bloco-depoimentos").each(function () {
        $(this).find('.slider').slick({
        dots: false,
        infinite: true,
        speed: 1000,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $(this).find('.fa-arrow-left'),
        nextArrow: $(this).find('.fa-arrow-right'),
        autoplaySpeed: 12000,
        });
    })
}


jQuery(window).on('resize, load', function() {
    var viewportWidth = jQuery(window).width();

    if (viewportWidth < 700) {
    } else {
        runSliderHome()
        runSliderDepo()
    }
});


$('.bloco-posts .abas .item').click(function () {

$(this).parent().find('.item').removeClass('active')
$(this).addClass('active')
$(this).parent().parent().parent().find('.contents .item').removeClass('active')
$(this).parent().parent().parent().find('.contents .item').eq($(this).index()).addClass('active')

if( $(this).hasClass('click-slider') ){
    if (jQuery(window).width() > 700) {
    runSliderHome()
    }
}
})