var $ = jQuery;

$(document).on("click", ".buttons-qty span", function (b) {
  var c = $(this).parent()[0],
    c = $(c).children("input");
  $("body").hasClass("woocommerce-cart") && (c = $(c[0]));
  var d = parseInt(c.val());
  if ("+" == $(this).html()) var d = d + 1;
  else {
    var d = d - 1;
    if (d < 0) return;
  }
  c.val(d), c.trigger("change");

});

$(document).on("change", ".buttons-qty input", function (b) {
  $(this).parent().parent().find('.add_to_cart_button').attr('data-quantity', $(this).val())
  $(this).parent().parent().find('[name="quantity"]').val($(this).val())
});


$('.site-header .lineBottom .main .menu .closeMenuMobile').click(function(){
  $('.site-header .lineBottom .menu').removeClass('active')
})


$('.btnUp').click(function () {
  $('body, html').animate({ scrollTop: 0 }, 1000);
});


/*
 * Modal busca
 */
$('.clickBusca').click(function () {
  $('.overModalBusca, .modalBusca').fadeIn();
});

$('.modalBusca .close, .overModalBusca').click(function () {
  $('.overModalBusca, .modalBusca').fadeOut();
});


$(window).on("load resize",function(e){
  sliderResponsive();
});
sliderResponsive();

function sliderResponsive(){
	jQuery(".home-page .sliderHome img").each(function(){
		var desktop = jQuery(this).attr('data-src-desktop');
		var mobile = jQuery(this).attr('data-src-mobile');

		if( jQuery(document).width() > 500 ) {
			jQuery(this).attr('src', desktop).addClass('active');
		}else{
			jQuery(this).attr('src', mobile).addClass('active');
		}
	});
}


$('#loginHeader').submit(function (e) {
  e.preventDefault()

  let login = $(this).find('[name="login"]').val()
  let password = $(this).find('[name="password"]').val()
  $(this).find('button').prop('disabled', true).addClass('loading')

  $('form.xoo-el-action-form').find('[name="xoo-el-username"]').val(login)
  $('form.xoo-el-action-form').find('[name="xoo-el-password"]').val(password)
  $('form.xoo-el-action-form').submit()
})


$('.single-product div.product p.price').appendTo($('.woocommerce-product-details__short-description')).show()


function relDiff(a, b) {
  return a - b === 0 ? 0 : 100 * Math.abs((a - b) / b) || 'input error';
}

$('.single-product div.product.sale').each(function () {

  const regular = $('.price del').text().replace('R$', '').replace(',', '.')
  const promo = $('.price ins').text().replace('R$', '').replace(',', '.')

  const regularF = parseFloat(regular).toFixed(2)
  const promoF = parseFloat(promo).toFixed(2)

  $('.onsale').html(parseInt(relDiff(promoF, regularF)) + '<span>%</span>').addClass('active')
})

$('.woocommerce-tabs').each(function () {
  $(this).prepend('<div class="title-bloco"><h2>Informações do produto</h2></div>');
})

$('.related.products').each(function () {
  $(this).find('h2').not('h2.woocommerce-loop-product__title').remove()
  $(this).prepend('<div class="title-bloco"><h2>Produtos relacionados</h2></div>');
})


$('.post-type-archive-product h1').each(function () {
  $(this).text('Produtos')
})

$('.collapse .top').click(function () {

  if( $(this).parent().hasClass('active') ){
    $(this).parent().removeClass('active')
  }else{
    $(this).parent().addClass('active')
  }
})

$('.product-quantity input').on('change keyup', function() {
  $('button[name="update_cart"]').trigger("click");
}); 

$('.iconMenu').click(function(){
  $('.site-header .lineBottom .menu').toggleClass('active')
})


$('.product_type_simple.add_to_cart_button').each(function(){
  $(this).html('<svg id="Grupo_914" data-name="Grupo 914" xmlns="http://www.w3.org/2000/svg" width="36.473" height="32.414" viewBox="0 0 36.473 32.414"><path id="Caminho_4363" data-name="Caminho 4363" d="M2922.948,2920.587v4.939a1.024,1.024,0,0,0,2.047,0v-4.939A1.024,1.024,0,0,0,2922.948,2920.587Z" transform="translate(-2910.655 -2900.386)"  fill-rule="evenodd"/><path id="Caminho_4364" data-name="Caminho 4364" d="M3035.875,2888.351v6.911a1.023,1.023,0,0,0,2.047,0v-6.911A1.023,1.023,0,0,0,3035.875,2888.351Z" transform="translate(-3018.662 -2869.555)"  fill-rule="evenodd"/><path id="Caminho_4365" data-name="Caminho 4365" d="M3148.8,2920.587v4.939a1.024,1.024,0,0,0,2.047,0v-4.939A1.024,1.024,0,0,0,3148.8,2920.587Z" transform="translate(-3126.668 -2900.386)"  fill-rule="evenodd"/><path id="Caminho_4366" data-name="Caminho 4366" d="M2676.309,2488.982h-9.967l-4.85-9.158c-.634-1.2-2.4-.165-1.769,1.032l4.3,8.126h-9.891l4.3-8.127c.633-1.2-1.135-2.229-1.768-1.032l-4.85,9.158h-9.966a1.024,1.024,0,0,0,0,2.047h1.257l2.219,15.53c.381,2.665,1.173,5.146,4.564,5.146h18.372c3.052,0,4.135-2.137,4.505-4.732l2.279-15.944h1.257A1.024,1.024,0,0,0,2676.309,2488.982Zm-5.569,17.919c-.208,1.458-.677,2.757-2.472,2.757H2649.9c-2.095,0-2.318-1.683-2.54-3.234l-2.2-15.4h5.581l-.719,1.358h-.7a1.024,1.024,0,0,0,0,2.047h3.081a1.024,1.024,0,0,0,.119-2.04l-.181-.021.712-1.344h12.059l.712,1.344-.182.021a1.024,1.024,0,0,0,.119,2.04h3.082a1.024,1.024,0,0,0,0-2.047h-.7l-.719-1.358h5.581Z" transform="translate(-2640.845 -2479.292)"  fill-rule="evenodd"/></svg> Adicionar')
})


$('.product_type_subscription.add_to_cart_button').each(function(){
  $(this).html('<svg id="Grupo_914" data-name="Grupo 914" xmlns="http://www.w3.org/2000/svg" width="36.473" height="32.414" viewBox="0 0 36.473 32.414"><path id="Caminho_4363" data-name="Caminho 4363" d="M2922.948,2920.587v4.939a1.024,1.024,0,0,0,2.047,0v-4.939A1.024,1.024,0,0,0,2922.948,2920.587Z" transform="translate(-2910.655 -2900.386)"  fill-rule="evenodd"/><path id="Caminho_4364" data-name="Caminho 4364" d="M3035.875,2888.351v6.911a1.023,1.023,0,0,0,2.047,0v-6.911A1.023,1.023,0,0,0,3035.875,2888.351Z" transform="translate(-3018.662 -2869.555)"  fill-rule="evenodd"/><path id="Caminho_4365" data-name="Caminho 4365" d="M3148.8,2920.587v4.939a1.024,1.024,0,0,0,2.047,0v-4.939A1.024,1.024,0,0,0,3148.8,2920.587Z" transform="translate(-3126.668 -2900.386)"  fill-rule="evenodd"/><path id="Caminho_4366" data-name="Caminho 4366" d="M2676.309,2488.982h-9.967l-4.85-9.158c-.634-1.2-2.4-.165-1.769,1.032l4.3,8.126h-9.891l4.3-8.127c.633-1.2-1.135-2.229-1.768-1.032l-4.85,9.158h-9.966a1.024,1.024,0,0,0,0,2.047h1.257l2.219,15.53c.381,2.665,1.173,5.146,4.564,5.146h18.372c3.052,0,4.135-2.137,4.505-4.732l2.279-15.944h1.257A1.024,1.024,0,0,0,2676.309,2488.982Zm-5.569,17.919c-.208,1.458-.677,2.757-2.472,2.757H2649.9c-2.095,0-2.318-1.683-2.54-3.234l-2.2-15.4h5.581l-.719,1.358h-.7a1.024,1.024,0,0,0,0,2.047h3.081a1.024,1.024,0,0,0,.119-2.04l-.181-.021.712-1.344h12.059l.712,1.344-.182.021a1.024,1.024,0,0,0,.119,2.04h3.082a1.024,1.024,0,0,0,0-2.047h-.7l-.719-1.358h5.581Z" transform="translate(-2640.845 -2479.292)"  fill-rule="evenodd"/></svg> Assinar')
})


// Block user another city
// $('#billing_postcode').each(function(){
//   if( $('#billing_postcode').val() != '' ){
//     checkCepCheckout( $('#billing_postcode').val() )
//   }
// })

$("#billing_postcode, #shipping_postcode").blur(function() {

  if( $('#ship-to-different-address-checkbox').is(':checked') ){
    checkCepCheckout( $('#shipping_postcode').val(), true )
  }else{
    checkCepCheckout( $('#billing_postcode').val(), true )
  }

})

// $('#shipping_postcode').each(function(){
//   if( $('#shipping_postcode').val() != '' ){
//     checkCepCheckout( $('#shipping_postcode').val() )
//   }
// })

// $("#shipping_postcode").blur(function() {
//   checkCepCheckout( $('#shipping_postcode').val() )
// })

jQuery( document.body ).on( 'updated_checkout', function(){
  console.log('checkout updated'); 
  if( !$('.msg-aviso-erro').length )
    checkBairroField()


  if( $('#ship-to-different-address-checkbox').is(':checked') ){
    checkCepCheckout( $('#shipping_postcode').val() )
  }else{
    checkCepCheckout( $('#billing_postcode').val() )
  }
});

$('#ship-to-different-address-checkbox').change(function(){

  if( $(this).is(':checked') ){
    checkCepCheckout( $('#shipping_postcode').val() )
  }else{
    checkCepCheckout( $('#billing_postcode').val() )
  }
  console.log('checked-change')
})

function checkCepCheckout(cep, autocomplete = null){
  $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
    if (!("erro" in dados)) {

      let atendidas = $('#cidadesAtendidas').val().split(',');
      let bairrosAtendidos = $('#bairrosAtendidos').val();

      $('#billing_postcode_field .msg-aviso-erro').remove()
      // $('#place_order').prop('disabled', true)

      if( !atendidas.includes(dados.localidade) ){
          $('#billing_postcode_field').prepend('<div class="msg-aviso-erro">Não atendemos nesse CEP</div>')
          setTimeout(function(){
            $('#place_order').prop('disabled', true)
          }, 200)
      }else{

        if( !bairrosAtendidos.includes(dados.localidade+'-') ){
          $('#place_order').prop('disabled', false)
        }else{

          let bairroSearched = dados.localidade+'-'+dados.bairro
          if( !bairrosAtendidos.includes(bairroSearched) ){
            $('#billing_postcode_field').prepend('<div class="msg-aviso-erro">Não atendemos nesse CEP</div>')
            $('#place_order').prop('disabled', true)
          }else{
            $('#place_order').prop('disabled', false)
            console.log('atendidas')
          }
        }
      }

      if( autocomplete ){
        if( dados.logradouro ) $("#billing_address_1").val(dados.logradouro);
        if( dados.bairro ) $("#billing_neighborhood").val(dados.bairro);
        if( dados.localidade ) $("#billing_city").val(dados.localidade);
      }


    } //end if.
    else {
        alert("CEP não encontrado.");
    }
});
}


$('.wc-block-product-categories-list-item').each(function(){
  if( $(this).find('a:first').text() == 'Cesta' )
    $(this).remove()
})

$('.home-page .bloco-posts .product_cat-assinatura-novas').each(function(){
  $(this).find('a').attr('href', url + '/beneficios-assinatura')
  $(this).find('.product_type_variable-subscription').html('<svg id="Grupo_914" data-name="Grupo 914" xmlns="http://www.w3.org/2000/svg" width="36.473" height="32.414" viewBox="0 0 36.473 32.414"><path id="Caminho_4363" data-name="Caminho 4363" d="M2922.948,2920.587v4.939a1.024,1.024,0,0,0,2.047,0v-4.939A1.024,1.024,0,0,0,2922.948,2920.587Z" transform="translate(-2910.655 -2900.386)"  fill-rule="evenodd"/><path id="Caminho_4364" data-name="Caminho 4364" d="M3035.875,2888.351v6.911a1.023,1.023,0,0,0,2.047,0v-6.911A1.023,1.023,0,0,0,3035.875,2888.351Z" transform="translate(-3018.662 -2869.555)"  fill-rule="evenodd"/><path id="Caminho_4365" data-name="Caminho 4365" d="M3148.8,2920.587v4.939a1.024,1.024,0,0,0,2.047,0v-4.939A1.024,1.024,0,0,0,3148.8,2920.587Z" transform="translate(-3126.668 -2900.386)"  fill-rule="evenodd"/><path id="Caminho_4366" data-name="Caminho 4366" d="M2676.309,2488.982h-9.967l-4.85-9.158c-.634-1.2-2.4-.165-1.769,1.032l4.3,8.126h-9.891l4.3-8.127c.633-1.2-1.135-2.229-1.768-1.032l-4.85,9.158h-9.966a1.024,1.024,0,0,0,0,2.047h1.257l2.219,15.53c.381,2.665,1.173,5.146,4.564,5.146h18.372c3.052,0,4.135-2.137,4.505-4.732l2.279-15.944h1.257A1.024,1.024,0,0,0,2676.309,2488.982Zm-5.569,17.919c-.208,1.458-.677,2.757-2.472,2.757H2649.9c-2.095,0-2.318-1.683-2.54-3.234l-2.2-15.4h5.581l-.719,1.358h-.7a1.024,1.024,0,0,0,0,2.047h3.081a1.024,1.024,0,0,0,.119-2.04l-.181-.021.712-1.344h12.059l.712,1.344-.182.021a1.024,1.024,0,0,0,.119,2.04h3.082a1.024,1.024,0,0,0,0-2.047h-.7l-.719-1.358h5.581Z" transform="translate(-2640.845 -2479.292)"  fill-rule="evenodd"/></svg> Assinar')
  $(this).find('.price, .buttons-qty').remove()
})


$('#selectOrder').change(function(e){
  e.preventDefault();

  var url = $('#filterProduct').attr('action');
  var order = $(this).val();

  var paramers = [];
  paramers.push('order='+order);

  var redirecto = url + '?' + paramers.join('&');
  window.location.href = redirecto;

});


$(window).click(function(){
  $('.dropdownConta').removeClass('active')
})

$('.ItemConta').click(function(e){
  e.stopPropagation();
  $('.dropdownConta').addClass('active')
})


/*
* Count carrinho
*/
updateNumberCart();

$( document.body ).on( 'added_to_cart', function(){
    $('.badgeAdded').addClass('active')

    setTimeout(function(){
      $('.badgeAdded').removeClass('active')
    }, 3000)
    updateNumberCart();
});

$('.badgeAdded i.close').click(function(){
  $('.badgeAdded').removeClass('active')
})

function updateNumberCart(){
  $('.linkCart .number').addClass('loading');
  $.ajax({
    type: "POST",
    url: url + "/wp-admin/admin-ajax.php",
    data: { action : 'updateNumberCart'},
    success: function(data) {
      console.log(data);
      $('.linkCart .number').text(data).removeClass('loading');
    }
  });
}

$('[href="#clickCateFilter"]').click(function(e){
  e.preventDefault()
  $('.tax-product_cat .sideBar, .page-categoria-loop .sideBar, .content-escolher-produtos .sidebar').addClass('active')
  $('.overflowMenu').show();
})

$('.sideBar .iconClose, .sidebar .iconClose, .overflowMenu').click(function(e){
  e.preventDefault()
  $('.tax-product_cat .sideBar, .page-categoria-loop .sideBar, .content-escolher-produtos .sidebar').removeClass('active')
  $('.overflowMenu').hide();
  console.log('ss')
})

$('.wsafp-form').each(function(){
  setTimeout(function(){
    // $(this).remove()
  }, 200)
})

$('.woocommerce-info').each(function(){
  let txt = $(this).text()
  if(txt.indexOf('Ponto(s)') != -1){
    // $(this).remove()
  }
})

$('.removeCart').click(function(e){
  e.preventDefault()

  $('.removeCart').text('Esvaziando...')
  
  $.ajax({
    type: "POST",
    url: url + "/wp-admin/admin-ajax.php",
    data: { action : 'clearToCart'},
    success: function(data) {
      window.location.reload();
    }
  });

})


$('.popup-abt .close, .popup-abt-overflow').click(function(){
    $('.popup-abt, .popup-abt-overflow').hide()
})