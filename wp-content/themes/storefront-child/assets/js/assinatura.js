function formatCurrency(number){
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(number)
}

function formatDecimal(number) {
    return number.replace('.', '').replace(',', '.')
}

$(document).on("click", ".listCategories .buttons-qty span", function (b) {
    // let element = $(this).parent().find('input')
    // if ($(this).text() == '+') {
    //     $(this).parent().find('input').val(+element.val() + 1)
    // } else {
    //     if (element.val() > element.attr('min'))
    //         $(this).parent().find('input').val(element.val() - 1)
    // }
    getPackageByQuant()
});

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
  }
  
  function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }
  
  function eraseCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

/*
* Assinaturas
*/
$('.assinatura-page .modal .top .close, [href="#closeModalAss"]').click(function () {
    $('.assinatura-page .modal').hide()
})

$('[href="#openModalAss"]').click(function (e) {
    e.preventDefault()

    if ($(this).attr('data-modal') == 'opcoes')
        buildModalProduct($(this).attr('data-type'))

    if ($(this).attr('data-modal') == 'restricoes')
        buildModalRestricoes()

    $('#modal-' + $(this).attr('data-modal')).show()
    $('body, html').animate({ scrollTop: 20 }, 1000);
})



function buildModalProduct(type) {
    $('#listModalProduct').html('').addClass('loading')
    $('#typeModalProduct').text(type)
    $.ajax({
        type: "POST",
        url: url + "/wp-admin/admin-ajax.php",
        data: { type: type, action: 'buildModalProducts' },
        success: function (data) {
            $('#listModalProduct').html(data).removeClass('loading')
        }
    });
}


$('.assinatura-page .modal .categories .item').click(function (e) {
    e.preventDefault()

    $('.assinatura-page .modal .categories .item').removeClass('active')
    $(this).addClass('active')
    var type = $(this).find('span').text()
    var search = null

    if ($('#modal-restricoes form [name="busca"]').val() != '')
        search = $('#modal-restricoes form [name="busca"]').val()
    buildModalRestricoes(type, search)
})

$('#modal-restricoes form').submit(function (e) {
    e.preventDefault()
    var type = $('.assinatura-page .modal .categories .item.active').find('span').text()
    buildModalRestricoes(type, $(this).find('[name="busca"]').val())
})

function buildModalRestricoes(type = null, search = null) {

    if (getCookie('assinatura_restricoes')) {
        var count = JSON.parse(getCookie('assinatura_restricoes')).length
        $('.assinatura-page .modal .lineBottom .left .checkeds').text(count)
    }

    $('#listProdRestricoes').html('').addClass('loading')
    $.ajax({
        type: "POST",
        url: url + "/wp-admin/admin-ajax.php",
        data: { type: type, search: search, action: 'buildModalRestricoes' },
        success: function (data) {
            $('#listProdRestricoes').html(data).removeClass('loading')

            checkRestricoesItems()

            $('[href="#checkRestricoes"]').click(function (e) {
                e.preventDefault()

                if ($(this).find('input').is(':checked')) {
                    $(this).find('input').prop('checked', false)
                    saveRestricoes($(this).find('input').val(), 'remove')
                } else {
                    if ($('.assinatura-page .modal .lineBottom .left .checkeds').text() == '10') return;
                    $(this).find('input').prop('checked', true)
                    saveRestricoes($(this).find('input').val(), 'add')
                }

                if (getCookie('assinatura_restricoes')) {
                    var count = JSON.parse(getCookie('assinatura_restricoes')).length
                    $('.assinatura-page .modal .lineBottom .left .checkeds').text(count)
                } else {
                    $('.assinatura-page .modal .lineBottom .left .checkeds').text($('[href="#checkRestricoes"] input:checked').length)
                }
            })

        }
    });
}




function checkRestricoesItems() {

    let restricoes = getCookie('assinatura_restricoes')
    if (!restricoes) return;

    let arr = JSON.parse(restricoes)
    $('#listProdRestricoes [name="productRestrict"]').each(function () {
        if (arr.includes($(this).val()) == true)
            $(this).prop('checked', true)
    })
}

function arrayRemove(arr, value) {
    return arr.filter(function (ele) {
        return ele != value;
    });
}

function saveRestricoes(value, action) {
    let restricoes = getCookie('assinatura_restricoes')
    let arr = []
    if (restricoes) {
        arr = JSON.parse(restricoes)
        if (action == 'add') {
            arr.push(value)
        } else {
            arr = arrayRemove(arr, value);
        }
    } else {
        arr.push(value)
    }

    setCookie('assinatura_restricoes', JSON.stringify(arr), 7);
    getListRestricoes()
}

function formatCurrency(number) {
    return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(number)
}

$('#gridPackage').each(function () {
    getPackage()
})

$('.assinatura-page .main .colMain .grid ._item .box').click(function () {
    $('.assinatura-page .main .colMain .grid ._item').removeClass('active')
    $(this).parent().addClass('active')

    getPackage()
})

function getPackageByQuant() {

    let arr = []
    var total = 0
    var totalPrice = 0
    $('.listCategories ._item').find('.buttons-qty input').each(function () {
        arr.push($(this).val())
        total += parseInt($(this).val())
        totalPrice += parseInt($(this).val()) * $(this).parent().parent().parent().attr('data-price')
    })


    let package = ''
    switch (arr.join(',')) {

        case '2,5,4,1': //Grande
            package = '12'
            break;

        case '2,5,2,1': //Medio
            package = '10'
            break;

        case '1,3,2,1': // Pequena
            package = '7'
            break;

        case '0,0,5,0': // Frutas
            package = '5'
            break;

        default:
            package = 'custom'
            break;
    }

    $('.assinatura-page .main .colMain .grid ._item').removeClass('active')

    if (package == 'custom') {
        $('#namePackage').text('CESTA CUSTOMIZADA')
        $('#unidPackage').text(total)
        $('#pricePackage').text(formatCurrency(totalPrice))
                   
        let produto = $('.assinatura-page .main .colMain .grid ._item[data-unid="custom"]').data('product')
        setCookie('assinatura_items', JSON.stringify(arr), 7);
        setCookie('assinatura_price', totalPrice, 7);
        setCookie('assinatura_product', produto, 7);

    } else {
        let current = $('.assinatura-page .main .colMain .grid ._item[data-unid="' + package + '"]').addClass('active')
        getPackage()
    }


}

function getPackage() {
    let package = $('#gridPackage').find('._item.active')
    $('#namePackage').text(package.find('.box').text())
    $('#pricePackage').text(formatCurrency(package.data('price')))
    $('#unidPackage').text(package.data('unid'))

    let arr = []
    switch (package.data('unid')) {
        
        case 12: //Grande
            arr = [2,5,4,1]
            break;

        case 10: //Media
            arr = [2,5,2,1]
            break;

        case 5: //Frutas
            arr = [0,0,5,0]
            break;

        default: //Pequeno
            arr = [1,3,2,1]
            break;
    }

    var i = 0
    $('.listCategories ._item .right .buttons-qty input').each(function () {
        $(this).val(arr[i])
        i++
    })

    setCookie('assinatura_items', JSON.stringify(arr), 7);
    setCookie('assinatura_price', package.data('price'), 7);
    setCookie('assinatura_product', package.data('product'), 7)
}


$('.boxAssinatura').each(function(){
    atualizaValoresAssinaturaTop()
})

function atualizaValoresAssinaturaTop(){
    let items = getCookie('assinatura_items')
    let price = getCookie('assinatura_price')

    let count = 0
    Object.entries( JSON.parse(items) ).forEach(([key, val]) => {
        count += parseInt(val)
    });

    if( getCookie('assinatura_product_avulsos') ){
        price = parseFloat(price)
        Object.entries( JSON.parse(getCookie('assinatura_product_avulsos')) ).forEach(([key, val]) => {
            count += parseInt(val.quantity)
            price += val.price
        });
    }

    $('#priceAssinatura').text( count + ' itens - ' + formatCurrency(price) )
}

$('.content-frequencia').each(function(){
    setCookie('assinatura_frequencia', $('[href="#checkFrequencia"]').find('input:checked').val(), 7);
})

$('[href="#checkFrequencia"]').click(function (e) {
    e.preventDefault()

    if ($(this).find('input').is(':checked')) {
        $(this).find('input').prop('checked', false)
        $('[href="#checkFrequencia"]').not( $(this) ).find('input').prop('checked', true)
    } else {
        $(this).find('input').prop('checked', true)
        $('[href="#checkFrequencia"]').not( $(this) ).find('input').prop('checked', false)
    }
    setCookie('assinatura_frequencia', $('[href="#checkFrequencia"]').find('input:checked').val(), 7);
})


/*
* Revisar assinatura
*/
$('.content-revise-sua-assinatura').each(function(){
    let items = getCookie('assinatura_items')
    let price = getCookie('assinatura_price')
    let frequencia = getCookie('assinatura_frequencia')

    let arr = JSON.parse(items)
    
    var i = 0
    $('.listCategories ._item .right .buttons-qty input').each(function () {
        $(this).val(arr[i])
        i++
    })

    if( getCookie('assinatura_product_avulsos') ){
        price = parseFloat(price)
        Object.entries( JSON.parse(getCookie('assinatura_product_avulsos')) ).forEach(([key, val]) => {
            price += val.price
        });
    }

    $('#priceAssinatura').text( formatCurrency(price) )
    $('#totalAssinatura').text( formatCurrency(price) )
    $('#frequenciaAssinatura').text( frequencia )

    getListRestricoes()
})

getListRestricoes()

function getListRestricoes(){
    if( getCookie('assinatura_restricoes') ){
        if( getCookie('assinatura_restricoes') == '' ) return;

        $('#listRestricoes').html('')

        var list = JSON.parse(getCookie('assinatura_restricoes'))
        if( list ){
            if( list.length == 0 ) return;
        }else{
            return;
        }

        $.ajax({
            type: "POST",
            url: url + "/wp-admin/admin-ajax.php",
            data: { list: list.join(','), action: 'getListRestricoes' },
            success: function (data) {
                $('#listRestricoes').html(data)
                $('#restricoesSelecionadas').show()
            }
        });
    }
}


$('[href="#saveOrderAssinatura"]').click(function(e){
    e.preventDefault()

    if( $(this).attr('data-error') == 'true' ){
        alert('Não atendemos nesse CEP. Consulte o nosso FAQ.')
        return;
    }

    if( $('#cityShipping').val() == '' || $('#dateShipping').val() == '' ){
        alert('Para continuar é necessário informar o endereço.')
        return;
    }

    let items = getCookie('assinatura_items')
    let totalPrice = getCookie('assinatura_price')

    let totalItems = 0
    Object.entries( JSON.parse(items) ).forEach(([key, val]) => {
        totalItems += parseInt(val)
    });

    if( getCookie('assinatura_product_avulsos') ){
        totalPrice = parseFloat(totalPrice)
        Object.entries( JSON.parse(getCookie('assinatura_product_avulsos')) ).forEach(([key, val]) => {
            totalPrice += val.price
            totalItems += parseInt(val.quantity)
        });
    }
    

    let data = [{
        product: getCookie('assinatura_product'),
        items: getCookie('assinatura_items'),
        price: getCookie('assinatura_price'),
        frequencia: getCookie('assinatura_frequencia'),
        restricoes: getCookie('assinatura_restricoes'),
        avulsos: getCookie('assinatura_product_avulsos'),
        entrega: $('#dateShipping').val(),
        totalPrice: totalPrice,
        totalItems: totalItems
    }]

    setCookie('assinatura_entrega', $('#dateShipping').val(), 7);

    $.ajax({
        type: "POST",
        url: url + "/wp-admin/admin-ajax.php",
        data: { data: data, action: 'saveOrderAssinatura' },
        success: function (data) {
            console.log(data)
            window.location.href = url + "/finalizar-compra";
        }
    });
})


$('.assinatura-page .lineRestricoes').each(function(){
    if( $(document).width() <= 700 ){
        $('.assinatura-page .lineRestricoes').appendTo(".assinatura-page .main .sidebar .listCategories");
    }
})

$('[href="#addProductAssinatura"]').click( async function(e){
    e.preventDefault()

    let productId = $(this).data('product_id')
    let quantity = $(this).attr('data-quantity')
    let productPrice = $(this).data('price')
    let avulsos = []

    // let item = [productId,quantity]
    let item = {
        productId: productId,
        quantity: quantity,
        price: productPrice * quantity
    }
    
    if( getCookie('assinatura_product_avulsos') ){
        arr = JSON.parse( getCookie('assinatura_product_avulsos') )

        if( arr.filter(d => d.productId == productId).length > 0 ){
            arr[ arr.findIndex(d => d.productId == productId) ].quantity = quantity
        }else{
            arr.push(item)
        }
        avulsos = arr

    }else{
        avulsos.push(item)
    }

    setCookie('assinatura_product_avulsos', JSON.stringify(avulsos), 7);

    $(this).text('Selecionado').addClass('added').parent().append('<a href="#removeProductAssinatura" data-product_id="'+productId+'">Remover</a>')
    removeProductAssinatura()
    atualizaValoresAssinaturaTop()
})

removeProductAssinatura()
function removeProductAssinatura(){
    $('[href="#removeProductAssinatura"]').click( function(e){
        e.preventDefault()
        let productId = $(this).data('product_id')
        let arr = JSON.parse( getCookie('assinatura_product_avulsos') )

        if( arr.findIndex(d => d.productId == productId) >= 0 ){
            let index = arr.findIndex(d => d.productId == productId)
            arr.splice(index, 1)
            setCookie('assinatura_product_avulsos', JSON.stringify(arr), 7);
        }

        $(this).parent().find('[href="#addProductAssinatura"]').text('Adicionar').removeClass('added')
        $(this).remove()
        atualizaValoresAssinaturaTop()
        
    })
}


$('.content-escolher-produtos').each(function(){
    
    if( getCookie('assinatura_product_avulsos') ){
        let arr = JSON.parse( getCookie('assinatura_product_avulsos') )

        if( arr.length == 0 ) return;

        $('.productAvulsoAssinatura').each(function(){
            let productId = $(this).data('id')
            let element = $(this)
            let index = arr.findIndex(d => d.productId == productId)
            if( index >= 0 ){
                element.find('[href="#addProductAssinatura"]').text('Selecionado').addClass('added').attr('data-quantity', arr[index].quantity)
                element.find('.buttons-qty input').val(arr[index].quantity)
                element.append('<a href="#removeProductAssinatura" data-product_id="'+productId+'">Remover</a>')
            }
        })

        removeProductAssinatura()
        atualizaValoresAssinaturaTop()
    }
})

$('#listAvulsos').each(function(){
    getListAvulsos()
})

function getListAvulsos(){
    if( getCookie('assinatura_product_avulsos') ){
        if( getCookie('assinatura_product_avulsos') == '' ) return;

        $('#listAvulsos').html('')

        var list = JSON.parse(getCookie('assinatura_product_avulsos'))
        if( list ){
            if( list.length == 0 ) return;
        }else{
            return;
        }

        $.ajax({
            type: "POST",
            url: url + "/wp-admin/admin-ajax.php",
            data: { list: list, action: 'getListAvulsos' },
            success: function (data) {
                $('#listAvulsos').html(data)
            }
        });
    }
}

$('.data_entrega_field').each(function(){
    
    if( getCookie('assinatura_entrega') )
        $('#data_entrega').val( getCookie('assinatura_entrega') )

    if( getCookie('assinatura_product') )
        $('#assinatura_product').val( getCookie('assinatura_product') )

    if( getCookie('assinatura_items') )
        $('#assinatura_itens').val( getCookie('assinatura_items') )

    if( getCookie('assinatura_price') )
        $('#assinatura_price').val( getCookie('assinatura_price') )

    if( getCookie('assinatura_frequencia') )
        $('#assinatura_frequencia').val( getCookie('assinatura_frequencia') )

    if( getCookie('assinatura_restricoes') )
        $('#assinatura_restricoes').val( getCookie('assinatura_restricoes') )

    if( getCookie('assinatura_product_avulsos') )
        $('#assinatura_product_avulsos').val( getCookie('assinatura_product_avulsos') )

})

$('body.woocommerce-order-received').each(function(){
    // Clear all cookies
    eraseCookie('assinatura_product')
    eraseCookie('assinatura_items')
    eraseCookie('assinatura_price')
    eraseCookie('assinatura_frequencia')
    eraseCookie('assinatura_restricoes')
    eraseCookie('assinatura_product_avulsos')
    eraseCookie('assinatura_entrega')
})