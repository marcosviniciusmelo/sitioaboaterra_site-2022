//#billing_postcode, #billing_city, #billing_neighborhood
$('#neighborhoodShipping, #cityShipping, #cepShipping').focusout(function(){
    checkBairroField()
})

jQuery('#billing_postcode, #shipping_postcode').focusout(function(){
    checkBairroField();
});

$('#cepShipping').each(function(){
    checkBairroField()
})

function checkBairroField(){

    var bairro = $('#neighborhoodShipping, #billing_neighborhood').val();
    var cidade = $('#cityShipping, #billing_city').val();
    var cep = $('#cepShipping, #billing_postcode').val();


    if(  $('#ship-to-different-address-checkbox').length ){
        if( $('#ship-to-different-address-checkbox').is(':checked') ) {
            var bairro = $('#shipping_neighborhood').val();
            var cidade = $('#shipping_city').val();
            var cep = $('#shipping_postcode').val();
        }
    }
    

    if( bairro != '' && cidade != '' && cep != '' ){
        getCalendarShipping(cep, cidade, bairro);
    }
}


function getCalendarShipping(cep, cidade, bairro){
    checkCEP(cep, cidade, bairro);
}

function checkCEP(cep, cidade, bairro){
    getRolesShipping(cidade, bairro);
}


function getRolesShipping(cidade, bairro){
    $('.calendario-datas .calendario').html('');

    $('#dateShipping').val('Carregando...')

    $.ajax({
        type: "POST",
        url: url + "/wp-admin/admin-ajax.php",
        data: { action: "checkRolesCEP", cidade: cidade, bairro: bairro },
        success: function (data) {
            data = JSON.parse(data);

            // if( getCookie('assinatura_entrega') && getCookie('assinatura_product') ){
            //     $('#dateShipping, #dateShippingFinal, #data_entrega').val( getCookie('assinatura_entrega') ).text( getCookie('assinatura_entrega') )
            // }else{
                $('#dateShipping, #dateShippingFinal, #data_entrega').val( data.date ).text( data.date )
                $('#timeShipping, #timeShippingFinal').val( data.time ).text( data.time )
                $('.lineTimeShipping').show()

                // $('#place_order').prop('disabled', false)
            // }

            $('#clickCalendario').show();
            calendarioDias(data.daysWeek, data.blockTomorrow, data.blockMonday);

            $('#datasRegrasArr').val( JSON.stringify(data.daysWeek) );
        }
    });
}


function calendarioDias(daysWeek, blockTomorrow, blockMonday){

    var arrayDays = ['', 'domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado'];
    var arrayDaysKeys = [ true ];

    var min = 1;
    if( blockTomorrow == true ){
        var min = 2;
    }

    if( blockMonday == true ){
        var min = 3;
    }

    daysWeek.forEach(function(item, indice){
        var index = arrayDays.indexOf(item.dia.toLowerCase());
        arrayDaysKeys.push(index);
    });

    var array = arrayDaysKeys;

    $('#clickCalendario').pickadate({
        today: '',
        closeOnSelect: false,
        closeOnClear: false,
        monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        weekdaysFull: ['Domingo', 'Segunda', 'terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        min: min,
        max: +30,
        onSet: function(context) {

            if( context.highlight ){
                disabledDays();
            }

            if( context.select ){
                $('#dataView').text(' Carregando... ');
                $('#horaView').text('');

                console.log($('[name="datasRegrasArr"]').val())

                var datasRegrasArr = JSON.parse( $('[name="datasRegrasArr"]').val() );

                $.ajax({
                    type: "POST",
                    url: url + "/wp-admin/admin-ajax.php",
                    data: { action: "selectDay", dia: context.select, regras: datasRegrasArr },
                    success: function (data) {
                        data = JSON.parse(data);
                        console.log( data )

                        $('#dataView').text(data.dataFinal);
                        $('#horaView').text(data.horarioFinal);
                        $('[name="data_entrega"]').val(data.dataFinal);

                        $('#dateShipping').val( data.dataFinal ).text( data.dataFinal )
                        $('#dateShippingFinal').text( data.dataFinal )
                        $('#data_entrega').val( data.dataFinal )

                        setCookie('assinatura_entrega', data.dataFinal, 7);

                        $('#timeShipping').val( data.horarioFinal )
                        $('#timeShippingFinal').text( data.horarioFinal )
                    },
                });

                if( context.select ){
                    $('.picker--opened .picker__holder').trigger('click');
                }
            }
            
        }
    });

    var $input = $('#clickCalendario').pickadate();
    var picker = $input.pickadate('picker');

    picker.set('enable', false);
    picker.set('disable', array);

    disabledDays();

}

function disabledDays(){
    setTimeout(function(){
        let i=0;
        $('.picker__table tr td[role="presentation"]').each(function(){
            var dateItem = $(this).find('.picker__day').attr('aria-label').replace(',', '').split(' ');

            if( dateItem[1] == 'Dezembro' ){
                if( dateItem[0] > 23 ){
                    $(this).find('.picker__day').addClass('picker__day--disabled').removeClass('picker__day--highlighted').attr('aria-disabled', true);
                }
            }

            if( dateItem[1] == 'Janeiro' ){
                if( dateItem[0] < 2 ){
                    $(this).find('.picker__day').addClass('picker__day--disabled').removeClass('picker__day--highlighted').attr('aria-disabled', true);
                }
            }

            if( $(this).find('.picker__day').hasClass('picker__day--outfocus') ){
                $(this).find('.picker__day').removeClass('picker__day--outfocus').addClass('picker__day--infocus');
            }

            if( i == 0 && !$(this).find('.picker__day').hasClass('picker__day--disabled') ){
                $(this).find('.picker__day').addClass('picker__day--selected');
                i++;
            }

        });
    }, 200);
}


function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#neighborhoodShipping").val("");
    $("#cityShipping").val("");
}

//Quando o campo cep perde o foco.
$("#cepShipping").blur(function() {
    
    console.log('autocomplete 02')

    var cep = $(this).val().replace(/\D/g, '');
    if (cep != "") {
        var validacep = /^[0-9]{8}$/;
        if(validacep.test(cep)) {
            $("#neighborhoodShipping").val('');
            $("#cityShipping").val('');
            $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {

                    let atendidas = $('#cidadesAtendidas').val().split(',');
                    let bairrosAtendidos = $('#bairrosAtendidos').val();

                    console.log( 'bairrosAtendidos' )

                    $('#formShippingAssinatura .msg-aviso-erro').remove()
                    $('[href="#saveOrderAssinatura"]').attr('data-error', false)

                    // if( !cidadeAtendidas.includes(dados.localidade) ){
                    //     $('[href="#saveOrderAssinatura"]').attr('data-error', true)
                    //     $('#formShippingAssinatura').prepend('<div class="msg-aviso-erro">Não atendemos nesse CEP</div>')
                    //     limpa_formulário_cep();
                    // }else{
                    //     $("#neighborhoodShipping").val(dados.bairro);
                    //     $("#cityShipping").val(dados.localidade);
                    //     $('[href="#saveOrderAssinatura"]').attr('data-error', false)
                    // }


                    // $('#place_order').prop('disabled', true)

                    if( !atendidas.includes(dados.localidade) ){
                        $('[href="#saveOrderAssinatura"]').attr('data-error', true)
                        $('#formShippingAssinatura').prepend('<div class="msg-aviso-erro">Não atendemos nesse CEP</div>')
                        $('#place_order').prop('disabled', true)
                        limpa_formulário_cep();
                    }else{

                        if( !bairrosAtendidos.includes(dados.localidade+'-') ){
                            $("#neighborhoodShipping").val(dados.bairro);
                            $("#cityShipping").val(dados.localidade);
                            $('[href="#saveOrderAssinatura"]').attr('data-error', false)
                        }else{

                            let bairroSearched = dados.localidade+'-'+dados.bairro
                            if( !bairrosAtendidos.includes(bairroSearched) ){
                                $('[href="#saveOrderAssinatura"]').attr('data-error', true)
                                $('#formShippingAssinatura').prepend('<div class="msg-aviso-erro">Não atendemos nesse CEP</div>')
                                $('#place_order').prop('disabled', true)
                            }else{
                                $("#neighborhoodShipping").val(dados.bairro);
                                $("#cityShipping").val(dados.localidade);
                                $('[href="#saveOrderAssinatura"]').attr('data-error', false)
                            }
                        }
                    }

                } //end if.
                else {
                    limpa_formulário_cep();
                    alert("CEP não encontrado.");
                }
            });
        } //end if.
        else {
            limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        limpa_formulário_cep();
    }
});