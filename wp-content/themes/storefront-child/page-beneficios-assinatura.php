<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="comofunciona-page">
    
    <header class="entry-header">
		  <h1 class="entry-title">BENEFÍCIOS DA ASSINATURA</h1>
    </header>

    <section class="conteiner">
        <img src="<?php echo $url_tema?>assets/images/beneficios.png" class="grafico">
        <a href="<?php echo $link_blog ?>/assinatura" class="btn center green" style="margin-top: 50px;">FAÇA SUA ASSINATURA</a>
    </section>

</main>	

<?php
get_footer();