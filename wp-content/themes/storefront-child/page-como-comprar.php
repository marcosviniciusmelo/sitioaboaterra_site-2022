<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="comofunciona-page">
    
    <header class="entry-header">
        <h1 class="entry-title"><?php the_title() ?></h1>
    </header>

    <section class="conteiner">
        <img src="<?php echo $url_tema?>assets/images/como-comprar.png" class="grafico">

        <div class="text center">
            <p><h3>CIDADES ATENDIDAS:</h3></p>
            <?php the_content() ?>
            <br><br>
            <p><h4><a href="<?php echo $link_blog ?>/beneficios-assinatura">Conheça também nossa assinatura orgânica!</a></h4></p>
            <a href="<?php echo $link_blog ?>/assinatura" class="btn center green" style="margin-top: 10px;">ASSINE AGORA</a>
        </div>
    </section>

</main>	

<?php
get_footer();