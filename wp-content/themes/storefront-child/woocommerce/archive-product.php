<?php
global $url_tema, $link_blog, $nome_blog;
get_header();


$cate = get_queried_object();
$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];

$showposts = 48;
$offset = ($paged - 1) * $showposts;



$args = array(
	'post_type'         => 'product',
	'post_status'       => 'publish',
	'posts_per_page' => $showposts,
	'offset' => $offset,
	'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => array('assinatura-novas', 'assinatura-cestas-organicas'),
			'operator' => 'NOT IN',
        ),
        array(
            'taxonomy' => 'product_tag',
            'field'    => 'slug',
            'terms'    => array('para-assinaturas'),
			'operator' => 'NOT IN',
		),
		array(
			'taxonomy'  => 'product_visibility',
			'terms'     => array( 'exclude-from-catalog' ),
			'field'     => 'name',
			'operator'  => 'NOT IN',
		)
    ),
	'meta_query' => array(
		array(
			'key' => '_stock_status',
			'value' => 'instock',
			'compare' => '=',
		)
	) 
);

$args['orderby'] = array(
	'title' => 'ASC',
	'_stock_status' => 'ASC'
);

if( is_shop() ){

	$args['category__not_in'] = array('assinatura-novas');
	$args['post__not_in'] = array(178);

}elseif( $cate->slug == 'variedades' ){

	$args['tax_query'] = array(
        array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => array('laticinios', 'ovos-frangos', 'cogumelos-e-tofus', 'oleos-essenciais', 'limpeza', 'pets'),
			'operator' => 'IN',
        ),
		array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => array('assinatura-novas', 'assinatura-cestas-organicas'),
			'operator' => 'NOT IN',
        ),
		array(
            'taxonomy' => 'product_tag',
            'field'    => 'slug',
            'terms'    => array('para-assinaturas'),
			'operator' => 'NOT IN',
        ),
		array(
			'taxonomy'  => 'product_visibility',
			'terms'     => array( 'exclude-from-catalog' ),
			'field'     => 'name',
			'operator'  => 'NOT IN',
		)
	);

}else{
	$args['tax_query'] = array(
        array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => $cate->slug,
        ),
		array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => array('assinatura-novas', 'assinatura-cestas-organicas'),
			'operator' => 'NOT IN',
        ),
		array(
            'taxonomy' => 'product_tag',
            'field'    => 'slug',
            'terms'    => array('para-assinaturas'),
			'operator' => 'NOT IN',
        ),
		array(
			'taxonomy'  => 'product_visibility',
			'terms'     => array( 'exclude-from-catalog' ),
			'field'     => 'name',
			'operator'  => 'NOT IN',
		)
	);

	
}

// function get_post_id_by_slug($post_slug, $slug_post_type = 'post') {
// 	$post = get_page_by_path($post_slug, OBJECT, $slug_post_type);
//     if ($post) {
// 		return $post->ID;
// 	} else {
// 		return null;
// 	}
// }

if( isset($_GET['min_price']) || isset($_GET['max_price']) ):


	$arr = array(
			'key' => '_price',
			'value' => array($_GET['min_price'], $_GET['max_price']),
			'compare' => 'BETWEEN',
			'type' => 'NUMERIC'
		);

	if( $args['meta_query'] ){
		array_push($args['meta_query'], $arr);
	}else{
		$args['meta_query'] = array($arr);
	}

endif;

if( isset($_GET['order']) ):

	switch ($_GET['order']) {
		case 'price':
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = '_price';
			$args['order'] = 'ASC';
			break;

		case 'price-desc':
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = '_price';
			$args['order'] = 'DESC';
			break;

		case 'popularity':
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = 'total_sales';
			$args['order'] = 'DESC';
			break;
		
		default:
			$args['orderby'] = $_GET['order'];
			$args['order'] = 'DESC';
			break;
	}

endif;

$loop = new WP_Query($args);

get_header( 'shop' );
do_action( 'woocommerce_before_main_content' );

?>

<div class="page-categoria-loop">
	<header class="entry-header">
		<h1 class="entry-title"><?php echo $cate->name; ?></h1>
	</header>

	<div class="main main-cat">
		
		<div class="sideBar">
			<div class="iconClose"><i class="fas fa-times"></i></div>
			<h4>CATEGORIAS</h4>
			<?php dynamic_sidebar('Sidebar Product 01') ?>
			<?php dynamic_sidebar('Sidebar Product 02') ?>

			<a href="<?php echo get_bloginfo('url') ?>/categoria-produto/<?php echo $cate->slug ?>" class="limparFiltros">Limpar filtros</a>
		</div>

		<a href="#clickCateFilter" class="btn-filter-cats"><i class="fas fa-list"></i> CATEGORIAS</a>

		<form action="<?php echo get_bloginfo('url') ?>/categoria-produto/<?php echo $cate->slug ?>" id="filterProduct">
			<!-- <input type="hidden" name="editora_p" value="<?php echo $_GET['editora_p'] ?>"> -->
		</form>


		<div class="mainProducts <?php echo ( is_page('loja') ) ? '' : 'full' ?>">

			<?php if( $loop->have_posts() ): ?>
			<div id="woocommerce_ordering_custom" class="widget woocommerce">
				<div class="select">
					<select name="order_by" id="selectOrder">
						<option value="date" <?php if( isset($_GET['order']) ) echo ($_GET['order'] == 'date' ) ? 'selected' : '' ?>>Mais recentes</option>
						<option value="popularity" <?php if( isset($_GET['order']) ) echo ($_GET['order'] == 'popularity' ) ? 'selected' : '' ?>>Popularidade</option>
						<option value="price" <?php if( isset($_GET['order']) ) echo ($_GET['order'] == 'price' ) ? 'selected' : '' ?>>Menor preço</option>
						<option value="price-desc" <?php if( isset($_GET['order']) ) echo ($_GET['order'] == 'price-desc' ) ? 'selected' : '' ?>>Maior preço</option>
					</select>
				</div>
			</div>
			<?php endif; ?>

			<?php
				woocommerce_product_loop_start();
				while ($loop->have_posts()) : $loop->the_post();
					wc_get_template_part( 'content', 'product' );
					
				endwhile;
				wp_reset_postdata();
				woocommerce_product_loop_end();

				if( $loop->found_posts > $showposts ):
					echo '<div class="pagination">';
					echo paginate_links(array(
						'format' => '?pagina=%#%',
						'current' => $paged,
						'total' => ceil($loop->found_posts / $showposts ),
						'prev_text' => __('<'),
						'next_text' => __('>'),
					));
					echo '</div>';
				endif;
			?>

			<?php
				if( !$loop->have_posts() ):
					if( get_field('produto_nao_encontrado', $cate) != '' ){
						echo '<h3>'.get_field('produto_nao_encontrado', $cate).'</h3>';
					}else{
						echo '<h3>Nenhum produto encontrado.</h3>';
					}
				endif;
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>