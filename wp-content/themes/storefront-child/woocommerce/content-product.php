<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}


?>
<li <?php wc_product_class( '', $product ); ?>>
	<div class="_inner">
	
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	echo '<div class="ctnImage">';
	do_action( 'woocommerce_before_shop_loop_item_title' );
	echo '</div>';

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */

	// $title = wp_trim_words( get_the_title( $product->get_id() ), 7, '...' );
	$title = get_the_title( $product->get_id() );
	if( strlen($title) > 34 ):

		$titleArr = explode(' ', $title);
		$titleFinal = '';
		foreach ($titleArr as $key) {
			if( strlen($titleFinal) < 34 ){
				$titleFinal .=  $key . ' ';
			}
		}
		$titleFinal .=  '...';
		// $title = substr($title, 0, 34) . '...';
	else:
		$titleFinal = $title;
	endif;

	// Get last last
	// if (strpos($title, '...') === false):
	// 	$titleArr = explode('...', $title);
	// 	$lastC = substr($titleArr[0], -1);
	// 	$res = preg_replace('/[0-9\@\.\;\" "]+/', '', $lastC);
	// 	$title = substr($title, 0, -1) . '' .$res;
	// endif;

	echo '<div class="ctnTitleProduct">';
	echo '<h2 class="woocommerce-loop-product__title">'.$titleFinal.'</h2>';
	// do_action( 'woocommerce_shop_loop_item_title' );
	echo '</div>';
	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );
	echo '<div class="installments_product">ou em <strong></strong> sem juros</div>';


	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
	</div>
</li>
