<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="comocomprar-page">
    
    <header class="entry-header">
		  <h1 class="entry-title">FAQ</h1>
    </header>

    <section class="conteiner">

        <h3>Veja se sua dúvida está aqui! ;) </h3>

        <div class="coll col-50">
            <h4>VANTAGENS</h4>
            <?php
                $i=1;
                foreach (get_field('lista') as $key):
                    $class = '';
                    if( $i == 1 )
                        $class = 'active';
                    echo '<div class="collapse '.$class.'">
                            <div class="top">'.$key['pergunta'].'</div>
                            <div class="ctn">'.$key['resposta'].'</div>
                        </div>';
                $i++;
                endforeach;
            ?>
        </div>

        <div class="coll col-50">
            <h4>ENTREGAS</h4>
            <?php
                foreach (get_field('lista_entrega') as $key):
                    echo '<div class="collapse">
                            <div class="top">'.$key['pergunta'].'</div>
                            <div class="ctn">'.$key['resposta'].'</div>
                        </div>';
                endforeach;
            ?>

            <h4>CESTAS</h4>
            <?php
                foreach (get_field('lista_cesta') as $key):
                    echo '<div class="collapse">
                            <div class="top">'.$key['pergunta'].'</div>
                            <div class="ctn">'.$key['resposta'].'</div>
                        </div>';
                endforeach;
            ?>
        </div>

        <div class="coll">
            <h4>VIVÊNCIAS NO SÍTIO</h4>
            <?php
                foreach (get_field('lista_sitio') as $key):
                    echo '<div class="collapse">
                            <div class="top">'.$key['pergunta'].'</div>
                            <div class="ctn">'.$key['resposta'].'</div>
                        </div>';
                endforeach;
            ?>
        </div>

        <div class="coll">
            <h4>Não encontrou o que precisava?</h4>
            <div class="contatos">
                <p><a href="mail:contato@aboaterra.com.br" target="_blank"><i class="far fa-envelope"></i> contato@aboaterra.com.br</a></p>
                <p><a href="https://api.whatsapp.com/send?phone=5519991697729" target="_blank"><i class="fab fa-whatsapp"></i> (19) 99169-7729</a></p>
            </div>
        </div>

    </section>

</main>	

<?php
get_footer();