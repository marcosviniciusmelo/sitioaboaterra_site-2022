<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="assinatura-page">
    
    <header class="entry-header">
		  <h1 class="entry-title"><?php the_title() ?></h1>
    </header>

    <div class="boxAssinatura">
		<div class="left">
			<div class="icone"><img src="<?php echo $url_tema?>assets/images/icons/assinatura.svg">ASSINATURA</div>
			<div class="bar"></div>
			<div class="ctnPrice" id="priceAssinatura"></div>
		</div>
		<div class="msg">Adicione mais produtos ou clique no botão ao lado para  escolher sua frequência.</div>
		<a href="<?php echo $link_blog ?>/escolher-frequencia" class="btn">ESCOLHER FREQUÊNCIA →</a>
	</div>

    <div class="content-escolher-produtos">

        <div class="main">
            <div class="sidebar">
                <div class="iconClose"><i class="fas fa-times"></i></div>
                <h4>CATEGORIAS</h4>

                <div class="list">
                    <?php
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page'    => 500,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'product_tag',
                                    'field'    => 'slug',
                                    'terms'    => 'para-assinaturas-avulsos',
                                ),
                            ),
                            'meta_query' => array(
                                array(
                                    'key' => '_stock_status',
                                    'value' => 'instock',
                                    'compare' => '=',
                                )
                            ),
                            'orderby' => 'menu_order', 
                            'order' => 'ASC', 
                        );
                        $query = new WP_Query( $args );
                        $categories = [];
                        while ($query->have_posts()) : $query->the_post();
                            $terms = get_the_terms( $post->ID, 'product_cat' );
                            foreach ($terms as $key) {
                                array_push($categories, $key->name);
                            }
                        endwhile;
                        $categories = array_unique($categories);

                        foreach ($categories as $key) {
                            $class = '';
                            if( $_GET['categoria'] )
                                if( $_GET['categoria'] == $key )
                                    $class = 'active';   
                            echo '<div class"item '.$class.'"><a href="'.$link_blog.'/escolher-produtos/?categoria='.$key.'">'.$key.'</a></div>';
                        }

                        echo '<a href="'.$link_blog.'/escolher-produtos/" class="limparFiltros">Limpar filtros</a>'
                    ?>
                </div>
            </div>

            <div class="main-right">

                <a href="#clickCateFilter" class="btn-filter-cats"><i class="fas fa-list"></i> CATEGORIAS</a>

                <div class="grid">
                    <?php
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page'    => 500,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'product_tag',
                                    'field'    => 'slug',
                                    'terms'    => 'para-assinaturas-avulsos',
                                ),
                                array(
                                    'taxonomy'  => 'product_visibility',
                                    'terms'     => array( 'exclude-from-catalog' ),
                                    'field'     => 'name',
                                    'operator'  => 'NOT IN',
                                )
                            ),
                            'meta_query' => array(
                                array(
                                    'key' => '_stock_status',
                                    'value' => 'instock',
                                    'compare' => '=',
                                )
                            )
                        );

                        if( $_GET['categoria'] ):
                            $args['tax_query'] = array(
                                array(
                                    'taxonomy' => 'product_tag',
                                    'field'    => 'slug',
                                    'terms'    => 'para-assinaturas-avulsos'
                                ),
                                array(
                                    'taxonomy' => 'product_cat',
                                    'field'    => 'name',
                                    'terms'    => $_GET['categoria']
                                )
                            );
                        endif;
                        $query = new WP_Query( $args );
                        $categories = [];
                        while ($query->have_posts()) : $query->the_post();

                            $product = new WC_Product($post->ID);

                            // print_r($product);

                            echo '<div class="_itemProduct productAvulsoAssinatura" data-id="'.$post->ID.'">
                                    <figure><img src="'.get_the_post_thumbnail_url($post->ID, 'medium').'" alt=""></figure>
                                    <h2 class="woocommerce-loop-product__title">'.wp_trim_words( get_the_title(), 6, '...').'</h2>
                                    <div class="price">'.$product->get_price_html().'</div>
                                    <div class="buttons-qty col-md-8 pull-left col-xs-12">
                                        <span>+</span>
                                        <input data-id="'.$post->ID.'" type="text" value="1">
                                        <span>-</span>
                                    </div>
                                    <a href="#addProductAssinatura" data-quantity="1" class="button add_to_cart_button" data-price="'.$product->price.'" data-product_id="'.$post->ID.'" rel="nofollow">Adicionar</a>
                                </div>';
                        endwhile;
                    ?>
                </div>
            </div>
        </div>
    </div>
</main>	


<?php
get_footer();