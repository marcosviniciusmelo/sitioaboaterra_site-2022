<?php
global $url_tema, $link_blog, $nome_blog;
get_header();

$paged = ($_GET['pagina']) ? $_GET['pagina'] : 1;
$showposts = 15;
$offset = ($paged - 1) * $showposts;

?>

<main class="page-blog">

    <header class="entry-header">
		  <h1 class="entry-title"><?php the_title() ?></h1>
    </header>

    <div class="main">
        <div class="posts col-3">
            <?php

                $args = array(
                    'post_type' 	 => 'post',
                    'post_status'    => 'publish',
                    'posts_per_page'	  => $showposts,
					'offset'			  => $offset,
                );

                $query = new WP_Query($args);
                while ($query->have_posts()) : $query->the_post();
                ?>	
                
                <li class="_itemGrid">
                        <div class="ctn">
                            <a href="<?php the_permalink($post->ID) ?>" class="link"></a>
                            <div class="bottom">
                                <ul class="tags">
                                    <?php
                                        $ic=0;
                                        foreach ( get_the_terms($post->ID, 'category') as $key):
                                            if( $ic > 0 ) continue;

                                            $idCat = $key->term_id;
                                            if( $key->name != 'Crítica' && $key->parent != '' )
                                                $idCat = $key->parent;

                                            echo '<li><a style="background-color: '.get_field('cor', 'category_'.$idCat).';" href="'.get_the_permalink().'">'.get_cat_name( $idCat ).'</a></li>';
                                            $ic++;
                                        endforeach;
                                    ?>
                                </ul>
                                <span class="date"><?php echo get_the_date('j \d\e F \d\e Y') ?></span>
                                <h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
                            </div>
                            <figure class="bg">
                                <?php 
                                    if( get_field('imagem_listagem') != '' ):
                                        echo '<img src="'.get_field('imagem_listagem')['url'].'" alt="">';
                                    else:
                                        the_post_thumbnail('medium');
                                    endif; 
                                ?>
                            </figure>
                        </div>
                    </li>

                <?php	
                endwhile;
                wp_reset_postdata();
            ?>
        </div>

        <?php
            $big = 999999999; // need an unlikely integer
            if( !isset($showposts) )
                $showposts = 8;
            
            echo '<div class="pagination">';
            echo paginate_links(array(
                'format' => '?pagina=%#%',
                'current' => $paged,
                'total' => ceil($query->found_posts / $showposts ),
                'prev_text' => __('<'),
                'next_text' => __('>'),
            ));
            echo '</div>';
        ?>
    </div>

</main>

<?php
get_footer();