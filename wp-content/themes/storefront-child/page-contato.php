<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="contato-page">
    
    <div class="banner"><img src="<?php echo get_field('imagem_de_destaque')['url'] ?>"></div>

    <section class="ctnForm">
        <div class="main">
            <h3>Envie uma mensagem</h3>
            <div class="text">Estamos sempre online! Clique no canto inferior direito da tela e fale conosco. Caso prefira o contato por email, envie-nos uma mensagem , em breve retornaremos.</div>
            <div class="form"><?php echo do_shortcode('[contact-form-7 id="22568" title="contato"]') ?></div>
        </div>
    </section>

</main>	

<?php
get_footer();