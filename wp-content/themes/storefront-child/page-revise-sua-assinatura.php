<?php
global $url_tema, $link_blog, $nome_blog;
if ( !is_user_logged_in() )
    header('Location: '.$link_blog.'/minha-conta?redirect='.$link_blog.'/revise-sua-assinatura');
get_header();
the_post();

$userId = get_current_user_id();
$customer = new WC_Customer( $userId );


?>

<main class="assinatura-page">
    
    <header class="entry-header">
		  <h1 class="entry-title"><?php the_title() ?></h1>
    </header>

    <div class="content-revise-sua-assinatura">
        <div class="main">
            <div class="col sidebar">
                <div class="top">ITENS DA ASSINATURA</div>
                <div class="listCategories">
                    <div class="_item">
                        <div class="icon"><img src="<?php echo $url_tema?>assets/images/icons/verduras.svg"></div>
                        <div class="right">
                            <div class="ctn">
                                <strong>Verduras</strong>
                                <span class="price">R$ 4,50 / un</span>
                            </div>
                            <div class="buttons-qty"><input type="text" value="0" min="0" disabled /></div>
                        </div>
                    </div>
                    <div class="_item">
                        <div class="icon yellow"><img src="<?php echo $url_tema?>assets/images/icons/legumes.svg"></div>
                        <div class="right">
                            <div class="ctn">
                                <strong>Legumes</strong>
                                <span class="price">R$ 8,50 / un</span>
                            </div>
                            <div class="buttons-qty"><input type="text" value="0" min="0" disabled /></div>
                        </div>
                    </div>
                    <div class="_item">
                        <div class="icon"><img src="<?php echo $url_tema?>assets/images/icons/frutas.svg"></div>
                        <div class="right">
                            <div class="ctn">
                                <strong>Frutas</strong>
                                <span class="price">R$ 10,00 / un</span>
                            </div>
                            <div class="buttons-qty"><input type="text" value="0" min="0" disabled /></div>
                        </div>
                    </div>
                    <div class="_item">
                        <div class="icon blue"><img src="<?php echo $url_tema?>assets/images/icons/temperos.svg"></div>
                        <div class="right">
                            <div class="ctn">
                                <strong>Temperos</strong>
                                <span class="price">R$ 4,00 / un</span>
                            </div>
                            <div class="buttons-qty"><input type="text" value="0" min="0" disabled /></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div id="listAvulsos" class="listCategories avulsos"></div>
                <a href="<?php echo $link_blog ?>/assinatura" class="btn" style="display: block">EDITAR ASSINATURA</a>
            </div>

            <div class="col">
                <div class="box">
                    <h4>ENTREGA</h4>
                    <p>Insira o seu destino para obter uma estimativa de envio.</p>

                    <form id="formShippingAssinatura">
                        <input type="hidden" name="datasRegrasArr" id="datasRegrasArr">
                        <div class="field">
                            <label>CEP</label>
                            <input type="text" name="cep" placeholder="00000-00" id="cepShipping" value="<?php if( isset($customer) ) echo $customer->get_billing_postcode(); ?>">
                        </div>
                        <div class="field">
                            <label>CIDADE</label>
                            <input type="text" name="cep" placeholder="CIDADE" id="cityShipping" value="<?php if( isset($customer) ) echo $customer->get_billing_city(); ?>">
                        </div>
                        <div class="field">
                            <label>BAIRRO</label>
                            <input type="text" name="cep" placeholder="BAIRRO" id="neighborhoodShipping" value="<?php echo get_user_meta( $userId, 'billing_neighborhood', true ); ?>">
                        </div>
                        <div class="field">
                            <label>DATA DA ENTREGA</label>
                            <input type="text" name="entrega" placeholder="Selecionar Data" id="dateShipping" disabled>
                        </div>
                        <div class="field">
                            <label>HORÁRIO DA ENTREGA</label>
                            <input type="text" name="cep" placeholder="07h às 13h." id="timeShipping" value="07h às 13h" disabled>
                        </div>
                    </form>

                    <div id="clickCalendario" class="btn" style="display: none;">Selecionar outra data</div>
                </div>

                <div class="box">
                    <h4>Restrições selecionadas</h4>
                    <div class="ctn" id="listRestricoes">Nenhuma restrição</div>
                </div>
            </div>

            <div class="col resume">
                <div class="box">
                    <div class="line">
                        <div class="left">
                            <img src="<?php echo $url_tema?>assets/images/icons/assinatura.svg">
                            <h5>PRODUTOS</h5>
                        </div>
                        <div class="right" id="priceAssinatura"></div>
                    </div>
                    <div class="line">
                        <div class="left">
                            <img src="<?php echo $url_tema?>assets/images/icons/frenquencia.svg">
                            <h5>FREQUÊNCIA</h5>
                        </div>
                        <div class="right" id="frequenciaAssinatura">SEMANAL</div>
                    </div>
                    <div class="line">
                        <div class="left">
                            <img src="<?php echo $url_tema?>assets/images/icons/entrega.svg">
                            <h5>ENTREGA</h5>
                        </div>
                        <div class="right">Grátis na primeira entrega*</div>
                        <p>* Seu frete será R$ 9,90 a partir da segunda entrega</p>
                    </div>
                    <div class="line lineTimeShipping" style="display: none;">
                        <p style="text-align: center; width: 100%;"><strong id="dateShippingFinal">-</strong></p>
                        <div class="left">
                            <img src="<?php echo $url_tema?>assets/images/icons/horario.svg">
                            <h5>HORÁRIO</h5>
                        </div>
                        <div class="right" id="timeShippingFinal">-</div>
                    </div>
                    <div class="line">
                        <p>Sua assinatura é uma compra que vai se repetir  toda semana. Você pode cancelar a qualquer momento.</p>
                    </div>
                </div>

                <div class="box">
                    <div class="line">
                        <h4>TOTAL</h4>
                        <div class="ctnPrice">
                            <strong>VALOR:</strong>
                            <h5 class="price" id="totalAssinatura">R$49.00</h5>
                        </div>
                        <a href="#saveOrderAssinatura" class="btn">FECHAR PEDIDO →</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>	

<?php
get_footer();