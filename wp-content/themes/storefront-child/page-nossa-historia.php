<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
the_post();
?>

<main class="sobre-page">
    
    <div class="banner"><img src="<?php echo get_field('imagem_de_destaque')['url'] ?>"></div>

    <section class="ctnText">
        <div class="main">
            <h2>HISTÓRIA</h2>
            <div class="content"><?php the_content() ?></div>
        </div>
    </section>

    <section class="ctnVideos">
        <div class="main">
            <?php
                foreach(get_field('videos') as $video){
                    parse_str( parse_url( $video['video'], PHP_URL_QUERY ), $id );
                    echo '<div class="video"><a href="'.$video['video'].'" data-fancybox="videos"><img src="http://i3.ytimg.com/vi/'.$id['v'].'/mqdefault.jpg"/></a></div>';
                }
            ?>
        </div>
    </section>
    
</main>	

<?php
get_footer();