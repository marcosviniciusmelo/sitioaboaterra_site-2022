<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */
// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'aboaterra_site' );
/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );
/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );
/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );
/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );
/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );
/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'x0J0kqNwZ}@^c]b>rNeg~;McK2%,ck) CcXv)R&M6AK,Gr4YT[9GI:0]+K]~<u>!' );
define( 'SECURE_AUTH_KEY',  '&6sRk@[c)Qv8#_xT4Nk3&Gd,#Cyj~7#H} >5A>bN]_{1+0vY=VT&`(8^utH|q(`L' );
define( 'LOGGED_IN_KEY',    'C(o0E}/%yGdr+*#0%%>1YbIl=5&8y2)6DuSSX{^?^-DC_iD4^1hJ3cU<T}i @+h;' );
define( 'NONCE_KEY',        'cN9]Lj@F]Z/>|cn*F&EEU8T>UgW42mF1{7z+(]pp0Xmb~4%<QiM6]/z+3FK>V-M/' );
define( 'AUTH_SALT',        'L5Ldzy 5H!Ye~.EqNl/=Xw4^ o3Fb_~|6tP0YVeaQ|FIXu1#slw:IHZpw J~g`h?' );
define( 'SECURE_AUTH_SALT', 'p568V.;1r^F%,{FV.n%,8!6J(E@|pE7H8.KZ_K)Hp[IkUBSTfj2iN[V~g(&|t|mN' );
define( 'LOGGED_IN_SALT',   'WE<#$W$(.(rV98!51a.#kb2Dx.b:@S5b{vA*.Ytw>UU~<_wFvyfz^8v|%G2}xb^O' );
define( 'NONCE_SALT',       'l.$j<hP-rc&5e[XoL[x(<G&!s@50.nvuVT:1j[7;ZBs[gjcsNC{F4Da#.=hTzmo*' );
define('WP_CACHE_KEY_SALT', '><i $$pr/8++%>V3]a0|k<|@NFVXpu R%.:s>L`i<Ou_vxVH(`G`2s/2/6}}-;tZ' );
/**#@-*/
/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';
/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
/* Adicione valores personalizados entre esta linha até "Isto é tudo". */
define( 'WP_HOME', 'http://localhost/www/sitioaboaterra_site-2022' );
define( 'WP_SITEURL', 'http://localhost/www/sitioaboaterra_site-2022' );
/* Isto é tudo, pode parar de editar! :) */
/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}
/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

